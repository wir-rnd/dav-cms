<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		
	}

	function index()
	{
		$this->authCheck();
		$user = $this->authData;
		//$this->_print_data($data);die();
		if ($user->status>=2)
		{
			$data['page'] = "home";
			$data['total_store'] = $this->db->get('store')->num_rows();
			$data['total_device'] = $this->db->get('devices')->num_rows();
			$data['total_target'] = $this->db->get('target')->num_rows();
			$data['geo'] = $this->db->select('store_id, store_name, latitude, longitude')->from('store')->where(array('latitude !=' => 0, 'longitude !=' => 0))->get()->result();
			$this->load->view('home', $data);
		}
		else if($user->status==1)
		{
			$this->load->model('analytics');
			$data['page'] = 'dashboard';
			$data['user'] = $user;
			$data['brand'] = $this->db->get_where('brand', array('id' => $user->brand_id))->first_row();
			$data['product'] = $this->analytics->product($user->brand_id);
			$this->load->view('analytics/index', $data);
		}
	}
	
	function product_report()
	{
		$this->load->model('read');
		$data['page'] = "product_report";
		$this->load->view('analytics/index', $data);
	}

	function sendStatistic()
	{
		$tid = $this->input->post('target_id');
		$sn = $this->input->post('serial_number');
		date_default_timezone_set('Asia/Jakarta');
		$dt = new DateTime();
		$datetime = $dt->format('Y-m-d H:i:s');
		$this->load->model('insert');
		$data = array('target_id' => $tid, 'serial_number' => $sn, 'datetime' => $datetime);
		$res = $this->insert->addIntoTable('activity', $data);
		header("Content-Type: application/json");
		$res = array('result' => $res);
		print_r(json_encode($res));
	}

	function ping()
	{
		$sn = $this->input->post('serial_number');
		date_default_timezone_set('Asia/Jakarta');
		$dt = new DateTime();
		$datetime = $dt->format('Y-m-d H:i:s');
		$this->load->model('insert');
		$data = array('serial_number' => $sn, 'datetime' => $datetime);
		$this->insert->addIntoTable('device_activity', $data);
	}

	function deviceRegister($sn)
	{
		//$tid = $this->input->post('brand');
		//$sn = $this->input->post('serial_number');
		$this->load->model('read');
		if($this->read->checkAvailableSerial($sn)==1)
		{
			$this->load->model('insert');
			$data = array('serial_number' => $sn, 'status' => 'good');
			$res = $this->insert->addIntoTable('devices', $data);
			echo 1;
		} else if($this->read->checkAvailableSerial($sn)==0){
			echo 0;
		}
	}

	function getLastMD5APK()
	{
		echo md5_file(base_url().'mobile_apk/DAV.apk');
	}

	function head2head()
	{
		if($this->session->userdata('is_logged_in'))
		{
			$this->load->model('read');
			//$data['geo'] = $this->read->readGeolocation();
			$data['page'] = "home/head2head";
			$this->load->view('home', $data);
		} else {
			redirect('auth', 'refresh');
		}
	}

	function submit_compare(){
		if($this->session->userdata('is_logged_in'))
		{
			$this->load->model('read');
			$data = $this->_get_post_data();
			$this->_print_data($data);
			die();
		} else {
			redirect('auth', 'refresh');
		}
	}
}