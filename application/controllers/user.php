<?php
class User extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->authCheck();
		$this->statusCheck(3);
	}

	function index()
	{
		$data['page'] = "user";
		$user = $this->db->get_where('users', array('status !=' => 3))->result();
		$data['brand'] = $this->db->get('brand')->result();
		$data['os_devices'] = $this->db->get('os_devices')->result();
		for($i=0;$i<count($user);$i++)
		{
			if($user[$i]->brand_id!=0)
				$user[$i]->brand_name = $this->db->get_where('brand', array('id' =>$user[$i]->brand_id))->first_row()->name;
			else
				$user[$i]->brand_name = '<i>Administrator</i>';
		}
		$data['user'] = $user;
		//$this->_print_data($data);die();
		$this->load->view('home', $data);
	}


	function add()
	{
		$data = $this->input->post();
		if($data['repassword'] != '' || $data['password'] != '')
		{
			if($data['repassword']!=$data['password'])
			{
				$this->message('Password not same on Re-Enter Password', 'warning'); 
			}
		}
		else
		{
			$this->message('Password cant be blank'); 
		}
		
		$brand_id = $data['brand_id'];
		$check = array();
		if($brand_id!=0)
		{
			$check = $this->db->get_where('users', array('brand_id' => $brand_id))->first_row();
		}
		else
		{
			$data['status'] = 2;
		}
		if(count($check)>0)
		{
			$brand = $this->db->get('brand', array('id' => $brand_id))->first_row();
			$this->message('Brand ' . $brand->name . ' already taken', 'warning'); 
		}
		
		if($this->total_message()==0)
		{
			unset($data['repassword']);
			$data['password'] = md5($data['password']);
			if($this->db->insert('users', $data))
			{
				$this->message('User with username \''. $data['username'] .'\' has been added'); 
			}
		}
		
		//print_r($data);die();
		redirect('user');
	}

	function edit($id)
	{
		$data['page'] = "user/edit";
		$data['user'] = $this->db->get_where('users', array('id' => $id))->first_row();
		$data['brand'] = $this->db->get('brand')->result();
		//print_r($data['devices']);die();
		$this->load->view('home', $data);
	}

	function update()
	{
		$data = $this->input->post();
		$id = $data['id'];
		unset($data['id']);
		
		if($data['repassword'] != '' || $data['password'] != '')
		{
			if($data['repassword']!=$data['password'])
			{
				$this->message('Password not same on Re-Enter Password', 'warning'); 
			}
		}
		else
		{
			unset($data['repassword']);
			unset($data['password']);
		}
		
		$brand_id = $data['brand_id'];
		$check = array();
		if($brand_id!=0)
		{
			$check = $this->db->get_where('users', array('brand_id' => $brand_id))->first_row();
		}
		else
		{
			$data['status'] = 2;
		}
		if(count($check)>0)
		{
			$brand = $this->db->get('brand', array('id' => $brand_id))->first_row();
			$this->message('Brand ' . $brand->name . ' already taken', 'warning'); 
		}
		
		//echo $this->total_message();
		
		if($this->total_message()==0)
		{
			unset($data['repassword']);
			$data['password'] = md5($data['password']);
			$user = $this->db->get_where('users', array('id' => $id))->first_row();
			$this->db->where('id', $id);
			if($this->db->update('users', $data))
			{
				$this->message('User with username \''. $user->username .'\' has been updated'); 
			}
		}
		//$this->_print_data($data);die();
		redirect('user');
		
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		if($this->db->delete('user'))
		{
			$this->message('User with username \''. $data['username'] .'\' has been updated'); 
		}
		redirect('user');
	}

}