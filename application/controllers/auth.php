<?php
class auth extends MY_Controller {

	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		//$this->message('Incorrect Username or Password');
		if($this->session_data)
		{
			redirect('/');
		}
		else
		{
			redirect('auth/page');
		}
	}
	
	function page()
	{
		$this->load->view('login');
	}

	function login()
	{
		$data = $this->input->post();
		$data['password'] = md5($data['password']);
		$user = $this->db->get_where('users', $data)->first_row();

		if(count($user)>0) // if the user's credentials validated...
		{
			unset($user->password);
			$this->saveAuth($user);
			$this->flash_message('Authentication Success');
			redirect('/');
		}
		else // incorrect username or password //
		{
			$this->message('Incorrect Username or Password', 'warning');
			//$this->flash_message('Incorrect Username or Password');
			redirect('auth/page');
		}
	}

	function logout()
	{
		$this->session->unset_userdata('is_logged_in');
		redirect('auth');
	}
}
?>