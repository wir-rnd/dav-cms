<?php
class Map extends MY_Controller {
	var $db;

	function __construct(){
		parent::__construct();
		$this->load->model('common');
		$this->load->model('update');
		$this->load->model('delete');
		$this->session_data = $this->session->userdata('is_logged_in');
		$this->db = $this->common->db();
	}

	function index(){
		if($this->session->userdata('is_logged_in'))
		{
			$data['geo'] = $this->db->select('store_id, store_name, latitude, longitude')->from('store')->where(array('latitude !=' => 0, 'longitude !=' => 0))->get()->result();
			//print_r($data);
			$this->load->view('fullmap', $data);
		} else {
			redirect('login', 'refresh');
		}
	}
}