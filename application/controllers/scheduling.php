<?php
class Scheduling extends MY_Controller {
	
	function __construct() {
		parent::__construct();
	}
	
	function index() {
		$this->load->model('read');
		$res = $this->read->isStatusDevice();
		$this->_print_data($res);
		
	}
}
?>