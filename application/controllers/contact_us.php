<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact_us extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->authCheck();
		$this->statusCheck(1);
	}

	function index()
	{
		$data['page'] = "contact_us";
		//$data['companyName'] = $this->read->getCompanyName($this->session_data['username']);
		//$data['getProduct'] = $this->read->getProduct($data['companyName']);
		$this->load->view('analytics/index', $data);
	}
	function post_contact_us()
	{
		if ($this->input->is_ajax_request()) {
			$dataJson["t"] = 1;
			if($this->session->userdata('is_logged_in')){
				if($this->input->post("name") == ""){
					$dataJson["t"] = 0;
					$dataJson["message"][0] = "Please enter your name";
				}
				elseif(strlen($this->input->post("name")) < 3){
					$dataJson["t"] = 0;
					$dataJson["message"][0] = "Your name is too short";
				}
				if($this->input->post("email") == ""){
					$dataJson["t"] = 0;
					$dataJson["message"][1] = "Please enter your email";
				}
				elseif(!filter_var($this->input->post("email"), FILTER_VALIDATE_EMAIL)){
					$dataJson["t"] = 0;
					$dataJson["message"][1] = "Your email address is not valid";
				}
				if($this->input->post("message") == ""){
					$dataJson["t"] = 0;
					$dataJson["message"][2] = "Please enter your message";
				}
				if($dataJson["t"] == 1){
					/*$this->load->library('email');

					$this->email->from('your@example.com', 'Your Name');
					$this->email->to('someone@example.com'); 
					$this->email->cc('another@another-example.com'); 
					$this->email->bcc('them@their-example.com'); 

					$this->email->subject('Email Test');
					$this->email->message('Testing the email class.');	

					$this->email->send();*/

					
					$dataJson["message"][3] = "Success";

				}
			}
			else{
				$dataJson["t"] = -1;
			}

			echo json_encode($dataJson);
		}
		else{
			redirect("");
		}
	}
	
}?>