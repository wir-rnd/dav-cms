<?php

class Statistic extends MY_Controller {

	function __construct() {
		parent::__construct();
		$this->load->library('pagination');
	}

	function index(){
		if($this->session->userdata('is_logged_in'))
		{
			$this->load->model('read');
			$data['page'] = "statistic";
			$data['alamat'] = $this->read->alamat();

			$this->_print_data($data);die();
			$this->load->view('home', $data);
		} else {
			redirect('login', 'refresh');
		}
	}

	function getTotalDevicePerLocation($location_id)
	{
		if($this->session->userdata('is_logged_in'))
		{
			$this->load->model('read');
			$t = $this->read->getTotalDevicePerLocation($location_id);
			echo $t;
		} else {
			redirect('login', 'refresh');
		}
	}

	function getTotalContentPerLocation($location_id)
	{
		if($this->session->userdata('is_logged_in'))
		{
			$this->load->model('read');
			$t = $this->read->getTotalContentPerLocation($location_id);
			echo $t;
		} else {
			redirect('login', 'refresh');
		}
	}

	function getTotalInteractionPerLocation($location_id)
	{
		if($this->session->userdata('is_logged_in'))
		{
			$this->load->model('read');
			$t = $this->read->getTotalInteractionPerLocation($location_id);
			echo $t;
		} else {
			redirect('login', 'refresh');
		}
	}

	function device_activity(){
		if($this->session->userdata('is_logged_in'))
		{
			$this->load->model('read');
			$data['page'] = "statistic/device_activity";
			//$this->_print_data($data);die();
			$data['totalDevices'] = $this->read->countTotalDevices();
			$data['device_activity_log'] = $this->read->device_activity_log();
			$val = "";

			if ($_POST) {
				$val = $this->_get_post_data();
				$data['store_locations'] = $this->read->device_statistic($val['search']);
			} else {
				$data['store_locations'] = $this->read->device_statistic();
			}
			//$this->_print_data($data);die();
			//$this->_print_data($data['location_list']);
			$this->load->view('home', $data);
		} else {
			redirect('login', 'refresh');
		}
	}

	function brand_activity() {
		if($this->session->userdata('is_logged_in'))
		{
			$this->load->model('read');
			$data['page'] = "statistic/brand_activity";
			$data['totalBrand'] = $this->read->totalBrand();
			$data['totalProduct'] = $this->read->totalProduct();
			$data['brand_activity_data'] = $this->read->brand_activity();

			//$this->_print_data($data['brand_activity_data']);
			$this->load->view('home', $data);
		} else {
			redirect('login', 'refresh');
		}
	}

	function brand_activity_detail($product_id){
		if($this->session->userdata('is_logged_in'))
		{
			$arr = explode("_", $product_id);
			$product_id = $arr[0];
			$variant_id = $arr[1];
			$this->load->model('read');
			$data['page'] = "statistic/brand_activity_detail";
			$data['brand_activity'] = $this->read->brand_activity_detail($product_id, $variant_id);
			//$this->_print_data($data);
			$this->load->view('home', $data);
		} else {
			redirect('login', 'refresh');
		}
	}

	function brand_activity_more_detail($product_id){
		if($this->session->userdata('is_logged_in'))
		{
			$data['page'] = "statistic/brand_activity_more_detail";
			$arr = explode("_", $product_id);
			$product_id = $arr[0];
			$variant_id = $arr[1];
			$store_id = $arr[2];
			$this->load->model('read');
			$data['page'] = "statistic/brand_activity_more_detail";
			$data['brand_activity'] = $this->read->brand_activity_more_detail($product_id, $variant_id, $store_id);
			//$this->_print_data($data);
			$this->load->view('home', $data);
		} else {
			redirect('login', 'refresh');
		}
	}

	function getDeviceData(){
		if($this->session->userdata('is_logged_in'))
		{
			$this->load->model('read');
			$provinsi_id = $_POST['provinsi_id'];
			$store_locations = $this->read->device_statistic_by_provinsi($provinsi_id);

			if (isset($store_locations)) {
				foreach ($store_locations as $data) {
					$id = $data->provinsi;
					if (isset($result[$id])) {
						$result[$id][] = $data;
					} else {
						$result[$id] = array($data);
					}
				}
			}
			$i = 0;
			if(isset($result)) {
				foreach (array_keys($result) as $val) {
					echo "<tr class='table_devices'>";
					echo "<td>".strtoupper($val)."</td>";
					echo "<td  style='text-align: center;'>".count($result[$val])."</td>";
					echo "<td style='text-align: center;'><a class='parameter' href='". $result[$val][$i]->provinsi."'><div class='detail glyphicon glyphicon-tasks'></div></a></td>";
					echo "</tr>";
				}
			} else {
				$result ="No Device Installed";
				echo "<tr class='table_devices'> <td colspan='3' style='text-align:center;'> ".$result." </td></tr>";
			}
			//$this->_print_data($result);
		} else {
			redirect('login', 'refresh');
		}
	}

	function getDeviceData_detail(){
		if($this->session->userdata('is_logged_in'))
		{
			$this->load->model('read');
			$provinsi_name = $_POST['provinsi_name'];

			$store_locations = $this->read->device_statistic();

			if (isset($store_locations)) {
				foreach ($store_locations as $data) {
					$id = $provinsi_name;
					if (isset($result[$id])) {
						$result[$id][] = $data;
					} else {
						$result[$id] = array($data);
					}
				}
			}

			//$this->_print_data($result);
			echo "<table class='table table-list-search'>";
			echo "<thead style='background-color: #29b6d8;'>";
			echo "<tr style='color: white;'>";
			echo "<th> Store ID </th>";
			echo "<th> Kabupaten/Kota </th>";
			echo "<th> Store Name </th>";
			echo "<th> Shelf Name </th>";
			echo "<th style='text-align: center;'> Date Added </th>";
			echo "<th style='text-align: center;'> Status </th>";

			echo "</tr>";
			echo "</thead>";
			foreach (array_keys($result) as $val) {
				$length = count($result[$val]);

				for($j=0;$j<$length;$j++) {
					if ($val == $provinsi_name ) {
						echo "<tr class='table_devices'>";
						echo "<td>".$result[$val][$j]->store_id."</td>";
						echo "<td>".$result[$val][$j]->kabupaten."</td>";
						echo "<td>".$result[$val][$j]->store_name."</td>";
						echo "<td>".$result[$val][$j]->shelf_name."</td>";
						echo "<td style='text-align: center;'>".$result[$val][$j]->device_date_added."</td>";
						if ($result[$val][$j]->status == "good") {
							echo "<td style='text-align: center;'><span class='label label-success'>Good</span></td>";
						} else  {
							echo "<td style='text-align: center;'><span class='label label-danger'>Bad</span></td>";
						}
						echo "</tr>";

					}
				}
			}
			echo "</table>";
			echo "<a href='".base_url()."statistic/device_activity"."'><button> Back </button></a>";
		} else {
			redirect('login', 'refresh');
		}
	}
}
