<?php


class Mobileversion extends MY_Controller
{
	function __construct() {
		parent::__construct();
		$this->load->model('read');
		$this->load->model('update');
		$this->load->model('delete');
		$this->session_data = $this->session->userdata('is_logged_in');
	}

	function check($sn)
	{
		$this->load->model("read");
		$result = $this->read->checkMobileVersion($sn);
		if($result!="0")
		{
			$result->md5 = md5_file('./mobile_apk/DAV_'.$result->version.'.apk');
				
			header("Content-Type: application/json");
			print_r(json_encode($result));
		}
		else
		{
			echo $result;
		}
	}

	function update($sn)
	{
		$this->load->model("read");
		echo $this->read->updateMobileVersion($sn);
	}
}

?>