<?php

class product extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->authCheck();
		$this->statusCheck(2);
	}

	function index()
	{
		$data['page'] = "product";
		$product = $this->db->get('product')->result();
		$data['brand'] = $this->db->get('brand')->result();
		$i=0;
		$newProduct = array();
		foreach($product as $p)
		{
			$newProduct[$i] = new stdClass();
			$newProduct[$i]->id = $p->id;
			$newProduct[$i]->product_name = $p->name;
			$newProduct[$i]->brand_name = $this->db->get_where('brand', array('id' => $p->brand_id))->first_row()->name;
			$i++;
		}
		$data['product'] = $newProduct;
		//$this->_print_data($data);die();
		$this->load->view('home', $data);
	}
	
	function add()
	{
		$data = $this->input->post();
		$config['upload_path'] = './products/';
		$config['allowed_types'] = 'jpg';
		$config['file_name'] = 'tmp';
		$msg = array();
		$this->load->library('upload', $config);
		$error = array();
		
		$this->load->model('insert');
		$this->insert->addIntoTable('product', $data);
		$this->load->model('read');
		$product = $this->db->get_where('product', array('name' => $data['name'], 'brand_id' => $data['brand_id']))->first_row();
		if(!$this->upload->do_upload())
		{
			$msg = $this->upload->display_errors();
			if(strpos($msg, 'did not select'))
				$this->message('Product ' . $data['name'] . ' successfully added without image');
		}
		else
		{
			if(rename('./products/tmp.jpg', './products/'.$product->id.'.jpg'))
			{
				$this->message('Product ' . $data['name'] . ' successfully added');
			}
			else
			{
				$this->load->model('delete');
				$this->delete->deleteRecord('product', $product->id);
				$this->message('Product ' . $data['name'] . ' failed to add', 'warning');
			}
			//print_r($data);die();
		}
		$this->session->set_flashdata('message', $msg);
		redirect('product');
	}

	function add_variant()
	{
		$this->load->model('insert');
		$data = $this->input->post();
		$this->insert->addIntoTable('variant', $data);
		$this->session->set_flashdata('message', 'Variant ' . $data['name'] . ' successfully added');
		//print_r($data);die();
		redirect('product/variant/' . $data['product_id']);
	}

	function variant($id)
	{
		$this->load->model('read');
		$data['page'] = "product/variant";
		$data['id'] = $id;
		
		$target = $this->read->get('target');
		$newTarget = $target;
	
		$i=0;
		foreach($target as $t)
		{
			if(count($this->read->variant(array('target_id' => $t->target_id)))>0)
			unset($newTarget[$i]);
			$i++;
		}
		
		$data['target'] = $newTarget;
		
		$data['product'] = $this->read->get('product', $id);
		$variant = $this->read->get('variant', array('product_id' => $id));
		$i=0;
		$newVariant = array();

		foreach($variant as $v)
		{
			$newVariant[$i] = new stdClass();
			$newVariant[$i]->id = $v->id;
			$newVariant[$i]->variant_name = $v->name;
			$newVariant[$i]->target_id = $v->target_id;
			$newVariant[$i]->target_name = $this->db->get_where('target', array('target_id' => $v->target_id))->first_row()->name;
			$i++;
		}
		$data['variant'] = $newVariant;
		//print_r($data['devices_detail']);die();
		$this->load->view('home', $data);
	}

	function edit($id)
	{
		$this->load->model('read');
		$data['page'] = "product/edit";
		$data['id'] = $id;
		$data['product'] = $this->read->product($id);
		$data['brand'] = $this->read->brand();
		//print_r($data['devices_detail']);die();
		$this->load->view('home', $data);
	}

	function update()
	{
		$this->load->model('update');
		$data = $this->_get_post_data();
		
		$config['upload_path'] = './products/';
		$config['allowed_types'] = 'jpg';
		$config['file_name'] = 'tmp';
		$msg = array();
		$this->load->library('upload', $config);
		$error = array();
		
		//print_r($data);
		$db_data = $data;
		unset($db_data['userfile']);
		$this->update->tableUpdate('product', $db_data, $data['id']);
		
		if(!$this->upload->do_upload())
		{
			$msg = $this->upload->display_errors();
			if(strpos($msg, 'did not select'))
				$msg = array('Product ' . $data['name'] . ' successfully updated without image');
		}
		else
		{
			if(file_exists('./products/'.$data['id'].'.jpg'))
			{
				unlink('./products/'.$data['id'].'.jpg');
			}
			
			if(rename('./products/tmp.jpg', './products/'.$data['id'].'.jpg'))
				$msg = array('Product ' . $data['name'] . ' successfully updated');
			else
			{
				$msg = array('Product ' . $data['name'] . 'images failed to update');
			}
			//print_r($data);die();
		}
		
		$this->session->set_flashdata('message', $msg);
		redirect('product');
		//$this->_print_data($data);die();
		//$this->load->view('home', $data);
	}

	function delete($id)
	{
		$this->load->model('delete');
		$this->load->model('read');
		$data = $this->read->variant(array('product_id' => $id));
		$msg = array();
		if(count($data)==0)
		{
			$msg = array('Product successfully deleted');
			$this->delete->deleteRecord('product', $id);
			unlink('./products/'.$id.'.jpg');
		}
		else
		{
			$msg = array('Product delete failed');
		}
		$this->session->set_flashdata('message', $msg);
		redirect('product');
	}

	function delete_variant($p_id, $id)
	{
		$this->load->model('delete');
		$this->delete->deleteRecord('variant', $id);
		$this->session->set_flashdata('message', 'Variant successfully deleted');
		redirect('product/variant/' . $p_id);
	}

	function ajax_get_product_by_brand_id($brand_id)
	{
		$this->load->model('read');
		$res = $this->read->product(array('brand_id' => $brand_id));
		header("Content-Type: application/json");
		print_r(json_encode($res));
	}

	function ajax_get_variant_by_product_id($product_id)
	{
		$this->load->model('read');
		$res = $this->read->variant(array('product_id' => $product_id));
		header("Content-Type: application/json");
		print_r(json_encode($res));
	}
	
}