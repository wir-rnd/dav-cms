<?php
class Device extends MY_Controller
{


	function __construct()
	{
		parent::__construct();
		$this->load->model('read');
		$this->load->model('update');
		$this->load->model('delete');
		$this->load->library('pagination');
		$this->authCheck();
		$this->statusCheck(2);
	}

	function index()
	{
		$this->load->model('read');
		$data['page'] = "device";
		$devices = $this->read->get('devices');
		$data['brand_devices'] = $this->read->brand_devices();
		$data['os_devices'] = $this->read->os_devices();
		for($i=0;$i<count($devices);$i++)
		{
			$devices[$i]->brand_name = $this->read->brand_devices($devices[$i]->brand_devices_id)->brand_name;
			$devices[$i]->os_name = $this->read->os_devices($devices[$i]->os_devices_id)->os_name;
			if($devices[$i]->store_id!=0)
			{
				$devices[$i]->store_id = $this->read->get('store', $devices[$i]->store_id)->store_id;
				$devices[$i]->shelf = $this->read->get('shelf', $devices[$i]->shelf_id)->name;
			}
			else
			{
				$devices[$i]->store_id = 'Unsigned';
				$devices[$i]->shelf = '';
			}
		}
		$data['devices'] = $devices;
		//$this->_print_data($devices);die();
		$this->load->view('home', $data);
	}


	function add()
	{
		$this->load->model('insert');
		$table = 'devices';
		$data = $this->_get_post_data();
		$this->insert->addIntoTable($table, $data);
		//print_r($data);die();
		redirect('device');
	}

	function edit($id)
	{
		$this->load->model('read');
		$data['page'] = "device/edit";
		$data['id'] = $id;
		$data['brand_devices'] = $this->db->get('brand_devices')->result();
		$data['os_devices'] = $this->db->get('os_devices')->result();
		$devices = $this->db->get_where('devices', array('id' => $id))->first_row();
		$devices->brand_name = $this->read->brand_devices($devices->brand_devices_id)->brand_name;
		$devices->os_name = $this->read->os_devices($devices->os_devices_id)->os_name;
		$data['devices'] = $devices;
		//print_r($data['devices']);die();
		$this->load->view('home', $data);
	}

	function update()
	{
		$this->load->model('update');
		$data = $this->input->post();
		$id = $data['id'];
		unset($data['id']);
		$this->db->where('id', $id);
		$this->db->update('devices', $data);
		$this->message('Device `' . $data['serial_number'] . '` has been updated');
		//$this->update->tableUpdate('devices', $data, $id);
		redirect('device');
		//$this->_print_data($data);die();
		//$this->load->view('home', $data);
	}

	function delete($id)
	{
		$this->load->model('delete');
		$this->delete->deleteRecord('devices', $id);
		redirect('device');
	}
	
	function reset($id)
	{
		$dev = $this->db->get_where('devices', array('id' => $id))->first_row();
		$sn = $dev->serial_number;
		
		$any = $this->db->get_where('mobile_update_log', array('device_id' => $id))->first_row();
		//$this->message('Device ' . count($a) . ' update log has been reset');
		//
		
		if($any)
		{
			$this->db->where('device_id', $id);
			$this->db->delete('mobile_update_log');
			$this->message('Device ' . $sn . ' update log has been reset');
		}
		else
		{	
			$this->message('Device ' . $sn . ' update log fail to reset');
		}
		redirect('device');
	}
	
	function dummy_update($id)
	{
		$number_ver = $this->db->select('number_ver')->from('mobile_version')->order_by('number_ver', 'desc')->get()->first_row()->number_ver;
		date_default_timezone_set('Asia/Jakarta');
		$dt = new DateTime();
		$datetime = $dt->format('Y-m-d H:i:s');
		
		$data = array(
		   'mobile_version_id' => $number_ver,
		   'device_id' => $id,
		   'datetime' => $datetime
		);
		
		$any = $this->db->get_where('mobile_update_log', array('device_id' => $id))->first_row();
		
		if(!$any)
		{
			$this->message('Device ' . $sn . ' has been created for dummy mobile updated');
			$this->db->insert('mobile_update_log', $data); 
		}
		else
		{
			$this->message('Device ' . $sn . ' already updated');
		}
		redirect('device');
	}

}