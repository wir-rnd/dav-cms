<?php


class brand extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('read');
		$this->load->model('update');
		$this->load->model('delete');
		$this->load->library('pagination');
		$this->session_data = $this->session->userdata('is_logged_in');
	}

	function index()
	{
		if($this->session->userdata('is_logged_in'))
		{
			$this->load->model('read');
			$data['page'] = "brand";
			$brand = $this->read->brand();
			$data['agent'] = $this->read->agent();
			$newBrand = array();
			$i=0;
			foreach($brand as $b)
			{
				$newBrand[$i] = new stdClass();
				$newBrand[$i]->id = $b->id;
				$newBrand[$i]->agent_name = $this->read->agent($b->agent_id)->name;
				$newBrand[$i]->brand_name = $b->name;
				$i++;
			}
			$data['brand'] = $newBrand;
			//$this->_print_data($data);die();
			$this->load->view('home', $data);
		} else {
			redirect('auth', 'refresh');
		}
	}

	function add()
	{
		if($this->session->userdata('is_logged_in'))
		{
			$this->load->model('insert');
			$data = $this->_get_post_data();
			$this->insert->addIntoTable('brand', $data);
			$this->session->set_flashdata('message', 'Brand ' . $data['name'] . ' successfully added');
			//print_r($data);die();
			redirect('brand');
		} else {
			redirect('auth', 'refresh');
		}
	}

	function edit($id)
	{
		if($this->session->userdata('is_logged_in'))
		{
			$this->load->model('read');
			$data['page'] = "brand/edit";
			$data['id'] = $id;
			$data['brand'] = $this->read->brand($id);
			$data['agent'] = $this->read->get('agent');
			//print_r($data['devices_detail']);die();
			$this->load->view('home', $data);
		} else {
			redirect('auth', 'refresh');
		}
	}

	function update()
	{
		if($this->session->userdata('is_logged_in'))
		{
			$this->load->model('update');
			$data = $this->_get_post_data();
			//print_r($data);
			$this->update->tableUpdate('brand', $data, $data['id']);
			redirect('brand');
			//$this->_print_data($data);die();
			//$this->load->view('home', $data);
		} else {
			redirect('auth', 'refresh');
		}
	}

	function delete($id)
	{
		if($this->session->userdata('is_logged_in'))
		{
			$this->load->model('delete');
			$this->load->model('read');
			$data = $this->read->target_brand(array('brand_id' => $id));
			if(count($data)==0)
			{
				$this->session->set_flashdata('message', 'Brand successfully deleted');
				$this->delete->deleteRecord('brand', $id);
			}
			else
			{
				$this->session->set_flashdata('message', 'Brand delete failed');
			}
			redirect('brand');
		} else {
			redirect('auth', 'refresh');
		}
	}

}