<?php


class ajax extends MY_Controller
{
	var $db;
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->model('common');
		$this->load->model('update');
		$this->load->model('delete');
		$this->load->library('pagination');
		$this->session_data = $this->session->userdata('is_logged_in');
		$this->db = $this->common->db();
	}
	
	function get($table)
	{
		header("Content-Type: application/json");
		print_r(json_encode($this->db->get($table)->result()));
	}
	
	function city($id = null)
	{
		if(!isset($id))
			$res = $this->db->get('city_area')->result();
		else
			$res = $this->db->get_where('city_area', array('province_area_id' => $id))->result();
		
		header("Content-Type: application/json");
		print_r(json_encode($res));
	}
	
	function subdistrict($id = null)
	{
		if(!isset($id))
			$res = $this->db->get('subdistrict_area')->result();
		else
			$res = $this->db->get_where('subdistrict_area', array('city_area_id' => $id))->result();
		
		header("Content-Type: application/json");
		print_r(json_encode($res));
	}
	
	function village($id = null)
	{
		if(!isset($id))
			$res = $this->db->get('village_area')->result();
		else
			$res = $this->db->get_where('village_area', array('subdistrict_area_id' => $id))->result();
		
		header("Content-Type: application/json");
		print_r(json_encode($res));
	}
	
	private function total_interaction_raw($target_id, $province_id = 0, $city_id = 0, $start_date = 0, $end_date = 0)
	{
		$this->db->from('activity a');
		$this->db->join('devices d', 'd.serial_number = a.serial_number');
		$this->db->join('store s', 's.id = d.store_id');
			
		if($province_id>0)
		{
			if($city_id>0)
			{
				$this->db->like('s.village_area_id', $city_id, 'after');
			}
			else
			{
				$this->db->like('s.village_area_id', $province_id, 'after');
			}
		}
		
		if($start_date!=0)
		{
			if($end_date!=0)
			{
				$this->db->where('a.datetime >=', $start_date);
				$this->db->where('a.datetime <=', $end_date);
			}
		}
		$this->db->where('a.target_id', $target_id);
	}
	
	function total_interaction()
	{
		$city_id = $this->input->post('city_id');
		$province_id = $this->input->post('province_id');
		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date');
		$var_id = $this->input->post('variant_id');
		
		$target_id = $this->db->get_where('variant', array('id' => $var_id))->first_row()->target_id;
		$this->total_interaction_raw($target_id, $province_id, $city_id, $start_date, $end_date);
		$res = $this->db->get();
		header("Content-Type: application/json");
		print_r(json_encode(array('total' => $res->num_rows())));
	}
	
	function topten_by_store()
	{
		$param = $this->session->userdata('topten');
		$start_date = $this->input->post('start_date');
		if($start_date==0) $start_date = $param['start_date'];
		$end_date = $this->input->post('end_date');
		if($end_date==0) $end_date = $param['end_date'];
		$this->db->select('s.store_id, s.store_name, COUNT(*) as total');
		$this->total_interaction_raw($param['target_id'], $param['province'], $param['city'], $start_date, $end_date);
		$this->db->group_by('s.store_id');
		$this->db->order_by('total', 'desc');
		$this->db->limit(10);
		$res = $this->db->get()->result();
		header("Content-Type: application/json");
		print_r(json_encode($res));
	}
	
	function topten_by_region()
	{
		$param = $this->session->userdata('topten');
		$start_date = $this->input->post('start_date');
		if($start_date==0) $start_date = $param['start_date'];
		$end_date = $this->input->post('end_date');
		if($end_date==0) $end_date = $param['end_date'];
		$this->db->select('s.store_id, s.store_name, COUNT(*) as total');
		$this->db->select('SUBSTRING(s.village_area_id, 1, 7) as sda_id', FALSE);
		$this->total_interaction_raw($param['target_id'], $param['province'], $param['city'], $start_date, $end_date);
		$this->db->group_by('sda_id');
		$this->db->order_by('total', 'desc');
		$this->db->limit(10);
		$res = $this->db->get()->result();
		$i=0;
		foreach($res as $val)
		{
			$res[$i]->subdistrict_name = $this->db->get_where('subdistrict_area', array('id' => $val->sda_id))->first_row()->name;
			$i++;
			//$data['top10byregion'][]-> = $this->db->get_where('area_kecamatan', array('id_kabupaten' => $val->store_district_id))->first_row()-
		}
		header("Content-Type: application/json");
		print_r(json_encode($res));
	}
	
	/*function ajax_total_interaction()
	{
		if($this->session->userdata('is_logged_in'))
		{
			$id = $this->input->post('variant_id');
			$target_id = $this->db->get_where('variant', array('id' => $variant_id))->first_row()->target_id;
			$this->total_interaction_raw($target_id);
			echo $this->read->total_interaction($id);
		}
	}*/
	
	function daily_average()
	{
		$variant_id = $this->input->post('variant_id');
		$total_daily = 0;
		$var = $this->db->get_where('variant', array('id' => $variant_id))->first_row();
		$oldestDate = $this->db->distinct()->select('DATE_FORMAT(datetime, "%Y-%m-%d") as oldestDate', FALSE)->from('activity')->where('target_id', $var->target_id)->order_by('datetime', 'ASC')->get()->first_row();
		
		//$SQL = "SELECT count(`datetime`) as daily FROM `activity` WHERE `target_id` = '".$target_id."' AND datetime BETWEEN '".$startDate."' AND NOW() GROUP BY day(datetime) ORDER BY datetime";
		if(count($oldestDate)>0)
		{
			$oldestDate = $oldestDate->oldestDate;
			$daily = $this->db->select('count(datetime) as daily')->from('activity')->where('target_id', $var->target_id)->where('datetime BETWEEN '. $oldestDate .' AND NOW()')->group_by('day(datetime)')->order_by('datetime')->get()->result();
			foreach($daily as $val) $total_daily += $val->daily;
			$total_daily = round($total_daily/count($daily));
		}
		header("Content-Type: application/json");
		print_r(json_encode(array('total' => $total_daily)));
	}
	
	
	function graph_daily_interaction()
	{
		
		//$id = 1;
		$id = $this->input->post('variant_id');
		date_default_timezone_set('Asia/Jakarta');
		$dt = new DateTime();
		$year = $dt->format('Y');
		//$year = 2014;
		$var = $this->db->get_where('variant', array('id' => $id))->first_row();
		$province_id = $this->input->post('province_id');
		$city_id = $this->input->post('city_id');
		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date');
		
		$this->db->select('DATE_FORMAT(datetime, "%Y-%m-%d") as date', FALSE);
		$this->db->select('count(datetime) as total');
		/*$this->db->select('count(datetime) as total')->from('activity')->where('target_id', $var->target_id);
		
		if($start_date != 0 && $end_date != 0)
		{
			$this->db->where('DATE_FORMAT(datetime, "%Y-%m-%d") BETWEEN "' . $start_date .'" AND "' . $end_date . '"');
			
		}
		else
		{
			$this->db->where('DATE_FORMAT(datetime, "%Y") = ', $year);
		}*/
		$this->total_interaction_raw($var->target_id, $province_id, $city_id, $start_date, $end_date);
			
		$this->db->group_by('date');
		$data = $this->db->get()->result();
		$res = array();
		$j=0;
		for($i=0;$i<count($data);$i++)
		{
			$res[$i] = new stdClass();
			$res[$i]->label = $data[$i]->date;
			$res[$i]->y = $data[$i]->total;
		}
		$z = 0;
		$newRes = array();
		if($start_date == 0 && $end_date == 0)
		{
			
			
			for($i=1;$i<=12;$i++)
			{
				$days = $this->ListOfDay($i, $year);
				for($j=0;$j<count($days);$j++)
				{
					//	echo $days[$j].'<br />';
					
					for($k=0;$k<count($res);$k++)
					{
						if($data[$k]->date==$days[$j])
						{
							$newRes[$z] = new stdClass();
							$newRes[$z]->label = $days[$j];
							$newRes[$z]->y = $data[$k]->total;
							break;
						}
						else
						{
							$newRes[$z] = new stdClass();
							$newRes[$z]->label = $days[$j];
							$newRes[$z]->y = 0;
						}
					}
					//unset($res[$any]);
					$z++;
					
					
				}
			}
		}
		else
		{
			$days = $this->ListOfDayRangeDate($start_date, $end_date);
			for($j=0;$j<count($days);$j++)
			{
				//	echo $days[$j].'<br />';
				
				for($k=0;$k<count($res);$k++)
				{
					if($data[$k]->date==$days[$j])
					{
						$newRes[$z] = new stdClass();
						$newRes[$z]->label = $days[$j];
						$newRes[$z]->y = $data[$k]->total;
						break;
					}
					else
					{
						$newRes[$z] = new stdClass();
						$newRes[$z]->label = $days[$j];
						$newRes[$z]->y = 0;
					}
				}
				//unset($res[$any]);
				$z++;
				
				
			}
		}
		
		
		header("Content-Type: application/json");
		print_r(json_encode($newRes));
	}
	
	function ListOfDay($month, $year)
	{
		//$month = "05";
		//$year = "2014";

		$start_date = "01-".$month."-".$year;
		$start_time = strtotime($start_date);

		$end_time = strtotime("+1 month", $start_time);

		for($i=$start_time; $i<$end_time; $i+=86400)
		{
		   $list[] = date('Y-m-d', $i);
		}
		//print_r($list);
		return $list;
	}
	
	function ListOfDayRangeDate($start_date, $end_date)
	{
		//$month = "05";
		//$year = "2014";

		//$start_date = "01-".$month."-".$year;
		$start_time = strtotime($start_date);

		$end_time = strtotime($end_date);

		for($i=$start_time; $i<$end_time; $i+=86400)
		{
		   $list[] = date('Y-m-d', $i);
		}
		//print_r($list);
		return $list;
	}
	
	function graph_today_interaction()
	{
		$id = $this->input->post('variant_id');
		$var = $this->db->get_where('variant', array('id' => $id))->first_row();

		date_default_timezone_set('Asia/Jakarta');
		$dt = new DateTime();
		$datetime = $dt->format('Y-m-d H:i:s');
		//$time = explode(" ", $datetime)[1];
		$hour = $dt->format('H');
		//$hour = 18;
		//$date = '2014-12-05';
		$date = $dt->format('Y-m-d');
		//echo $hour;
		$res = array();
		$j=0;
		for($i=6;$i<($hour+1);$i++)
		{
			$res[$j] = new stdClass();
			$res[$j]->label = $i . ':00:00';
			$res[$j]->y = $this->db->select('datetime')->from('activity')->where('target_id', $var->target_id)->where('DATE_FORMAT(datetime, "%Y-%m-%d %H:%m:%s") BETWEEN "' . $date .' '.$i.':00:00" AND "' . $date . ' ' . $i . ':59:59"')->get()->num_rows();
			$j++;
		}
		header("Content-Type: application/json");
		print_r(json_encode($res));
	}
	
	function graph_weekly_interaction()
	{
		$id = $this->input->post('variant_id');
		$var = $this->db->get_where('variant', array('id' => $id))->first_row();
		date_default_timezone_set('Asia/Jakarta');
		$dt = new DateTime();
		$datetime = $dt->format('Y-m-d H:i:s');
		//$year = '2014';
		//$month = '12';
		$year = $dt->format('Y');
		$month = $dt->format('m');
		//$SQL = "SELECT WEEK(`datetime`,6) - WEEK(DATE_SUB(`datetime`, INTERVAL DAYOFMONTH(`datetime`)-1 DAY),6)+1 as week, count(*) as total FROM `activity` WHERE `target_id` = '".$target_id."' AND YEAR(`datetime`) = '".$year."' AND MONTH(`datetime`) = '".$month."' GROUP BY WEEK(`datetime`)";
		$this->db->select('WEEK(datetime, 6) - WEEK(DATE_SUB(datetime, INTERVAL DAYOFMONTH(datetime)-1 DAY), 6)+1 as label', FALSE);
		$res = $this->db->select('count(*) as y')->from('activity')->where(array('target_id' => $var->target_id, 'YEAR(datetime)' => $year, 'MONTH(datetime)' => $month))->group_by('WEEK(datetime)')->get();

		header("Content-Type: application/json");
		print_r(json_encode($res->result()));
	}
	
	function graph_monthly_interaction()
	{
		$id = $this->input->post('variant_id');
		$var = $this->db->get_where('variant', array('id' => $id))->first_row();
		date_default_timezone_set('Asia/Jakarta');
		$dt = new DateTime();
		$datetime = $dt->format('Y-m-d H:i:s');
		//$year = '2014';
		$year = $dt->format('Y');
		//$SQL = "SELECT MONTHNAME(`datetime`) as month, count(*) as total FROM `activity` WHERE `target_id` = '$target_id' AND YEAR(`datetime`) = ".$year." GROUP BY MONTH(`datetime`)";
		$this->db->select('MONTHNAME(`datetime`) as label', FALSE);
		$res = $this->db->select('count(*) as y')->from('activity')->where(array('target_id' => $var->target_id, 'YEAR(datetime)' => $year))->group_by('MONTH(datetime)')->get();
		header("Content-Type: application/json");
		print_r(json_encode($res->result()));
	}
}

