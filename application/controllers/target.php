<?php

class Target extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('pagination');
	}

	function index()
	{
		$data['page'] = "target";
		$this->load->model('read');
		$data['target'] = $this->read->target();
		$this->load->view('home', $data);
	}
	

	function refresh()
	{
		$this->load->model('delete');
		$this->delete->truncate('target');
		$dir = scandir('./targets/');
		unset($dir[0]);
		unset($dir[1]);
		$i=0;
		$data['target'] = array();
		foreach($dir as $d) {$target_dir[$i] = $d;$i++;}
		$i=0;
		foreach($target_dir as $dir_name)
		{
			$data['target'][$i] = new stdClass();
			$xml = simplexml_load_file('./targets/' . $dir_name . '/' . $dir_name . '.xml') or die("Error: Cannot create object");
			$name = $xml->Tracking->ImageTarget['name'];
			$size = explode(' ', $xml->Tracking->ImageTarget['size']);

			$data['target'][$i]->target_id = $this->GetTargetID($dir_name);
			$data['target'][$i]->name = (string)$name;
			$data['target'][$i]->width = $size[0];
			$data['target'][$i]->height = $size[1];
			$data['target'][$i]->tracking_rating = $this->TrackingRating($dir_name);
			$i++;
		}
		
		$this->load->model('insert');
		for($i=0;$i<count($data['target']);$i++) $this->insert->addIntoTable('target', $data['target'][$i]);
		//for($i=0;$i<count($data['target']);$i++) print_r($data['target'][$i]);
		
		redirect('target');
	}
	
	function GetTargetID($dir)
	{
		$z = new ZipArchive;
		$target_id = '';
		if (!file_exists('./targets/' . $dir . '/' . 'config.info') && !file_exists($dir.'.tex.jpg'))
		{
			if ($z->open('./targets/' . $dir . '/' . $dir . '.dat'))
			{
				if($z->extractTo('./targets/' . $dir . '/', array('config.info', $dir.'.tex.jpg')))
				{
					$xml = simplexml_load_file('./targets/' . $dir . '/config.info') or die("Error: Cannot create object");
					$target_id = (string)$xml->TargetSet->ImageTarget['targetId'];
				}
			}
		}
		else
		{
			$xml = @simplexml_load_file('./targets/' . $dir . '/config.info');
			if(!$xml)
			{
				if($z->open('./targets/' . $dir . '/' . $dir . '.dat'))
				{
					if($z->extractTo('./targets/' . $dir . '/', array('config.info', $dir.'.tex.jpg')))
					{
						$xml = simplexml_load_file('./targets/' . $dir . '/config.info') or die("Error: Cannot create object");
						$target_id = (string)$xml->TargetSet->ImageTarget['targetId'];
					}
				}
			}
			else
			{
				$xml = simplexml_load_file('./targets/' . $dir . '/config.info') or die("Error: Cannot create object");
				$target_id = (string)$xml->TargetSet->ImageTarget['targetId'];
			}
		}
		
		chmod('./targets/' . $dir . '/' . 'config.info', 0777);
		chmod('./targets/' . $dir . '/' . $dir.'.tex.jpg', 0777);
		return $target_id;
	}
	
	function TrackingRating($dir, $rate = 0)
	{
		if(file_exists('./targets/' . $dir . '/tracking_rating.info'))
		{
			$this->load->model('read');
			$tr = $this->read->target(array('name' => $dir));
			$tr = $tr[0];
			$tr = $tr->tracking_rating;
			if($rate!=$tr)
			{
				unlink('./targets/' . $dir . '/tracking_rating.info');
				$myfile = fopen('./targets/' . $dir . '/tracking_rating.info', 'x');
				$txt = (string)$rate;
				fwrite($myfile, $txt);
				fclose($myfile);
				chmod('./targets/' . $dir . '/tracking_rating.info', 0777);
			}
			else
			{
				$myfile = fopen('./targets/' . $dir . '/tracking_rating.info', 'r');
				$rate = fread($myfile, filesize('./targets/' . $dir . '/tracking_rating.info'));
				fclose($myfile);
			}
			
			// update
		}
		else
		{
			//mkdir('./targets/'. $dir, 0777);
			$myfile = fopen('./targets/' . $dir . '/tracking_rating.info', 'x');
			$txt = (string)$rate;
			fwrite($myfile, $txt);
			fclose($myfile);
			chmod('./targets/' . $dir . '/tracking_rating.info', 0777);
		}
		return $rate;
	}
	
	function detail($target_id)
	{
		$data['page'] = "target/detail";
		$this->load->model('read');
		$data['target'] = $this->read->target($target_id);
		$this->load->view('home', $data);
	}

	function edit($target_id)
	{
		$data['page'] = "target/edit";
		$this->load->model('read');
		$data['target'] = $this->read->target($target_id);
		//print_r($data['marker']);
		$this->load->view('home', $data);
	}

	function delete($target_id)
	{
		$this->load->model('delete');
		$this->load->model('read');
		$target = $this->read->target($target_id);
		$name = $target->name;
		$dir = './targets/' . $name;
		$this->delete->deleteTarget($target_id);
		$this->delete->deleteRecord('target_content', array('target_id' => $target_id));
		foreach(glob($dir .'/*.*') as $filename) unlink($filename);
		rmdir($dir.'/');
		$msg = array('Target ' . $name . ' deleted');
		$this->session->set_flashdata('message', $msg);
		redirect('target');
		
	}
	
	function content($target_id)
	{
		$this->session->set_userdata('tmpTargetId', $target_id);
		$data['page'] = "target/content";
		$this->load->model('read');
		$data['target_content'] = $this->read->target_content($target_id);
		$data['content'] = $this->read->content();
		$this->load->view('home', $data);
	}

	function content_add($target_id, $content_id)
	{
		//$data['page'] = "marker/content";
		$this->load->model('read');
		if($this->read->check_target_content(array('target_id' => $target_id, 'content_id' => $content_id)))
		{
			$this->load->model('insert');
			$data = array('target_id' => $target_id, 'content_id' => $content_id);
			$this->insert->addIntoTable('target_content', $data);
			$this->session->set_flashdata('message', 'Content added successfully for this target marker');
		}
		else
		{
			$this->session->set_flashdata('message', 'Content already added for this target marker');
		}
		redirect('target/content/'.$target_id);
	}

	function content_delete($content_id)
	{
		$this->load->model('delete');
		$data = array('target_id' => $this->session->userdata('tmpTargetId'), 'content_id' => $content_id);
		print_r($data);
		$this->delete->deleteRecord('target_content', $data);
		redirect('target/content/'.$this->session->userdata('tmpTargetId'));
	}

	function marker_content_json()
	{
		$target_id = $this->input->post('target_id');
		$this->load->model('read');
		$result = array('TargetID' => $target_id, 'Contents' => $this->read->marker_content_json($target_id));
		header("Content-Type: application/json");
		print_r(json_encode($result));
	}
	
	function upload()
	{
		//C:\xampp\htdocs\dav\assets\marker
		$config['upload_path'] = './tmp/';
		$config['allowed_types'] = 'zip';
		$config['file_name'] = 'tmptarget';
		$msg = array();
		$this->load->library('upload', $config);
		$error = array();
		if(!$this->upload->do_upload())
		{
			$msg = $this->upload->display_errors();
		}
		else
		{
			$xml = null;
			$data = array('upload_data' => $this->upload->data());
			$filename = $data['upload_data']['file_name'];

			$name = '';
			$z = new ZipArchive(); 
			$success = false;
			if ($z->open('./tmp/tmptarget.zip'))
			{
				if($z->extractTo('./tmp/'))
				{
					//print_r(glob('./tmp/*.xml'));
					foreach(glob('./tmp/*.xml') as $filename)
					{
						
						$xml = simplexml_load_file($filename) or die("Error: Cannot create object");
						$name = (string)$xml->Tracking->ImageTarget['name'];
					}
				}
				
				$dir = './targets/' . $name;
				//echo $dir;
				if(!file_exists($dir))
				{
					mkdir($dir, 0777);
					
					$size = explode(' ', $xml->Tracking->ImageTarget['size']);
					$width = $size[0];
					$height = $size[1];
					
					foreach(glob('./tmp/*.dat') as $filename) rename($filename, './targets/' . $name . '/' . $name . '.dat');
					foreach(glob('./tmp/*.xml') as $filename) rename($filename, './targets/' . $name . '/' . $name . '.xml');
					//foreach(glob('./tmp/*.unity3d') as $filename) rename($filename, './targets/' . $name . '/' . $name . '.unity3d');
					chmod('./targets/' . $name, 0777);
					chmod('./targets/' . $name . '/' . $name . '.dat', 0777);
					chmod('./targets/' . $name . '/' . $name . '.xml', 0777);
					//chmod('./targets/' . $name . '/' . $name . '.unity3d', 0777);
					$target_id = $this->GetTargetID($name);
					$tracking_rating = $this->TrackingRating($name, $this->input->post('tracking_rating'));
					unlink('./tmp/tmptarget.zip');
					$this->load->model('read');
					//print_r($this->read->target($target_id));
					if(is_array($this->read->target($target_id)))
					{
						$this->load->model('insert');
						$this->insert->addIntoTable('target', array('target_id' => $target_id, 'name' => $name, 'width' => $width, 'height' => $height, 'tracking_rating' => $tracking_rating));
						$msg = array('Target ID ' . $target_id . ' named ' . $name . ' succesfully uploaded');
					}
					else
					{
						$msg = array('Target ID ' . $target_id . ' already exist');
						foreach(glob('./targets/' . $name .'/*.*') as $filename) unlink($filename);
						rmdir('./targets/' . $name);
					}
				}
				else
				{
					$msg = array('Target name ' . $name . ' already exist');
				}
				$z->close();
				$success = true;

			}
			else
			{
				$msg = array('Can`t Open Zip Archive');
			}
			
			if($success)
			{
				foreach(glob('./tmp/*.*') as $filename)
				{
					unlink($filename);
				}
			}
		}
		$this->session->set_flashdata('message', $msg);
		redirect('target');
	}

	/*
	function index()
	{
		$data['noty'] = true;
		$data['datatables'] = true;
		$data['sweetalert'] = true;
		$data['page'] = "target";
		$this->load->model('read');
		$tdb = $this->read->get('target_db');
		$iddb = array();
		for($i=0;$i<count($tdb);$i++)
		{
			$iddb[$i] = new stdClass();
			$iddb[$i]->id = $tdb[$i]->id;
			$iddb[$i]->name = $tdb[$i]->name;
			$tmp = $this->read->get('target_id_db', array('target_db_id' => $tdb[$i]->id));
			$iddb[$i]->total = count($tmp);
		}
		$data['target'] = $iddb;
		$this->load->view('home', $data);
	}
	
	
	function detail($id)
	{
		$data['page'] = "target/detail";
		$this->load->model('read');
		$target = $this->read->get('target', array('target_id' => $id));
		$data['target'] = $target[0];
		$tdbid = $this->read->get('target_id_db', array('target_id' => $id));
		$tdbid = $tdbid[0];
		$tdb = $this->read->get('target_db', $tdbid->target_db_id);
		$data['dbname'] = $tdb->name;
		$data['dbid'] = $tdb->id;
		$this->load->view('home', $data);
	}
	
	function upload() // plan c
	{
		$this->load->model('read');
		$this->load->model('insert');
		$this->load->model('delete');
		//C:\xampp\htdocs\dav\assets\marker
		$config['upload_path'] = './tmp/';
		$config['allowed_types'] = 'zip';
		$config['file_name'] = 'tmptarget';
		$msg = array();
		$this->load->library('upload', $config);
		$error = array();
		if(!$this->upload->do_upload())
		{
			$msg = $this->upload->display_errors();
		}
		else
		{
			$xml = null;
			$data = array('upload_data' => $this->upload->data());
			$filename = $data['upload_data']['file_name'];
			
			$dbname = '';
			//$a = $this->read->get('target_db', array('name' => $filename));
			
			$name = '';
			$z = new ZipArchive(); 
			$success = false;
			if ($z->open('./tmp/tmptarget.zip'))
			{
				$target = array();
				if($z->extractTo('./tmp/'))
				{
					$imgTarget = array();
					$arr_target_name = array();
					//print_r(glob('./tmp/*.xml'));
					foreach(glob('./tmp/*.xml') as $filename)
					{
						$fn = explode('/', $filename)[2];
						$dbname = explode('.', $fn)[0];
						$xml = simplexml_load_file($filename) or die("Error: Cannot create object");
						
						//$name = (string)$xml->Tracking->ImageTarget['name'];
						$imgTarget = $xml->Tracking->ImageTarget;
					}
					
					$tdb = $this->read->get('target_db', array('name' => $dbname));

					if(count($tdb)==0) 
					{
						$this->insert->addIntoTable('target_db', array('name' => $dbname));
						mkdir('./targets/' . $dbname, 0777);
						$tdb = $this->read->get('target_db', array('name' => $dbname));
						$tdb = $tdb[0];
					}
					else
					{
						$tdb = $tdb[0];
						$tiddb = $this->read->get('target_id_db', array('target_db_id' => $tdb->id));
						for($i=0;$i<count($tiddb);$i++)
							$this->delete->deleteRecord('target', array('target_id' => $tiddb[$i]->target_id));
						
						$this->delete->deleteRecord('target_id_db', array('target_db_id' => $tdb->id));
						foreach(glob('./targets/'.$dbname.'/*.*') as $filename)
						{
							//echo $filename.'<br />';
							//chmod($filename, 0777);
							unlink($filename);
						}
						rmdir('./targets/' . $dbname);
						mkdir('./targets/' . $dbname, 0777);
					}
					
					foreach(glob('./tmp/*.dat') as $filename) copy($filename, './targets/'.$dbname.'/'.$dbname.'.dat');
					foreach(glob('./tmp/*.xml') as $filename) copy($filename, './targets/'.$dbname.'/'.$dbname.'.xml');
					chmod('./targets/'.$dbname.'/'.$dbname.'.dat', 0777);
					chmod('./targets/'.$dbname.'/'.$dbname.'.xml', 0777);
					
					
					for($i=0;$i<count($imgTarget);$i++) $arr_target_name[$i] = (string)$imgTarget[$i]['name'];
					$arr_target_id = $this->GetTargetID($dbname, $arr_target_name);
					
					for($i=0;$i<count($imgTarget);$i++)
					{
						$size = explode(' ', $imgTarget[$i]['size']);
						$target[$i] = new stdClass();
						$target[$i]->target_id = $arr_target_id[$i];
						$target[$i]->name = (string)$imgTarget[$i]['name'];
						$target[$i]->width = $size[0];
						$target[$i]->height = $size[1];
						$target[$i]->tracking_rating = $this->TrackingRating($dbname, (string)$imgTarget[$i]['name']);
					}
				}
				
				for($i=0;$i<count($target);$i++) $this->insert->addIntoTable('target', $target[$i]);
				//$tdb = $this->read->get('target_db', array('name' => $dbname));
				//$tdb = $tdb[0];
				//print_r($tdb);
				for($i=0;$i<count($target);$i++) $this->insert->addIntoTable('target_id_db', array('target_id' => $target[$i]->target_id, 'target_db_id' => $tdb->id));
				
				$msg = array('Target database ' . $dbname .' and its content have been uploaded ');
				$z->close();
				$success = true;
			}
			else
			{
				$msg = array('Can`t Open Zip Archive');
			}
			if($success) 
			{
				foreach(glob('./tmp/*.*') as $filename)
				{
					unlink($filename);
				}
			}
		}
		$this->session->set_flashdata('message', $msg);
		redirect('target');
	}
	
	
	function GetTargetID($dbname, $arr_target_name)
	{
		$z = new ZipArchive;
		$target_id = array();
		$extrackList = array('config.info');
		for($i=0;$i<count($arr_target_name);$i++)
		{
			array_push($extrackList, $arr_target_name[$i].'.tex.jpg');
		}
		//print_r($extrackList);
		if (!file_exists('./targets/' . $dbname . '/' . 'config.info'))
		{
			if ($z->open('./targets/' . $dbname . '/' . $dbname . '.dat'))
			{
				if($z->extractTo('./targets/' . $dbname . '/', $extrackList))
				{
					$xml = simplexml_load_file('./targets/' . $dbname . '/config.info') or die("Error: Cannot create object");
					$imgTarget = $xml->TargetSet->ImageTarget;
					for($i=0;$i<count($imgTarget);$i++)
					{
						$target_id[$i] = (string)$imgTarget[$i]['targetId'];
					}
				}
			}
		}
		else
		{
			$xml = @simplexml_load_file('./targets/' . $dbname . '/config.info');
			if(!$xml)
			{
				if($z->open('./targets/' . $dbname . '/' . $dbname . '.dat'))
				{
					if($z->extractTo('./targets/' . $dbname . '/', $extrackList))
					{
						$xml = simplexml_load_file('./targets/' . $dbname . '/config.info') or die("Error: Cannot create object");
						$imgTarget = $xml->TargetSet->ImageTarget;
						for($i=0;$i<count($imgTarget);$i++)
						{
							$target_id[$i] = (string)$imgTarget[$i]['targetId'];
						}
					}
				}
			}
			else
			{
				$xml = simplexml_load_file('./targets/' . $dbname . '/config.info') or die("Error: Cannot create object");
				$imgTarget = $xml->TargetSet->ImageTarget;
				for($i=0;$i<count($imgTarget);$i++)
				{
					$target_id[$i] = (string)$imgTarget[$i]['targetId'];
				}
			}
		}
		
		chmod('./targets/' . $dbname . '/' . 'config.info', 0777);
		for($i=0;$i<count($arr_target_name);$i++)
		{
			chmod('./targets/' . $dbname . '/' . $arr_target_name[$i].'.tex.jpg', 0777);
		}
		return $target_id;
	}
	
	function TrackingRating($dbname, $target_name, $rate = 0)
	{
		if(file_exists('./targets/' . $dbname . '/' . $target_name . '-rate.info'))
		{
			$this->load->model('read');
			$tr = $this->read->get('target', array('name' => $target_name));
			$tr = $tr[0];
			$tr = $tr->tracking_rating;
			if($rate!=$tr)
			{
				unlink('./targets/' . $dbname . '/' . $target_name . '-rate.info');
				$myfile = fopen('./targets/' . $dbname . '/' . $target_name . '-rate.info', 'x');
				$txt = (string)$rate;
				fwrite($myfile, $txt);
				fclose($myfile);
				chmod('./targets/' . $dbname . '/' . $target_name . '-rate.info', 0777);
			}
			else
			{
				$myfile = fopen('./targets/' . $dbname . '/' . $target_name . '-rate.info', 'r');
				$rate = fread($myfile, filesize('./targets/' . $dbname . '/' . $target_name . '-rate.info'));
				fclose($myfile);
			}
			
			// update
		}
		else
		{
			//mkdir('./targets/'. $dir, 0777);
			$myfile = fopen('./targets/' . $dbname . '/' . $target_name . '-rate.info', 'x');
			$txt = (string)$rate;
			fwrite($myfile, $txt);
			fclose($myfile);
			chmod('./targets/' . $dbname . '/' . $target_name . '-rate.info', 0777);
		}
		return $rate;
	}

	
	function dbdetail($id)
	{
		$data['datatables'] = true;
		$data['page'] = "target/dbdetail";
		$this->load->model('read');
		$data['dbname'] = $this->read->get('target_db', $id)->name;
		$iddb = $this->read->get('target_id_db', array('target_db_id' => $id));
		$data['target'] = array();
		for($i=0;$i<count($iddb);$i++)
		{
			$tmp = $this->read->get('target', array('target_id' => $iddb[$i]->target_id));
			$data['target'][$i] = $tmp[0];
		}
		//echo json_encode($data['target']);
		$this->load->view('home', $data);
	}
	
		function delete($target_db_id)
	{
		$this->load->model('delete');
		$this->load->model('read');
		$tdb = $this->read->get('target_db', $target_db_id);
		$targets = $this->read->get('target_id_db', array('target_db_id' => $tdb->id));
		//delete physical data
		foreach(glob('./targets/' . $tdb->name .'/*.*') as $filename) unlink($filename);
		rmdir('./targets/'.$tdb->name);
		//delete data in database
		for($i=0;$i<count($targets);$i++)
		{
			$this->delete->deleteRecord('target', array('target_id' => $targets[$i]->target_id));
		}
		$this->delete->deleteRecord('target_id_db', array('target_db_id' => $target_db_id));
		$this->delete->deleteRecord('target_db', $target_db_id);
		$msg = array('Target ' . $tdb->name . ' deleted');
		$this->session->set_flashdata('message', $msg);
		redirect('target');
	}
	
	function ajax_get_target_by_targetdb_id($target_db_id)
	{
		if($this->session->userdata('is_logged_in'))
		{
			$this->load->model('read');
			$target = $this->read->get('target_id_db', array('target_db_id' => $target_db_id));
			$tmpTarget = array();
			$i=0;
			foreach($target as $t)
			{
				if(count($this->read->get('variant', array('target_id' => $t->target_id)))==0)
				{
					$tmpTarget[$i] = $this->read->get('target', array('target_id' => $t->target_id))[0];
					$i++;
				}
			}
			header("Content-Type: application/json");
			print_r(json_encode($tmpTarget));
		} else {
			redirect('auth', 'refresh');
		}
	}
	*/
	
	
	function update()
	{
		$this->statusCheck(2);
		$this->load->model('read');
		$target_id = $this->input->post('target_id');
		$target = $this->read->target($target_id);
		
		$tracking_rating = $this->input->post('tracking_rating');
		$this->load->model('update');
		$this->TrackingRating($target->name, $tracking_rating);
		$data = array('tracking_rating' => $tracking_rating);
		print_r($data);
		$this->update->targetUpdate('target', $data, $target_id);
		$this->message('Target ' . $target->name . ' updated succesfully');
		redirect('target/detail/'.$target_id);
	}
	
	function tag()
	{
		$data['page'] = "marker/tag";
		$this->load->model('read');
		$data['marker_tag'] = $this->read->marker_tag($this->uri->segment(3));
		$data['tag'] = $this->read->tag();
		//$this->_print_data($data);die();
		$this->load->view('home', $data);
	}

	function tag_submit()
	{
		$this->load->model('insert');
		$data = $this->_get_post_data();
		$this->_print_data($data);
		$length = sizeof($data['tag']) - 1;
		//echo $data['tag'][0];
		for ($i=0; $i<=$length;$i++ ) {
			$this->insert->addTag($data['marker_id'], $data['tag'][$i]);
		}
		redirect ('marker/tag/'.$data['marker_id']);
	}
	
	

}