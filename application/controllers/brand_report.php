<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Brand_report extends MY_Controller {

	
	function __construct()
	{
		parent::__construct();
		$this->load->model('analytics');
		$this->load->model('update');
		$this->load->model('delete');
		$this->load->library('pagination');
	}

	function index()
	{
		$this->authCheck();
		$this->load->model('read');
		$data['page'] = "brand_report";
		$data['user'] = $this->db->get_where('users', array('username' => $this->session_data->username))->first_row();

		$data['product'] = $this->analytics->product($data['user']->brand_id);
		//$this->_print_data($data);die();
		$this->load->view('analytics/index', $data);
	}
	
	function product($variant_id)
	{
		$this->authCheck();
		$this->statusCheck(1);
		$data['page'] = "report_product";
		$this->session->set_userdata('variant_id', $variant_id);
		$user = $this->db->get_where('users', array('username' => $this->session_data->username))->first_row();
		$data['variant'] = $this->db->get_where('variant', array('id' => $variant_id))->first_row();
		$data['product'] = $this->db->get_where('product', array('id' => $data['variant']->product_id))->first_row();
		$data['province'] = $this->db->get('province_area')->result();
		//$this->_print_data($data);die();
		$this->load->view('analytics/index', $data);
	}
	
	

	function topten()
	{
		$this->authCheck();
		$this->statusCheck(1);
		$data['page'] = "report_topten"; 
		$var_id = $this->session->userdata('variant_id');
		$target_id = $this->db->get_where('variant', array('id' => $var_id))->first_row()->target_id;
		$post = $this->input->post();
		$post['target_id'] = $target_id;
		$this->session->set_userdata('topten', $post);
		//$province_id = $this->input->post('province');
		//$city_id = $this->input->post('city');
		
		//$start_date = $this->input->post('start_date');
		//$end_date = $this->input->post('end_date');
		
				//$user = $this->db->from('users')->where('username', $this->session_data['username'])->get()->first_row();
		$data['variant'] = $this->db->get_where('variant', array('id' => $var_id))->first_row();
		$data['product'] = $this->db->get_where('product', array('id' => $data['variant']->product_id))->first_row();
		$target_id = $this->db->select('target_id')->where('id', $var_id)->get('variant')->first_row()->target_id;
		
		$this->load->view('analytics/index', $data);
	}
	



	function pdf()
	{
		
		//$this->load->view('client/pdf_report', $data);
		$this->load->view('jpgraph_view');
		// Get output html
		$html = $this->output->get_output();
		 
		// Load library
		$this->load->library('dompdf_gen');
		 
		// Convert to PDF
		$this->dompdf->load_html($html);
		$this->dompdf->render();
		$this->dompdf->stream("welcome.pdf",array('Attachment'=>0));
		//or without preview in browser
		//$this->dompdf->stream("welcome.pdf");


	}
	
	
	function showGraph() {
		$this->load->library('jpgraph');
	
		$bar_graph = $this->jpgraph->barchart();
		$datax = array(2,10,20);
		$datay = array("rendah","sedang","bagus");
	
		$graph = new PieGraph(400,270,"auto");
		$graph->SetScale('textint');
		$graph->img->SetMargin(50,30,70,100);
		$graph->SetShadow();
			
		$bplot = new PiePlot3D($datax);
		$bplot->SetCenter(0.45,0.40);
		$bplot->SetLegends($datay);
		$bplot->value->Show();
		$bplot->value->SetFont(FF_ARIAL,FS_BOLD);
	
		$graph->Add($bplot);
		$graph->Stroke();
	}
	
	function html2pdf(){
		
		//Load the library
		$this->load->library('html2pdf');
		 
		//Set folder to save PDF to
		$this->html2pdf->folder('./assets/pdfs/');
		 
		//Set the filename to save/download as
		$this->html2pdf->filename('test.pdf');
		 
		//Set the paper defaults
		$this->html2pdf->paper('a4', 'portrait');
		 
		$data = array(
				'title' => 'PDF Created',
				'message' => 'Hello World!'
		);
		 
		//Load html view
		$this->html2pdf->html($this->load->view('pdf', $data, true));
		 
		if($this->html2pdf->create('save')) {
			//PDF was successfully saved or downloaded
			echo 'PDF saved';
		}
		
	}
	
}?>