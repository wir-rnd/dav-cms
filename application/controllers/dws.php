<?php

class dws extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('dwsm');
		$this->load->model('read');
    }

    function index()
    {
        
    }
	
	function target()
    {
		$sn = $this->input->post('sn');
		//$sn = '55dfc788c5406c948376bd137e5a1769';
		//$sn = 'a49e8acdfec2b9b0a4d82daf497f0f02e9e48fc3';
		$data = $this->dwsm->variant($sn);
		$target = array();
		for($i=0;$i<count($data);$i++)
		{
			//print_r($this->read->variant($data[$i])).'<br />';
			$target[$i] = $this->read->target($this->read->variant($data[$i])->target_id);
			$name = $target[$i]->name;
			$target[$i]->dat_md5 = md5_file(base_url().'targets/' . $name . '/' . $name . '.dat');
			$target[$i]->xml_md5 = md5_file(base_url().'targets/' . $name . '/' . $name . '.xml');
			//$target[$i]->unity3d_md5 = md5_file(base_url().'targets/' . $name . '/' . $name . '.unity3d');
		}
		$target = array('SerialNumber' => $sn, 'Tracking' => $target);
		header("Content-Type: application/json");
        print_r(json_encode($target));
		//print_r($target);
	}
	
	function target1()
    {
		//$sn = $this->input->post('sn');
		//$sn = '55dfc788c5406c948376bd137e5a1769';
		$sn = '91bbbbde1777715fa2db5a28aeea72d2783872ff';
		$data = $this->dwsm->variant($sn);
		$target = array();
		for($i=0;$i<count($data);$i++)
		{
			//print_r($this->read->variant($data[$i])).'<br />';
			$target_id = $this->read->variant($data[$i])->target_id;
			$target[$i] = $this->read->target($this->read->variant($data[$i])->target_id);
			$name = $target[$i]->name;
			$target[$i]->dat_md5 = md5_file(base_url().'targets/' . $name . '/' . $name . '.dat');
			$target[$i]->xml_md5 = md5_file(base_url().'targets/' . $name . '/' . $name . '.xml');
			//$target[$i]->unity3d_md5 = md5_file(base_url().'targets/' . $name . '/' . $name . '.unity3d');
		}
		$target = array('SerialNumber' => $sn, 'Tracking' => $target);
		header("Content-Type: application/json");
        print_r(json_encode($target));
		//print_r($target);
	}
	
	
	function create_zip($files = array(), $dir, $zip_name = '')
	{
		$error = '.';
		$zip = new ZipArchive(); // Load zip library
		if($zip->open($zip_name, ZIPARCHIVE::CREATE)!==TRUE)
		{
			// Opening zip file to load files
			$error .= "* Sorry ZIP creation failed at this time";
		}
		
		foreach($files as $file)
		{
			$zip->addFile($dir.$file); // Adding files into zip
		}
		
		$zip->close();
		if(file_exists($zip_name))
		{
			// push to download the zip
			header('Content-type: application/zip');
			header('Content-Disposition: attachment; filename="'.$zip_name.'"');
			readfile($zip_name);
			// remove zip file is exists in temp path
			unlink($zip_name);
		}
		//return $error;
	}

    //function content($type, $sn, $target_id = null)
	function content()
    {
        $type = $this->input->post('bytype');
		$sn = $this->input->post('sn');
        if($type=="ByTargetID")
        {
            $target_id = $this->input->post('target_id');
			$device = $this->dwsm->device($sn);
            $contents = $this->dwsm->content(array('target_id' => $target_id, 'store_id' => $device->store_id, 'shelf_id' => $device->shelf_id), "ByTarget");
            $fin_contents = array();
            $i=0;
			$cid = 0;
			if(isset($contents))
			{
				foreach($contents as $c)
				{
					$fin_contents[$i] = new stdClass();
					$fin_contents[$i]->ContentID = $c->ContentID;
					$fin_contents[$i]->Name = $c->Name;
					$fin_contents[$i]->px = $c->px;
					$fin_contents[$i]->py = $c->py;
					$fin_contents[$i]->pz = $c->pz;
					$fin_contents[$i]->rx = $c->rx;
					$fin_contents[$i]->ry = $c->ry;
					$fin_contents[$i]->rz = $c->rz;
					$fin_contents[$i]->sx = $c->sx;
					$fin_contents[$i]->sy = $c->sy;
					$fin_contents[$i]->sz = $c->sz;
					$cd = $this->read->content_detail(array('content_id' => $c->ContentID));
					$ncd = array();
					for($j=0;$j<count($cd);$j++)
					{
						$ncd[$j] = new stdClass();
						$ncd[$j]->ContentDetailID = $cd[$j]->id;
						$ncd[$j]->Name = $cd[$j]->name;
						$ncd[$j]->MD5Checksum = md5_file(base_url().'contents/' . $c->Name . '/' . $cd[$j]->name);
					}
					$fin_contents[$i]->ContentDetail = $ncd;
					$i++;
				}
			}
			
            $result = array('TargetID' => $target_id, 'Contents' => $fin_contents);
        }
        else if($type=="BySN")
        {
            $device = $this->dwsm->device($sn);
            $contents = $this->dwsm->content(array('store_id' => $device->store_id, 'shelf_id' => $device->shelf_id), "ByStoreAndRack");
			//print_r($contents);
            $fin_contents = array();
            $i=0;
            foreach($contents as $c)
            {
                $fin_contents[$i] = new stdClass();
                $fin_contents[$i]->ContentID = $c->ContentID;
                $fin_contents[$i]->Name = $c->Name;
                $fin_contents[$i]->TargetName = $c->TargetName;
				$fin_contents[$i]->px = $c->px;
				$fin_contents[$i]->py = $c->py;
				$fin_contents[$i]->pz = $c->pz;
				$fin_contents[$i]->rx = $c->rx;
				$fin_contents[$i]->ry = $c->ry;
				$fin_contents[$i]->rz = $c->rz;
				$fin_contents[$i]->sx = $c->sx;
				$fin_contents[$i]->sy = $c->sy;
				$fin_contents[$i]->sz = $c->sz;
                $cd = $this->read->content_detail(array('content_id' => $c->ContentID));
				$ncd = array();
				for($j=0;$j<count($cd);$j++)
				{
					$ncd[$j] = new stdClass();
					$ncd[$j]->ContentDetailID = $cd[$j]->id;
					$ncd[$j]->Name = $cd[$j]->name;
					$ncd[$j]->MD5Checksum = md5_file(base_url().'contents/' . $c->Name . '/' . $cd[$j]->name);
				}
				$fin_contents[$i]->ContentDetail = $ncd;
                $i++;
            }
            $result = array('SerialNumber' => $sn, 'Contents' => $fin_contents);
        }
        header("Content-Type: application/json");
        print_r(json_encode($result));
    }
}