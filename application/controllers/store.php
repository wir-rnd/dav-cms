<?php
class store extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}

	function index()
	{
		$this->statusCheck(2);
		$data['page'] = 'store';
		$store = $this->db->get_where('store', array('trash' => 0))->result();
		$data['province'] = $this->db->get('province_area')->result();
		$newStore = array();
		$i=0;
		foreach($store as $s)
		{
			//$newStore[$i] = new stdClass();
			$newStore[$i] = $s;
			//print_r($this->read->get('province_area', substr($s->village_area_id, 0, 2)));
			$newStore[$i]->province_name = $this->db->get_where('province_area', array('id' => substr($s->village_area_id, 0, 2)))->first_row()->name;
			$newStore[$i]->district_name = $this->db->get_where('city_area', array('id' => substr($s->village_area_id, 0, 4)))->first_row()->name;
			$i++;
		}
		$data['store'] = $newStore;
		$this->load->view('home', $data);
	}

	function add()
	{
		$this->load->model('insert');
		$table = 'store';
		$data = $this->input->post();
		$dt = new DateTime();
		$datetime = $dt->format('Y-m-d H:i:s');
		$data['date_added'] = $datetime;
		$data['chain_id'] = 1;
		$data['alfamart_type'] = 1;
		
		$this->insert->addIntoTable('store', $data);
		$this->message('Store ' . $data['post']['store_id'] . ' added sucessfully');
		//print_r($data);die();
		redirect('store');
	}

	function edit($id)
	{
		$this->load->model('read');
		$data['page'] = "store/edit";
		$data['id'] = $id;
		$store = $this->read->get('store', $id);
		$store->province_area_id = $this->read->get('province_area', substr($store->village_area_id, 0, 2))->id;
		$store->city_area_id = $this->read->get('city_area', substr($store->village_area_id, 0, 4))->id;
		$store->subdistrict_area_id = $this->read->get('subdistrict_area', substr($store->village_area_id, 0, 7))->id;
		$store->village_area_id = $this->read->get('village_area', $store->village_area_id)->id;
		
		$data['province'] = $this->read->get('province_area');
		$data['city'] = $this->read->get('city_area', array('province_area_id' => $store->province_area_id));
		$data['subdistrict'] = $this->read->get('subdistrict_area', array('city_area_id' => $store->city_area_id));
		$data['village'] = $this->read->get('village_area', array('subdistrict_area_id' => $store->subdistrict_area_id));
		$data['store'] = $store;
		//print_r($data);die();
		$this->load->view('home', $data);
	}

	function update()
	{
		$this->load->model('update');
		$data = $this->input->post();
		$store_id = $data['id'];
		$store = $this->db->get('store', array('id' => $store_id))->first_row();
		unset($data['id']);
		$this->update->tableUpdate('store', $data, $store_id);
		$this->message('The store id ' . $store->store_id . ' has been updated');
		//$this->update->_where('store', $data, $store_id, 'store_id');
		//$this->_print_data($data);die();
		redirect('store');
	}

	function delete($id)
	{
		$this->load->model('delete');
		$store = $this->db->get_where('store', array('id' => $id))->first_row();
		$this->db->where('id', $id)->update('store', array('trash' => 1));
		$this->message('The store id ' . $store->store_id . '  has been deleted');
		//$this->delete->deleteRecord('store_locations', $id);
		redirect('store');
	}
	
	function modem($store_id)
	{
		$this->load->model('read');
		$data['page'] = "store/modem";
		$data['store'] = $this->read->get('store', $store_id);
		$data['store_id'] = $store_id;
		$this->load->model('read');
		$modem = $this->read->get('modem', array('store_id' => 0));
		//print_r($modem);
		for($i=0;$i<count($modem);$i++)
		{
			//echo $devices[$i]->os_devices_id.' '.$devices[$i]->brand_devices_id.'<br />';
			$brand = $this->read->get('brand_devices', $modem[$i]->brand_devices_id);
			$modem[$i]->brand_name = $brand->brand_name;
		}
		
		$data['modem'] = $modem;
		$modem = $this->read->get('modem', array('store_id' => $store_id));
		for($i=0;$i<count($modem);$i++)
		{
			if($modem[$i]->store_id!=0)
			{
				$brand = $this->read->get('brand_devices', $modem[$i]->brand_devices_id);
				$modem[$i]->brand_name = $brand->brand_name;
			}
		}
		$data['store_modem'] = $modem;
		$mh = $this->read->get('modem_history', array('store_id' => $store_id));
		$modem_history = array();
		for($i=0;$i<count($mh);$i++)
		{
			if($mh[$i]->last_used!='0000-00-00 00:00:00')
			{
				$modem_history[$i] = new stdClass();
				$m = $this->read->get('modem', $mh[$i]->modem_id);
				$modem_history[$i]->IMEI = $m->IMEI;
				$modem_history[$i]->brand_name = $this->read->get('brand_devices', $m->brand_devices_id)->brand_name;
				$modem_history[$i]->provider = $m->provider;
				$modem_history[$i]->model = $m->model;
				$modem_history[$i]->use = $mh[$i]->use;
				$modem_history[$i]->last_used = $mh[$i]->last_used;
			}
		}
		$data['modem_history'] = $modem_history;
		//print_r($devices);
		$this->load->view('home', $data);
	}

	function device($store_id)
	{
		$this->load->model('read');
		$data['page'] = "store/device";
		$data['shelf'] = $this->read->shelf();
		$data['store'] = $this->read->store($store_id);
		$data['store_id'] = $store_id;
		$devices = $this->read->get('devices', array('store_id' => 0, 'shelf_id' => 0));
		//print_r($devices);
		for($i=0;$i<count($devices);$i++)
		{
			//echo $devices[$i]->os_devices_id.' '.$devices[$i]->brand_devices_id.'<br />';
			$os = $this->read->get('os_devices', $devices[$i]->os_devices_id);
			$brand = $this->read->get('brand_devices', $devices[$i]->brand_devices_id);
			$devices[$i]->os_name = $os->os_name;
			$devices[$i]->brand_name = $brand->brand_name;
			
		}
		//print_r($devices);
		$data['devices'] = $devices;
		$devices = $this->read->get('devices', array('store_id' => $store_id));
		for($i=0;$i<count($devices);$i++)
		{
			if($devices[$i]->shelf_id!=0)
			{
				$shelf = $this->read->shelf($devices[$i]->shelf_id);
				$os = $this->read->os_devices($devices[$i]->os_devices_id);
				$brand = $this->read->brand_devices($devices[$i]->brand_devices_id);
				$devices[$i]->shelf_name = $shelf->name;
				$devices[$i]->os_name = $os->os_name;
				$devices[$i]->brand_name = $brand->brand_name;
			}
		}
		
		$data['store_device'] = $devices;
		$dh = $this->read->device_history(array('store_id' => $store_id));
		$device_history = array();
		for($i=0;$i<count($dh);$i++)
		{
			if($dh[$i]->last_used!='0000-00-00 00:00:00')
			{
				$device_history[$i] = new stdClass();
				$device_history[$i]->store_id = $this->read->store($dh[$i]->store_id)->id;
				$device_history[$i]->shelf_name = $this->read->shelf($dh[$i]->shelf_id)->name;
				$device_history[$i]->serial_number = $this->read->get('devices', $dh[$i]->device_id)->serial_number;
				$device_history[$i]->use = $dh[$i]->use;
				$device_history[$i]->last_used = $dh[$i]->last_used;
			}
		}
		$data['device_history'] = $device_history;
		//$data['devices'] = $this->read->phone_already_used();
// 		$this->_print_data($data);die();
		//$data['sweetalert'] = true;
		//$data['datatables'] = true;
		$this->load->view('home', $data);
	}
	
	function delete_modem($id, $store_id)
	{
		$this->load->model('update');
		$this->load->model('read');
		$modem = $this->db->get_where('modem', array('id' => $id))->first_row();
		
		date_default_timezone_set('Asia/Jakarta');
		$dt = new DateTime();
		$datetime = $dt->format('Y-m-d H:i:s');
		
		//$this->load->model('insert');
		$where = array(
			'store_id' => $modem->store_id,
			'modem_id' => $id
		);
		
		$this->update->tableUpdates('modem_history', array('last_used' => $datetime), $where);
		//$this->insert->addIntoTable('device_history', $data);
		$this->update->tableUpdate('modem', array('store_id' => 0), $id);
		
		$this->message('The modem ' . $modem->imei . ' has been removed');
		redirect('store/modem/' . $store_id);
	}
	
	function delete_device($id, $store_id)
	{
		$this->load->model('update');
		$this->load->model('read');
		$device = $this->read->get('devices', $id);
		
		date_default_timezone_set('Asia/Jakarta');
		$dt = new DateTime();
		$datetime = $dt->format('Y-m-d H:i:s');
		
		//$this->load->model('insert');
		$where = array(
			'store_id' => $device->store_id,
			'shelf_id' => $device->shelf_id,
			'device_id' => $id
		);
		$this->update->tableUpdates('device_history', array('last_used' => $datetime), $where);
		//$this->insert->addIntoTable('device_history', $data);
		$this->update->tableUpdate('devices', array('shelf_id' => 0, 'store_id' => 0), $id);
		$this->message('The device ' . $device->serial_number . ' has been removed');
		redirect('store/device/' . $store_id);
	}
	
	function modem_add($store_id, $modem_id)
	{
		// cek available space untuk rack pada store id tertentu, jika available update
		$this->load->model('read');
		$this->load->model('update');
		$this->load->model('insert');
		//$devices_id = $this->input->post('id');
		//$store_id = $this->input->post('store_id');
		$data = array('store_id' => $store_id);
		$res = $this->read->get('modem', $data);
		//print_r($res);
		if(count($res)==0)
		{
			$this->update->tableUpdate('modem', $data, $modem_id);
			date_default_timezone_set('Asia/Jakarta');
			$dt = new DateTime();
			$datetime = $dt->format('Y-m-d H:i:s');
			$data = array_merge($data, array('modem_id' => $modem_id, 'use' => $datetime, 'last_used' => '0000-00-00 00:00:00'));
			$this->insert->addIntoTable('modem_history', $data);
			$this->message('The modem id ' . $modem_id . ' has been installed');
		}
		else
		{
			$this->message('The rack already installed by another modem');
		}
		//print_r($this->session->flashdata('message'));
		redirect('store/modem/'. $store_id);
	}
	
	function device_add($store_id, $devices_id, $shelf_id)
	{
		
		// cek available space untuk rack pada store id tertentu, jika available update
		$this->load->model('read');
		$this->load->model('update');
		$this->load->model('insert');
		//$devices_id = $this->input->post('id');
		//$store_id = $this->input->post('store_id');
		
		$data = array('store_id' => $store_id, 'shelf_id' => $shelf_id);
		$res = $this->read->get('devices', $data);
		
		//print_r($res);
		if(count($res)==0)
		{
			$this->update->tableUpdate('devices', $data, $devices_id);
			date_default_timezone_set('Asia/Jakarta');
			$dt = new DateTime();
			$datetime = $dt->format('Y-m-d H:i:s');
			$data = array_merge($data, array('device_id' => $devices_id, 'use' => $datetime, 'last_used' => '0000-00-00 00:00:00'));
			$this->insert->addIntoTable('device_history', $data);
			$this->message('The device id ' . $devices_id . ' has been installed');
		}
		else
		{
			$dev = $this->read->devices($devices_id);
			$shelf = $this->read->shelf($dev->shelf_id);
			$this->message('The rack already installed by another device');
		}
		redirect('store/device/'. $store_id);
	}
	
	function pv_position($store_id, $sortBy = null)
	{
		$data['page'] = "store/pv_position";
		$this->load->model('read');
		if(!isset($sortBy))
			$store_marker = $this->read->pv_position($store_id, "store");
		else
		{
			if($sortBy=="shelf") $sortBy = "shelf_id";
			if($sortBy=="variant") $sortBy = "variant_id";
			$store_marker = $this->read->pv_position($store_id, "store", $sortBy);
		}
		$id = $this->utilities->filterTableToArray($store_marker, "id");
		$variant_id = $this->utilities->filterTableToArray($store_marker, "variant_id");
		$shelf_id = $this->utilities->filterTableToArray($store_marker, "shelf_id");
		$data['brand'] = $this->read->brand();
		$data['product'] = $this->read->product();
		$data['variant'] = $this->read->variant();
		$data['shelf'] = $this->read->shelf();
		$arr[] = array();

		for($i=0;$i<count($id);$i++)
		{
			$id_ = $this->read->pv_position($id[$i]);
			$variant = $this->read->variant($variant_id[$i]);
			$shelf = $this->read->shelf($shelf_id[$i]);
			$arr[$i] = new stdClass();
			$arr[$i]->id = $id_->id;
			$arr[$i]->variant_name = $variant->name;
			$product = $this->read->product($variant->product_id);
			$arr[$i]->product_id = $product->id;
			$arr[$i]->product_name = $product->name;
			$brand = $this->read->brand($product->brand_id);
			$arr[$i]->brand_name = $brand->name;
			$arr[$i]->shelf_name = $shelf->name;
		}
		
		if (count($id) != 0){
			$data['store_marker'] = $arr;
		}
		//$this->_print_data($data);die();
		$this->load->view('home', $data);
	}
	
	function pv_add()
	{
		$this->load->model('read');
		$this->load->model('insert');
		$store_id = $this->input->post('store_id');
		$variant_id = $this->input->post('variant_id');
		$shelf_id = $this->input->post('shelf_id');
		//print_r($this->input->post());
		// cek marker yang sama
		$data = array('store_id' => $store_id, 'variant_id' => $variant_id, 'shelf_id' => $shelf_id);
		$res = $this->read->pv_position($data);
		//print_r($data);
		//print_r($res);
		
		$v = $this->read->variant($variant_id);
		$s = $this->read->store($store_id);
		$r = $this->read->shelf($shelf_id);
		if(count($res)>0)
		{
			$this->session->set_flashdata('message', 'Variant ' . $v->name . ' already added at Store ' . $s->store_name . ' [' . $s->store_id . '] and Shelf ' . $r->name);
		}
		else
		{
			// cek jumlah marker pada rak yang sama dan store bersangkutan
			$data = array('store_id' => $store_id, 'shelf_id' => $shelf_id);
			$res = $this->read->pv_position($data);
			$max_target_per_device = $this->read->config('max_target_per_device')->value;
			if(count($res)<$max_target_per_device)
			{
				$data = array('store_id' => $store_id, 'variant_id' => $variant_id, 'shelf_id' => $shelf_id);
				$this->insert->addIntoTable('pv_position', $data);
				$this->session->set_flashdata('message', 'Variant ' . $v->name . ' has just been added at Store ' . $s->store_name . ' [' . $s->store_id . '] and Shelf ' . $r->name . ' successfully');
			}
			else
			{
				$this->session->set_flashdata('message', 'Shelf ' . $r->name . ' at Store ' . $s->store_name . ' [' . $s->store_id . '] already reached maximum slot');
			}
		}
		redirect('store/pv_position/' . $store_id);
		
	}
	
	function pv_delete($store_id, $pv_id)
	{
		$this->load->model('delete');
		$this->delete->deleteRecord('pv_position', $pv_id);
		$this->session->set_flashdata('message', 'Successfully deleted');
		redirect('store/pv_position/' . $store_id);
	}
	
	function geolocation()
	{
		$this->load->model('read');
		$data['geo'] = $this->read->geolocation($this->uri->segment(3));
		$data['page'] = "location/geolocation";
		//$this->_print_data($data);die();
		$this->load->view('home', $data);
	}
	
	
	function geolocation_submit()
	{
		$data = $this->_get_post_data();
		$table = 'geolocation';
		//$this->_print_data($data);die();
		$this->load->model('insert');
		$this->insert->addIntoTable($table, $data);
		redirect('location');
	}
	
	function geolocation_update()
	{
		$this->load->model('update');
		$this->load->model('read');
		$data = $this->_get_post_data();
		//$this->_print_data($data);die();
		//$read = $this->read->geolocation_detail($store_id);
		$this->update->geolocationUpdate('store', $data, $data['store_id']);
		
		redirect('store/geolocation/'.$data['store_id']);
	}
	
	

}