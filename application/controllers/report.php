<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report extends MY_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('read');
		$this->load->model('update');
		$this->load->model('delete');
		$this->load->library('pagination');
		$this->session_data = $this->session->userdata('is_logged_in');
	}

	function product($variant_id)
	{
		if($this->session->userdata('is_logged_in'))
		{
			$data['page'] = "report_product";
			//$this->_print_data($data);die();
			if ($this->session_data['status']=="1"){
				$data['companyName'] = $this->read->getCompanyName($this->session_data['username']);
				$data['getProduct'] = $this->read->getProduct($data['companyName']);
				$data['totalInteraction']=$this->read->totalInteraction($variant_id);
				$data['daily_average'] = $this->read->daily_average($variant_id);
				$data['province'] = $this->read->getProvince();
				//$this->_print_data($data);die();
				$this->load->view('client/index', $data);
			} else {
				$this->load->view('home', $data);
			}
		} else {
			redirect('login', 'refresh');
		}
	}
	
	function top_ten(){
		//$this->_print_data($data);die();
		if($this->session->userdata('is_logged_in'))
		{
			$data['page'] = "product_report";
			//$this->_print_data($data);die();
			if ($this->session_data['status']=="1"){
				$data['companyName'] = $this->read->getCompanyName($this->session_data['username']);
				$data['getProduct'] = $this->read->getProduct($data['companyName']);
				$data['province'] = $this->read->getProvince();
				//$this->_print_data($data);die();
				$this->load->view('client/index', $data);
			} else {
				$this->load->view('home', $data);
			}
		} else {
			redirect('login', 'refresh');
		}
	}
	
	function getCity(){
		if($this->session->userdata('is_logged_in'))
		{
			$data_post = $this->_get_post_data();
			//echo $data_post['sub_industries_id'];
			$data['city'] = $this->read->getCity($data_post['provinceId']);
			//$this->_print_data($data);
				
			echo '<select id="city" name="city">';
			foreach ($data['city'] as $key) {
				echo "<option value=".$key->id.">".$key->nama."</option>";
			}
			echo '</select>';
				
			//$this->_print_data($data_post);die();
		} else {
			redirect('login', 'refresh');
		}
	}
	
	function getTotalReco_date(){
		if($this->session->userdata('is_logged_in'))
		{
			$data_post = $this->_get_post_data();
				
			$target_id = $this->read->getTarget_id($data_post['variant_id']);
			$data['result'] = $this->read->getTotalRecoByCityByDate($data_post['cityId'], $data_post['startDate'], $data_post['endDate'], $target_id, $data_post['variant_id']);
			//$this->_print_data($data);die();
			echo '<table width="100%" class="most-brand-table">';
			echo "<tbody>";
			if ($data['result'] != "") {
				foreach ($data as $val) {
					echo "<tr>";
					echo "<td style='vertical-align: middle; padding-left: 10px; height: 50px;'> ".$val['product_name'].", ".$val['variant_name']." </td>";
					echo "<td style='vertical-align: middle; text-align: center;'> <font color='#0A293B' style='font-size: 24px;'>".$val['total_reco']."</font></td>";
					echo "<td style='vertical-align: middle; text-align: left;'> &nbsp; TOTAL INTERACTIONS </td>";
					echo "<td style='vertical-align: middle; text-align: center;'> <a href='".base_url()."report/top_ten/".$val['variant_id']."'>View Detail</a></td>";
					echo "</tr>";
				}
			} else {
				echo "<tr>";
				echo "<td colspan='3' style='vertical-align: middle; text-align: center;'> No Data</td>";
				echo "</tr>";
			}
			echo '</tbody>';
			echo '</table>';
	
			//$this->_print_data($data_post);die();
		} else {
			redirect('login', 'refresh');
		}
	}
	
	function getTotalReco(){
		if($this->session->userdata('is_logged_in'))
		{
			$data_post = $this->_get_post_data();
			
			$target_id = $this->read->getTarget_id($data_post['variant_id']);
			$data['result'] = $this->read->getTotalRecoByCity($data_post['cityId'], $target_id, $data_post['variant_id']);
			//$this->_print_data($data);die();
			echo '<table width="100%" class="most-brand-table">';
			echo "<tbody>";
			if ($data['result'] != "") {
				foreach ($data as $val) {
					echo "<tr>";
					echo "<td style='vertical-align: middle; padding-left: 10px; height: 50px;'> ".$val['product_name'].", ".$val['variant_name']." </td>";
					echo "<td style='vertical-align: middle; text-align: center;'> <font color='#0A293B' style='font-size: 24px;'>".$val['total_reco']."</font></td>";
					echo "<td style='vertical-align: middle; text-align: left;'> &nbsp; TOTAL INTERACTIONS </td>";
					echo "<td style='vertical-align: middle; text-align: center;'> <a href='".base_url()."report/top_ten/".$val['variant_id']."'>View Detail</a></td>";
					echo "</tr>";
				}
			} else {
				echo "<tr>";
				echo "<td colspan='3' style='vertical-align: middle; text-align: center;'> No Data</td>";
				echo "</tr>";
			}
			echo '</tbody>';
			echo '</table>';
		
			//$this->_print_data($data_post);die();
		} else {
			redirect('login', 'refresh');
		}
	}
	
	
}