<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reporting extends MY_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('read');
		$this->load->model('update');
		$this->load->model('delete');
		$this->load->library('pagination');
		$this->session_data = $this->session->userdata('is_logged_in');
	}

	public function index()
	{
		if($this->session->userdata('is_logged_in'))
		{
			$data['page'] = "reporting";
			$data['reporting'] = $this->read->getReporting($this->session_data['username'], $this->session_data['brand_id']);
			//$this->_print_data($data);
			$this->load->view('home', $data);
		}
		else
		{
			redirect('login', 'refresh');
		}
	}

	public function detail($id) {
		if($this->session->userdata('is_logged_in'))
		{
			$data['page'] = "reporting_detail";
			
			//$this->_print_data($data);
			$this->load->view('home', $data);
		}
		else
		{
			redirect('login', 'refresh');
		}
	}
}