<?php
class content extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('read');
		$this->load->model('update');
		$this->load->model('delete');
		$this->load->library('pagination');
		$this->session_data = $this->session->userdata('is_logged_in');
	}

	function index()
	{
		if($this->session->userdata('is_logged_in'))
		{
			$this->load->model('read');
			$data['page'] = "content";
			$content = $this->read->content();
			for($i=0;$i<count($content);$i++)
			{
				$tmp = new stdClass();
				$tmp = $content[$i];
				$tmp->total_contents = count($this->read->content_detail(array('content_id' => $content[$i]->id)));
				$content[$i] = $tmp;
			}
			$data['content'] = $content;
			//$this->_print_data($data);die();
			$this->load->view('home', $data);
		} else {
			redirect('auth', 'refresh');
		}
	}

	function detail($id)
	{
		if($this->session->userdata('is_logged_in'))
		{
			$this->load->model('read');
			$data['page'] = "content/detail";
			$content_detail = $this->read->content_detail(array('content_id' => $id));
			for($i=0;$i<count($content_detail);$i++)
			{
				$tmp = new stdClass();
				$tmp = $content_detail[$i];
				$tmp->content_name = $this->read->content($content_detail[$i]->content_id)->name;
				$size = filesize('./contents/'.$tmp->content_name.'/'.$tmp->name);
				$tmp->size = sprintf("%.2f", ($size/1024));
				$tmp->md5 = md5_file('./contents/'.$tmp->content_name.'/'.$tmp->name);
				$content_detail[$i] = $tmp;
			}
			$data['content_detail'] = $content_detail;
			$this->load->view('home', $data);
		} else {
			redirect('auth', 'refresh');
		}
	}


	function delete($id)
	{
		if($this->session->userdata('is_logged_in'))
		{
			$this->load->model('read');
			$name = $this->read->getContentNameById($id);
			$this->load->model('delete');
			$this->delete->deleteRecord('content', $id);
			unlink('./contents/'.$name.'.unity3d');
			redirect('content');
		} else {
			redirect('auth', 'refresh');
		}
	}

	function add()
	{
		if($this->session->userdata('is_logged_in'))
		{
			$data = $this->input->post();
			$this->load->model('read');
			if(count($this->read->content($data))>0)
			{
				$this->session->set_flashdata('message', 'The content ' . $data['name'] . ' already exist');
			}
			else
			{
				if(!file_exists('./contents/'. $data['name']))
				{
					mkdir('./contents/'. $data['name'], 0777, false);
				}
				else
				{
					$files = scandir('./contents/'. $data['name']);
					foreach($files as $f)
					{
						unlink('./contents/'.$f);
					}
				}
				$this->load->model('insert');
				$data['px'] = 0;$data['py'] = 0;$data['pz'] = 0;
				$data['rx'] = 0;$data['ry'] = 0;$data['rz'] = 0;
				$data['sx'] = 1;$data['sy'] = 1;$data['sz'] = 1;
				$this->insert->addIntoTable('content', $data);
				$this->session->set_flashdata('message', 'The content ' . $data['name'] . ' successfully created');
			}
			redirect('content');
		} else {
			redirect('auth', 'refresh');
		}
	}
	
	function edit($id)
	{
		if($this->session->userdata('is_logged_in'))
		{
			$this->load->model('read');
			$data['page'] = "content/edit";
			$data['id'] = $id;
			$data['content'] = $this->read->content($id);
			//print_r($data['devices_detail']);die();
			$this->load->view('home', $data);
		} else {
			redirect('auth', 'refresh');
		}
	}
	
	function update()
	{
		if($this->session->userdata('is_logged_in'))
		{
			$data = $this->_get_post_data();
			$this->load->model('read');
			$ctn = $this->read->get('content', $data['id']);
			$old = $ctn->name;
			$this->load->model('update');
			//print_r($data);
			$this->update->tableUpdate('content', $data, $data['id']);
			rename('./contents/'. $old, './contents/'.$data['name']);
			redirect('content');
			
		} else {
			redirect('auth', 'refresh');
		}
	}

	function upload($id)
	{
		if($this->session->userdata('is_logged_in'))
		{
			$error = null;
			$contentName = $this->input->post('name');
			$this->load->model('read');
			$c = $this->read->content($id);
			$config['upload_path'] = './contents/' .  $c->name .'/';
			$config['allowed_types'] = '*';
			$config['file_name'] = $contentName;
			$config['max_size'] = 20480;

			//$dimension = $this->input->post('dimension');
			//redirect('http://google.com');

			$this->load->library('upload', $config);
			if($this->read->checkContentByName($id, $contentName))
			{
				if ( !$this->upload->do_upload())
				{
					$error = array('error' => $this->upload->display_errors());
					$this->session->set_flashdata('upload_error', $error);
					redirect('content/detail/' . $id);
					//$this->load->view('upload_form', $error);
				}
				else
				{
					$data = array('upload_data' => $this->upload->data());
					//$filename = $data['upload_data']['file_name'];
					//echo $targetName;
					//echo $dimension;
					//redirect("http://planet-ar.com/vws/PostNewTarget.php?filename=".$filename."&targetName=".$targetName."&dimension=".$dimension);
					//$error[0] = $data['upload_data']['file_name'];

					$this->load->model('insert');
					$table = 'content_detail';
					$ins = array('content_id' => $id, 'name' => $data['upload_data']['raw_name'].'.'.substr($data['upload_data']['file_ext'], 1));

					$this->insert->addIntoTable($table, $ins);

					$this->session->set_flashdata('upload', $data);
					redirect('content/detail/' . $id);
					//$this->load->view('upload_success', $data);
				}
			}
			else
			{
				$error[0] = "Content Name Already Taken";
				$this->session->set_flashdata('upload_error', $error);
				redirect('content');
			}
		} else {
			redirect('auth', 'refresh');
		}
	}
}