<?php


class apk extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('pagination');
    }

    function index()
    {
        $this->load->model('read');
        $data['page'] = "apk";
		$apk = $this->read->mobile_version();
		$nApk = array();
		for($i=0;$i<count($apk);$i++)
		{
			$nApk[$i] = new stdClass();
			$nApk[$i] = $apk[$i];
			
			$size = filesize('./mobile_apk/DAV_'.$apk[$i]->version.'.apk');
			$nApk[$i]->size = sprintf("%.2f", ($size/1024/1024));
			$nApk[$i]->md5 = md5_file('./mobile_apk/DAV_'.$apk[$i]->version.'.apk');
		}
		$data['apk'] = $nApk;
        $this->load->view('home', $data);
    }
	
	function add()
	{
		$modul = 'DAV';
		//$data = $this->input->post();
		$this->load->model('read');
		
		$lv = $this->read->getLastVersion($modul);
		$last_ver = $lv->version;
		$last_number_ver = $lv->number_ver;
		$ver = explode(".", $last_ver);
		$version = $ver[0].$ver[1].$ver[2];
		$version++;
		$last_ver = substr($version, 0, 1).'.'.substr($version, 1, 1).'.'.substr($version, 2, 1);
		
		
		$error = null;
        $config['upload_path'] = './mobile_apk/';
        $config['allowed_types'] = '*';
        $config['file_name'] = 'DAV_'.$last_ver.'.apk';
        $config['max_size'] = 102400;

		//blm selesai
        $this->load->library('upload', $config);
        if ( !$this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());
			$this->session->set_flashdata('upload_error', $error);
			redirect('apk');
			//$this->load->view('upload_form', $error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			$this->load->model('insert');
			$table = 'mobile_version';
			$ins = array('modul' => $modul, 'version' => $last_ver, 'number_ver' => ($last_number_ver+1));
			$this->insert->addIntoTable($table, $ins);
			$this->session->set_flashdata('upload', $data);
			redirect('apk');
			//$this->load->view('upload_success', $data);
		}
	}
}