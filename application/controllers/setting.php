<?php
class Setting extends CI_Controller {

	function __construct(){
		parent::__construct();
	}

	function index() {
		$this->load->view('setting');
	}

	function tambah_kota(){
		$this->load->model('read');
		$data['kioskList'] = $this->read->readKioskList();
		$data['MerchantList'] = $this->read->readMerchantList();
		$data['page'] = "tambah_kota";
		$this->load->view('setting', $data);
	}

	function addKiosk(){
		$this->load->model('insert');
		$data['merchant_id'] = $this->input->post('merchant_id');
		$data['nama_kiosk'] = $this->input->post('nama_kiosk');
		$data['kota'] = $this->input->post('region');
		//print_r($data);die();
		$this->insert->addKiosk($data);
		redirect('setting/tambah_kota');
	}
	
	function merchant(){
		$this->load->model('read');
		$data['merchant'] = $this->read->readMerchantList();
		$data['page'] = "merchant";
		$this->load->view('setting', $data);
	}
	
	function addMerchant(){
		$this->load->model('insert');
		$data['merchant_name'] = $this->input->post('merchant_name');
		//print_r($data);die();
		$this->insert->addMerchant($data);
		redirect('setting/merchant');
	}
	
	function userManagement(){
		$this->load->model('read');
		$data['user'] = $this->read->readUserList();
		$data['MerchantList'] = $this->read->readMerchantList();
		$data['page'] = "user";
		$this->load->view('setting', $data);
	}
	
	function addUser(){
		$this->load->model('insert');
		$data['username'] = $this->input->post('username');
		$data['merchant_id'] = $this->input->post('merchant_id');
		$data['password'] = $this->input->post('password');
		$repassword = $this->input->post('repassword');
		if ($data['password'] == $repassword ) {
			$this->insert->addUser($data);
		} else {
			echo '<script language="javascript">';
			echo 'alert("message successfully sent")';
			echo '</script>';
			//
		}
		redirect('setting/userManagement');
	}
}
?>