<?php


class Modem extends MY_Controller {
	var $module = 'modem';

	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->authCheck();
		$this->statusCheck(2);
	}

	function index()
	{
		$this->load->model('read');
		$data['page'] = $this->module;
		$modems = $this->db->get('modem')->result();
		$data['brand_devices'] = $this->read->brand_devices();
		for($i=0;$i<count($modems);$i++)
		{
			$modems[$i]->brand_name = $this->read->brand_devices($modems[$i]->brand_devices_id)->brand_name;
			if($modems[$i]->store_id!=0)
				$modems[$i]->store_id = $this->db->get_where('store', array('id' => $modems[$i]->store_id))->first_row()->store_id;
			else
				$modems[$i]->store_id = 'Unassign';
		}
		$data[$this->module] = $modems;
		//$this->_print_data($data);die();
		$this->load->view('home', $data);
	}
	
	function add()
	{
		$this->load->model('insert');
		$data = $this->input->post();
		$this->insert->addIntoTable($this->module, $data);
		//print_r($data);die();
		redirect($this->module);
	}
	
	function edit($id)
	{
		$this->load->model('read');
		$data['page'] = $this->module."/edit";
		$data['id'] = $id;
		$data['brand_devices'] = $this->read->brand_devices();
		$data['os_devices'] = $this->read->os_devices();
		$modem = $this->read->get($this->module, $id);
		$modem->brand_name = $this->read->brand_devices($modem->brand_devices_id)->brand_name;
		//$devices->os_name = $this->read->os_devices($devices->os_devices_id)->os_name;
		$data[$this->module] = $modem;
		//print_r($data['devices']);die();
		$this->load->view('home', $data);
	}
	
	function update()
	{
		$this->load->model('update');
		$data = $this->_get_post_data();
		$this->update->tableUpdate($this->module, $data, $data['id']);
		redirect($this->module);
	}
	
	function delete($id)
	{
		$this->load->model('delete');
		$this->delete->deleteRecord($this->module, $id);
		redirect($this->module);
	}

}