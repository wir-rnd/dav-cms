<?php

class devicestatus extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->model('read');
		$this->load->model('update');
		$this->load->model('delete');
		$this->session_data = $this->session->userdata('is_logged_in');
	}

	function index()
	{
		if($this->session->userdata('is_logged_in'))
		{
			$data['page'] = "devicestatus";
			$this->load->model('read');
			$ds = $this->read->devicestatus();
			$newDs = array();
			$j=0;
			for($i=0;$i<count($ds);$i++)
			{
				$newDs[$j] = new stdClass();
				$newDs[$j]->serial_number = $ds[$i]->serial_number;
				$devices = $this->read->devices(array('serial_number' => $ds[$i]->serial_number));
				$newDs[$j]->brand_name = $this->read->brand_devices($devices[0]->brand_devices_id)->brand_name;
				$newDs[$j]->store_id = $this->read->store($devices[0]->store_id)->store_id;
				$newDs[$j]->shelf_name = $this->read->shelf($devices[0]->shelf_id)->name;
				$newDs[$j]->datetime = $ds[$i]->datetime;
				$j++;
			}
			$data['devicestatus'] = $newDs;
			//$this->_print_data($data);die();
			$this->load->view('home', $data);
		} else {
			redirect('login', 'refresh');
		}
	}
}
