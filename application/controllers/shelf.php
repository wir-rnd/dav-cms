<?php


class shelf extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}

	function index()
	{
		if($this->session->userdata('is_logged_in'))
		{
			$data['datatables'] = true;
			$data['sweetalert'] = true;
			$data['noty'] = true;
			$this->load->model('read');
			$data['page'] = "shelf";
			$data['shelf'] = $this->read->shelf();
			//$this->_print_data($data);die();
			$this->load->view('home', $data);
		} else {
			redirect('login', 'refresh');
		}
	}

	function add()
	{
		if($this->session->userdata('is_logged_in'))
		{
			$this->load->model('insert');
			$table = 'shelf';
			$data = $this->_get_post_data();
			$this->insert->addIntoTable($table, $data);
			//print_r($data);die();
			redirect('shelf');
		} else {
			redirect('login', 'refresh');
		}
	}

	function edit($id)
	{
		if($this->session->userdata('is_logged_in'))
		{
			$this->load->model('read');
			$data['page'] = "shelf/edit";
			$data['id'] = $id;
			$data['shelf'] = $this->read->shelf($id);
			//print_r($data['devices_detail']);die();
			$this->load->view('home', $data);
		} else {
			redirect('login', 'refresh');
		}
	}

	function update()
	{
		if($this->session->userdata('is_logged_in'))
		{
			$this->load->model('update');
			$data = $this->_get_post_data();
			//print_r($data);
			$this->update->tableUpdate('shelf', $data, $data['id']);
			redirect('shelf');
			//$this->_print_data($data);die();
			//$this->load->view('home', $data);
		} else {
			redirect('login', 'refresh');
		}
	}

	function delete($id)
	{
		if($this->session->userdata('is_logged_in'))
		{
			$this->load->model('delete');
			$this->load->model('read');
			$data = $this->read->devices(array('shelf_id' => $id));
			if(count($data)==0)
			{
				$this->session->set_flashdata('message', 'Shelf successfully deleted');
				$this->delete->deleteRecord('shelf', $id);
			}
			else
			{
				$this->session->set_flashdata('message', 'Shelf delete failed');
			}
			redirect('shelf');
		} else {
			redirect('login', 'refresh');
		}
	}

}