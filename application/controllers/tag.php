<?php


class Tag extends MY_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('pagination');
	}
	
	function index()
	{
		$data['datatables'] = true;
		$data['sweetalert'] = true;
		$data['noty'] = true;
		$this->load->model('read');
		$data['page'] = "tag";
		$data['tag'] = $this->read->get('tag');
		$this->load->view('home', $data);
	}
	
	function add(){
		$this->load->model('insert');
		$data = $this->_get_post_data();
		$this->insert->addIntoTable('tag', $data);
		redirect ('tag');	
	}
	
	function delete($id){
		$this->load->model('delete');
		$this->delete->deleteRecord('tag', $id);
		redirect('tag');
	}
	
	function edit($id)
	{
		if($this->session->userdata('is_logged_in'))
		{
			$this->load->model('read');
			$data['page'] = "tag/edit";
			$data['id'] = $id;
			$data['tag'] = $this->read->get('tag', $id);
			//print_r($data['devices_detail']);die();
			$this->load->view('home', $data);
		} else {
			redirect('auth', 'refresh');
		}
	}
	
	function update()
	{
		if($this->session->userdata('is_logged_in'))
		{
			$this->load->model('update');
			$data = $this->_get_post_data();
			//print_r($data);
			$this->update->tableUpdate('tag', $data, $data['id']);
			redirect('tag');
			//$this->_print_data($data);die();
			//$this->load->view('home', $data);
		} else {
			redirect('auth', 'refresh');
		}
	}
		
}