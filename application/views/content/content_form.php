<div class="modal-body">
	<?php if($page=="content/edit") echo form_open('content/update'); else echo form_open('content/add'); ?>
		<?php if($page=="content/edit") { ?><input type="hidden" name="id" value="<?php echo $content->id; ?>" /><?php } ?>
		<div class="form-group">
			<label for="targetName">Content Name</label>
			<input type="text" class="form-control" id="name" name="name" placeholder="" <?php if($page=="content/edit") {  ?> value="<?php echo $content->name; ?>" <?php  } ?> />
		</div>
		<br /><br />
		<input class="btn btn-primary" id="upload" type="submit" <?php if($page=="content/edit") {  ?> value="Change" <?php  } else { ?> value="Add" <?php } ?> />
		<input class="btn btn-primary" data-dismiss="modal" id="cancel" type="reset" value="Cancel" />
	<?php echo form_close();?>
</div>