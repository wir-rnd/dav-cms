<div class="col-md-12">				
	<div>
	<a title="Add Widget" data-toggle="modal" href="#addWidgetModal" class="btn btn-primary ">
	Add Content</a>
	</div>
	<br />
	<div>
		<?php 
			$error = $this->session->flashdata('upload_error');
			
			if($error!='')
			{
				echo 'Error<br /><ul>';
				foreach($error as $er)
				{
					echo '<li>'.$er. '</li>';
				}
				echo '</ul>';
			}
		?>
	</div>
	<div class="panel panel-default">
            
		<table class="table table-striped paginated" border="0">
			<thead>
				<tr>
					<th style="text-align: center;">No</th>
					<th>File Name</th>
					<th>Size</th>
					<th>MD5 Checksum</th>
				</tr>
			</thead>
			
				<?php $no=1;?>
				<?php if(count($content_detail) > 0) { foreach($content_detail as $val) {?>
				<tr>
					<td style="text-align: center;"> <?php echo $no;?></td>
					<td> <?php echo $val->name;?></td>
					<td> <?php echo $val->size;?> KB</td>
					<td> <?php echo $val->md5;?></td>
					
					<td style="vertical-align: middle; text-align: center;">
						<div id = "test">
						<p>
							<a class="edit" href="<?php echo $this->config->item('base_url')."/content/detail/".$val->id;?>">
								<button class="btn btn-primary btn-xs edit" data-title="Edit" rel="tooltip">
									<span class="glyphicon glyphicon-pencil"></span>
								</button>
							</a>
							<a class="edit" href="<?php echo $this->config->item('base_url')."/content/delete/".$val->id;?>" onclick="javascript: return confirm('Are you SURE you want to delete this item?')">
								<button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" data-placement="top" rel="tooltip">
									<span class="glyphicon glyphicon-trash"></span>
								</button>
							</a>
						</p>
						</div>
					</td>
				</tr>
					<?php $no++;?>
				<?php }
                                }?>
			
			<tbody>
				
			</tbody>
		</table>
	</div>
	<?php echo $this->pagination->create_links(); ?>

</div>

<div class="modal fade" id="addWidgetModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <a href="#" class="pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle pull-right"></span></a>
                        <h4 class="modal-title">Add Content</h4>
                    </div>
                    <div class="modal-body">
                        <?php echo form_open_multipart('content/upload/'. $this->uri->segment(3));?>
                            <div class="form-group">
                                <label for="targetName">File Name</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="">
                            </div>
                            <div style="margin-bottom: 10px;">
                                    <label for="userfile">File</label>
                                    <input style="margin-bottom: 5px;" type="file" name="userfile" id="userfile" size="20" />
                            </div>
                            <br /><br />
                            <input class="btn btn-primary" id="upload" type="submit" value="Upload" />
                            <input class="btn btn-primary" data-dismiss="modal" id="cancel" type="reset" value="Cancel" />
                        <?php echo form_close();?>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dalog -->
    </div>
<!--/col-span-6-->