<?php $this->load->view('generaljs'); ?>
<div class="modal-header">
	<h4 class="modal-title">EDIT STORE</h4>
</div>
<?php if (isset($store)) { ?>
	<div class="modal-body">
		<?php echo form_open('store/update');?>
			<?php include 'store_form.php'; ?>
			<input type="hidden" class="form-control" name="id" id="id" value="<?php echo $id; ?>">
			<br />
			<input class="btn btn-primary" id="upload" type="submit" value="Submit" />
			<a href="<?php echo $this->config->item('base_url')."/store";?>"><input class="btn btn-primary" id="cancel" type="button" value="Cancel" /></a>
		<?php echo form_close();?>
	</div>
<?php } ?>