<div class="row">
	<?php include 'nav.php'?>
</div>
<div class="row">
	<?php include 'store_list.php'?>
</div>
<div class="modal fade" id="addWidgetModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<a href="#" class="pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle pull-right"></span></a>
				<h4 class="modal-title">Create Store</h4>
			</div>
			
			<div class="modal-body">
				<?php echo form_open('store/add');?>
					<div class="form-group">
						<label for="alfamart">Store ID</label>
						<input type="text" id="store_id" name="store_id" style="width: 70px;" size="3" class="form-control" required />
					</div>
					<div class="form-group">
						<label for="alfamart">Store name</label>
						<input type="text" id="store_name" name="store_name" style="width: 400px;" class="form-control" required />
					</div>
					
					<?php include 'store_form.php'; ?>
					
					<br />
					<input class="btn btn-primary" id="upload" type="submit" value="Upload" />
					<input class="btn btn-primary" data-dismiss="modal" id="cancel" type="reset" value="Cancel" />
				<?php echo form_close();?>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dalog -->
</div>
<!-- /.modal -->


