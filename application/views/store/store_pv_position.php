<script type="text/javascript">
	$(document).ready(function() {
		//alert('asdasd');
		$('#brand').trigger('onchange');
	});
	function getProductByBrandId(id)
	{
		$.ajax({
			type: "POST",
			url: "<?php echo base_url().'product/ajax_get_product_by_brand_id/'; ?>" + id,
			success:function(result)
			{
				//alert(result[0].name);
				$('#product').html('');
				var html = '';
				for(var i=0;i<result.length;i++)
				{
					if(i==0) getVariantByProductId(result[i].id)
					html += '<option value="'+ result[i].id +'">'+ result[i].name +'</option>';
				}
				$('#product').html(html);
			}
		});
	}
	
	function getVariantByProductId(id)
	{
		$.ajax({
			type: "POST",
			url: "<?php echo base_url().'product/ajax_get_variant_by_product_id/'; ?>" + id,
			success:function(result)
			{
				var html = '';
				$('#variant').html('');
				for(var i=0;i<result.length;i++)
				{
					html += '<option value="'+ result[i].id +'">'+ result[i].name +'</option>';
				}
				$('#variant').html(html);
			}
		});
	}
</script>
<?php $this->load->view('generaljs'); ?>
<div class="row">
	<?php include 'nav.php'?>
</div>

<div class="row">
    <div class="col-md-12">
		<table class="table table-striped table-bordered" border="0">
			<thead>
				<tr>
					<!-- <th>Target ID</th>  -->
					<th>No.</th>
					<th><a href="<?php echo base_url().'store/marker/' . $this->uri->segment(3). '/brand'; ?>">Brand</a></th>
					<th><a href="<?php echo base_url().'store/marker/' . $this->uri->segment(3). '/brand'; ?>">Product</a></th>
					<th><a href="<?php echo base_url().'store/marker/' . $this->uri->segment(3). '/brand'; ?>">Variant</a></th>
					<th><a href="<?php echo base_url().'store/marker/' . $this->uri->segment(3). '/rack'; ?>">Shelf</a></th>
					<th style="text-align: center;">Action</th>
				</tr>
			</thead>
			<tbody>
			<?php $i = 1;?>
		   
			<?php if (isset($store_marker)) { ?>
				<?php foreach($store_marker as $val) {?>
					<tr>
						<td><?php echo $i; ?></td>
						<td><?php echo $val->brand_name; ?></td>
						<td><a href="<?php echo base_url().'product/variant/' . $val->product_id; ?>"><?php echo $val->product_name; ?></a></td>
						<td><?php echo $val->variant_name; ?></td>
						<td><?php echo $val->shelf_name; ?></td>
						<td style="vertical-align: middle; text-align: center;">
							<p>
								<a style="text-decoration: none;" href="<?php echo base_url().'store/pv_delete/'.$this->uri->segment(3).'/'.$val->id;?>" title="unassign marker">
									<button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" data-placement="top" rel="tooltip">
											<span class="glyphicon glyphicon-trash"></span>
									</button>
								</a>
							</p>
						</td>
					</tr>
					<?php $i++; ?>
				<?php }?>
			<?php } else { ?>
				<tr>
					<td style="padding-left: 20px;"><?php echo "No Marker Added !" ; ?></td><td></td><td></td><td></td><td></td><td></td>
				</tr>
			<?php }?>
			</tbody>
		</table>
		<?php echo $this->pagination->create_links(); ?>
	</div>
</div>

<div class="modal fade" id="addWidgetModal">
    <div class="modal-dialog">
            <div class="modal-content">
                    <div class="modal-header">
                            <a href="#" class="pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle pull-right"></span></a>
                            <h4 class="modal-title">Assign Product Variant</h4>
                    </div>
                    <div class="modal-body">
						<?php echo form_open('store/pv_add');?>
						<input type="hidden" name="store_id" value="<?php echo $this->uri->segment(3); ?>" />
						<div class="form-group">
							<label>Brand</label>
							<br />
							<select class="form-control" id="brand" onchange="getProductByBrandId(this.value);">
							<?php foreach($brand as $val) { ?>
								<option value="<?php echo $val->id; ?>"><?php echo $val->name; ?></option>
							<?php } ?>
							</select>
						</div>
						<div class="form-group">
							<label>Product</label>
							<br />
							<select class="form-control" id="product" onchange="getVariantByProductId(this.value);">
								<option value="">- Choose Product -</option>
							</select>
						</div>
						<div class="form-group">
							<label>Variant</label>
							<br />
							<select class="form-control" id="variant" name="variant_id">
								<option value="">- Choose Variant -</option>
							</select>
						</div>
						<div class="form-group">
							<label>Shelf</label>
							<br />
							<select class="form-control" name="shelf_id">
							<?php foreach($shelf as $val) { ?>
								<option value="<?php echo $val->id; ?>"><?php echo $val->name; ?></option>
							<?php } ?>
							</select>
						</div>
						<br />
						<input class="btn btn-primary" id="upload" type="submit" value="Assign" />
						<?php echo form_close();?>
                    </div>
            </div>
            <!-- /.modal-content -->
    </div>
    <!-- /.modal-dalog -->
</div>
<!-- /.modal -->
