<?php $this->load->view('generaljs'); ?>
<div class="row">
	<?php include 'nav.php'?>
</div>
<div class="col-md-13">
	<hr />
	<button class="btn btn-primary"><?php echo $store->store_id; ?></button>
	<button class="btn btn-primary"><?php echo $store->store_name; ?></button>
	<hr />
	<ul class="nav nav-tabs">
		<li role="presentation" class="active"><a data-toggle="tab" href="#list">LIST</a></li>
		<li role="presentation"><a data-toggle="tab" href="#hist">HISTORY</a></li>
	</ul>
	<div class="tab-content">
		<div id="list" class="tab-pane fade in active">
			<br />
			<table class="table table-striped table-bordered" border="0">
				<thead>
					<tr>
						<th style="text-align: center;">No</th>
						<th style="text-align: left;">Device Brand</th>
						<th style="text-align: left;">Provider</th>
						<th style="text-align: left;">Model</th>
						<th style="text-align: center;">IMEI</th>
						<th style="text-align: center;">Action</th>
					</tr>
				</thead>
				<tbody>
				<?php if (isset($store_modem)) { $no = 1;?>
					<?php foreach($store_modem as $val) {?>
					<tr>
						<td style="text-align: center;"><?php echo $no++;?></td>
						<td style="text-align: left;"><?php echo $val->brand_name;?></td>
						<td style="text-align: left;"><?php echo $val->provider;?></td>
						<td style="text-align: left;"><?php echo $val->model;?></td>
						<td style="text-align: left;"><?php echo $val->IMEI;?></td>
						
						<td style="vertical-align: middle; text-align: center;">
							<div id = "test">
							<p>
								<a class="edit" href="#" onclick="$.fn.delete('<?php echo base_url().'store/delete_modem/'.$val->id.'/'.$store_id; ?>');">
									<button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" data-placement="top" rel="tooltip">
										<span class="glyphicon glyphicon-trash"></span>
									</button>
								</a>
							</p>
							</div>
						</td>
					</tr>
					<?php } ?>
				<?php } else { ?>
				<tr>
					<td style="text-align: center;">NO MODEM ADDED</td><td></td><td></td><td></td><td></td><td></td>
				</tr>
				<?php } ?>
				</tbody>
			</table>
		</div>
		<div id="hist" class="tab-pane fade">
			<br />
			<table class="table table-striped table-bordered" border="0">
				<thead>
					<tr>
						<th style="text-align: center;">No</th>
						<th style="text-align: left;">IMEI</th>
						<th style="text-align: left;">Device Brand</th>
						<th style="text-align: left;">Provider</th>
						<th style="text-align: left;">Model</th>
						<th style="text-align: left;">Use</th>
						<th style="text-align: left;">Last Used</th>
					</tr>
				</thead>
				<tbody>
					<?php if (isset($modem_history)) { $no = 1;?>
					<?php foreach($modem_history as $val) {?>
					<tr>
						<td style="text-align: center;"><?php echo $no++;?></td>
						<td style="text-align: left;"><?php echo $val->IMEI;?></td>
						<td style="text-align: left;"><?php echo $val->brand_name;?></td>
						<td style="text-align: left;"><?php echo $val->provider;?></td>
						<td style="text-align: left;"><?php echo $val->model;?></td>
						<td style="text-align: left;"><?php echo $val->use;?></td>
						<td style="text-align: left;"><?php echo $val->last_used;?></td>
					</tr>
					<?php } } ?>
				</tbody>
			</table>
		</div>
	</div>

</div>
<!--/col-span-6-->

<div class="modal fade" id="addWidgetModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<a href="#" class="pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle pull-right"></span></a>
					<h4 class="modal-title">Assign Modem</h4>
				</div>
				<table class="table table-striped" border="0">
					<thead>
						<tr>
							<th style="text-align: center;">No</th>
							<th style="text-align: center;">Device Brand</th>
							<th style="text-align: center;">Provider</th>
							<th style="text-align: center;">Model</th>
							<th style="text-align: center;">IMEI</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php $uri = $this->uri->segment(3); ?>
						<?php if (isset($modem)) { $no = 1; ?>
						<?php foreach($modem as $val) {?>
						<?php echo form_open('store/device_add'); ?>
							<tr>
								<td style="text-align: center;"><?php echo $no++; ?></td>
								<td style="text-align: center;">
									<input type="hidden" name="id" value="<?php echo $val->id; ?>" />
									<input type="hidden" name="store_id" value="<?php echo $store_id; ?>" />
									<?php echo $val->brand_name;?>
								</td>
								
								<td style="text-align: center; width:100px;"><?php echo $val->provider;?></td>
								<td style="text-align: center; width:100px;"><?php echo $val->model;?></td>
								<td style="text-align: center; width:350px;"><?php echo $val->IMEI; ?></td>

								<td style="text-align: center;">
									<a href="<?php echo base_url().'store/modem_add/' . $store_id . '/' . $val->id; ?>">
									<button class="btn btn-primary btn-xs edit" data-title="Edit" rel="tooltip">
										<span class="glyphicon glyphicon-pencil"></span>
									</button>
									</a>
								</td>
							</tr>
						<?php echo form_close(); ?>
						<?php } } else { ?>
						<tr>
							<td style="text-align: center;"> <?php echo "NO MODEM ADDED";?></td><td></td><td></td><td></td><td></td><td></td>
						</tr>
						<?php }?>
					</tbody>
				</table>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dalog -->
	</div>