<style>
      #map {
		  height: 300px;
		  border: 1px solid #000;
		}
		
	.controls {
        margin-top: 16px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      }

      #pac-input {
        background-color: #fff;
        padding: 0 11px 0 13px;
        width: 300px;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        text-overflow: ellipsis;
      }

      #pac-input:focus {
        border-color: #4d90fe;
        margin-left: -1px;
        padding-left: 14px;  /* Regular padding-left + 1. */
        width: 401px;
      }
      
      #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
      }

      #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }
      
</style>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places"></script>
<script>

window.onload = function() {
    var latlng = new google.maps.LatLng(<?php if(isset($geo)) { echo $geo[0]->latitude; } else { echo "-6.1755"; }?>, <?php if(isset($geo)) { echo $geo[0]->longitude; } else { echo "106.8273"; }?>);
    var map = new google.maps.Map(document.getElementById('map'), {
        center: latlng,
        zoom: 14,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    var markers = [];
    var marker = new google.maps.Marker({
        position: latlng,
        map: map,
        title: 'Set lat/lon values for this property',
        draggable: true
    });

    
 	// Create the search box and link it to the UI element.
    var input = /** @type {HTMLInputElement} */(
        document.getElementById('pac-input'));
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    var searchBox = new google.maps.places.SearchBox(
    	    /** @type {HTMLInputElement} */(input));

    	  // [START region_getplaces]
    	  // Listen for the event fired when the user selects an item from the
    	  // pick list. Retrieve the matching places for that item.
    	  google.maps.event.addListener(searchBox, 'places_changed', function() {
    		  	var places = searchBox.getPlaces();

    		    if (places.length == 0) {
    		      return;
    		    }
    		    for (var i = 0, marker; marker = markers[i]; i++) {
    		      marker.setMap(null);
    		    }	

    		 // For each place, get the icon, place name, and location.
    		    markers = [];
    		    var bounds = new google.maps.LatLngBounds();
    		    for (var i = 0, place; place = places[i]; i++) {
    		      var image = {
    		        url: place.icon,
    		        size: new google.maps.Size(71, 71),
    		        origin: new google.maps.Point(0, 0),
    		        anchor: new google.maps.Point(17, 34),
    		        scaledSize: new google.maps.Size(25, 25)
    		      };

    		      // Create a marker for each place.
    		      var marker = new google.maps.Marker({
    		        map: map,
    		        icon: image,
    		        title: place.name,
    		        position: place.geometry.location
    		      });

    		      markers.push(marker);

    		      bounds.extend(place.geometry.location);
    		    }
    		    map.fitBounds(bounds);
    });    	  

    google.maps.event.addListener(marker, 'dragend', function(a) {
        console.log(a.latLng.lat().toFixed(4));
        $("#latitude").val(a.latLng.lat().toFixed(4));
        $("#longitude").val(a.latLng.lng().toFixed(4));
        var bounds = map.getBounds();
        searchBox.setBounds(bounds);
       // var div = document.createElement('div');
        //div.innerHTML = a.latLng.lat().toFixed(4) + ', ' + a.latLng.lng().toFixed(4);
        //document.getElementsByTagName('body')[0].appendChild(div);
    });
};

</script>
<b>Address : </b><?php if(isset($geo)) { echo $geo[0]->store_address_1.", ".$geo[0]->store_address_2; }?>  <br /><br />
<input id="pac-input" class="controls" type="text" placeholder="Search Alfamart">
<div id="map"></div>
<?php if(isset ($geo)) { ?>
	<?php echo form_open("location/geolocation_update"); ?>
<?php } else { ?>
	<?php echo form_open("location/geolocation_submit"); ?>
<?php }?>
<input type="text"	class="form-control" name="location_id" value="<?php echo $this->uri->segment(3);?>" style="visibility: hidden;">
<div class="form-group">
	<div class="row">
		<div class="col-md-5">
			<label for="latitude">Latitude</label> 
			<input type="text"	class="form-control" name="latitude" id="latitude" value="<?php if(isset($geo)) { echo $geo[0]->latitude; }?>" placeholder="Latitude" required>
		</div>
		
		<div class="col-md-5">
			<label for="longitude">Longitude</label> 
			<input type="text"	class="form-control" name="longitude" id="longitude" value="<?php if(isset($geo)) { echo $geo[0]->longitude; }?>" placeholder="Longitude" required>
		</div>
		
		<div class="col-md-3">
			<br />
			<input class="btn btn-primary" id="upload" type="submit" value="Submit" />
			<a href="<?php echo $this->config->item('base_url')."/location"?>" ><input class="btn btn-primary" type="button" value="Cancel" /></a>
		</div>
	</div>
	
</div>
<?php echo form_close(); ?>
