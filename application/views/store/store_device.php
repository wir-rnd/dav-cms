<?php $this->load->view('generaljs'); ?>
<div class="row">
	<?php include 'nav.php'?>
</div>
<div class="col-md-13">
	<hr />
	<button class="btn btn-primary"><?php echo $store->store_id; ?></button>
	<button class="btn btn-primary"><?php echo $store->store_name; ?></button>
	<hr />
	<ul class="nav nav-tabs">
		<li role="presentation" class="active"><a data-toggle="tab" href="#list">LIST</a></li>
		<li role="presentation"><a data-toggle="tab" href="#hist">HISTORY</a></li>
	</ul>
	<div class="tab-content">
		<div id="list" class="tab-pane fade in active">
			<br />
			<table class="table table-striped table-bordered" border="0">
				<thead>
					<tr>
						<th style="text-align: center;">No</th>
						<th style="text-align: left;">Shelf</th>
						<th style="text-align: left;">Device Brand</th>
						<th style="text-align: left;">Operating System</th>
						<th style="text-align: left;">Serial Number</th>
						<th style="text-align: center;">Action</th>
					</tr>
				</thead>
				<tbody>
				<?php if (isset($store_device)) { $no = 1;?>
				<?php foreach($store_device as $val) {?>
				<tr>
					<td style="text-align: center;"><?php echo $no++;?></td>
					<td style="text-align: left;"><?php echo $val->shelf_name;?></td>
					<td style="text-align: left;"><?php echo $val->brand_name;?></td>
					<td style="text-align: left;"><?php echo $val->os_name;?></td>
					<td style="text-align: left;"><?php echo $val->serial_number;?></td>
					<td style="vertical-align: middle; text-align: center;">
						<div id = "test">
						<p>
							<a class="edit" href="#" onclick="$.fn.delete('<?php echo base_url().'store/delete_device/'.$val->id.'/'. $store_id; ?>');">
								<button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" data-placement="top" rel="tooltip">
									<span class="glyphicon glyphicon-trash"></span>
								</button>
							</a>
						</p>
						</div>
					</td>
				</tr>
					
				<?php } } else { ?>
				<tr>
					<td colspan="7" style="text-align: center;"> <?php echo "NO DEVICE ADDED";?></td>
				</tr>
				<?php }?>
				</tbody>
			</table>
		</div>
		<div id="hist" class="tab-pane fade">
			<br />
			<table class="table table-striped table-bordered" border="0">
				<thead>
					<tr>
						<th style="text-align: center;">No</th>
						<th style="text-align: left;">Device ID</th>
						<th style="text-align: left;">Shelf</th>
						<th style="text-align: left;">Use</th>
						<th style="text-align: left;">Last Used</th>
					</tr>
				</thead>
				<tbody>
					<?php if (isset($device_history)) { $no = 1;?>
					<?php foreach($device_history as $val) {?>
					<tr>
						<td style="text-align: center;"><?php echo $no++;?></td>
						<td style="text-align: left;"><?php echo $val->serial_number;?></td>
						<td style="text-align: left;"><?php echo $val->shelf_name;?></td>
						<td style="text-align: left;"><?php echo $val->use;?></td>
						<td style="text-align: left;"><?php echo $val->last_used;?></td>
					</tr>
					<?php } } ?>
				</tbody>
			</table>
		</div>
	</div>

</div>
<!--/col-span-6-->

<div class="modal fade" id="addWidgetModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<a href="#" class="pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle pull-right"></span></a>
					<h4 class="modal-title">Add Device</h4>
				</div>
				<script>
					$(document).ready(function() {
						$.fn.deviceAdd = function(link) {
							//alert(link);
							$(location).attr('href', link);
						};
					});
				</script>
				<table class="table table-striped" border="0">
					<thead>
						<tr>
							<th style="text-align: center;">Shelf</th>
							<th style="text-align: center;">Device Brand</th>
							<th style="text-align: center;">Operating System</th>
							<th style="text-align: center;">Serial Number (Last 10 Code)</th>
							<th style="text-align: center;">Condition</th>
							<th></td>
						</tr>
					</thead>
					<tbody>
						<?php $uri = $this->uri->segment(3); ?>
						<?php if (isset($devices)) { ?>
						<?php foreach($devices as $val) {?>
						<?php echo form_open('store/device_add'); ?>
							<tr>
								<td style="text-align: center;">
									<input type="hidden" name="id" value="<?php echo $val->id; ?>" />
									<input type="hidden" name="store_id" value="<?php echo $store_id; ?>" />
									
									<select id="shelf-<?php echo $val->id; ?>" name="shelf_id" style="width:100px;">
										<?php foreach($shelf as $r) { ?>
											<option value="<?php echo $r->id; ?>"><?php echo $r->name; ?></option>
										<?php } ?>
									</select>
								</td>
								<td style="text-align: center;"><?php echo $val->brand_name;?></td>
								<td style="text-align: center; width:100px;"><?php echo $val->os_name;?></td>
								<td style="text-align: center; width:350px;"><?php echo strtoupper(substr($val->serial_number, strlen($val->serial_number)-10, 10));?></td>
								<td style="text-align: center;">
									<?php if($val->status == "good") { ?>
										<span class="label label-success">Good</span>
									<?php } else { ?>
										<span class="label label-danger">Bad</span>
									<?php } ?>
								</td>
								<td style="text-align: center;">
									
									<a id="a-<?php echo $val->id; ?>" href="#" onclick="javascript:$.fn.deviceAdd('<?php echo base_url().'store/device_add/' . $store_id . '/' . $val->id . '/'; ?>'+ $('#shelf-<?php echo $val->id; ?>').val());">
									<button class="btn btn-primary btn-xs edit" data-title="Edit" rel="tooltip">
										<span class="glyphicon glyphicon-pencil"></span>
									</button>
									</a>
								</td>
							</tr>
						<?php echo form_close(); ?>
						<?php } } else { ?>
						<tr>
							<td colspan="5" style="text-align: center;"> <?php echo "NO DEVICE ADDED";?></td>
						</tr>
						<?php }?>
					</tbody>
				</table>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dalog -->
	</div>