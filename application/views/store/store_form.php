
<script type="text/javascript">
$(document).ready(function() {
	$("#province").change(function()
	{
		$('#city').empty();
		$.ajax({
			url : $.fn.domain() + 'ajax/city/' + $(this).find("option:selected").val(),
			type : 'POST',
			success : function(data) {
				$.each(data, function (i, item) {
					$('#city').append($('<option>', { 
						value: item.id,
						text : item.name
					}));
				});
				$.fn.city();
			}	
		});
	});
	$("#city").change(function()
	{
		$.fn.city();
	});
	
	$.fn.city = function()
	{
		$('#subdistrict').empty();
		$.ajax({
			url : $.fn.domain() + 'ajax/subdistrict/' + $('#city').find("option:selected").val(),
			type : 'POST',
			success : function(data) {
				$.each(data, function (i, item) {
					$('#subdistrict').append($('<option>', { 
						value: item.id,
						text : item.name
					}));
				});
				$.fn.subdistrict();
			}	
		});
	};
	
	$("#subdistrict").change(function()
	{
		$.fn.subdistrict();
	});
	
	$.fn.subdistrict = function()
	{
		$('#village').empty();
		$.ajax({
			url : $.fn.domain() + 'ajax/village/' + $('#subdistrict').find("option:selected").val(),
			type : 'POST',
			success : function(data) {
				$.each(data, function (i, item) {
					$('#village').append($('<option>', { 
						value: item.id,
						text : item.name
					}));
				});
			}	
		});
	}
});

</script>
<div class="form-group" >
	<div class="row">    	
		<div class="col-md-11">
			<label for="alamat">Alamat 1</label>
			<textarea class="form-control" id="store_address_1" name="store_address_1" rows="4" ><?php if($page=='store/edit') echo $store->store_address_1;?></textarea>
		</div>
	</div>
</div>

<div class="form-group" >
	<div class="row">    	
		<div class="col-md-11">
			<label for="alamat">Alamat 2</label>
			<textarea class="form-control" id="store_address_2" name="store_address_2" rows="4" ><?php if($page=='store/edit') echo $store->store_address_2; ?></textarea>
		</div>
	</div>
</div>

<div class="form-group">
	<label for="targetName">Provinsi</label>
	<div class="row">    	
		<div class="col-md-11">
			<select id="province" class="form-control">
				<?php //if($page!='store/edit') { ?>
					<?php if (isset($province)) { ?>
						<?php foreach($province as $val) { ?> 
							<?php if ($val->id == $store->province_area_id) {?>
								<option value="<?php echo $val->id;?>" selected="selected"><?php echo $val->name;?></option>
							<?php } else {?>
								<option value="<?php echo $val->id;?>"><?php echo $val->name;?></option>
							<?php }?>
						<?php } ?>           
					<?php } ?>
				<?php //} ?>
			 </select> 
		</div>        
	</div>
</div>
<div class="form-group">
	<label for="targetName">Kota/Kabupaten</label>
	<div class="row">    	
		<div class="col-md-11">
			<select id="city" class="form-control">
				<?php if($page=='store/edit') { ?>
					<?php if (isset($city)) { ?>
						<?php foreach($city as $val) { ?> 
							<?php if ($val->id == $store->city_area_id) {?>
								<option value="<?php echo $val->id;?>" selected="selected"><?php echo $val->name;?></option>
							<?php } else {?>
								<option value="<?php echo $val->id;?>"><?php echo $val->name;?></option>
							<?php }?>
						<?php } ?>           
					<?php } ?>
				<?php } ?>
			</select> 
		</div>        
	</div>
</div>
<div class="form-group">
	<label for="targetName">Kecamatan</label>
	<div class="row">    	
		<div class="col-md-11">
			<select id="subdistrict" class="form-control">
				<?php if($page=='store/edit') { ?>
					<?php if (isset($subdistrict)) { ?>
						<?php foreach($subdistrict as $val) { ?> 
							<?php if ($val->id == $store->subdistrict_area_id) {?>
								<option value="<?php echo $val->id;?>" selected="selected"><?php echo $val->name;?></option>
							<?php } else {?>
								<option value="<?php echo $val->id;?>"><?php echo $val->name;?></option>
							<?php }?>
						<?php } ?>           
					<?php } ?>
				<?php } ?>
			</select> 
		</div>        
	</div>
</div>
<div class="form-group">
	<label for="targetName">Keluarahan/Desa</label>
	<div class="row">
		<div class="col-md-11">
			<select id="village" name="village_area_id" class="form-control">
				<?php if($page=='store/edit') { ?>
					<?php if (isset($village)) { ?>
						<?php foreach($village as $val) { ?> 
							<?php if ($val->id == $store->village_area_id) { ?>
								<option value="<?php echo $val->id;?>" selected="selected"><?php echo $val->name;?></option>
							<?php } else { ?>
								<option value="<?php echo $val->id;?>"><?php echo $val->name; ?></option>
							<?php } ?>
						<?php } ?>           
					<?php } ?>
				<?php } ?>
			</select> 
		</div>        
	</div>
</div>