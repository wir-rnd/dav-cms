
<div class="col-md-12">	
	<table class="table table-striped table-bordered" border="0">
		<thead>
			<tr>
				<th style="text-align: center;">Store ID</th>
				<th>Store Name</th>
				<th>Provinsi </th>
				<th>Kabupaten/Kota</th>
				<th style="text-align: center;">Action</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($store as $val) { ?>
			<tr>
				<td style="text-align: center;"> <?php echo $val->store_id;?></td>
				<td> <?php echo $val->store_name;?></td>
				<td> <?php echo $val->province_name;?></td>
				<td> <?php echo $val->district_name;?></td>
				
				<td style="vertical-align: middle; text-align: center;">
					<div id = "test">
					<p>
						<a style="text-decoration: none;" href="<?php echo $this->config->item('base_url')."/store/pv_position/".$val->id;?>" title="assign marker">
							<button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" data-placement="top" rel="tooltip">
								<span class="glyphicon glyphicon-picture"></span>
							</button>
						</a>
												
						<a class="edit" href="<?php echo $this->config->item('base_url')."/store/geolocation/".$val->store_id;?>" title="Longitude Latitude">
						<button class="btn btn-primary btn-xs edit" data-title="Edit" rel="tooltip">
							<span class="glyphicon glyphicon-map-marker"></span>
						</button>
						</a>
						
						<a class="edit" href="<?php echo $this->config->item('base_url')."/store/device/".$val->id;?>" title="Assign Device">
						<button class="btn btn-primary btn-xs edit" data-title="Edit" rel="tooltip">
							<span class="glyphicon glyphicon-phone"></span>
						</button>
						</a>
						
						<a class="edit" href="<?php echo $this->config->item('base_url')."/store/modem/".$val->id;?>" title="Assign Modem">
						<button class="btn btn-primary btn-xs edit" data-title="Edit" rel="tooltip">
							<span class="glyphicon glyphicon-signal"></span>
						</button>
						</a>
						
						<a class="edit" href="<?php echo $this->config->item('base_url')."/store/edit/".$val->id;?>" title="edit">
						<button class="btn btn-primary btn-xs edit" data-title="Edit" rel="tooltip">
							<span class="glyphicon glyphicon-pencil"></span>
						</button>
						</a>
											
						<a class="trash" href="#" onclick="$.fn.delete('<?php echo base_url().'store/delete/'. $val->id; ?>');">
						<button class="btn btn-danger btn-xs" data-title="Delete" rel="tooltip">
							<span class="glyphicon glyphicon-trash"></span>
						</button>
						</a>

					</p>
					</div>
				</td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
			
</div>