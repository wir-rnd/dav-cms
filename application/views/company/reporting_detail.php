<style>
@import
	url(http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css)
	;

.page-header {
	text-align: left;
}

/*social buttons*/
.btn-social {
	color: white;
	opacity: 0.9;
}

.btn-social:hover {
	color: white;
	opacity: 1;
}

.btn-facebook {
	background-color: #3b5998;
	opacity: 0.9;
}

.btn-twitter {
	background-color: #00aced;
	opacity: 0.9;
}

.btn-linkedin {
	background-color: #0e76a8;
	opacity: 0.9;
}

.btn-github {
	background-color: #000000;
	opacity: 0.9;
}

.btn-google {
	background-color: #c32f10;
	opacity: 0.9;
}

.btn-stackoverflow {
	background-color: #D38B28;
	opacity: 0.9;
}

/* resume stuff */
.bs-callout {
	-moz-border-bottom-colors: none;
	-moz-border-left-colors: none;
	-moz-border-right-colors: none;
	-moz-border-top-colors: none;
	border-color: #eee;
	border-image: none;
	border-radius: 3px;
	border-style: solid;
	border-width: 1px 1px 1px 5px;
	margin-bottom: 5px;
	padding: 20px;
}

.bs-callout:last-child {
	margin-bottom: 0px;
}

.bs-callout h4 {
	margin-bottom: 10px;
	margin-top: 0;
}

.bs-callout-danger {
	border-left-color: #d9534f;
}

.bs-callout-danger h4 {
	color: #d9534f;
}

.resume .list-group-item:first-child,.resume .list-group-item:last-child
	{
	border-radius: 0;
}

/*makes an anchor inactive(not clickable)*/
.inactive-link {
	pointer-events: none;
	cursor: default;
}

.resume-heading .social-btns {
	margin-top: 15px;
}

.resume-heading .social-btns i.fa {
	margin-left: -5px;
}

@media ( max-width : 992px) {
	.resume-heading .social-btn-holder {
		padding: 5px;
	}
}

/* skill meter in resume. copy pasted from http://bootsnipp.com/snippets/featured/progress-bar-meter */
.progress-bar {
	text-align: left;
	white-space: nowrap;
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
	cursor: pointer;
}

.progress-bar>.progress-type {
	padding-left: 10px;
}

.progress-meter {
	min-height: 15px;
	border-bottom: 2px solid rgb(160, 160, 160);
	margin-bottom: 15px;
}

.progress-meter>.meter {
	position: relative;
	float: left;
	min-height: 15px;
	border-width: 0px;
	border-style: solid;
	border-color: rgb(160, 160, 160);
}

.progress-meter>.meter-left {
	border-left-width: 2px;
}

.progress-meter>.meter-right {
	float: right;
	border-right-width: 2px;
}

.progress-meter>.meter-right:last-child {
	border-left-width: 2px;
}

.progress-meter>.meter>.meter-text {
	position: absolute;
	display: inline-block;
	bottom: -20px;
	width: 100%;
	font-weight: 700;
	font-size: 0.85em;
	color: rgb(160, 160, 160);
	text-align: left;
}

.progress-meter>.meter.meter-right>.meter-text {
	text-align: right;
}
</style>

<div class="container col-md-12">
	
	<div class="resume">
		<header class="page-header">
			<h1 class="page-title">MILO</h1>
			<small> <i class="fa fa-clock-o"></i> Last Updated on: <time>Sunday,
					January 22, 2015</time>
			</small>
			
			<div style="position: absolute; right: 15px; top:20px;"> <img src="<?php echo base_url('assets/img/')."/logo.png"?>" width="70px" />
			</div>
		</header>
		
		<div class="row">
			
			
			<div
				class="col-xs-12 col-sm-12 col-md-10 col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading resume-heading">
						<div class="row">
							<div class="col-lg-12">
								<div class="col-xs-12 col-sm-4">
									<figure>
										<img class="img-circle img-responsive" style="width: 220px; height: 200px;" alt=""
											src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxQSEhUUExQWFhQVFRQUGBcXFRUUGBgWFxQWFxQUFBUYHCggGBolHBQUITEhJSkrLi4uFx80ODMsNygtLisBCgoKDg0OGxAQGywkICQsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLP/AABEIAL8BCAMBEQACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAAAwQFBgcBAgj/xABLEAABAwEEBQgHBQYDBgcAAAABAAIDEQQFEiEGMUFRYQcTIjJxgZGhQlJykrHB0RQjYoKiQ5OywuHwM1PSFRYXw+LxJERUY3OD0//EABsBAQACAwEBAAAAAAAAAAAAAAADBAECBQYH/8QANxEAAgICAAUCBAMHBAIDAAAAAAECAwQRBRIhMUETUSIyYXEUQlIVI4GRobHBQ1PR8DThBhYz/9oADAMBAAIRAxEAPwBKJjWsDavc4Z43OBqXPcS0tpkGggA1Naahs4d065/EujONdOE/iXRnFXK4IAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAEALG0bKLfZHtkLjqa49gJWrsgvJuqbH2iz39kk9R/uu+ix6sPc2/D2/pYm5hGsEdoIW6kn2I3XJd0eVk1BDAIAQAgBACAEAIAQAgBACAEAIAQABVYbS7m0YuXRI9PjLdYIrvFFiM4y7G06pw1zLR5WxGCAEAIAQAgBACA9xxF2oKOVsY92WqMO675IseQ3cTrPh9VVnmJfKjr08Dl/qS/kPobuYNle3NVZ5c2dGvhePDxsfwwAagB2ABVpWSfdluNNcPlSHLWqPubdBeNiljE0bHDWVyOpWYRZDJRfdCM9ywya2AHe3ony1q3CUl5KVuJTPuiEvHRN7RiiOMeqcnd2x3krUW2ci/h8o9YPZXXNINCCCMiDkRwIWxzmmnpnEMAgBACAEAIAQAgBACAEAvZ7I5+oZbzkFBbkQr79y7j4Ft3ZdPck7NdI21cdw+gzKpSy7J9II7VPCKq+tj2TVluKU9WEtHEBvxzSODmW9eVl+MsarpFL+BSdMJ3w2oxuyLWMHjV383kupjYjqhyz7lDiVaurjYhGx2jGFvKOjzlkOVjhaEYIAQAgBOgPbIyVFO6MTo4vDLr/Gl7juGzDtVOzIk+3Q9Hj8Iop6y6v6j6KNVJSOjpR6LsOo2KNsw2OGNWmm+yNG0hy2E7j4Fbqi1/lf8mRuyPuLRwncfAqVY9i8P+Ro7I+4vG1SRg13NHIcRsViESNscMYrEIkbYuAp0iPZDX/cLbQC5tGyjU7UHfhfw47PJZKeTiRtW10ZQJonMcWuBDmmhB1grBwpwcHpnhDUEAIAQAgBACAEB6jjLjQCpWk5xgtyZJVTK2XLDqyeujR90hybjO31R2k61UjK/JfLSunuegx+G1ULmue37FzsGi7RQynEfVGTfHWfJdTH4JBfFa9v2LM8t/LBaRO2eysjFGNDewAeO9diuiutahFIqynKXcWUpqZDyyXOROycdSRgjJ3SMqR4t/hK5+WnGSmdHEUba3U/4FKumYtNHKpPUltHCzsWVbe0TgVc5R1ACbRvGEpfKtgFFK1I6eNwi+3q1pfUWjjVadzl2PQYvCKaesvif1HccarOWzp9EiYu+5pZeow09Y9EeJ1qarCvt+WP8yrZk1w7ssNi0RP7R/c0V/UfounVwPfWyX8inZnv8qJmzXBAz0cXtGvlqXRq4VjV/l39+pVllWy8j6Ozsb1WtA4AD4K5GmuPaK/kQOUn3YrRb6RgKLOkBKSzNdraD3KKVFcvmijKk15G0l2N9HLzCp2cOg+sehIrn5Gj4S3WFSnRKvuiRT2eVoAQyVrTK6cbOeYOnGOl+Jg19419lUKGbj88eZd0UgFanEOoYBACAEAIAQyL2SymQ5ZAazu/qoLr41ot4mHPIlpdvLL1o/otkHPBa3XT0ncSdgUuLw2eR+8v6L2O/F140eSpdfcuMEDWANaAANgyXoq641rUFpFZycurFVIYBACAZXvdkdpidFKKscO8EanA7CDmtJwU1ys3hNwlzIx+/wDQi0WVxLWmWLY9gqQPxtGYPHVkuPdjWQfTqjsQyab48s+/1ISKZwyqqUpNGkuEYs+uv6i7ZHHatHYzaHB8WP5RVg3lQuTZchRTV8qSJmwXFPJ1IXniRhHi6gW8MW6ztFmtmVVDuyzXfoQ85yvDRuaMR8dQ81dr4POWueWvsUbOIx/KizXfo9BDm1lXD0ndI/Qdy6lOBTV2XX6lCzJss7slQrmkQHVkAhgEMggCqGAqsGQWQeZGAihWsoqS0wnojbTZ8J4f3kuRkY7re12J4y2IKsbgQhhra0Zde1j5ieSPYDVvsuzb4Vp3LDPPZVfJNoarBWBACAEAIBzYrIZD+Eaz8hxVe+9VrS7l/CwnkS69vJpGjejoY1r5G562t9Xi7e5XuH8O6+rf38L2O3OxQj6dfRFlou6VjqyAQAgBACA5RNAa2m7IZDV8UbjvcxpPiQo3VB90bxsnHsxrHo5ZWmos8VfYC0/DVfpRt69v6mPYLFGzqRsb7LQ34BSKuK7I0c5Puxei3NTFtNtJLXFb52RzyMaxzQ1oIoBzbTq7SVysi6cbGkz3nCeF4t2HGU4Jt+TStE7+FssjZRTGAWyDdI0Z9xyI4FX6bOeGzyXEMJ4uS6n28fYzTRy8L0t73titRDmjGQ4hozNKCjSqNVl1jaTPVZ2LwzChF2V737P/ANkpbLHekRpJeMTCdjpgD4FilauXeaKVdvDLPkx5P7b/AORxDc18PGJltY5p2tlqPEMWVC99pEcsrhEXqVEl/wB+43tljvSI0kvGJhOx0wB8CxYcbl3miSFvDLF8GPJ/bf8AyTei9nt0bnTWi1xywCN+p4eMeWE1wgCme3apK/UW3KWyhnTwrOWuipxlvzvsRvJbpHLK+YWm0YgGxFvOOaMyX1w6uHktMW2Um+Zlzj3DqqIVumGt99bDRq/bRJe0sLpXOjBnAYaYRhPRoOCxVdOVzjvoZzsDHr4bC6MdSeuoteUd7MH3tvs8YOr/AA2e7VlfNbTVy/MiHGlw2SWqJSf3Z5ue5rykkikNubJE2RjnYZC4FocC5tAKGoypxSFdrabltGcnK4fGuUI0OMmumzR3MBFCrcoqS0zzS6EVNHhNP7ouJdW65aLCltCaiNij6fw0lhk9ZrmH8pBH8RWO6OXxGHZlcWDkAgBACAWslnL3UGradwUN1yrjst4eLLIsUV28mi6J3IABI4UA6g/nKscMwXN+vb/A9DbKNUPSr7FrC9EVDqAEAIAQAgBACAEAIAQAgMVvixNnvx8T+rJKGnvhbmFy5xUsjTPeY10qODqyHdL/ACd0St77tt0lnmPQeeaedQxfspBwNfB3BYpk6bHFmOJUx4lhxyK11XX/AJQ20KvQ2VtulHWbC0NrqxukLW1G3MhYx58ilI34vjfiJ49T8vqIXALBJzkl4TymV7jk1rzu6bnBpqa1y2ALWv0pbdj6k2ZHOqca8GCUUvp/keaGXp9mt5is0pkgmLmNqC2ri2sbi0jJwIDSdq3os5bNR7Mr8VxXfhK6+KjOPf8Az/yRd0us/wBol/2kJsROZb1g+px84Ne6lPotIcnO/VLd8chY8P2frX/ff+pa9H9GLPN9oEFs5yyyNJdEAWyAtc18ZcHbi3XTMKxXTF71Lp7HFzeIZEHX61XLZF/N00/BBcnejUNvdK2bFRjWEYTTrFwNcuAUONVGyT2dPjvEbcWuHp6+LY6uO2CC9bTKdUbbW7twgkBZrfLdJ/cjza3bwyqHu4r+bI26J7LaZZJrynfidSga15rxq0HC0ZABaQlCbcrWWcmnKxq41YEFry+n+Re6ryisV4MdY5XPs73Ma4Oa5pwudRzXAgYi2tQafNZhNQsXI+hHlYtuVgy/EwSnFNp/Y3ILrnz4bW+Ora7vhtVPLr5ob9jeD6kW964spFlIqPKBnDGfVlHm1w+ixU9sp58d17KoCtzz/k6hgEB1jSSANZyWspcq2zeuDnJRXkuWi1zB7g30W0c879ze/wCFVVxKZZl+38qPV11Rw6eVd2aE1tMhqXropRSS7IpfUHOA1rII59/2VpINpgBGsGaMHvGJNo25JewM0hshIAtUBJIAAmjJJOoAYtaxtDkl7ElVZRqdWQcKAaWu9IYv8WaOP25Gs/iKxtGVGT8Df/eOx/8AqrP+/i/1JtGeSXse4L8sz3BrLRC5zsg1ssbnE7gAalY2hyy9iQC2NTqAEBkEzD/vDqP+O06jq5lua5un+J2e0VkP2G1vrr/JM8rOj3ORttUbavj6MgAqTHXJ1B6pPgTuUmZTzLmXco//ABziKpsdFj+GXb7/APsq+gF0m1NtsOovgbhJBoHh5cwnvAUGNW5KSZ1+OZcaLabIvs/6CVzXjDYccNusLZHhxILmMxDYRVw6Tcqgg7ViEo1bjOOzOXj2Z7V2Jdpa6rb/AMdib0PdNarQ6WGyWeGJgeWO5gAh2EiNrZMqmpBJGyu9TUuU3tRSRzOJV049SrndKUnra30+vQayaVDE+O9bG2R4NARGGOHCp1jVRwK1d/Vq2JZr4XtKeBfpedv/AL/US0AsDprwM0Ebo7MOdrUkgNexzWsxekcRaacOxa48G7OaK0iXjGRGvCVV0lKza7fR9/p06DfRa9n3RPMyeF5qAwhuR6JJDmVycCCfJKZOmbUkZ4jRXxLGhOqxfD16/wBh1o/Yueva0xuBDXi1NOWoOyrn2hKoN3Pf1Nc3IjDhlTi1tcv9Bjdz23bLJFbrG2YGmFxY06tsbnCjmkccqdq1jqltTjsnyFLidcbMa3lflb1/YldHZzbLaw2ew2eOzte1xcYGkta01rzmQxmmQGrjRS1vnn8MFo52dRHFxmrb5Sm/Cl06/T2NdC6J5E44VyWso7WgVuc0JG4kLzF3wzaL0OqKrp1J/wCHH/yM+a1x3uZXz1+6KrHqCsM81Lue1g1BASt0Wb0z2D5n5Lm5lrb9NHouEYml60v4GoXFYeZiAPWPSd2nZ3al6fh2L6FKXl9WS32epPfgkVfITCuUrSt9ptD4WPIs8TjHhBID3tNHuf6wqCBsoK7VUsnt6Onj0qK5n3KXRRFkAaZjIjMdqw1tA+ktFraZrOx5NTQcciAR5GncpMGxzr0+66HKyYKM+hLq6QFA5TdNDZW/Z4HUneKucP2bDu3POzcM9yhsnroi1j0c72+xi0ji5xc4lzjmXE1JO8k5lVW9nRSS6I5RDJaeT+y1mfLsiZkfxO1U7mu8lzuIWOMVGPdsdH3NmvTSCGxQsfapA0kAUAq57gBiwNGZ+S7cJcsFzdzjqtzk+UrB5XLH/lWn3Yv/ANE9ZE/4SZ6j5WbK4hohtJLiGgYYsyTQD/EWryIpbH4SZdmTxlnO1aG0JLjQUA11OymdVJXONkVKPYryUk+Qptt5VLExxa0TSgZYmMbhPZjc0nwWruiTRxbH17DZnKxYhqgtA7GQj/mLHrLwjd4tku7LJo7f8F4MxtjIoSAJWsxEDIubQnKuSxDIrnY4eURTrsqXfoT7WACgFFYIG99xG1MjpikDKNzJfhoBvJOpYevJtGUl0TKlefKRYLP0WOdKRshaC33yQ0jsJUbtiiaOPZPq/wCpXbZyutNMFjxe3IB4UaVG717E8cSSXzHLLyuipMljA4slBPfiYEVy9jDxJa0pFiuzlEu+0kNkJiOwTtaG19sEtHeQt1bGXcidFsO39C5QBuEYKYTmMNKU3iilWvBXbbfUUWTBxAVq98pXdx8QvMcQXLcy/R1iUjTyX7uNu+SvcGmvmQosTrJsrcReq0iCh1BWWeal3PawaikEWJwaNp/7lR2z5IuRPjUu2xRRddG7EHzMFOizpH8uoeNFS4bU78ncvHU9fbqmnlj9i+he1OUR2kV4fZrNNMf2cb3DZVwBwjvNB3rWT0jauPNJI+aCSczmTme3aqJ2UjiGQQG6clFrx2JgOsAt9x7mj9PNrXD+G2cf4nPzF8WyW0z0lZYLOZDQyO6MTPWfvOfVGs/1CvzlyogqqdktHz3bLU+WR8khxPe4uc47SVTb29nVjFRWkIrBsCA1Pk3uykEVdc0uM+w3UOwhn61zZr1cuMfYhtnywbK1yo3oZ7fI2tWQgRNGyozee3ESPyjcuta9s1xoahsqKjLBO6FWPnLWyvVjDpD3Cjf1EeBVTNs5KmvcIvvKFbuYuyKEGj7U7E6hocAONw7M42ngabVZxYeljRiU4fHe37GSrYuAsN66g3DQG7sGBv8AlRCvF7sif4/ELncMi7cmdr8ditmS1Wolsve847NC+aU0YwVO87mtG0k5AL0Lels50YuT0jBNLdLp7e84iWQg9CIE4QNhf6zuJ1bFTlY5HUqpjBFeWhMCAEGwQFy5PtM5LHI2KRxdZnkAgknmyaAOZubvbq25Z12Vjh9ivdQpra7m6xvBAIzBFQeCuRkpLaOW1o9LYFVv533zvy/wheX4s/3/APA6GKvgM701nxSxM9Vrne8aD+Fa4K+ByKHE5dUhqwZBTM88+56QwSlzQ63dw+fyXNzrO0Eeh4JR3sf2RoGhtnox7/WdhHYB9T5Lr8Bq1W7H5LubPc9FiXfKRn/LPeXN2RkQOc0or7DAXH9WAeKhuekWsSO579jFlVOkCAEBovJXfbYGy867DHGS8k6gHMNO01jpTe4KBS5MmL90yDIr54dCp6V6Qvt1oMz6hvVjZ6jNg9o6yd/CitTlzM2qq5I6RDLQlBAe4Yi9zWt1uIaO0mg+KxJ6TZhm8XSGWWN8n7Oy2enutqa020jHvKpw6PNZK1lPIe9R9zCJ5i9znu6z3Oe72nEl3mSrm9suJaWhNDJoPJrdmKOR+2V7IW+yOuR7x9xc/JTsthWaTnyrfsNuV68RJbuaHVs8bWU2BzwHup3Fg7uC6lmt8q8EGLHUeZ+SjqMtEno3ZOdtMbdgdjPYzpfIDvVbLs9OpsyjfNFYMMRcdb3E9w6I7sie9S8Ip5KN+/U5mZPms17GZ8sN/GW0CytP3cIDn8ZXDb7LSPeKtXS30JsSvS5jPFCXB1ddgfaJWRR9Z5pwA1lx4AAlaWTUIuUjDels126NCIY24WRNkcOtJKA7PvBp2Adq5sIZOU9xekV5XqPcYacaDMZZZJ2iMOibj6DObq0dYUGRyqrtWHdV1lPaEMuMpJaMnVgsggPoLk8t5lsceI1c1rPNo/mD/BMCzalD9LOblw5Z7Xks66BVKbf8tZn8KDwAXj+Kz5sho6mNHVaMwvOfnbVI7YHYB2Ny+IKu0Q5KkjhcQs3Yx0Fg5DOoNFisceFjRwr3nMrgXz5rGe3wqfSojE0LRyPDZ4+ILvEkjyovZ8Lhy4sfqc/Ie7GSa6BAYdywXjztuEYPRgja2n43kvf5GMU4cVUue5HSxI6hv3KMoi0CAEB6DzQipoaVGw01V3rDSb69weVlgEAICf0GsfOWxldUYMp/LQN/U5qqZtnJU/qYfY0DT+3czdeH0rXKB+QdKvZhYwfmCnxIenjr6lOPx3fYyBSF0CUBuGg9hEEUQdkIInSvJ2OfUknxl8FTw/3uTKfhFPJl8Ovcxi87cbRNJM7Iyvc+h2YjUA9goO5XW9vZahFRSQ2WDYuvJzdxdzswGYwxN9pxBI/g8VzOINz5a15ZjnUe5tkLBFGB6MbAO5rf6Lv1QUIKK8I4zblLZ8zXjazNNJKTUySPk99xd81Ub2zsQjyxSG6wbF85MLFnJLTpEthZ2kguPm3wK5uc3Ocao+TSb1Ftm02eEMaGjUPPiu9VXGuKijjyk5PbK1ynW0RXbPveGxAb8bg007G4j3Ja9RJceO7EfP6pnWBAbpyZMwwNH/swk9pBJ+JVXhkt3W+2ynnLpEuhNF2W9LbOeZlpDeWBssu3pEdpNGjzC8Q95GS/udaT9Knf0KHdUe09q7Vj10PJ5EtvqSihKgpZ2YnNG8j+qjtlqDZPjQ57VH3ZZKLz/dnuktJGiXS2kMQ3Rs/hC9/hrVEPsjh2v439xxI8NBJ1AEnsGZVkjPmO97cZ55Zia87I9+3U5xLRnsAoO5UJPctnZhHlikNFg3JmyXdWxTTEZh8YHY09KnbjHgqc7+W+NfuZ10IZXDAICY0fusSYppB9zCC534iBUMHz/qqmTfyNQXzMykRD3kkk6ySTszOvJWktIwcWQXzk5sZ5uR/pSPbE3yAPvP8A0lcnOlz2xqX/AHZhvSbfg7ywW6tpis7epZ4W+8/ZTg1rPErtTWko+xWxV0cvcoSjZaH9xWTnrRFGcw54r7Lek7yBUORPkrcgavpTbuYuud4NHzuELdnRrgdSmeoSkdoWvDocuPzv8xTl8dyj7GMqwXAQG18nN24LNZ20zdW0O782145x+CoUr1cvfsU8iWkyz6Yyltgtbm5EWacg7jzTqFd2fyspVLc0fNqonZBAbByXWKjIeDXzHtcSG+TvJc/Fj6mbKX6UQZb5a/uaSvQHKMp5bL2/wbM075n/AMMYp3vPcFXul4L2JDvIyxVy8L2GzGWRkY1vc1vicz4VK0tnyQcgfQOh8FGPOzEGDsaNfi4juUHBovklN+WUc5/El9B/pBaubgcdrugO127uqe5WeJ3+jjyfl9EV8eHPYkY3pfbMTmQjZR7v5B8T4LhcNp0nazbil+vgQ3scdGq5JnmrHtjhakY7uptZW958iq+U/wB2zocMW8lFljjXIjE9e2Xy6j9zH7DfIUXucN7oh9kcW3539yC5Sbx5i75zXORvMjtk6Jp+UuPcprXqJtRHmsR8+qmdYE8A1jR64OcshgI/8vITlXpvaaZb6vPguVhx9fLlJ+CO+zkite5k4XVJF2H9yXU+1SiJm3NztjWjW4/TbVRXWxqg5Mw3ouGlsYs9iEUYwsLxEN5pV789+QqfxLl4Vc7bHfPt4NuZb5SgLsmAKMG06D3ZzTYWuy5uPnX7g6lT5v8A0rlYcfWzHJ+CDJnqvS8mS3/eBtFpmmP7SV7h7NaMHc0NFeC60nttklceWCQwWDctnJ9ZCZJJaVwNDG8XPOoeA94LmcSk+VQXdsymktsmuWC1YTZbI05RRmRw3udRrCe5snvLrOKhCNa8Ip43Vyl7mcrQti1ig5yRjPWc1pO4E5nwWlkuWDYPoLRy0RBpdjYNTQMbdQ7/AO6KHhNbUZTl3bOfl75tJC+kkkc1ktETZY6yQTMHTbrdG4DbvK60tNFevakno+cAqR2AQGyckt6RvjLMQEjWRswkgE4S/MDaKEKLCq5LZv3KebtpFo0n0rs9ijLpHgvp0YmkF7jsyHVHE5LpTmolOuqU3o+f73vJ9pmkmkNXyOqaahlRrRwAAHcqcnt7OtCKjFRGawbFy5PLpLnPtLh0YwWMy6zzrI7AadruC5vELNpVx7s1bS7m2XVZeaiY06wKu9o5u8yV2sWr0qoxOTbPnm2VLTa9mgmp6ELSTxdtA8h2led4pa8jIVMOy/uXMdKqt2SMqs+KWR0jtbiSeHDsGpXNKuKivBwMq5yk2yWAUJzjqGCQuEVmHY74Kvlf/mdLhX/kItcca58YnqJSLXcbqxAbiR51+a9bw2W6F9DmXr42Zzy3XlnZ7ONmKd3myP8A5n9lT3vsifCj3kZYq5fJHR6wGe0RRgVBcC7g1ubie4U71DkT5KmzD7G96LWbCx7vWdQdjf6krTg1WqnN+Wc/MnuSRg1+3cYrZNA0VLZnta0Z5F1WDwLVZm1HbZdqfNBM1XQ3RwWeERtpz0tHSP10A1DsFaDeSSuRqWbZpdiKyzl6sqHK3am/ao7Ozq2aKn55aPdXiQIyuzKEYJQj4MY22nN+SjLUtEpoxd5tFqij2Yw53sNOJ3kKd4UORPkrbNZPSNb0mtf2e77VIDR0g5hhGur+jUcRjd7qi4ZXyVSm/JUfx2xj7GIBWy6CA2Dk2ufBBDiFDI4zO7PQ8gzxXOivWzF7IrX2ai9GdaaXl9pt1olBq0vwt3YWAMbTPUcNe+u1dSb3LZJRHkgkQi0JQQHMA3BYA7umwc9NHGB1nAHL0a1ce4VUd01Ctsx4JTSy43WeTGGkQyOdgOwEHpM4UOrgosS12VRcgpJvRAq1sz4OOaDrFUANaBqFEB1AT2i+i8tseKAthB6UhGXYz1nfDaq1+TGpfU0lPRs9wXSxoY1jaQxdUes4ba7aHOu09iiwMaVs/WsKV1uunuSd93gIYyfSOTRx39gV/iGWsenm8vsQ01epLXgxTSq8TLJzTTVrTV53vzy7vieC4mFS4p2y7sxxDJXyLsjzY4cIVmUtnnbJbY4WhGCAkdHz9+zjiH6Sor1uOi/w6XLei6xxqpGPQ9M2TNyuoS3fn3jX/fBdvhk9NxKd631Kbp3oU602kzl8hDmtaAxmLCGjUaV3k14rfNsvhLcI7RYxbIKOn0IGLk4qc3T/ALo/RUvxOS/9MsO2C7Mt2jWiMdlB5pjzI4UMkgoabtQAbwA2BYlTkZD1JaRXncu+y6WWAMYGjYPE7Su/TUq4KK8FCUm3tlHvrRml4PtIGIzBgYKZNcGBjyOJAHiVxuKq2U41wXRnQxbYqGn4Ljd1jETaa3HWfgBwC6WHixx60vPkpW2c8tma6UaAuktMkrnyuMri+rI8QFcg00BpQUHcqGXbkVz6Q2mX8e2HIk3ojY+TmpzM/wC6P0Vf8Tk/7ZM7a12aLhotopHZQeajeXuFDJIKOprpqFG5DIDYFj0cnJlqS0itbcvcdac6MG1WaOMPcBE/nCGtBLjhLa02npOy4rqXVzpoSqW9EWPavU3LyUIcnXGf9yfouZ+Kyf8AbL/qV/qJW6uTqFrg6QTyUPVc3Cw+0A2p8Vl25U1pQ0QzujrozQ7LYHBr88DnNLG09AUyOW3V4BX8HElTFyl8zKVtik0jKZeTjAS0unNMqtiLgeIIFCqFmRkwk4+n/c6EboNJ70eP+HnG0fuHf6Vr+Lyv9v8AuberD3Qf8PONo/cO/wBKfi8r/b/uPVh7oVh5OATm6cf/AEn5hbLJyX/pmruh7ls0d0Njs1TEx7pHDCZJdYB1jUAB2CuS3lTfkdGtIgleu+yz2u5IpbPzEjQ5tNoBz9btqSV1PwkfSVft5Kiukp8yM1vnkxwkmMvp+Ec4Pd6w81Qn+Kp/LzL3R0K8mEl1emV6XQiUftY/zBzfKhVd8RS+aDX8CwtPyeotCHHrTMHYCfCpC0fE4vtFjRZ7j0DgBBLHzn8Q+78BQHsJKwrsq56hFogssjHyaBY7poAHUDRqY3IU3GmzgFdx+Gdea17KM799IkhNK2NhcaBrR5DYF1LJwqhzPokQpOT0jLNONJSTl13CjG16jc+mePz7F5jmlm3+pL5F2RbunHGr0u7Kdd1l2ntNdqvSmuy7HnLrdvZKKEqAgBAOLumwSsdsDhXsrmtZLaJqJ8lkX7M0mONRRgepch1CcJB3K1VJ1yTRHLqtEyxwIBG1d6MlJJoqs9rcBRACA4QmgFEB1ACAEAIAQHEB1AcogOoDixoHUALIBAcogPLowdYB7RVauEX3RnbOCBvqjwCx6cfYbZ7WyMCc8rWAucQAMyStbLI1x5pPSMpOT0jPNMdKRhr6Negza9293D4Ly+RkTzrOSHSCL2o4sOaXczqNr5XmR5q4mvZuA3AK4oxriox7I4OTkOcm33JRjaCijb2c5vZ6WDAIAQAhk0u4bXzsDH7aYXe03I/XvRHo8aznrTJBbEw6sdow5HUfJXcS/l+Fkc4+SRqupsiOrIBACAEAIAQAgBAcQHiWYNFXEAbyaBayko9zKTfYgrfplZY9TzIdzBX9RoPNUrOI0R7Pf2LVeFbLxor9r5Rj+zgH5n/IDJVZcU38sS3Dhj/MyJk5RrVsbD7jz/Oo/wBpWt9kS/s2v3Z2HlMtAPTjicPw42eZcVJHiM/zJGJcMh4bLHc3KLZpiGyAwuOQxEFnvjV3gK7VmQn0fQpW4NkOq6lxa6oqMwdoVzZSPSAEAIDiAaXheDIW1eewbT2BVcnLqx4803/A3rrlN/CZ7pXpTQVecvQiBzJ3n6rzdl12fPXaJefp40dvqygOL53435k6hsA3Dgr8IRqjyxODlZTsltsk4ow0KNvZzZS2z2sGoIAQAgBAWLQy8+bkMTj0ZDlwfs8Rl3BZR0cC7llyPsy8rJ2gQDmz2otyOY+Ct0ZTh0fVEcob7D+KUO1FdOFsZroyJpoUUhgEAIAQHKoAQDC33xDD15AD6ozd4BVbsymr5mSwonP5UVa9NNnZiFuH8Tsz3N1DvquTdxdvpWv5nQq4f+tlQvC8ZJTWR7nHicu4ah3Lmztste5PZ0YUwrXREHbb0YzW6p3DMqerGnPqkR25lVXdkTLfDj1G95z8grcMOK+ZnPt4s+0UImWZ22nYAFMq60UJ8Stf5g5qX1is/B7EP7Qs/Uw+8GvMLDhB9i1TxWa6PqX7kw0vdFI2yyurFIcMZcc2SE5Mr6rjlTYab1ZxrXF8rLV8IXQ9Wv8AibCF0TnHaoBG02lkYq9waOJUVl0K1uT0bRi5dkV28dJ9kI/M4fBv1XByuNpfDSt/UuVYfmZn9/6UYXENPOzaiSataeJ29g8lQrxLciXqXMzfl10LlgVcMc8mSQlzjtPkBuXVSjFcseiOFZc7Z9WO4JS00yyNDQaulhrWu/4FYlEW40XHex8oTmAgBACAEAIAQynrqXzRe/hM0RyH75o98D0hx3hbHcxclWR0+5PoXvJ5c5aSloykIPlooHa12JFH3OtvWRu2vaK+akjxS6HnZj8PFig0hI1sB7HEfIqVcca+aJj8Hvszw7Sc/wCUPf8A+lP/ALBH9H9TP4J+4lJpUdkQrxfXywrWXH34gbLA92MbRpTLsDW91fioJcavl8qSJY4MPOyHtt8TP60jiNwOEeAoFUnl32fNJ/2/sWoY1ceyIiWVRxjtljpogrxv1jMm9N24au9yv04U5dX0Kl+fXX0XVkDaLbLNto3cMh9SulCiFZx786c/OkK2W6idazK05k8jXklYLsaNahlY2VZ3tjtsDRsUfMyJzZ6wDcm2a8zPEkDTsWVJmym0QN5QmNwLOsCC3tGbT40ViEt9TvcKvbnyPszeTpK0NFGEuoK1IaK0z3rFnGqo9IpsuLDk2Rlr0jld1aM7BU+JXLv41fPpHoWIYcF1l1KrfGkMcZPOSF7/AFR03d+7voqscbIyXzS392bzvqpWkVS335NaOi37uM7Aakj8TvkKd66tGFVT17s5OTnyl0XRHmx3fTWppWdNHHsuJHmxSmw5KLZApuMuZCYs+eZrTgBXOuffmtucsSydrsLrQqAgBACAEAIAQHNxBIINQRkQRqIKymbRk4vaLRdGmdKMtI4CUDXs6bRq7R4BZknrodjHzU+kyyMtbXtDmODmnUWkEeSpWSa7nWhqS+ESfIqs5k8YjeSRVpT0SRiNnvUDeyRIRe9ZSN0htJIpEjZIaSyqSMdm33IS8b9ijyxYneq3PxOoK/ThWT7rRXtzKq/JW7beMs+XVZ6o+Z2rqVY1da+pxsjPlZ50jtkuolSStOXZeTFnsLWqu57KkrWx0AtCE6gBACAEA1tDWh8bn1LWua4gCpOE1pr20otltxaXkv4FyrsTY7tumh1Rw973fyj6qrDhifWcjuS4kvyohLVelpmydIQ0+iwYR4jM+Ku141FfaJStzpy8niy3ZwUkrShZeSsFlDVA5bKkrGxwtSMEAIAQAgBACAEAIAQAgPLmg60TaMp6EWNfGcUT3MP4TSvaNR71tuMlqRaqyZQ7MeR6VWlmTwyTiQWnxbl5KGeHVPt0OnVxKS79RZumnrwOHsuB+ICrS4VvtIuw4nHyjw/TVn+VJ4t+qwuEy/UiX9pQ9hrPpmPRhd3uA+AKkjwrXeRh8Tj4RHz6UTu6sbG+Lj8h5KzDh9Ue72RT4nLx0I+Z883Xe4jdqHgMlajCqHZFKzOnLvIVst0b0laUZ5OyUgsTWqGU2yrO1scgLQjb2dQwCAEAIAQAgPL2AihWU9GU9dhD7E1bc7JPVYoyBo2LDk2aubYqtTQEAIAQAgBACAEAIAQAgBACAEAIDhaCm2Z2JOszTsW3MzZWNCRsLVn1Gb+sw/2e1Z9Rj1pHttjaNixzs1dkhZsYGoLXbNXJs9LBqCAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAEAID/9k=">
									</figure>
								</div>

								<div class="col-xs-12 col-sm-8">
									<ul class="list-group">
										<li class="list-group-item">Nestle</li>
										<li class="list-group-item">Milo</li>
										<li class="list-group-item">Jl. MH Thamrin</li>
										<li class="list-group-item"><i class="fa fa-phone"></i>&nbsp;000-000-0000</li>
										<li class="list-group-item"><i class="fa fa-envelope"></i>
											milo@nestle.com</li>
									</ul>
								</div>
							</div>
						</div>
					</div>


					
					<div class="bs-callout bs-callout-danger">
						<h4>Top 5 Store Interaction</h4>
						<ul class="list-group">
							<a class="list-group-item inactive-link" href="#">


								<div class="progress">
									<div data-placement="top" style="width: 80%;"
										aria-valuemax="100" aria-valuemin="0" aria-valuenow="80"
										role="progressbar" class="progress-bar progress-bar-success">
										<span class="sr-only">80%</span> <span class="progress-type">K179 (JEMBATAN LIMA, Kota Jakarta Barat) </span>
									</div>
								</div>
								<div class="progress">
									<div data-placement="top" style="width: 70%;"
										aria-valuemax="100" aria-valuemin="0" aria-valuenow="1"
										role="progressbar" class="progress-bar progress-bar-success">
										<span class="sr-only">70%</span> <span class="progress-type">K222 (MUWARDI, Kota Jakarta Barat)</span>
									</div>
								</div>
								<div class="progress">
									<div data-placement="top" style="width: 70%;"
										aria-valuemax="100" aria-valuemin="0" aria-valuenow="1"
										role="progressbar" class="progress-bar progress-bar-success">
										<span class="sr-only">70% </span> <span class="progress-type">K592 (TAWAKAL 3, Kota Jakarta Barat)</span>
									</div>
								</div>
								<div class="progress">
									<div data-placement="top" style="width: 65%;"
										aria-valuemax="100" aria-valuemin="0" aria-valuenow="1"
										role="progressbar" class="progress-bar progress-bar-warning">
										<span class="sr-only">65%</span> <span class="progress-type">K307 (CITRA 2 EXT, Kota Jakarta Barat)</span>
									</div>
								</div>
								<div class="progress">
									<div data-placement="top" style="width: 60%;"
										aria-valuemax="100" aria-valuemin="0" aria-valuenow="60"
										role="progressbar" class="progress-bar progress-bar-warning">
										<span class="sr-only">60%</span> <span class="progress-type">K510 (ANGGREK UTAMA UJUNG, Kota Jakarta Barat)</span>
									</div>
								</div>
								

								<div class="progress-meter">
									<div style="width: 25%;" class="meter meter-left">
										<span class="meter-text">10 Users</span>
									</div>
									<div style="width: 25%;" class="meter meter-left">
										<span class="meter-text">100 Users</span>
									</div>
									<div style="width: 30%;" class="meter meter-left">
										<span class="meter-text">1000 Users</span>
									</div>
									<div style="width: 20%;" class="meter meter-left">
										<span class="meter-text">10000 Users</span>
									</div>
								</div>

							</a>

						</ul>
					</div>
					<div class="bs-callout bs-callout-danger">
						<h4>MILO Recognizing on Shelfing</h4>
						<table class="table table-striped table-responsive ">
							<thead>
								<tr>
									<th>Store ID</th>
									<th>Kabupaten/Kota</th>
									<th>Store Name</th>
									<th>Shelf Name</th>
									
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>K307</td>
									<td>Kota Jakarta Barat</td>
									<td>CITRA 2 EXT</td>
									<td>HOUSEHOLD CARE</td>
								</tr>
								<tr>
									<td>K307</td>
									<td>Kota Jakarta Barat</td>
									<td>CITRA 2 EXT</td>
									<td>FOOD</td>
								</tr>
								<tr>
									<td>K307</td>
									<td>Kota Jakarta Barat</td>
									<td>CITRA 2 EXT</td>
									<td>BEVERAGES</td>
								</tr>
								<tr>
									<td>K5910</td>
									<td>Kota Jakarta Barat</td>
									<td>DAV Control Station</td>
									<td>HOUSEHOLD CARE</td>
								</tr>
								<tr>
									<td>K5910</td>
									<td>Kota Jakarta Barat</td>
									<td>DAV Control Station</td>
									<td>FOOD</td>
								</tr>
								<tr>
									<td>K5910</td>
									<td>Kota Jakarta Barat</td>
									<td>DAV Control Station</td>
									<td>PERSONAL CARE & MISC</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>

			</div>
		</div>
		
		<div class="col-xs-12 col-sm-12 col-md-10 col-lg-12">
				<input type="checkbox" id="period"> Filter By Period
			</div>
	</div>
	


</div>
