<style>
	#map-canvas {
		height:400px;
		width: 96%;
		margin-left: 24px;
		padding: 0px;
	}
</style>
<script>
	$(document).ready(function() {
	  	$("#map-canvas").show();
	  	$("#full_map").click(function() {
	  		$("#map-canvas").slideUp(700);
		});
	});
</script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
<script>
function initialize() {
	//var myLatlng = new google.maps.LatLng(-6.175307, 106.826771);
	
	var LocationData = [
		<?php foreach ($geo as $val) { ?>
			[<?php echo $val->latitude; ?>, <?php echo $val->longitude; ?>, '<?php echo "<h3>(" . $val->store_id . ") " . $val->store_name . "</h3>"; ?>' ], 
		<?php } ?>
	];
				   
	
	var mapOptions = {
		zoom: 14,
		center: new google.maps.LatLng(LocationData[0][1], LocationData[0][2]),
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
		
	var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
	var bounds = new google.maps.LatLngBounds();
	var infowindow = new google.maps.InfoWindow();

	for (var i in LocationData)
	{
		var p = LocationData[i];
		var latlng = new google.maps.LatLng(p[0], p[1]);
		bounds.extend(latlng);

		var marker = new google.maps.Marker({
			position: latlng,
			map: map,
			title: p[2],
			icon: 'location_marker.png'
		});

		google.maps.event.addListener(marker, 'click', (function(mm, tt) {
		return function() {
				infowindow.setContent(tt);
				infowindow.open(map, mm);
			}
			})(marker, p[2]));
	}
	map.fitBounds(bounds);
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>
<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-3">
				<div class="small_box bg-aqua">
					<div style="float: left;">
						<span style="font-size:xx-large; margin-left: 15px; color:#fff;" class="glyphicon"><?php echo $total_store;?></span>
						<p class="paragraph_box">Total of Store</p>
					</div>
					<div style="float: right; margin-right: 10px; margin-top: 5px; color: #00a2ca;">
						<i class="fa fa-location-arrow fa-5x"></i>
					</div>
					<a href="<?php echo base_url()."store";?>">
						<div class="small_box_more bg-small_box_aqua">More<span class="glyphicon glyphicon-circle-arrow-right"></span></div>
					</a>
				</div>
			</div>
			<div class="col-md-3">
				<div class="small_box bg-red">
					<div style="float: left;">
					<span style="font-size:xx-large; margin-left: 15px; color:#fff;" class="glyphicon">0</span> <span style="font-size: 15px; color: #fff;" >%</span>
					<p class="paragraph_box">Error Rate</p>
					</div>
					<div style="float: right; margin-right: 30px; margin-top: 10px; font-size:50px; color: #dc5e4b;">
						<i class="glyphicon glyphicon-stats"></i>
					</div>
					<a href="<?php echo $this->config->item('base_url')."/device";?>">
					<div class="small_box_more bg-small_box_red">More <span class="glyphicon glyphicon-circle-arrow-right"></span></div>
					</a>
				</div>
			</div>
			<div class="col-md-3">
				<div class="small_box bg-orange">
					<div style="float: left;">
					<span style="font-size:xx-large; margin-left: 15px; color:#fff;" class="glyphicon "><?php echo $total_device;?></span>
					<p class="paragraph_box">Total of Devices</p>
					</div>
					<div style="float: right; margin-right: 30px; margin-top: 8px; color: #da8c10;">
						<i class="fa fa-mobile-phone fa-5x"></i>
					</div>
					<a href="<?php echo $this->config->item('base_url')."/device";?>">
					<div class="small_box_more bg-small_box_orange" >More <span class="glyphicon glyphicon-circle-arrow-right"></span></div>
					</a>
				</div>
			</div>
			<div class="col-md-3">
				<div class="small_box bg-green">
					<div style="float: left;">
					<span style="font-size:xx-large; margin-left: 15px; color:#fff;" class="glyphicon "><?php echo $total_target;?> </span>
					<p class="paragraph_box">Total of Target</p>
					</div>
					<div style="float: right; margin-right: 20px; margin-top: 10px; color: #009550;">
						<i class="fa fa-tablet fa-5x"></i>
					</div>
					<a href="<?php echo $this->config->item('base_url')."/target";?>">
					<div class="small_box_more bg-small_box_green">More <span class="glyphicon glyphicon-circle-arrow-right"></span></div>
					</a>
				</div>
			</div>
		</div>
	</div>
		&nbsp;&nbsp;
	<hr />
	<div id="map-canvas"></div>
	<br />
	<div style="padding-left: 25px;">
		<a href="<?php echo $this->config->item('base_url')."/map";?>" target="_blank" id="full_map"><button class="btn btn-success" type="button">Show Full Maps</button></a>
		<a href="<?php echo $this->config->item('base_url')."/home/log";?>"><button class="btn btn-success" type="button">Log Activity</button></a>
	</div>
	<br />
</div>
