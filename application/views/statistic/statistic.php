<script	src="<?php echo $this->config->item('base_url')."/assets/bootstrap/js/jquery.knob.min.js";?>"></script>
<script	src="<?php echo $this->config->item('base_url')."/assets/bootstrap/js/flot/jquery.flot.min.js";?>"></script>
<script	src="<?php echo $this->config->item('base_url')."/assets/bootstrap/js/flot/jquery.flot.pie.min.js";?>"></script>
<script	src="<?php echo $this->config->item('base_url')."/assets/bootstrap/js/flot/jquery.flot.resize.min.js";?>"></script>
<link rel="stylesheet" href="<?php echo $this->config->item('base_url')."/assets/bootstrap/css/extend.css" ;?>">


<script	src="<?php echo $this->config->item('base_url')."/assets/bootstrap/js/bootstrap-select.js";?>"></script>
<link rel="stylesheet" href="<?php echo $this->config->item('base_url')."/assets/bootstrap/css/bootstrap-select.css" ;?>">

<script type="text/javascript">
	$(document).ready(function() {
		$('.knob').knob(); 
		$("#device_number").val("0");
		$("#content_number").val("0");
		$("#interaction").val("0");
		$("#error").val("0");
		$(".selectpicker").change(function(){

			/*if ($('.selectpicker').val() == "1") {
				$("#device_number").val("1");
				$("#content_number").val("2");
				$("#interaction").val("6");
				$("#error").val("0");
			} else if ($('.selectpicker').val() == "2") {
				$("#device_number").val("1");
				$("#content_number").val("2");
				$("#interaction").val("10");
				$("#error").val("1");
			} else {
				$("#device_number").val("2");
				$("#content_number").val("4");
				$("#interaction").val("16");
				$("#error").val("1");
			}*/
			getTotalDevicePerLocation($('.selectpicker').val());
			getTotalContentPerLocation($('.selectpicker').val());
			getTotalInteractionPerLocation($('.selectpicker').val());
			//$("#device_number").val( totalDev );
			
		});
	});

	$(window).on('load', function () {

        $('.selectpicker').selectpicker({
            'selectedText': 'cat'
        });

        // $('.selectpicker').selectpicker('hide');
    });

	function getTotalDevicePerLocation(loc_id)
	{
	     $.ajax({
	         type: "POST",
	         url: "<?php echo $this->config->item('base_url').'/statistic/getTotalDevicePerLocation/'; ?>"+loc_id,
	         success: function(ret){
	             $("#device_number").val( ret );
	         },
	         error: function(x,e){
	             alert("error occur");
	             res = -1;
	         } 
	     });
	}

	function getTotalContentPerLocation(loc_id)
	{
	     $.ajax({
	         type: "POST",
	         url: "<?php echo $this->config->item('base_url').'/statistic/getTotalContentPerLocation/'; ?>"+loc_id,
	         success: function(ret){
	             $("#content_number").val( ret );
	         },
	         error: function(x,e){
	             alert("error occur");
	             res = -1;
	         } 
	     });
	}

	function getTotalInteractionPerLocation(loc_id)
	{
	     $.ajax({
	         type: "POST",
	         url: "<?php echo $this->config->item('base_url').'/statistic/getTotalInteractionPerLocation/'; ?>"+loc_id,
	         success: function(ret){
	             $("#interaction").val( ret );
	         },
	         error: function(x,e){
	             alert("error occur");
	             res = -1;
	         } 
	     });
	}

    
</script>



<div class="span4">
	<div class="box-wrap">
		<div class="box-heading">
			<h4>
				<i class="icon-dashboard"></i> ON-SITE ACTIVITIES OVERVIEW :
			</h4>
		</div>

		<div class="box-inner">
			<div class="control-group">
				<div class="row-fluid">
					<table style="text-align: center; margin: 20px; "> 
						
						<tr>
							<td colspan="2" style="text-align: left; "> 
								<div class="col-lg-3" id="search-by" style="width: 150px; margin-left: -15px;">
									<select class="selectpicker" id="by" name="by">
										  <option value="">Store Location</option>
										  <?php foreach ($alamat as $val) {?>
										  <option value="<?php echo $val->id;?>"><?php echo $val->alamat;?> </option>
										  <?php } ?>
									</select>
									<br /><br />
								</div>
							</td>
							
						</tr>
						
						<tr>
							<td>
								<div class="span4">
					              <input type="text" class="input-small knob" data-min="1" id="device_number" value="2"
					              	data-max="10" data-width="80" data-height="80" data-thickness=".2" data-fgColor="#4D3A7D" data-angleOffset="90" data-cursor="true" />
					            </div>
							</td>
							
							
							<td style="padding-left: 20px;"> 
								<div class="span4">
									<input type="text" class="input-small knob" id="content_number" value="5"
										data-min="0" data-max="100" data-width="80" data-height="80"
										data-thickness=".2" data-fgColor="#DF6E1E"
										data-displayPrevious="true" data-angleArc="250"
										data-angleOffset="-125" />
								</div>
							</td>
							
							<td style="padding-left: 20px;">  
								<div class="span4">
									<input type="text" class="input-small knob" id="interaction" value="15"
										data-min="0" data-max="100" data-step="10" data-width="80"
										data-height="80" data-thickness=".2" data-fgColor="#2091CF" />
								</div>
							</td>
							
							<td style="padding-left: 20px;">  
								<div class="span4">
									<input type="text" class="input-small knob" id="error" value="1.5"
										data-min="0" data-max="100" data-step="10" data-width="80"
										data-height="80" data-thickness=".2" data-fgColor="red" />
								</div>
							</td>
						
						</tr>
						
						<tr>
							<td style="padding-left: 20px;"> <a href="#" style="text-decoration: none;">Total number of Devices </a></td>
							<td style="padding-left: 20px;"><a href="#" style="text-decoration: none;">Total number of Contents </a></td>
							<td style="padding-left: 20px;"> <a href="#" style="text-decoration: none;">Total number of interactions </a></td>
							<td style="padding-left: 20px;"> <a href="#" style="text-decoration: none;">Error Rate (%) </a></td>
						</tr>
					</table>

				</div>
			</div>
		</div>
	</div>
</div>
<br />

<div class="row-fluid">
	<div class="span6">
		<div class="box-wrap">
			<div class="box-heading">
				<h4>
					<i class="icon-signal"></i> TOTAL ACTIVITY
				</h4>
			</div>
			<div class="box-inner">
				<div class="row-fluid">
					<div class="widget-main">
						<table style="width: 100%">
							<tr>
								<td width="50%">
									<div id="piechart-placeholder"></div>
								</td>
								<td width="50%">
									<div id="piechart-placeholder-2"></div>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br />
	
	
	
	
</div>



<script type="text/javascript">
      $(function() {  
        var data = [
	       
	        { label: "Chips",  data: 50, color: "yellow"},
	        { label: "DANCOW",  data: 40, color: "red"}
	        
        ];

        var data2 =  [
          	        { label: "BAD",  data: 0, color: "red"},
        	        { label: "GOOD",  data: 100, color: "green"}
        	        
                ];
        
        var placeholder = $('#piechart-placeholder').css({'width':'40%' , 'min-height':'150px'});
        var placeholder_2 = $('#piechart-placeholder-2').css({'width':'40%' , 'min-height':'150px'});
        
        $.plot(placeholder, data, {
        
        series: {
            pie: {
                  show: true,
            tilt:0.8,
            highlight: {
              opacity: 0.25
            },
            stroke: {
              color: '#fff',
              width: 2
            },
            startAngle: 2
            
              }
          },
          legend: {
              show: true,
          position: "ne", 
            labelBoxBorderColor: null,
          margin:[-30,15]
          }
        ,
        grid: {
          hoverable: true,
          clickable: true
        },
        
        tooltip: true, //activate tooltip
        tooltipOpts: {
          content: "%s : %y.1",
          shifts: {
            x: -30,
            y: -50
          }
        }
        
       });

        $.plot(placeholder_2, data2, {
            
            series: {
                pie: {
                      show: true,
                tilt:0.8,
                highlight: {
                  opacity: 0.25
                },
                stroke: {
                  color: '#fff',
                  width: 2
                },
                startAngle: 2
                
                  }
              },
              legend: {
                  show: true,
              position: "ne", 
                labelBoxBorderColor: null,
              margin:[-30,15]
              }
            ,
            grid: {
              hoverable: true,
              clickable: true
            },
            
            tooltip: true, //activate tooltip
            tooltipOpts: {
              content: "%s : %y.1",
              shifts: {
                x: -30,
                y: -50
              }
            }
            
           });
      
       
        var $tooltip = $("<div class='tooltip top in' style='display:none;'><div class='tooltip-inner'></div></div>").appendTo('body');
        placeholder.data('tooltip', $tooltip);
        var previousPoint = null;
      
        placeholder.on('plothover', function (event, pos, item) {
        if(item) {
          if (previousPoint != item.seriesIndex) {
            previousPoint = item.seriesIndex;
            var tip = item.series['label'] + " : " + item.series['percent']+'%';
            $(this).data('tooltip').show().children(0).text(tip);
          }
          $(this).data('tooltip').css({top:pos.pageY + 10, left:pos.pageX + 10});
        } else {
          $(this).data('tooltip').hide();
          previousPoint = null;
        }
        
       });
      
      
        var d1 = [];
        for (var i = 0; i < Math.PI * 2; i += 0.5) {
          d1.push([i, Math.sin(i)]);
        }
      
        var d2 = [];
        for (var i = 0; i < Math.PI * 2; i += 0.5) {
          d2.push([i, Math.cos(i)]);
        }
      
        var d3 = [];
        for (var i = 0; i < Math.PI * 2; i += 0.2) {
          d3.push([i, Math.tan(i)]);
        }
        
      
      

        
        
      });
    </script>
