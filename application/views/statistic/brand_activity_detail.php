
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="<?php echo $this->config->item('assets')."/chart/Chart.js" ?>"></script>
<script src="<?php echo $this->config->item('highcharts')."/js/highcharts.js" ?>"></script>
<script src="<?php echo $this->config->item('highcharts')."/js/modules/data.js" ?>"></script>
<script src="<?php echo $this->config->item('highcharts')."/js/modules/exporting.js" ?>"></script>
<!-- Additional files for the Highslide popup effect -->
<script type="text/javascript" src="http://www.highcharts.com/media/com_demo/highslide-full.min.js"></script>
<script type="text/javascript" src="http://www.highcharts.com/media/com_demo/highslide.config.js" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="http://www.highcharts.com/media/com_demo/highslide.css" />

<script>

$(document).ready(function() {
	// Handler for .ready() called.
	$("#box1").show();
	$("#box2").hide();
	$("#box3").hide();
	$("#region").hide();
	$('#region_checkbox').click(function() {
	    $("#region").toggle(this.checked);
	});
	
	$("#menu1").click(function(){
		$("#box1").fadeIn();
		$("#box2").fadeOut();
		$("#box3").fadeOut();
	});

	$("#menu2").click(function(){
		$("#box1").fadeOut();
		$("#box2").fadeIn();
		$("#box3").fadeOut();
	});
	
	$("#menu3").click(function(){
		$("#box1").fadeOut();
		$("#box2").fadeOut();
		$("#box3").fadeIn();
	});
	
});
	
	

	
</script>
<script type="text/javascript">
$(function () {

    // Get the CSV and create the chart
   $(function () { 
        
        $('#container').highcharts({
			
        	 chart: {
                 type: 'line'
             },
             title: {
                 text: 'Brand & Product comparison'
             },
             xAxis: {
                 categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
             },
             yAxis: [{ // left y axis
                 title: {
                     text: null
                 },
                 labels: {
                     align: 'left',
                     x: 3,
                     y: 16,
                     format: '{value:.,0f}'
                 },
                 
                 showFirstLabel: false
             }, { // right y axis
                 linkedTo: 0,
                 gridLineWidth: 0,
                 opposite: true,
                 title: {
                     text: null
                 },
                 labels: {
                     align: 'right',
                     x: -3,
                     y: 16,
                     format: '{value:.,0f}'
                 },
                 showFirstLabel: false
             }],
             legend: {
                 align: 'left',
                 verticalAlign: 'bottom',
                 y: 15,
                 floating: true,
                 borderWidth: 0
             },

             tooltip: {
                 shared: true,
                 crosshairs: true
             },

             plotOptions: {
                 series: {
                     cursor: 'pointer',
                     point: {
                         events: {
                             click: function (e) {
                                 hs.htmlExpand(null, {
                                     pageOrigin: {
                                         x: e.pageX,
                                         y: e.pageY
                                     },
                                     headingText: this.series.name,
                                     maincontentText: Highcharts.dateFormat('%A, %b %e, %Y', this.x) +':<br/> '+
                                         this.y +' visits',
                                     width: 200
                                 });
                             }
                         }
                     },
                     marker: {
                         lineWidth: 1
                     }
                 }
             },
             series: [{
                 name: 'Coca-Cola Company (Freshtea)',
                 data: [-0.9, 0.6, 3.5, 8.4, 13.5, 17.0, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0],
             	 visible: true,
             	 id: "product1"
             }]
        });
    });

});



$('.highcharts-legend-item').click(function() {
    chart.xAxis[0].setCategories(['One', 'Two', 'Three', 'Three', 'Three', 'Three','Three' ,'Three','Three','Three','Three','Three']);
});

$(function () {
    $('#container-3').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 1,//null,
            plotShadow: false
        },
        title: {
            text: 'Current Device Status'
        },
        tooltip: {
    	    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Good Device ',
            data: [
                ['Good',   90],
                ['Bad',    10],
               
            ]
        }]
    });
});
		</script>
<style>
.btn {
	margin-top: 4px;
	
}

.btn-xs {
	font-weight: 300;
}

.btn-hot {
	color: #fff;
	background-color: #db5566;
	border-bottom: 2px solid #af4451;
}

.btn-hot:hover,.btn-sky.active:focus,.btn-hot:focus,.open>.dropdown-toggle.btn-hot
	{
	color: #fff;
	background-color: #df6a78;
	border-bottom: 2px solid #b25560;
	outline: none;
}

.btn-hot:active,.btn-hot.active {
	color: #fff;
	background-color: #c04b59;
	border-top: 2px solid #9a3c47;
	margin-top: 2px;
}

.btn-sunny {
	color: #fff;
	background-color: #f4ad49;
	border-bottom: 2px solid #c38a3a;
}

.btn-sunny:hover,.btn-sky.active:focus,.btn-sunny:focus,.open>.dropdown-toggle.btn-sunny
	{
	color: #fff;
	background-color: #f5b75f;
	border-bottom: 2px solid #c4924c;
	outline: none;
}

.btn-sunny:active,.btn-sunny.active {
	color: #fff;
	background-color: #d69840;
	border-top: 2px solid #ab7a33;
	margin-top: 2px;
}

.btn-fresh {
	color: #fff;
	background-color: #51bf87;
	border-bottom: 2px solid #41996c;
}

.btn-fresh:hover,.btn-sky.active:focus,.btn-fresh:focus,.open>.dropdown-toggle.btn-fresh
	{
	color: #fff;
	background-color: #66c796;
	border-bottom: 2px solid #529f78;
	outline: none;
}

.btn-fresh:active,.btn-fresh.active {
	color: #fff;
	background-color: #47a877;
	border-top: 2px solid #39865f;
	outline: none;
	outline-offset: none;
	margin-top: 2px;
}

.btn-sky {
	color: #fff;
	background-color: #0bacd3;
	border-bottom: 2px solid #098aa9;
}

	.btn-sky:hover,.btn-sky.active:focus,.btn-sky:focus,.open>.dropdown-toggle.btn-sky
		{
		color: #fff;
		background-color: #29b6d8;
		border-bottom: 2px solid #2192ad;
		outline: none;
	}

	.btn-sky:active,.btn-sky.active {
		color: #fff;
		background-color: #0a97b9;
		border-top: 2px solid #087994;
		outline-offset: none;
		margin-top: 2px;
	}

	.btn:focus,.btn:active:focus,.btn.active:focus {
		outline: none;
		outline-offset: 0px;
	}
	
	#sub_container {
		border: 0px solid black;
		text-align: center;
	}
.view_all {
	color: #29b6d8;
	text-align: center;
	font-size: 15px;
	cursor: pointer;
}

.view_all:hover {
	color:#2391ab;
}


</style>

<div class="wrapper" >
	<div class="row">
		<div class="text-left" style="padding-left: 25px;">
			<button type="button" id="menu1" class="btn btn-sky text-uppercase btn-xs">Brand Interaction</button>
			<!-- <button type="button" id="menu2" class="btn btn-sky text-uppercase btn-xs">Graphic Activity Installed</button>  -->
			<!-- <button type="button" id="menu3" class="btn btn-sky text-uppercase btn-xs">Device status</button> -->
		</div>
	</div>
	<br />
	<div id="box1" class="col-md-12">
		<div id="sub_container">
			
			<table width="100%" border=0>
				<tr>
					<td style="text-align: left;"> 
						<h3> Brand : <?php echo $brand_activity[0]['brand_name'];?> (<?php echo $brand_activity[0]['product_name'];?>)</h3>
						
						<table class="table table-list-search">
		                    <thead style="background-color: #29b6d8;">
		                        <tr style="color: white;">
		                            <th>Location (Store ID)</th>
		                            <th>Store Name</th>
		                            <th>Provinsi</th>
		                            <th style="text-align: center;">Total Interactions</th>
		                            <th style="text-align: center;">More Details</th>
		                          	
		                        </tr>
		                    </thead>
		                    <tbody>
		                    	<?php foreach ($brand_activity as $val) {?>
		                        <tr>
		                            <td><?php echo $val['store_id'];?></td>
		                            <td><?php echo $val['store_name'];?></td>
		                            <td><?php echo $val['province'];?></td>
		                            <td style="text-align: center;"><?php echo "<b>".$val['total_reco']."</b> Times Recoginition";?></td>
		                            <td style="text-align: center;">
		                            	<a href="<?php echo base_url()."statistic/brand_activity_more_detail/".$val['product_id']."_".$val['variant_id']."_".$val['store_id']; ?>"><span class="glyphicon glyphicon-tasks" ></span></a>
		                            	<a href="<?php echo base_url()."store/geolocation/".$val['store_id']; ?>"><span class="glyphicon glyphicon-map-marker" ></span></a>
		                            </td>
		                        </tr>
		                        <?php } ?>
		                        
		                    </tbody>
		                </table>   
					</td>
				</tr>
				<!-- 
				<tr>
					<td colspan="2">
						<div style="width:100%;" >
							<div>
								<div id="container" style="min-width: 800px; height: 300px; margin: 0 auto"></div>
							</div>
						</div>
				
						
					</td>
				</tr>
				 -->
			</table>
			<br />
			<div class="form-group">
			
				<!-- 
				<div class="checkbox" style="text-align: left; padding-left: 30px;">
			    	<label>
			      		<input type="checkbox" id="region_checkbox"> Show by region
			    	</label>
			 	</div>
			 	 -->
			    
				
			</div>
			
		</div>


	</div>

	<div id="box2">ssss</div>

	<div id="box3">
		<div id="container-3" style="min-width: 700px; height: 300px; margin: 0 auto"></div>
	</div>
</div>
