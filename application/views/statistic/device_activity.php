
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="<?php echo $this->config->item('assets')."/chart/Chart.js" ?>"></script>
<script src="<?php echo $this->config->item('highcharts')."/js/highcharts.js" ?>"></script>
<script src="<?php echo $this->config->item('highcharts')."/js/modules/data.js" ?>"></script>
<script src="<?php echo $this->config->item('highcharts')."/js/modules/exporting.js" ?>"></script>
<!-- Additional files for the Highslide popup effect -->
<script type="text/javascript" src="http://www.highcharts.com/media/com_demo/highslide-full.min.js"></script>
<script type="text/javascript" src="http://www.highcharts.com/media/com_demo/highslide.config.js" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="http://www.highcharts.com/media/com_demo/highslide.css" />

<script>

$(document).ready(function() {
	// Handler for .ready() called.
	$("#box1").show();
	$("#box2").hide();
	$("#box3").hide();
	$("#region").hide();
	$('#region_checkbox').click(function() {
	    $("#region").toggle(this.checked);
	});
	$("#spinner").hide();
	$("#menu1").click(function(){
		$("#box1").fadeIn();
		$("#box2").fadeOut();
		$("#box3").fadeOut();
	});

	$("#menu2").click(function(){
		$("#box1").fadeOut();
		$("#box2").fadeIn();
		$("#box3").fadeOut();
	});
	
	$("#menu3").click(function(){
		$("#box1").fadeOut();
		$("#box2").fadeOut();
		$("#box3").fadeIn();
	});
	
	$("#prop").change(function () {
		var provinsi_id = $("#prop").val();
		var arr = {};
		arr.provinsi_id = provinsi_id;
		
		$.ajax({
			type: "POST",
			url:"getDeviceData",
			data: arr,
			beforeSend: function(){
				 $(".table_devices").hide();
                 $("#spinner").show();
             },
			success:function(result)
			{
		    	$(".table_devices").replaceWith(result);
		    	$("#spinner").hide();
		    	//alert(result);
		  	}
		 });
    });

	
	$(".detail").click(function(){
		event.preventDefault();
		$("#region_checkbox").attr("disabled", true);
		$("#prop").attr("disabled", true);
		$("#title").remove();
		$("#desc").remove();
		$("#total_device").remove();
		
		var provinsi_name = $('.parameter').attr('href');
		var arr = {};
		arr.provinsi_name = provinsi_name;
		$.ajax({
			type: "POST",
			url:"getDeviceData_detail",
			data: arr,
			beforeSend: function(){
				 $(".table-list-search").hide();
                 $("#spinner").show();
             },
			success:function(result)
			{
		    	$(".table-list-search").replaceWith(result);
		    	$("#spinner").hide();
		    	//alert(result);
		  	}
		 });
	});
    
});
	
</script>

<style>
.btn {
	margin-top: 4px;
	
}

.btn-xs {
	font-weight: 300;
}

.btn-hot {
	color: #fff;
	background-color: #db5566;
	border-bottom: 2px solid #af4451;
}

.btn-hot:hover,.btn-sky.active:focus,.btn-hot:focus,.open>.dropdown-toggle.btn-hot
	{
	color: #fff;
	background-color: #df6a78;
	border-bottom: 2px solid #b25560;
	outline: none;
}

.btn-hot:active,.btn-hot.active {
	color: #fff;
	background-color: #c04b59;
	border-top: 2px solid #9a3c47;
	margin-top: 2px;
}

.btn-sunny {
	color: #fff;
	background-color: #f4ad49;
	border-bottom: 2px solid #c38a3a;
}

.btn-sunny:hover,.btn-sky.active:focus,.btn-sunny:focus,.open>.dropdown-toggle.btn-sunny
	{
	color: #fff;
	background-color: #f5b75f;
	border-bottom: 2px solid #c4924c;
	outline: none;
}

.btn-sunny:active,.btn-sunny.active {
	color: #fff;
	background-color: #d69840;
	border-top: 2px solid #ab7a33;
	margin-top: 2px;
}

.btn-fresh {
	color: #fff;
	background-color: #51bf87;
	border-bottom: 2px solid #41996c;
}

.btn-fresh:hover,.btn-sky.active:focus,.btn-fresh:focus,.open>.dropdown-toggle.btn-fresh
	{
	color: #fff;
	background-color: #66c796;
	border-bottom: 2px solid #529f78;
	outline: none;
}

.btn-fresh:active,.btn-fresh.active {
	color: #fff;
	background-color: #47a877;
	border-top: 2px solid #39865f;
	outline: none;
	outline-offset: none;
	margin-top: 2px;
}

.btn-sky {
	color: #fff;
	background-color: #0bacd3;
	border-bottom: 2px solid #098aa9;
}

	.btn-sky:hover,.btn-sky.active:focus,.btn-sky:focus,.open>.dropdown-toggle.btn-sky
		{
		color: #fff;
		background-color: #29b6d8;
		border-bottom: 2px solid #2192ad;
		outline: none;
	}

	.btn-sky:active,.btn-sky.active {
		color: #fff;
		background-color: #0a97b9;
		border-top: 2px solid #087994;
		outline-offset: none;
		margin-top: 2px;
	}

	.btn:focus,.btn:active:focus,.btn.active:focus {
		outline: none;
		outline-offset: 0px;
	}
	
	#sub_container {
		border: 0px solid black;
		text-align: center;
	}
}
</style>

<?php 

$i = 0;
$result = array();
//print "<pre>";
//print_r($store_locations);
//print "</pre>";
$jan=0;$feb=0;$mar=0;$apr=0;$may=0;$jun=0;$jul=0;$aug=0;$sep=0;$oct=0;$nov=0;$dec=0;
$condition_1=0; $condition_2=0;
foreach ($store_locations as $data) {
	$id = $data->provinsi;
	if (isset($result[$id])) {
		$result[$id][] = $data;
		
		date_default_timezone_set('Asia/Jakarta');
		$date = $data->device_date_added;
		$dt = new DateTime($date);
		$month = $dt->format('M');
		
		if ($month == "Jan") {
			$jan = $jan + 1;
		} else if ($month == "Feb") {
			$feb = $feb + 1;
		} else if ($month == "Mar") {
			$mar = $mar + 1;
		} else if ($month == "Apr") {
			$apr = $apr + 1;
		} else if ($month == "May") {
			$may = $may + 1;
		} else if ($month == "Jun") {
			$jun = $jun + 1;
		} else if ($month == "Jul") {
			$jul = $jul + 1;
		} else if ($month == "Aug") {
			$aug = $aug + 1;
		} else if ($month == "Sep") {		
			$sep = $sep + 1;
		} else if ($month == "Oct") {
			$oct = $oct + 1;
		} else if ($month == "Nov") {
			$nov = $nov + 1;
		} else if ($month == "Dec") {
			$dec = $dec + 1;
		}
		if($data->status == "good") {
			$condition_1 = $condition_1 + 1;
		} else if($data->status == "good") {
			$condition_2 = $condition_2 + 1;
		}
		
	} else {
		$result[$id] = array($data);
		date_default_timezone_set('Asia/Jakarta');
		$date = $data->device_date_added;
		
		$dt = new DateTime($date);
		$month = $dt->format('M');
		if ($month == "Jan") {
			$jan = $jan + 1;
		} else if ($month == "Feb") {
			$feb = $feb + 1;
		} else if ($month == "Mar") {
			$mar = $mar + 1;
		} else if ($month == "Apr") {
			$apr = $apr + 1;
		} else if ($month == "May") {
			$may = $may + 1;
		} else if ($month == "Jun") {
			$jun = $jun + 1;
		} else if ($month == "Jul") {
			$jul = $jul + 1;
		} else if ($month == "Aug") {
			$aug = $aug + 1;
		} else if ($month == "Sep") {		
			$sep = $sep + 1;
		} else if ($month == "Oct") {
			$oct = $oct + 1;
		} else if ($month == "Nov") {
			$nov = $nov + 1;
		} else if ($month == "Dec") {
			$dec = $dec + 1;
		}
		if($data->status == "good") {
			$condition_1 = $condition_1 + 1;
		} else if($data->status == "good") {
			$condition_2 = $condition_2 + 1;
		}
		
	}
	
}
//echo $condition_1;
	
//  print "<pre>";
//  print_r($store_locations);
//  print "</pre>";
?>
<script type="text/javascript">
$(function () {

   // Get the CSV and create the chart
   $(function () { 
        
        $('#container').highcharts({
			
        	 chart: {
                 type: 'line'
             },
             title: {
                 text: 'Device Installation Progress'
             },
             xAxis: {
                 categories: [<?php echo "'Jan' , 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'" ?>]
             },
            
             legend: {
                 align: 'left',
                 verticalAlign: 'bottom',
                 y: 15,
                 x: 50,
                 floating: true,
                 borderWidth: 0
             },

             tooltip: {
                 shared: true,
                 crosshairs: true
             },

             plotOptions: {
                 series: {
                     cursor: 'pointer',
                     point: {
                         events: {
                             click: function (e) {
                                 hs.htmlExpand(null, {
                                     pageOrigin: {
                                         x: e.pageX,
                                         y: e.pageY
                                     },
                                     headingText: this.series.name,
                                     maincontentText: 
                                         this.y +' Devices Installed',
                                     width: 200
                                 });
                             }
                         }
                     },
                     marker: {
                         lineWidth: 1
                     }
                 }
             },
             series: [{
                 name: 'Monthly',
                 data: [<?php echo $jan.",".$feb.",".$mar.",".$apr.",".$may.",".$jun.",".$jul.",".$aug.",".$sep.",".$oct.",".$nov.",".$dec;?>],
             	 visible: true,
             	 id: "monthly"
             }]
        });
    });

});



$('.highcharts-legend-item').click(function() {
    chart.xAxis[0].setCategories(['One', 'Two', 'Three', 'Three', 'Three', 'Three','Three' ,'Three','Three','Three','Three','Three']);
});

$(function () {
    $('#container-3').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 1,//null,
            plotShadow: false
        },
        title: {
            text: 'Current Device Status'
        },
        tooltip: {
    	    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Device Condition',
            data: [
                ['Good',   100],
                ['Bad',    0],
            ]
        }]
    });
});
		</script>
<div class="wrapper" >
	<div class="row">
		<div class="text-left" style="padding-left: 25px;">
			<button type="button" id="menu1" class="btn btn-sky text-uppercase btn-xs">On site device installation progress</button>
			<!--  <button type="button" id="menu2" class="btn btn-sky text-uppercase btn-xs">Graphic Activity Installed</button>  -->
			<!-- <button type="button" id="menu3" class="btn btn-sky text-uppercase btn-xs">Device status</button> --> 
			<button type="button" id="menu3" class="btn btn-sky text-uppercase btn-xs">Device Activity Log</button>
		</div>
	</div>
	<br />
	<div id="box1" class="col-md-12">
		<div id="sub_container">
			
			<table border=0>
				<tr>
					<td width="20%">
						<div id="title">
							<h2>Right now</h2>
						</div>
						<div id="total_device">
							<h1><a style="text-decoration: none;" href="<?php echo base_url('device');?>"><?php echo $totalDevices;?></a></h1>
						</div>
						<div id="desc">
							devices installed
						</div>
					</td>
					<td style="text-align: left;"> 
						<div id="most_device_installed">
							<h3> Most device installed on region</h3>
							<div class="form-group">
									<div class="checkbox" style="text-align: left; padding-left: 22px;">
								    	<label>
								      		<input type="checkbox" id="region_checkbox"> Filter by region
								    	</label>
								 	</div>
								    <table id="region">
										<tr>
										
											<td style="padding-left: 3px;">
												<script type="text/javascript" src="<?php echo $this->config->item('base_url')."/assets/ajax_kota.js"?>"></script>
												<select name="prop" id="prop" class="form-control">
													<option value="">Pilih Provinsi</option>
													<?php 
													$queryProvinsi=mysql_query("SELECT * FROM `area_provinsi` order by nama");
													while ($dataProvinsi=mysql_fetch_array($queryProvinsi)){
														echo '<option value="'.$dataProvinsi['id'].'">'.$dataProvinsi['nama'].'</option>';
													}
													?>
												<select>
											</td>
										</tr>
										
									</table>
								
								
							</div>
							<table class="table table-list-search">
			                    <thead style="background-color: #29b6d8;">
			                        <tr style="color: white;">
			                            <th>Provinsi</th>
			                            <th style="text-align: center;">Total Device Assigned</th>
			                            <th style="text-align: center;">Detail</th>
			                        </tr>
			                    </thead>
			                    <tbody>
			                    <?php 
								foreach (array_keys($result) as $val) { ?>
			                        <tr class="table_devices">
			                            <td><?php echo strtoupper($val);
			                            //echo $result[$val][$i]->store_name;
			                            ?></td>
			                            <td  style="text-align: center;"><?php echo count($result[$val]); ?></td>
			                            <td style="text-align: center;"><a class="parameter" href="<?php echo $result[$val][$i]->provinsi; ?>"><div class="detail glyphicon glyphicon-tasks"></div></a></td>
			                        </tr>
			                     <?php }?>
			                     	<tr>
			                     		<td colspan="3" style="text-align: center;"><div id="spinner"><img src="<?php echo $this->config->item("assets")."/img/ajax-loader.gif"?>" /> </div></td>
			                     	</tr>
			                    </tbody>
			                </table>   
		                </div>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<div style="width:100%;" >
							<div>
								<div id="container" style="min-width: 700px; height: 300px; margin: 0 auto"></div>
							</div>
						</div>
					</td>
				</tr>
				
			</table>
			<br />
			
			
		</div>


	</div>
	<div id="box3" style="padding-left: 10px;">
		<table class="table table-list-search"">
                    <thead style="background-color: #29b6d8;">
                        <tr style="color: white;">
                            <th style="text-align: center;">Store Name</th>
                            <th style="text-align: center;">Shelf Name</th>
                            <th style="text-align: center;">Mobile Version</th>
                           
                            <!-- <th style="text-align: center;">Serial Number</th> -->
                            <th style="text-align: center;">Time Installed</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
						foreach ($device_activity_log as $val) { ?>
                        <tr class="table_devices">
                            <td  style="text-align: center;"><?php echo $val->store_name; ?></td>
                            <td  style="text-align: center;"><?php echo $val->shelf_name; ?></td>
                            
                            <td  style="text-align: center;"><?php echo $val->mobile_version_id; ?></td>
                            <!--<td  style="text-align: center;"><?php echo $val->serial_number; ?></td>-->
                            <td  style="text-align: center;"><?php echo $val->datetime; ?></td>
                        </tr>
                     <?php }?>
                     
                    </tbody>
                </table>   
	</div>
	
</div>
