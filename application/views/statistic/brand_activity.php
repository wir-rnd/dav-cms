
<script>

$(document).ready(function() {
	// Handler for .ready() called.
	$("#box1").show();
	$("#box2").hide();
	$("#box3").hide();
	$("#region").hide();

	$('.all_brand').hide();
	
	$('#region_checkbox').click(function() {
	    $("#region").toggle(this.checked);
	});
	
	$("#menu1").click(function(){
		$("#box1").fadeIn();
		$("#box2").fadeOut();
		$("#box3").fadeOut();
	});

	$("#menu2").click(function(){
		
		$("#box1").fadeOut();
		$("#box2").fadeIn();
		$("#box3").fadeOut();
	});
	
	$("#menu3").click(function(){
		$("#box1").fadeOut();
		$("#box2").fadeOut();
		$("#box3").fadeIn();
	});

	$(".view_all").click(function(){
		$(".all_brand").show();
		$(".view_all").hide();
		$("#container").hide();
	});
});
	
	
</script>

<style>
.btn {
	margin-top: 4px;
	
}

.btn-xs {
	font-weight: 300;
}

.btn-hot {
	color: #fff;
	background-color: #db5566;
	border-bottom: 2px solid #af4451;
}

.btn-hot:hover,.btn-sky.active:focus,.btn-hot:focus,.open>.dropdown-toggle.btn-hot
	{
	color: #fff;
	background-color: #df6a78;
	border-bottom: 2px solid #b25560;
	outline: none;
}

.btn-hot:active,.btn-hot.active {
	color: #fff;
	background-color: #c04b59;
	border-top: 2px solid #9a3c47;
	margin-top: 2px;
}

.btn-sunny {
	color: #fff;
	background-color: #f4ad49;
	border-bottom: 2px solid #c38a3a;
}

.btn-sunny:hover,.btn-sky.active:focus,.btn-sunny:focus,.open>.dropdown-toggle.btn-sunny
	{
	color: #fff;
	background-color: #f5b75f;
	border-bottom: 2px solid #c4924c;
	outline: none;
}

.btn-sunny:active,.btn-sunny.active {
	color: #fff;
	background-color: #d69840;
	border-top: 2px solid #ab7a33;
	margin-top: 2px;
}

.btn-fresh {
	color: #fff;
	background-color: #51bf87;
	border-bottom: 2px solid #41996c;
}

.btn-fresh:hover,.btn-sky.active:focus,.btn-fresh:focus,.open>.dropdown-toggle.btn-fresh
	{
	color: #fff;
	background-color: #66c796;
	border-bottom: 2px solid #529f78;
	outline: none;
}

.btn-fresh:active,.btn-fresh.active {
	color: #fff;
	background-color: #47a877;
	border-top: 2px solid #39865f;
	outline: none;
	outline-offset: none;
	margin-top: 2px;
}

.btn-sky {
	color: #fff;
	background-color: #0bacd3;
	border-bottom: 2px solid #098aa9;
}

	.btn-sky:hover,.btn-sky.active:focus,.btn-sky:focus,.open>.dropdown-toggle.btn-sky
		{
		color: #fff;
		background-color: #29b6d8;
		border-bottom: 2px solid #2192ad;
		outline: none;
	}

	.btn-sky:active,.btn-sky.active {
		color: #fff;
		background-color: #0a97b9;
		border-top: 2px solid #087994;
		outline-offset: none;
		margin-top: 2px;
	}

	.btn:focus,.btn:active:focus,.btn.active:focus {
		outline: none;
		outline-offset: 0px;
	}
	
	#sub_container {
		border: 0px solid black;
		text-align: center;
	}
.view_all {
	color: #29b6d8;
	text-align: center;
	font-size: 15px;
	cursor: pointer;
}

.view_all:hover {
	color:#2391ab;
}


</style>

<?php 
/*
foreach ($brand_activity_data as $val) {
	$post[] = $val->serial_number;	
}
$counts = array_count_values($post);

print "<pre>";
print_r($post);
print "</pre>";
echo $counts['e5566a9a76bd1a1530319dc8b1bfbfbc'];
*/


function sortByOrder($a, $b) {
	return $b['total_reco'] - $a['total_reco'];
}
if(isset($brand_activity_data)) {
	usort($brand_activity_data, 'sortByOrder');
}

?>

<div class="wrapper" >
	<div class="row">
		<div class="text-left" style="padding-left: 25px;">
			<button type="button" id="menu1" class="btn btn-sky text-uppercase btn-xs">Brand Interaction</button>
			<button type="button" id="menu2" class="btn btn-sky text-uppercase btn-xs">Store Interaction</button>
			<!-- <button type="button" id="menu3" class="btn btn-sky text-uppercase btn-xs">Device status</button> -->
		</div>
	</div>
	<br />
	<div id="box1" class="col-md-12">
		<div id="sub_container">
			<table>
				<tr>
					<td width="20%">
						<div id="title">
							<h2>Right now</h2>
						</div>
						<div id="total_device">
							<h1><a style="text-decoration: none;" href="<?php echo base_url('brand');?>"><?php echo $totalBrand; ?></a></h1>
						</div>
						<div id="desc">
							Brands on DAV
						</div>
					</td>
					<td>
						<h1>&</h1>
					</td>
					<td width="20%">
						<div id="title">
							<h2>Right now</h2>
						</div>
						<div id="total_device">
							<h1><a style="text-decoration: none;" href="<?php echo base_url('product');?>"><?php echo $totalProduct; ?></a></h1>
						</div>
						<div id="desc">
							Products on DAV
						</div>
					</td>
				</tr>
			</table>
			
			<table border=0 width="100%">
				<tr>
					<td style="text-align: left;"> 
						<h3> Most brands Interactions</h3>
						
						<table class="table table-list-search">
		                    <thead style="background-color: #29b6d8;">
		                        <tr style="color: white;">
		                            <th>Brand Name</th>
		                            <th>Product Name</th>
		                            <th>Variant</th>
		                            <th>Total Interactions</th>
		                            <th style="text-align: center;">Detail</th>
		                        </tr>
		                    </thead>
		                    
		                    <tbody>
		                    	<?php 
		                    	if(isset($brand_activity_data)) {
			                    	foreach ($brand_activity_data as $v) {
			                    		echo "<tr>";
			                    		echo "<td>".$v['brand_name']."</td>";
			                    		echo "<td>".$v['product_name']."</td>";
			                    		echo "<td>".$v['variant_name']."</td>";
			                    		echo "<td><b>".$v['total_reco']."</b> Times Recoginition</td>";
			                    		
		                    	?> 
		                    		<td style="text-align: center;"><a href="<?php echo base_url()."statistic/brand_activity_detail/".$v['product_id']."_".$v['variant_id']; ?>"><span class="glyphicon glyphicon-tasks" ></span></a>
								<?php
		                    		echo "</tr>";
		                    	}} else { 
		                    	?>
		                    		<td colspan="5" style="text-align: center;"> <b>NO ACTIVITY </b></td>
		                    	<?php }?>
		                       
		                    </tbody>
		                </table>   
					</td>
				</tr>
			
			</table>
			<br />
		</div>


	</div>

	
	<div id="box2">
		stest
	</div>
	<div id="box3">
		<div id="container-3" style="min-width: 700px; height: 300px; margin: 0 auto"></div>
	</div>
</div>
