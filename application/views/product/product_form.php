
	<div class="modal-body">
		<div class="form-group">
			<div class="row">    	
				<div class="col-md-11">
					<label for="name">Brand</label><br />
						<select class="form-control" name="brand_id">
							<?php foreach($brand as $val) { ?>
								<option value="<?php echo $val->id; ?>"><?php echo $val->name; ?></option>
							<?php } ?>
						</select>
				</div>
			</div>
			<div class="row">    	
				<div class="col-md-11">
					<label for="name">Product Name</label>
						<input type="text" class="form-control" name="name" placeholder="Brand Name" /> 
				</div>
			</div>
			<div class="row">    	
				<div class="col-md-11">
					<label for="userfile">Product Image</label>
						<input style="margin-bottom: 5px;" type="file" name="userfile" id="userfile" size="20" />
				</div>
			</div>
		</div>
		<br />
		<button class="btn btn-success" type="submit">Submit</button>
		<input class="btn btn-primary" data-dismiss="modal" id="cancel" type="reset" value="Cancel" />
	</div>