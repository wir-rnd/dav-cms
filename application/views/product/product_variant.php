<script type="text/javascript">
	$(document).ready(function() {
		//alert('asdasd');
		$('#targetdb').trigger('onchange');
	});
	
	/*function getTargetByTargetDBId(id)
	{
		$.ajax({
			type: "POST",
			url: "<?php echo base_url().'target/ajax_get_target_by_targetdb_id/'; ?>" + id,
			success:function(result)
			{
				var html = '';
				$('#target').html('');
				for(var i=0;i<result.length;i++)
				{
					html += '<option value="'+ result[i].target_id +'">'+ result[i].name +'</option>';
				}
				$('#target').html(html);
			}
		});
	}*/
	
</script>
<div class="col-md-12">				
	<div>
		<a title="Add Widget" data-toggle="modal" href="#addWidgetModal" class="btn btn-primary ">Add Variant</a>
	</div>
	<br />
	<div class="panel panel-default">
            <table id="tbl" class="table table-striped paginated" border="0">
                <thead>
                    <tr>
                        <th style="text-align: center;">No</th>
                        <th style="text-align: center;">Variant Name</th>
						<th style="text-align: center;">Target Name</th>
                        <th style="text-align: center;">Action</th>
                    </tr>
                </thead>

                <?php if (isset($variant)) {  $no = 1; ?>
                <?php foreach($variant as $val) { ?>
                <tr>
                    <td style="text-align: center;"> <?php echo $no++; ?></td>
                    <td style="text-align: center;"> <?php echo $val->variant_name;?></td>
					<td style="text-align: center;"><a href="<?php echo base_url().'target/detail/' . $val->target_id; ?>"><?php echo $val->target_name;?></a></td>
                    <td style="vertical-align: middle; text-align: center;">
                        <div id = "test">
                        <p>
                            <a class="edit" href="<?php echo $this->config->item('base_url')."/product/delete_variant/". $id . "/" . $val->id;?>" onclick="javascript: return confirm('Are you SURE you want to delete these item?')">
                                <button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" data-placement="top" rel="tooltip">
                                        <span class="glyphicon glyphicon-trash"></span>
                                </button>
                            </a>
                        </p>
                        </div>
                    </td>
                </tr>

                <?php } } else { ?>
                <tr>
                        <td colspan="7" style="text-align: center;"> <?php echo "NO CONTENT ADDED FOR THIS MARKER";?></td>
                </tr>
                <?php }?>
            </table>
	</div>
	
</div>
<!--/col-span-6-->

<div class="modal fade" id="addWidgetModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                    <a href="#" class="pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle pull-right"></span></a>
                    <h4 class="modal-title">Add Variant</h4>
            </div>
            <div class="modal-body">
				<?php echo form_open('product/add_variant');?>
				<input type="hidden" class="form-control" name="product_id" value="<?php echo $id; ?>" /> 
                <div class="row">    	
					<div class="col-md-11">
						<label for="name">Variant Name</label>
							<input type="text" class="form-control" name="name" placeholder="Variant Name" value="" />
						<!-- div><label for="name">Target DB</label><br />
							<select id="targetdb" class="form-control" onchange="getTargetByTargetDBId(this.value);">
								<?php foreach($targetdb as $val) { ?>
									<option value="<?php echo $val->id; ?>"><?php echo $val->name; ?></option>
								<?php } ?>
							</select>
						</div -->
						<label for="name">Target</label><br />
							<select class="form-control" id="target" name="target_id" >
								<?php foreach($target as $val) { ?>
									<option value="<?php echo $val->target_id; ?>"><?php echo $val->name; ?></option>
								<?php } ?>
							</select>
					</div>
				</div>
				<br />
				<input class="btn btn-primary" id="upload" type="submit" value="Submit" />
				<?php echo form_close();?>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dalog -->
</div>