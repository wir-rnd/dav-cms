<div class="col-md-12">
	<div style="margin-left: -15px;">
		<a title="Add Widget" data-toggle="modal" href="#addWidgetModal" class="btn btn-primary ">Add New Product</a>
	</div>
	<br />
	<table id="tbl" class="table table-striped table-bordered" border="0">
		<thead>
			<tr>
				<th style="text-align: center;">No</th>
				<th style="text-align: center;">Name</th>
				<th style="text-align: center;">Images</th>
				<th style="text-align: center;">Brand</th>
				<th style="text-align: center;">Action</th>
			</tr>
		</thead>
		<tbody>
			<?php $no=1; ?>
			<?php foreach($product as $val) { ?>
			<tr>
				<td style="text-align: center;"><?php echo $no++;?></td>
				<td style="text-align: center;"><?php echo $val->product_name;?></td>
				<td style="text-align: center;">
					<?php if(file_exists('./products/'.$val->id.'.jpg')) { ?>
						<img src="<?php echo base_url().'products/'.$val->id.'.jpg'; ?>" width="120" height="120" />
					<?php } else { ?>
						<img src="<?php echo base_url().'products/default.jpg'; ?>"  width="120" height="120" />
					<?php } ?>
				</td>
				<td style="text-align: center;"><?php echo $val->brand_name;?></td>
				<td style="vertical-align: middle; text-align: center;">
					<div id = "test">
					<p>
						<a class="edit" href="<?php echo $this->config->item('base_url')."/product/edit/".$val->id;?>">
							<button class="btn btn-primary btn-xs edit" data-title="Edit" rel="tooltip">
								<span class="glyphicon glyphicon-pencil"></span>
							</button>
						</a>
						<a class="edit" href="<?php echo $this->config->item('base_url')."/product/variant/".$val->id;?>">
							<button class="btn btn-primary btn-xs edit" data-title="Edit" rel="tooltip">
								<span class="glyphicon glyphicon-pencil"></span>
							</button>
						</a>
						<a class="trash" href="#" onclick="$.fn.delete('<?php echo base_url().'product/delete/'. $val->id; ?>');">
						<button class="btn btn-danger btn-xs" data-title="Delete" rel="tooltip">
							<span class="glyphicon glyphicon-trash"></span>
						</button>
						</a>
					</p>
					</div>
				</td>
			</tr>
			<?php }?>
		</tbody>
	</table>
	<?php echo $this->pagination->create_links(); ?>
	<?php $this->load->view('ping_footer'); ?>
<!--/col-span-6-->
</div>