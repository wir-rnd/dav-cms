<script type="text/javascript">
$(document).ready(function() {
	$('#id').hide();
});
</script>
<div class="modal-header">
	<h4 class="modal-title">EDIT PRODUCT</h4>
</div>
<?php 
	//print_r($location_list_detail);
	if (isset($product)) {
?>
    <div class="modal-body">
        <?php echo form_open_multipart('product/update');?>
        <input type="hidden" name="id" value="<?php echo $product->id; ?>" />
            <div class="form-group">
				<div class="row">    	
					<div class="col-md-11">
						<label for="name">Product Name</label>
							<input type="text" class="form-control" name="name" placeholder="Brand Name" value="<?php echo $product->name; ?>" /> 
					</div>
				</div>
				<div class="row">    	
					<div class="col-md-11">
						<label for="name">Brand</label><br />
							<select class="form-control" name="brand_id">
								<?php foreach($brand as $val) { ?>
									<option <?php if($product->brand_id==$val->id) { ?>selected="selected"<?php } ?> value="<?php echo $val->id; ?>"><?php echo $val->name; ?></option>
								<?php } ?>
							</select>
					</div>
				</div>
				<div class="row">    	
					<div class="col-md-11">
						<label for="userfile">Product Image</label>
							<input style="margin-bottom: 5px;" type="file" name="userfile" id="userfile" size="20" />
					</div>
				</div>
				<?php if(file_exists('./products/'. $product->id .'.jpg')) { ?>
				<div class="row">    	
					<div class="col-md-11">
						<label for="userfile">Image</label>
							<img src="<?php echo base_url().'products/'. $product->id .'.jpg'; ?>" />
					</div>
				</div>
				<?php } ?>
			</div>

            <input class="btn btn-primary" id="upload" type="submit" value="Submit" />
            <a href="<?php echo $this->config->item('base_url')."/product";?>"><input class="btn btn-primary" data-dismiss="modal" id="cancel" type="button" value="Cancel" /></a>
        <?php echo form_close();?>
    </div>
				
<?php } else {
        echo "DATA NOT FOUND";
}?>