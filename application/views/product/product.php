<?php $this->load->view('generaljs'); ?>
<div class="row">
	<?php include 'product_list.php' ?>
</div>
<div class="modal fade" id="addWidgetModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<a href="#" class="pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle pull-right"></span></a>
				<h4 class="modal-title">Add Product</h4>
			</div>
		<?php echo form_open_multipart('product/add'); ?>
		<?php $this->load->view('product/product_form'); ?>
		<?php echo form_close(); ?>
<!-- /.modal-dalog -->
		</div>
	</div>
</div>


<!-- /.modal -->


	