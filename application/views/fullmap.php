<style>
     html { height: 100% }
		body { height: 100%; margin: 0px; padding: 0px }

      #map {
		  height: 100%;
		  width: 100%;
		}
</style>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
<?php //print_r($geo);?>
<script>

function initialize() {
	//var myLatlng = new google.maps.LatLng(-6.175307, 106.826771);
	
	var LocationData = 
		[
		<?php foreach ($geo as $val) { ?>
			[<?php echo $val->latitude; ?>, <?php echo $val->longitude; ?>, '<?php echo "<h3>(" . $val->store_id . ") " . $val->store_name . "</h3>"; ?>' ], 
		<?php } ?>
		];
				   
	/*var LocationData = [
		[-6.1932,106.7690, "<h3>DAV Control Center</h3>"],
		];*/
	  var mapOptions = {
	    zoom: 14,
	    center: new google.maps.LatLng(LocationData[0][1], LocationData[0][2]),
	   	mapTypeId: google.maps.MapTypeId.ROADMAP
	   	
	  };
		
	  var map = new google.maps.Map(document.getElementById('map'), mapOptions);
	  var bounds = new google.maps.LatLngBounds();
	  var infowindow = new google.maps.InfoWindow();

	  for (var i in LocationData)
	    {
	        var p = LocationData[i];
	        var latlng = new google.maps.LatLng(p[0], p[1]);
	        bounds.extend(latlng);
	         
	        var marker = new google.maps.Marker({
	            position: latlng,
	            map: map,
	            title: p[2],
	            icon: 'location_marker.png'
	        });
	     
	        google.maps.event.addListener(marker, 'click', (function(mm, tt) {
	            return function() {
	                infowindow.setContent(tt);
	                infowindow.open(map, mm);
	            }
	        })(marker, p[2]));
	    }
	  map.fitBounds(bounds);
	  /*	
	  var marker = new google.maps.Marker({
	      position: new google.maps.LatLng(locations[0][1], locations[0][2]),
	      map: map
	  });
	  */
		
	
  	  //google.maps.event.addListener(marker, 'click', toggleBounce);
}

google.maps.event.addDomListener(window, 'load', initialize);



</script>
<div id="map"></div>


