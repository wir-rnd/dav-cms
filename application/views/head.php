<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title></title>

<!-- Bootstrap -->
<link href="<?php echo $this->config->item('bootstrap')."/css/bootstrap.min.css" ;?>" rel="stylesheet">
<link href="<?php echo $this->config->item('assets')."/stylesheet.css" ;?>" rel="stylesheet">
<link href="<?php echo $this->config->item('assets')."/dav.css" ;?>" rel="stylesheet">
<link rel="stylesheet" href="<?php echo $this->config->item('assets')."/font_awesome/css/font-awesome.min.css" ;?>">

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="<?php echo base_url().'assets/jquery-2.1.3.min.js'; ?>"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo $this->config->item("bootstrap")."/js/bootstrap.min.js" ?>"></script>
<script src="<?php echo $this->config->item('assets')."/pagination.js" ?>"></script>
<?php //$plugins = $this->session->userdata('plugins'); echo 'asdasd'. $plugins[1].' asdasd'; ?>
<?php //echo isset($plugins['datatables']) . ' - '. isset($plugins['sweetalert']); ?>

<?php //if(isset($datatables)) { ?>
<link rel="stylesheet" href="<?php echo base_url().'assets/datatables/css/jquery.dataTables.min.css' ;?>">
<link rel="stylesheet" href="<?php echo base_url().'assets/datatables/css/dataTables.bootstrap.css'; ?>">
<script src="<?php echo base_url().'assets/datatables/js/jquery.dataTables.min.js'; ?>"></script>
<script src="<?php echo base_url().'assets/datatables/js/dataTables.bootstrap.js'; ?>"></script>
<?php //} ?>

<?php //if(isset($sweetalert)) { ?>
<script src="<?php echo base_url().'assets/sweetalert/sweetalert.min.js'; ?>"></script>
<link rel="stylesheet" href="<?php echo base_url().'assets/sweetalert/sweetalert.css' ;?>">
<?php //} ?>

<?php //if(isset($noty)) { ?>
<script type="text/javascript" src="<?php echo base_url().'assets/noty/jquery.noty.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/noty/layouts/center.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/noty/layouts/topCenter.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/noty/layouts/topLeft.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/noty/layouts/topRight.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/noty/themes/default.js'; ?>"></script>
<?php //} ?>
<link rel="stylesheet" href="<?php echo base_url().'assets/animate.css' ;?>">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<?php $this->load->view('generaljs'); ?>
</head>