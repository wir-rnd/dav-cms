<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('head'); ?>
<body>
	<div id="top-nav" class="navbar navbar-inverse navbar-static-top">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="icon-toggle"></span>
			</button>
		</div>
		<div class="navbar-collapse collapse">
			<img style="margin-left: 20px; margin-top: 7px;" src="<?php echo $this->config->item('assets')."/img/dcontrol_center.png"?>" />
			<?php if($this->session_data) { ?>
			<ul class="nav navbar-nav pull-right">
				<li class="dropdown">
					<a class="dropdown-toggle" role="button" data-toggle="dropdown" href="#">
						<strong><?php echo ucfirst($this->session_data->username);?></strong>
							<span class="caret"></span>
					</a>
					<ul id="g-account-menu" class="dropdown-menu" role="menu">
						<li><a href="#">Edit Account</a></li>
						<li><a href="#">Change Password</a></li>
					</ul>
				</li>
				<li><a href="<?php echo base_url('auth/logout')?>">Log out</a></li>
			</ul>
			<?php } ?>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="panel panel-info">
				<div class="panel-heading"><center><strong>DAV</strong> Control Center</center></div>
				<div class="panel-body">
					<div class="col-md-3">
						<strong>Tools</strong>
						<hr />
						<?php include 'menu.php';?>
					</div>
					<div class="col-md-9">
						<?php $crumb = $this->uri->segment(1);?>
						<a href="#"><strong><span class="glyphicon glyphicon-dashboard"></span> <?php echo ucfirst($crumb);?></strong> </a> 
						<hr />
						
						<?php $this->load->view('message'); ?>
						
						<?php switch($page) {
							case "login" : include 'login.php'; break;
							case "home" : include 'dashboard.php'; break;
							case "home/head2head" : include 'head2head.php';break;
							case "user" : include 'user/user.php'; break;
							case "user/edit" : include 'user/user_edit.php'; break;
							case "target" : include 'target/target.php'; break;
							case "target/edit" : include 'target/target_edit.php'; break;
							case "target/detail" : include 'target/target_detail.php'; break;
							case "target/dbdetail" : include 'target/target_db_detail.php'; break;
							case "target/content" : include 'target/target_content.php'; break;
							case "target/tag": include 'target/target_tag.php'; break;
							case "speech" : include 'speech.php'; break;
							case "store" : include 'store/store.php'; break;
							case "store/edit" : include 'store/store_edit.php'; break;
							case "store/geolocation" : include 'store/geolocation.php'; break;
							case "store/device" : include 'store/store_device.php'; break;
							case "store/modem" : include 'store/store_modem.php'; break;
							case "store/pv_position": include 'store/store_pv_position.php'; break;
							case "device" : include 'device/device.php'; break;
							case "device/edit" : include 'device/device_edit.php'; break;
							case "device/phone" : include 'device/device_phone.php'; break;
							case "modem" : include 'modem/modem.php'; break;
							case "modem/edit" : include 'modem/modem_edit.php'; break;
							case "content" : include 'content/content.php'; break;
							case "content/edit" : include 'content/content_edit.php'; break;
							case "content/detail" : include 'content/content_detail.php'; break;

							case "location/geolocation" : include 'geolocation/store_map.php'; break;
							case "statistic" : include 'statistic/statistic.php'; break;
							case "statistic/device_activity" : include 'statistic/device_activity.php'; break;
							case "statistic/brand_activity" : include 'statistic/brand_activity.php'; break;
							case "statistic/brand_activity_detail" : include 'statistic/brand_activity_detail.php'; break;
							case "statistic/brand_activity_more_detail" : include 'statistic/brand_activity_more_detail.php'; break;
							case "map" : include 'fullmap.php'; break;
							case "tag" : include 'tag/tag.php'; break;
							case "tag/edit" : include 'tag/tag_edit.php'; break;
							case "shelf" : include 'shelf/shelf.php'; break;
							case "shelf/edit" : include 'shelf/shelf_edit.php'; break;
							case "brand" : include 'brand/brand.php'; break;
							case "brand/edit" : include 'brand/brand_edit.php'; break;
							case "product" : include 'product/product.php'; break;
							case "product/edit" : include 'product/product_edit.php'; break;
							case "product/variant" : include 'product/product_variant.php'; break;
							case "devicestatus": include 'devicestatus.php'; break;
							case "apk": include 'apk/apk.php'; break;
							case "reporting": include 'company/reporting.php'; break;
							case "reporting_detail": include 'company/reporting_detail.php'; break;
						} ?>
					</div>
				</div>
				<div class="panel-footer"><center>&copy; DAV Global Pte Ltd. All right reserved.</center></div>
			</div>
		</div>
	</div>
</body>
</html>
