<div class="row">
	<?php include 'brand_list.php' ?>
</div>
<div class="modal fade" id="addWidgetModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<a href="#" class="pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle pull-right"></span></a>
				<h4 class="modal-title">Add Brand</h4>
			</div>
			<?php echo form_open('brand/add'); ?>
			<?php $this->load->view('brand/brand_form'); ?>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>
	