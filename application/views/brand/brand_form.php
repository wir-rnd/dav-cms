<div class="modal-body">	
	<div class="form-group">
		<div class="row">    	
			<div class="col-md-11">
				<label for="name">Brand Name</label>
					<input type="text" class="form-control" name="name" placeholder="Brand Name" <?php if($page=="brand/edit") {  ?> value="<?php echo $brand->name; ?>" <?php  } ?> /> 
			</div>
		</div>
		<div class="row">    	
			<div class="col-md-11">
				<label for="name">Agent</label><br />
					<select class="form-control" name="agent_id">
						<?php if(isset($agent)) { ?>
						<?php foreach($agent as $val) { ?>
							<option <?php if($page=="brand/edit") { if($brand->agent_id==$val->id) { ?> selected="selected" <?php } } ?> value="<?php echo $val->id;?>"><?php echo $val->name;?></option>
						<?php } ?>
					<?php }?>
					</select>
			</div>
		</div>
	</div>
	<br />
	<button class="btn btn-success" type="submit">Submit</button>
	<input class="btn btn-primary" data-dismiss="modal" id="cancel" type="reset" value="Cancel" />
</div>