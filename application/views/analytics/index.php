<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title></title>

<!-- Bootstrap -->
<link href="<?php echo $this->config->item('bootstrap')."/css/bootstrap.min.css" ;?>" rel="stylesheet">
<link href="<?php echo $this->config->item('assets')."/stylesheet.css" ;?>" rel="stylesheet">
<link href="<?php echo $this->config->item('assets')."/dav.css" ;?>" rel="stylesheet">
<link href="<?php echo $this->config->item('assets')."/daterangepicker-bs3.css" ;?>" rel="stylesheet">
<link href="<?php echo $this->config->item('assets')."/bootstrap-select.min.css" ;?>" rel="stylesheet">
<link rel="stylesheet" href="<?php echo $this->config->item('assets')."/font_awesome/css/font-awesome.min.css" ;?>">

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo $this->config->item("bootstrap")."/js/bootstrap.min.js" ?>"></script>
<script src="<?php echo $this->config->item('assets')."/pagination.js" ?>"></script>
<script src="<?php echo $this->config->item('assets')."/daterangepicker.js" ?>"></script>
<script src="<?php echo $this->config->item('assets')."/analytics/js/canvasjs.min.js" ?>"></script>
<script src="<?php echo $this->config->item('assets')."/bootstrap-select.min.js" ?>"></script>
</head>

<style>

.navbar-custom {
	background-color:#00adef;
    color:#ffffff;
  	border-radius:0;
  	height: 25px;
}
  
.navbar-custom .navbar-nav > li > a {
  	color:#fff;
  	padding-left:20px;
  	padding-right:20px;
}
.navbar-custom .navbar-nav > .active > a, .navbar-nav > .active > a:hover, .navbar-nav > .active > a:focus {
    color: #0a293b;
}
      
.navbar-custom .navbar-nav > li > a:hover, .nav > li > a:focus, .nav > li > a:visited {
    text-decoration: none;
    background-color: #0a293b;
}
      
.navbar-custom .navbar-brand {
  	color:#0a293b;
}
.navbar-custom .navbar-toggle {
  	background-color:#0a293b;
}
.navbar-custom .icon-bar {
  	background-color:#0a293b;
}

.site-footer {
	position: fixed;
	bottom: 0;
	background-color: #00adef;
	height: 35px;
	color: white;
	font-weight: bold;
	font-size: 11px;
	text-align: center;
	width: 100%;
}

</style>
<body style="background-color: #0a293b;">
	<!-- Header -->
	<div id="top-nav" class="navbar navbar-custom navbar-static-top">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target=".navbar-collapse">
				<span class="icon-toggle"></span>
			</button>
		</div>
		<div class="navbar-collapse collapse">
			<div class="col-md-2">
				<img style="margin-left: 35px; margin-top: 7px;" src="<?php echo $this->config->item('assets').'/analytics/images/dav_reporting.png'; ?>" />
			</div>
			<div class="col-md-4">
				<label style="padding-left: 50px; padding-top: 15px;"><?php echo date("d M Y");?></label>
			</div>
			<ul class="nav navbar-nav pull-right">
				
				<li class="dropdown">
					<a class="dropdown-toggle" role="button" data-toggle="dropdown" href="#">
						<strong><?php echo ucfirst($this->session_data->username); ?></strong>
						<span class="caret"></span>
					</a>
					<ul id="g-account-menu" class="dropdown-menu" role="menu">
						<li><a href="#">Edit Account</a></li>
						<li><a href="#">Change Password</a></li>
					</ul>
				</li>
				<li><a href="<?php echo base_url('auth/logout')?>">Log out</a></li>
			</ul>
		</div>
		
	</div>
	<!-- /Header -->
	<input type="hidden" id="variant_id" value="<?php echo $this->session->userdata('variant_id'); ?>" />
	<input type="hidden" id="base_url" value="<?php echo base_url(); ?>" />
	<content>
		<table width="100%">
			<tr>
				<td width="20%" valign="top">
					<?php include_once 'menu.php';?>
				</td>
				<td>
					<?php 
					if (isset($page))
					{	
						//die($page);
						switch($page)
						{
							case 'brand_report' : include 'brand_report/view.php'; break; // brand report
							case 'report_product' : include 'report_product.php'; break; 
							case 'contact_us' : include 'body_contact_us.php'; break; 
							case 'report_topten' : include 'report_topten.php'; break; //top ten report
							case 'dashboard' : include 'dashboard.php'; break; //dashboard dav analytics
							default: include 'dashboard.php';
						}
					}
					?>
				</td>			
			</tr>
			
		</table>
	</content>
	
	<footer class="site-footer">
	  	&copy; DAV Global Pte Ltd. All right reserved.
	</footer>
</body>
</html>
