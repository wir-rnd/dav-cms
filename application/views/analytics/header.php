<?php $varid = $this->session->userdata('variant_id'); ?>
<div class="col-md-12">
	<div class="box col-md-4">
		<div class="col-md-4 pull-left" style="padding-left: 18px; padding-top: 4px;">
			<img id="dav_img" width="100px" height="100px" style="border: 1px solid #0A293B;" src="<?php if(file_exists('./assets/analytics/images/variant/'. $varid .'.jpg')) echo base_url().'assets/analytics/images/variant/' . $varid . '.jpg'; else echo base_url().'assets/analytics/images/variant/default.png'; ?>" />
		</div>
		<div class="col-md-8 dav_title" style="padding-left: 10px; padding-top: 4px; text-align: left;"><?php echo $product->name.', '.$variant->name; ?></div>
	</div>

	<div class="box col-md-4" style="background-color: #00ADEF; color: white; text-align: center;">
		Total No. Of Interactions
		<div class="number" id="today_interaction">0</div>
		<div class="font_small">All Stores</div>
	</div>

	<div class="box col-md-4" style="background-color: #1f85a8; color: white;">
		Avg. No. Of Daily Interactions
		<div class="number" id="average_interaction">0</div>
		<div class="font_small">All Stores</div>
	</div>
</div>