<link rel="stylesheet"
	href="http://css-spinners.com/css/spinner/spinner.css" type="text/css">
<style>

.box {
	background-color: #3fc8f4;
	
	height: 129px;
	text-align: center;
	font-family: dav_font;
	padding-top: 10px;
	font-size: 20px;
	color: white;
	-webkit-box-shadow: inset -5px 65px 29px -49px rgba(0, 0, 0, 0.21);
	-moz-box-shadow: inset -5px 65px 29px -49px rgba(0, 0, 0, 0.21);
	box-shadow: inset -5px 65px 29px -49px rgba(0, 0, 0, 0.21);
}

.number {
	font-family: dav_font;
	font-size: 45px;
	color: white;
}

.dav-graph {
position: relative;
	background-color: #F0F0F0;
	width: 1122px;
	height:550px;
	border-bottom: 1px solid black;
	margin-left: 15px;
	
}

.font_small {

	font-size: 14px;
}

.detail{
	position: relative;
	background-color: #F0F0F0;
	width: 1122px;
	height:100%;
	border-bottom: 1px solid black;
	margin-left: 15px;
	margin-top: 129px;
	margin-right: 15px;
}
</style>
<script type="text/javascript" src="<?php echo base_url().'assets/analytics/js/da.common.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/analytics/js/da.dashboard.js'; ?>"></script>
<input type="hidden" id="dailyAverage" value="" />
<div class="col-md-12">
	<div class="box col-md-3">
		<select class="selectpicker dav_selector">
			<?php foreach($product as $val) { ?>
				<option value="<?php echo $val->variant_id;?>">
					<?php echo $val->product_name.", ".$val->variant_name; ?>
				</option>
			<?php } ?>
		</select>
		
		<div class="col-md-4 pull-left" style="padding-left: 18px; padding-top: 4px;">
			<div id="product_img"></div>
		</div>
		<div class="col-md-8 dav_title" style="padding-left: 10px; padding-top: 4px; text-align: left;">&nbsp;</div>
	</div>

	<div class="box col-md-3" style="background-color: #00ADEF; color: white;">
		Avg. Daily Interactions
		<div class="number" id="average_interaction">
			<div class="spinner">Loading...</div>
		</div>
	</div>

	<div class="box col-md-3" style="background-color: #1f85a8; color: white;">
		Total No. Of Interactions
		<div class="number" id="today_interaction">
			<div class="spinner">Loading...</div>
		</div>
	</div>

	<div class="box col-md-3" style="background-color: #0D5D73; color: white;">
		Highest No. Of Interactions
		<div class="number" id="highest_interaction">
			
			200
		</div>
		<div class="font_small">
			K6590 (Jakarta Barat)
		</div>
		
	</div>
</div>

<div class="col-md-12 dav-graph">
	<!-- Today Interactions  -->
	<div class="col-md-12">
		<div id="todayChart" style="height: 300px; width: 100%;"></div>
	</div>
	<!-- Weekly Interactions  -->
	<div class="col-md-6">
		<div id="weekChart" style="height: 200px; width: 100%; padding-top: 20px;"></div>
	</div>
	<!-- Monthly Interactions  -->
	<div class="col-md-6">
		<div id="monthChart" style="height: 200px; width: 100%; padding-top: 20px;"></div>
	</div>
</div>

<div class="detail">
	<?php include "table_detail.php" ;?>
</div>