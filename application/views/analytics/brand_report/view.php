<style>
.src-image {
  display: none;
}

.card {
  overflow: hidden;
  position: relative;
  border: 1px solid #CCC;
  border-radius: 8px;
  text-align: center;
  padding: 0;
  background-color: #00ADEF;
  color: white;
  font-family: dav_font;
}

.card .header-bg {
  /* This stretches the canvas across the entire hero unit */
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 70px;
  border-bottom: 1px #FFF solid;
  /* This positions the canvas under the text */
  z-index: 1;
}
.card .avatar {
  position: relative;
  margin-top: 15px;
  z-index: 100;
}

.card .avatar img {
  width: 100px;
  height: 100px;
  -webkit-border-radius: 50%;
  -moz-border-radius: 50%;
  border-radius: 50%;
  border: 5px solid rgba(0,0,30,0.8);
}
</style>
<div>
	
    	<?php foreach ($product as $val) { ?>
	        <div class="col-sm-3">
	            <div class="card">
	                <canvas class="header-bg" width="250" height="70" id="header-blur"></canvas>
	                <div class="avatar">
	                    <img id="prodImg-<?php echo $val->variant_id; ?>" src="" alt="" />
	                </div>
	                <div class="content">
	                    <p><?php echo $val->product_name?><br>
	                       <?php echo $val->variant_name;?></p>
	                    <p><a href="<?php echo base_url()."brand_report/product/".$val->variant_id;?>"><button type="button" class="btn btn-default">Detail</button></a></p>
	                </div>
	            </div>
	        </div>
        <?php } ?>
   
</div>
<script>
$(document).ready(function() {
	//alert('asdasdsa');
	$.fn.checkImgProduct = function(id)
	{
		$.ajax({
			url: "<?php echo base_url().'dashboard/ajax_check_variant_image/'; ?>" + id,
			success: function(data)
			{
				//alert(data);
				if(data==1)
				{
					$('#prodImg-' + id).attr('src', '<?php echo base_url()."assets/analytics/images/variant/"; ?>' + id + '.jpg');
				}
				else
				{
					$('#prodImg-' + id).attr('src', '<?php echo base_url()."assets/analytics/images/variant/default.png"; ?>');
				}
				//alert($('#prodImg-' + id).attr('src'));
			}
		});
	};
	<?php foreach ($product as $val) { ?>
	$.fn.checkImgProduct('<?php echo $val->variant_id; ?>');
	<?php } ?>
});
</script>
<?php foreach ($product as $val) { ?>
	<img class="src-image" src="<?php if(file_exists('./assets/analytics/images/variant/'.$val->variant_id.'.jpg')) echo base_url().'assets/analytics/images/variant/' . $val->variant_id . '.jpg'; else echo base_url().'assets/analytics/images/variant/default.png'; ?>" />
<?php } ?>
<script>
<!-- img class="src-image"  src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxQSEhUUExQWFhQVFRQUGBcXFRUUGBgWFxQWFxQUFBUYHCggGBolHBQUITEhJSkrLi4uFx80ODMsNygtLisBCgoKDg0OGxAQGywkICQsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLCwsLP/AABEIAL8BCAMBEQACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAAAwQFBgcBAgj/xABLEAABAwEEBQgHBQYDBgcAAAABAAIDEQQFEiEGMUFRYQcTIjJxgZGhQlJykrHB0RQjYoKiQ5OywuHwM1PSFRYXw+LxJERUY3OD0//EABsBAQACAwEBAAAAAAAAAAAAAAADBAECBQYH/8QANxEAAgICAAUCBAMHBAIDAAAAAAECAwQRBRIhMUETUSIyYXEUQlIVI4GRobHBQ1PR8DThBhYz/9oADAMBAAIRAxEAPwBKJjWsDavc4Z43OBqXPcS0tpkGggA1Naahs4d065/EujONdOE/iXRnFXK4IAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAEALG0bKLfZHtkLjqa49gJWrsgvJuqbH2iz39kk9R/uu+ix6sPc2/D2/pYm5hGsEdoIW6kn2I3XJd0eVk1BDAIAQAgBACAEAIAQAgBACAEAIAQABVYbS7m0YuXRI9PjLdYIrvFFiM4y7G06pw1zLR5WxGCAEAIAQAgBACA9xxF2oKOVsY92WqMO675IseQ3cTrPh9VVnmJfKjr08Dl/qS/kPobuYNle3NVZ5c2dGvhePDxsfwwAagB2ABVpWSfdluNNcPlSHLWqPubdBeNiljE0bHDWVyOpWYRZDJRfdCM9ywya2AHe3ony1q3CUl5KVuJTPuiEvHRN7RiiOMeqcnd2x3krUW2ci/h8o9YPZXXNINCCCMiDkRwIWxzmmnpnEMAgBACAEAIAQAgBACAEAvZ7I5+oZbzkFBbkQr79y7j4Ft3ZdPck7NdI21cdw+gzKpSy7J9II7VPCKq+tj2TVluKU9WEtHEBvxzSODmW9eVl+MsarpFL+BSdMJ3w2oxuyLWMHjV383kupjYjqhyz7lDiVaurjYhGx2jGFvKOjzlkOVjhaEYIAQAgBOgPbIyVFO6MTo4vDLr/Gl7juGzDtVOzIk+3Q9Hj8Iop6y6v6j6KNVJSOjpR6LsOo2KNsw2OGNWmm+yNG0hy2E7j4Fbqi1/lf8mRuyPuLRwncfAqVY9i8P+Ro7I+4vG1SRg13NHIcRsViESNscMYrEIkbYuAp0iPZDX/cLbQC5tGyjU7UHfhfw47PJZKeTiRtW10ZQJonMcWuBDmmhB1grBwpwcHpnhDUEAIAQAgBACAEB6jjLjQCpWk5xgtyZJVTK2XLDqyeujR90hybjO31R2k61UjK/JfLSunuegx+G1ULmue37FzsGi7RQynEfVGTfHWfJdTH4JBfFa9v2LM8t/LBaRO2eysjFGNDewAeO9diuiutahFIqynKXcWUpqZDyyXOROycdSRgjJ3SMqR4t/hK5+WnGSmdHEUba3U/4FKumYtNHKpPUltHCzsWVbe0TgVc5R1ACbRvGEpfKtgFFK1I6eNwi+3q1pfUWjjVadzl2PQYvCKaesvif1HccarOWzp9EiYu+5pZeow09Y9EeJ1qarCvt+WP8yrZk1w7ssNi0RP7R/c0V/UfounVwPfWyX8inZnv8qJmzXBAz0cXtGvlqXRq4VjV/l39+pVllWy8j6Ozsb1WtA4AD4K5GmuPaK/kQOUn3YrRb6RgKLOkBKSzNdraD3KKVFcvmijKk15G0l2N9HLzCp2cOg+sehIrn5Gj4S3WFSnRKvuiRT2eVoAQyVrTK6cbOeYOnGOl+Jg19419lUKGbj88eZd0UgFanEOoYBACAEAIAQyL2SymQ5ZAazu/qoLr41ot4mHPIlpdvLL1o/otkHPBa3XT0ncSdgUuLw2eR+8v6L2O/F140eSpdfcuMEDWANaAANgyXoq641rUFpFZycurFVIYBACAZXvdkdpidFKKscO8EanA7CDmtJwU1ys3hNwlzIx+/wDQi0WVxLWmWLY9gqQPxtGYPHVkuPdjWQfTqjsQyab48s+/1ISKZwyqqUpNGkuEYs+uv6i7ZHHatHYzaHB8WP5RVg3lQuTZchRTV8qSJmwXFPJ1IXniRhHi6gW8MW6ztFmtmVVDuyzXfoQ85yvDRuaMR8dQ81dr4POWueWvsUbOIx/KizXfo9BDm1lXD0ndI/Qdy6lOBTV2XX6lCzJss7slQrmkQHVkAhgEMggCqGAqsGQWQeZGAihWsoqS0wnojbTZ8J4f3kuRkY7re12J4y2IKsbgQhhra0Zde1j5ieSPYDVvsuzb4Vp3LDPPZVfJNoarBWBACAEAIBzYrIZD+Eaz8hxVe+9VrS7l/CwnkS69vJpGjejoY1r5G562t9Xi7e5XuH8O6+rf38L2O3OxQj6dfRFlou6VjqyAQAgBACA5RNAa2m7IZDV8UbjvcxpPiQo3VB90bxsnHsxrHo5ZWmos8VfYC0/DVfpRt69v6mPYLFGzqRsb7LQ34BSKuK7I0c5Puxei3NTFtNtJLXFb52RzyMaxzQ1oIoBzbTq7SVysi6cbGkz3nCeF4t2HGU4Jt+TStE7+FssjZRTGAWyDdI0Z9xyI4FX6bOeGzyXEMJ4uS6n28fYzTRy8L0t73titRDmjGQ4hozNKCjSqNVl1jaTPVZ2LwzChF2V737P/ANkpbLHekRpJeMTCdjpgD4FilauXeaKVdvDLPkx5P7b/AORxDc18PGJltY5p2tlqPEMWVC99pEcsrhEXqVEl/wB+43tljvSI0kvGJhOx0wB8CxYcbl3miSFvDLF8GPJ/bf8AyTei9nt0bnTWi1xywCN+p4eMeWE1wgCme3apK/UW3KWyhnTwrOWuipxlvzvsRvJbpHLK+YWm0YgGxFvOOaMyX1w6uHktMW2Um+Zlzj3DqqIVumGt99bDRq/bRJe0sLpXOjBnAYaYRhPRoOCxVdOVzjvoZzsDHr4bC6MdSeuoteUd7MH3tvs8YOr/AA2e7VlfNbTVy/MiHGlw2SWqJSf3Z5ue5rykkikNubJE2RjnYZC4FocC5tAKGoypxSFdrabltGcnK4fGuUI0OMmumzR3MBFCrcoqS0zzS6EVNHhNP7ouJdW65aLCltCaiNij6fw0lhk9ZrmH8pBH8RWO6OXxGHZlcWDkAgBACAWslnL3UGradwUN1yrjst4eLLIsUV28mi6J3IABI4UA6g/nKscMwXN+vb/A9DbKNUPSr7FrC9EVDqAEAIAQAgBACAEAIAQAgMVvixNnvx8T+rJKGnvhbmFy5xUsjTPeY10qODqyHdL/ACd0St77tt0lnmPQeeaedQxfspBwNfB3BYpk6bHFmOJUx4lhxyK11XX/AJQ20KvQ2VtulHWbC0NrqxukLW1G3MhYx58ilI34vjfiJ49T8vqIXALBJzkl4TymV7jk1rzu6bnBpqa1y2ALWv0pbdj6k2ZHOqca8GCUUvp/keaGXp9mt5is0pkgmLmNqC2ri2sbi0jJwIDSdq3os5bNR7Mr8VxXfhK6+KjOPf8Az/yRd0us/wBol/2kJsROZb1g+px84Ne6lPotIcnO/VLd8chY8P2frX/ff+pa9H9GLPN9oEFs5yyyNJdEAWyAtc18ZcHbi3XTMKxXTF71Lp7HFzeIZEHX61XLZF/N00/BBcnejUNvdK2bFRjWEYTTrFwNcuAUONVGyT2dPjvEbcWuHp6+LY6uO2CC9bTKdUbbW7twgkBZrfLdJ/cjza3bwyqHu4r+bI26J7LaZZJrynfidSga15rxq0HC0ZABaQlCbcrWWcmnKxq41YEFry+n+Re6ryisV4MdY5XPs73Ma4Oa5pwudRzXAgYi2tQafNZhNQsXI+hHlYtuVgy/EwSnFNp/Y3ILrnz4bW+Ora7vhtVPLr5ob9jeD6kW964spFlIqPKBnDGfVlHm1w+ixU9sp58d17KoCtzz/k6hgEB1jSSANZyWspcq2zeuDnJRXkuWi1zB7g30W0c879ze/wCFVVxKZZl+38qPV11Rw6eVd2aE1tMhqXropRSS7IpfUHOA1rII59/2VpINpgBGsGaMHvGJNo25JewM0hshIAtUBJIAAmjJJOoAYtaxtDkl7ElVZRqdWQcKAaWu9IYv8WaOP25Gs/iKxtGVGT8Df/eOx/8AqrP+/i/1JtGeSXse4L8sz3BrLRC5zsg1ssbnE7gAalY2hyy9iQC2NTqAEBkEzD/vDqP+O06jq5lua5un+J2e0VkP2G1vrr/JM8rOj3ORttUbavj6MgAqTHXJ1B6pPgTuUmZTzLmXco//ABziKpsdFj+GXb7/APsq+gF0m1NtsOovgbhJBoHh5cwnvAUGNW5KSZ1+OZcaLabIvs/6CVzXjDYccNusLZHhxILmMxDYRVw6Tcqgg7ViEo1bjOOzOXj2Z7V2Jdpa6rb/AMdib0PdNarQ6WGyWeGJgeWO5gAh2EiNrZMqmpBJGyu9TUuU3tRSRzOJV049SrndKUnra30+vQayaVDE+O9bG2R4NARGGOHCp1jVRwK1d/Vq2JZr4XtKeBfpedv/AL/US0AsDprwM0Ebo7MOdrUkgNexzWsxekcRaacOxa48G7OaK0iXjGRGvCVV0lKza7fR9/p06DfRa9n3RPMyeF5qAwhuR6JJDmVycCCfJKZOmbUkZ4jRXxLGhOqxfD16/wBh1o/Yueva0xuBDXi1NOWoOyrn2hKoN3Pf1Nc3IjDhlTi1tcv9Bjdz23bLJFbrG2YGmFxY06tsbnCjmkccqdq1jqltTjsnyFLidcbMa3lflb1/YldHZzbLaw2ew2eOzte1xcYGkta01rzmQxmmQGrjRS1vnn8MFo52dRHFxmrb5Sm/Cl06/T2NdC6J5E44VyWso7WgVuc0JG4kLzF3wzaL0OqKrp1J/wCHH/yM+a1x3uZXz1+6KrHqCsM81Lue1g1BASt0Wb0z2D5n5Lm5lrb9NHouEYml60v4GoXFYeZiAPWPSd2nZ3al6fh2L6FKXl9WS32epPfgkVfITCuUrSt9ptD4WPIs8TjHhBID3tNHuf6wqCBsoK7VUsnt6Onj0qK5n3KXRRFkAaZjIjMdqw1tA+ktFraZrOx5NTQcciAR5GncpMGxzr0+66HKyYKM+hLq6QFA5TdNDZW/Z4HUneKucP2bDu3POzcM9yhsnroi1j0c72+xi0ji5xc4lzjmXE1JO8k5lVW9nRSS6I5RDJaeT+y1mfLsiZkfxO1U7mu8lzuIWOMVGPdsdH3NmvTSCGxQsfapA0kAUAq57gBiwNGZ+S7cJcsFzdzjqtzk+UrB5XLH/lWn3Yv/ANE9ZE/4SZ6j5WbK4hohtJLiGgYYsyTQD/EWryIpbH4SZdmTxlnO1aG0JLjQUA11OymdVJXONkVKPYryUk+Qptt5VLExxa0TSgZYmMbhPZjc0nwWruiTRxbH17DZnKxYhqgtA7GQj/mLHrLwjd4tku7LJo7f8F4MxtjIoSAJWsxEDIubQnKuSxDIrnY4eURTrsqXfoT7WACgFFYIG99xG1MjpikDKNzJfhoBvJOpYevJtGUl0TKlefKRYLP0WOdKRshaC33yQ0jsJUbtiiaOPZPq/wCpXbZyutNMFjxe3IB4UaVG717E8cSSXzHLLyuipMljA4slBPfiYEVy9jDxJa0pFiuzlEu+0kNkJiOwTtaG19sEtHeQt1bGXcidFsO39C5QBuEYKYTmMNKU3iilWvBXbbfUUWTBxAVq98pXdx8QvMcQXLcy/R1iUjTyX7uNu+SvcGmvmQosTrJsrcReq0iCh1BWWeal3PawaikEWJwaNp/7lR2z5IuRPjUu2xRRddG7EHzMFOizpH8uoeNFS4bU78ncvHU9fbqmnlj9i+he1OUR2kV4fZrNNMf2cb3DZVwBwjvNB3rWT0jauPNJI+aCSczmTme3aqJ2UjiGQQG6clFrx2JgOsAt9x7mj9PNrXD+G2cf4nPzF8WyW0z0lZYLOZDQyO6MTPWfvOfVGs/1CvzlyogqqdktHz3bLU+WR8khxPe4uc47SVTb29nVjFRWkIrBsCA1Pk3uykEVdc0uM+w3UOwhn61zZr1cuMfYhtnywbK1yo3oZ7fI2tWQgRNGyozee3ESPyjcuta9s1xoahsqKjLBO6FWPnLWyvVjDpD3Cjf1EeBVTNs5KmvcIvvKFbuYuyKEGj7U7E6hocAONw7M42ngabVZxYeljRiU4fHe37GSrYuAsN66g3DQG7sGBv8AlRCvF7sif4/ELncMi7cmdr8ditmS1Wolsve847NC+aU0YwVO87mtG0k5AL0Lels50YuT0jBNLdLp7e84iWQg9CIE4QNhf6zuJ1bFTlY5HUqpjBFeWhMCAEGwQFy5PtM5LHI2KRxdZnkAgknmyaAOZubvbq25Z12Vjh9ivdQpra7m6xvBAIzBFQeCuRkpLaOW1o9LYFVv533zvy/wheX4s/3/APA6GKvgM701nxSxM9Vrne8aD+Fa4K+ByKHE5dUhqwZBTM88+56QwSlzQ63dw+fyXNzrO0Eeh4JR3sf2RoGhtnox7/WdhHYB9T5Lr8Bq1W7H5LubPc9FiXfKRn/LPeXN2RkQOc0or7DAXH9WAeKhuekWsSO579jFlVOkCAEBovJXfbYGy867DHGS8k6gHMNO01jpTe4KBS5MmL90yDIr54dCp6V6Qvt1oMz6hvVjZ6jNg9o6yd/CitTlzM2qq5I6RDLQlBAe4Yi9zWt1uIaO0mg+KxJ6TZhm8XSGWWN8n7Oy2enutqa020jHvKpw6PNZK1lPIe9R9zCJ5i9znu6z3Oe72nEl3mSrm9suJaWhNDJoPJrdmKOR+2V7IW+yOuR7x9xc/JTsthWaTnyrfsNuV68RJbuaHVs8bWU2BzwHup3Fg7uC6lmt8q8EGLHUeZ+SjqMtEno3ZOdtMbdgdjPYzpfIDvVbLs9OpsyjfNFYMMRcdb3E9w6I7sie9S8Ip5KN+/U5mZPms17GZ8sN/GW0CytP3cIDn8ZXDb7LSPeKtXS30JsSvS5jPFCXB1ddgfaJWRR9Z5pwA1lx4AAlaWTUIuUjDels126NCIY24WRNkcOtJKA7PvBp2Adq5sIZOU9xekV5XqPcYacaDMZZZJ2iMOibj6DObq0dYUGRyqrtWHdV1lPaEMuMpJaMnVgsggPoLk8t5lsceI1c1rPNo/mD/BMCzalD9LOblw5Z7Xks66BVKbf8tZn8KDwAXj+Kz5sho6mNHVaMwvOfnbVI7YHYB2Ny+IKu0Q5KkjhcQs3Yx0Fg5DOoNFisceFjRwr3nMrgXz5rGe3wqfSojE0LRyPDZ4+ILvEkjyovZ8Lhy4sfqc/Ie7GSa6BAYdywXjztuEYPRgja2n43kvf5GMU4cVUue5HSxI6hv3KMoi0CAEB6DzQipoaVGw01V3rDSb69weVlgEAICf0GsfOWxldUYMp/LQN/U5qqZtnJU/qYfY0DT+3czdeH0rXKB+QdKvZhYwfmCnxIenjr6lOPx3fYyBSF0CUBuGg9hEEUQdkIInSvJ2OfUknxl8FTw/3uTKfhFPJl8Ovcxi87cbRNJM7Iyvc+h2YjUA9goO5XW9vZahFRSQ2WDYuvJzdxdzswGYwxN9pxBI/g8VzOINz5a15ZjnUe5tkLBFGB6MbAO5rf6Lv1QUIKK8I4zblLZ8zXjazNNJKTUySPk99xd81Ub2zsQjyxSG6wbF85MLFnJLTpEthZ2kguPm3wK5uc3Ocao+TSb1Ftm02eEMaGjUPPiu9VXGuKijjyk5PbK1ynW0RXbPveGxAb8bg007G4j3Ja9RJceO7EfP6pnWBAbpyZMwwNH/swk9pBJ+JVXhkt3W+2ynnLpEuhNF2W9LbOeZlpDeWBssu3pEdpNGjzC8Q95GS/udaT9Knf0KHdUe09q7Vj10PJ5EtvqSihKgpZ2YnNG8j+qjtlqDZPjQ57VH3ZZKLz/dnuktJGiXS2kMQ3Rs/hC9/hrVEPsjh2v439xxI8NBJ1AEnsGZVkjPmO97cZ55Zia87I9+3U5xLRnsAoO5UJPctnZhHlikNFg3JmyXdWxTTEZh8YHY09KnbjHgqc7+W+NfuZ10IZXDAICY0fusSYppB9zCC534iBUMHz/qqmTfyNQXzMykRD3kkk6ySTszOvJWktIwcWQXzk5sZ5uR/pSPbE3yAPvP8A0lcnOlz2xqX/AHZhvSbfg7ywW6tpis7epZ4W+8/ZTg1rPErtTWko+xWxV0cvcoSjZaH9xWTnrRFGcw54r7Lek7yBUORPkrcgavpTbuYuud4NHzuELdnRrgdSmeoSkdoWvDocuPzv8xTl8dyj7GMqwXAQG18nN24LNZ20zdW0O782145x+CoUr1cvfsU8iWkyz6Yyltgtbm5EWacg7jzTqFd2fyspVLc0fNqonZBAbByXWKjIeDXzHtcSG+TvJc/Fj6mbKX6UQZb5a/uaSvQHKMp5bL2/wbM075n/AMMYp3vPcFXul4L2JDvIyxVy8L2GzGWRkY1vc1vicz4VK0tnyQcgfQOh8FGPOzEGDsaNfi4juUHBovklN+WUc5/El9B/pBaubgcdrugO127uqe5WeJ3+jjyfl9EV8eHPYkY3pfbMTmQjZR7v5B8T4LhcNp0nazbil+vgQ3scdGq5JnmrHtjhakY7uptZW958iq+U/wB2zocMW8lFljjXIjE9e2Xy6j9zH7DfIUXucN7oh9kcW3539yC5Sbx5i75zXORvMjtk6Jp+UuPcprXqJtRHmsR8+qmdYE8A1jR64OcshgI/8vITlXpvaaZb6vPguVhx9fLlJ+CO+zkite5k4XVJF2H9yXU+1SiJm3NztjWjW4/TbVRXWxqg5Mw3ouGlsYs9iEUYwsLxEN5pV789+QqfxLl4Vc7bHfPt4NuZb5SgLsmAKMG06D3ZzTYWuy5uPnX7g6lT5v8A0rlYcfWzHJ+CDJnqvS8mS3/eBtFpmmP7SV7h7NaMHc0NFeC60nttklceWCQwWDctnJ9ZCZJJaVwNDG8XPOoeA94LmcSk+VQXdsymktsmuWC1YTZbI05RRmRw3udRrCe5snvLrOKhCNa8Ip43Vyl7mcrQti1ig5yRjPWc1pO4E5nwWlkuWDYPoLRy0RBpdjYNTQMbdQ7/AO6KHhNbUZTl3bOfl75tJC+kkkc1ktETZY6yQTMHTbrdG4DbvK60tNFevakno+cAqR2AQGyckt6RvjLMQEjWRswkgE4S/MDaKEKLCq5LZv3KebtpFo0n0rs9ijLpHgvp0YmkF7jsyHVHE5LpTmolOuqU3o+f73vJ9pmkmkNXyOqaahlRrRwAAHcqcnt7OtCKjFRGawbFy5PLpLnPtLh0YwWMy6zzrI7AadruC5vELNpVx7s1bS7m2XVZeaiY06wKu9o5u8yV2sWr0qoxOTbPnm2VLTa9mgmp6ELSTxdtA8h2led4pa8jIVMOy/uXMdKqt2SMqs+KWR0jtbiSeHDsGpXNKuKivBwMq5yk2yWAUJzjqGCQuEVmHY74Kvlf/mdLhX/kItcca58YnqJSLXcbqxAbiR51+a9bw2W6F9DmXr42Zzy3XlnZ7ONmKd3myP8A5n9lT3vsifCj3kZYq5fJHR6wGe0RRgVBcC7g1ubie4U71DkT5KmzD7G96LWbCx7vWdQdjf6krTg1WqnN+Wc/MnuSRg1+3cYrZNA0VLZnta0Z5F1WDwLVZm1HbZdqfNBM1XQ3RwWeERtpz0tHSP10A1DsFaDeSSuRqWbZpdiKyzl6sqHK3am/ao7Ozq2aKn55aPdXiQIyuzKEYJQj4MY22nN+SjLUtEpoxd5tFqij2Yw53sNOJ3kKd4UORPkrbNZPSNb0mtf2e77VIDR0g5hhGur+jUcRjd7qi4ZXyVSm/JUfx2xj7GIBWy6CA2Dk2ufBBDiFDI4zO7PQ8gzxXOivWzF7IrX2ai9GdaaXl9pt1olBq0vwt3YWAMbTPUcNe+u1dSb3LZJRHkgkQi0JQQHMA3BYA7umwc9NHGB1nAHL0a1ce4VUd01Ctsx4JTSy43WeTGGkQyOdgOwEHpM4UOrgosS12VRcgpJvRAq1sz4OOaDrFUANaBqFEB1AT2i+i8tseKAthB6UhGXYz1nfDaq1+TGpfU0lPRs9wXSxoY1jaQxdUes4ba7aHOu09iiwMaVs/WsKV1uunuSd93gIYyfSOTRx39gV/iGWsenm8vsQ01epLXgxTSq8TLJzTTVrTV53vzy7vieC4mFS4p2y7sxxDJXyLsjzY4cIVmUtnnbJbY4WhGCAkdHz9+zjiH6Sor1uOi/w6XLei6xxqpGPQ9M2TNyuoS3fn3jX/fBdvhk9NxKd631Kbp3oU602kzl8hDmtaAxmLCGjUaV3k14rfNsvhLcI7RYxbIKOn0IGLk4qc3T/ALo/RUvxOS/9MsO2C7Mt2jWiMdlB5pjzI4UMkgoabtQAbwA2BYlTkZD1JaRXncu+y6WWAMYGjYPE7Su/TUq4KK8FCUm3tlHvrRml4PtIGIzBgYKZNcGBjyOJAHiVxuKq2U41wXRnQxbYqGn4Ljd1jETaa3HWfgBwC6WHixx60vPkpW2c8tma6UaAuktMkrnyuMri+rI8QFcg00BpQUHcqGXbkVz6Q2mX8e2HIk3ojY+TmpzM/wC6P0Vf8Tk/7ZM7a12aLhotopHZQeajeXuFDJIKOprpqFG5DIDYFj0cnJlqS0itbcvcdac6MG1WaOMPcBE/nCGtBLjhLa02npOy4rqXVzpoSqW9EWPavU3LyUIcnXGf9yfouZ+Kyf8AbL/qV/qJW6uTqFrg6QTyUPVc3Cw+0A2p8Vl25U1pQ0QzujrozQ7LYHBr88DnNLG09AUyOW3V4BX8HElTFyl8zKVtik0jKZeTjAS0unNMqtiLgeIIFCqFmRkwk4+n/c6EboNJ70eP+HnG0fuHf6Vr+Lyv9v8AuberD3Qf8PONo/cO/wBKfi8r/b/uPVh7oVh5OATm6cf/AEn5hbLJyX/pmruh7ls0d0Njs1TEx7pHDCZJdYB1jUAB2CuS3lTfkdGtIgleu+yz2u5IpbPzEjQ5tNoBz9btqSV1PwkfSVft5Kiukp8yM1vnkxwkmMvp+Ec4Pd6w81Qn+Kp/LzL3R0K8mEl1emV6XQiUftY/zBzfKhVd8RS+aDX8CwtPyeotCHHrTMHYCfCpC0fE4vtFjRZ7j0DgBBLHzn8Q+78BQHsJKwrsq56hFogssjHyaBY7poAHUDRqY3IU3GmzgFdx+Gdea17KM799IkhNK2NhcaBrR5DYF1LJwqhzPokQpOT0jLNONJSTl13CjG16jc+mePz7F5jmlm3+pL5F2RbunHGr0u7Kdd1l2ntNdqvSmuy7HnLrdvZKKEqAgBAOLumwSsdsDhXsrmtZLaJqJ8lkX7M0mONRRgepch1CcJB3K1VJ1yTRHLqtEyxwIBG1d6MlJJoqs9rcBRACA4QmgFEB1ACAEAIAQHEB1AcogOoDixoHUALIBAcogPLowdYB7RVauEX3RnbOCBvqjwCx6cfYbZ7WyMCc8rWAucQAMyStbLI1x5pPSMpOT0jPNMdKRhr6Negza9293D4Ly+RkTzrOSHSCL2o4sOaXczqNr5XmR5q4mvZuA3AK4oxriox7I4OTkOcm33JRjaCijb2c5vZ6WDAIAQAhk0u4bXzsDH7aYXe03I/XvRHo8aznrTJBbEw6sdow5HUfJXcS/l+Fkc4+SRqupsiOrIBACAEAIAQAgBAcQHiWYNFXEAbyaBayko9zKTfYgrfplZY9TzIdzBX9RoPNUrOI0R7Pf2LVeFbLxor9r5Rj+zgH5n/IDJVZcU38sS3Dhj/MyJk5RrVsbD7jz/Oo/wBpWt9kS/s2v3Z2HlMtAPTjicPw42eZcVJHiM/zJGJcMh4bLHc3KLZpiGyAwuOQxEFnvjV3gK7VmQn0fQpW4NkOq6lxa6oqMwdoVzZSPSAEAIDiAaXheDIW1eewbT2BVcnLqx4803/A3rrlN/CZ7pXpTQVecvQiBzJ3n6rzdl12fPXaJefp40dvqygOL53435k6hsA3Dgr8IRqjyxODlZTsltsk4ow0KNvZzZS2z2sGoIAQAgBAWLQy8+bkMTj0ZDlwfs8Rl3BZR0cC7llyPsy8rJ2gQDmz2otyOY+Ct0ZTh0fVEcob7D+KUO1FdOFsZroyJpoUUhgEAIAQHKoAQDC33xDD15AD6ozd4BVbsymr5mSwonP5UVa9NNnZiFuH8Tsz3N1DvquTdxdvpWv5nQq4f+tlQvC8ZJTWR7nHicu4ah3Lmztste5PZ0YUwrXREHbb0YzW6p3DMqerGnPqkR25lVXdkTLfDj1G95z8grcMOK+ZnPt4s+0UImWZ22nYAFMq60UJ8Stf5g5qX1is/B7EP7Qs/Uw+8GvMLDhB9i1TxWa6PqX7kw0vdFI2yyurFIcMZcc2SE5Mr6rjlTYab1ZxrXF8rLV8IXQ9Wv8AibCF0TnHaoBG02lkYq9waOJUVl0K1uT0bRi5dkV28dJ9kI/M4fBv1XByuNpfDSt/UuVYfmZn9/6UYXENPOzaiSataeJ29g8lQrxLciXqXMzfl10LlgVcMc8mSQlzjtPkBuXVSjFcseiOFZc7Z9WO4JS00yyNDQaulhrWu/4FYlEW40XHex8oTmAgBACAEAIAQynrqXzRe/hM0RyH75o98D0hx3hbHcxclWR0+5PoXvJ5c5aSloykIPlooHa12JFH3OtvWRu2vaK+akjxS6HnZj8PFig0hI1sB7HEfIqVcca+aJj8Hvszw7Sc/wCUPf8A+lP/ALBH9H9TP4J+4lJpUdkQrxfXywrWXH34gbLA92MbRpTLsDW91fioJcavl8qSJY4MPOyHtt8TP60jiNwOEeAoFUnl32fNJ/2/sWoY1ceyIiWVRxjtljpogrxv1jMm9N24au9yv04U5dX0Kl+fXX0XVkDaLbLNto3cMh9SulCiFZx786c/OkK2W6idazK05k8jXklYLsaNahlY2VZ3tjtsDRsUfMyJzZ6wDcm2a8zPEkDTsWVJmym0QN5QmNwLOsCC3tGbT40ViEt9TvcKvbnyPszeTpK0NFGEuoK1IaK0z3rFnGqo9IpsuLDk2Rlr0jld1aM7BU+JXLv41fPpHoWIYcF1l1KrfGkMcZPOSF7/AFR03d+7voqscbIyXzS392bzvqpWkVS335NaOi37uM7Aakj8TvkKd66tGFVT17s5OTnyl0XRHmx3fTWppWdNHHsuJHmxSmw5KLZApuMuZCYs+eZrTgBXOuffmtucsSydrsLrQqAgBACAEAIAQHNxBIINQRkQRqIKymbRk4vaLRdGmdKMtI4CUDXs6bRq7R4BZknrodjHzU+kyyMtbXtDmODmnUWkEeSpWSa7nWhqS+ESfIqs5k8YjeSRVpT0SRiNnvUDeyRIRe9ZSN0htJIpEjZIaSyqSMdm33IS8b9ijyxYneq3PxOoK/ThWT7rRXtzKq/JW7beMs+XVZ6o+Z2rqVY1da+pxsjPlZ50jtkuolSStOXZeTFnsLWqu57KkrWx0AtCE6gBACAEA1tDWh8bn1LWua4gCpOE1pr20otltxaXkv4FyrsTY7tumh1Rw973fyj6qrDhifWcjuS4kvyohLVelpmydIQ0+iwYR4jM+Ku141FfaJStzpy8niy3ZwUkrShZeSsFlDVA5bKkrGxwtSMEAIAQAgBACAEAIAQAgPLmg60TaMp6EWNfGcUT3MP4TSvaNR71tuMlqRaqyZQ7MeR6VWlmTwyTiQWnxbl5KGeHVPt0OnVxKS79RZumnrwOHsuB+ICrS4VvtIuw4nHyjw/TVn+VJ4t+qwuEy/UiX9pQ9hrPpmPRhd3uA+AKkjwrXeRh8Tj4RHz6UTu6sbG+Lj8h5KzDh9Ue72RT4nLx0I+Z883Xe4jdqHgMlajCqHZFKzOnLvIVst0b0laUZ5OyUgsTWqGU2yrO1scgLQjb2dQwCAEAIAQAgPL2AihWU9GU9dhD7E1bc7JPVYoyBo2LDk2aubYqtTQEAIAQAgBACAEAIAQAgBACAEAIDhaCm2Z2JOszTsW3MzZWNCRsLVn1Gb+sw/2e1Z9Rj1pHttjaNixzs1dkhZsYGoLXbNXJs9LBqCAEAIAQAgBACAEAIAQAgBACAEAIAQAgBACAEAID/9k="/ -->
	
	<!-- img class="src-image"  src="<?php echo base_url()."assets/img/koko.jpg"?>"/ -->
var mul_table = [
        512,512,456,512,328,456,335,512,405,328,271,456,388,335,292,512,
        454,405,364,328,298,271,496,456,420,388,360,335,312,292,273,512,
        482,454,428,405,383,364,345,328,312,298,284,271,259,496,475,456,
        437,420,404,388,374,360,347,335,323,312,302,292,282,273,265,512,
        497,482,468,454,441,428,417,405,394,383,373,364,354,345,337,328,
        320,312,305,298,291,284,278,271,265,259,507,496,485,475,465,456,
        446,437,428,420,412,404,396,388,381,374,367,360,354,347,341,335,
        329,323,318,312,307,302,297,292,287,282,278,273,269,265,261,512,
        505,497,489,482,475,468,461,454,447,441,435,428,422,417,411,405,
        399,394,389,383,378,373,368,364,359,354,350,345,341,337,332,328,
        324,320,316,312,309,305,301,298,294,291,287,284,281,278,274,271,
        268,265,262,259,257,507,501,496,491,485,480,475,470,465,460,456,
        451,446,442,437,433,428,424,420,416,412,408,404,400,396,392,388,
        385,381,377,374,370,367,363,360,357,354,350,347,344,341,338,335,
        332,329,326,323,320,318,315,312,310,307,304,302,299,297,294,292,
        289,287,285,282,280,278,275,273,271,269,267,265,263,261,259];
        
   
var shg_table = [
	     9, 11, 12, 13, 13, 14, 14, 15, 15, 15, 15, 16, 16, 16, 16, 17, 
		17, 17, 17, 17, 17, 17, 18, 18, 18, 18, 18, 18, 18, 18, 18, 19, 
		19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 20, 20, 20,
		20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 21,
		21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21,
		21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 22, 22, 22, 22, 22, 22, 
		22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22,
		22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 23, 
		23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23,
		23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23,
		23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 
		23, 23, 23, 23, 23, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 
		24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24,
		24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24,
		24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24,
		24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24 ];


function stackBlurCanvasRGBA( canvas, top_x, top_y, width, height, radius )
{
	if ( isNaN(radius) || radius < 1 ) return;
	radius |= 0;
	
	var context = canvas.getContext("2d");
	var imageData;
	
	try {
	  try {
		imageData = context.getImageData( top_x, top_y, width, height );
	  } catch(e) {
	  
		// NOTE: this part is supposedly only needed if you want to work with local files
		// so it might be okay to remove the whole try/catch block and just use
		// imageData = context.getImageData( top_x, top_y, width, height );
		try {
			netscape.security.PrivilegeManager.enablePrivilege("UniversalBrowserRead");
			imageData = context.getImageData( top_x, top_y, width, height );
		} catch(e) {
			alert("Cannot access local image");
			throw new Error("unable to access local image data: " + e);
			return;
		}
	  }
	} catch(e) {
	  alert("Cannot access image");
	  throw new Error("unable to access image data: " + e);
	}
			
	var pixels = imageData.data;
			
	var x, y, i, p, yp, yi, yw, r_sum, g_sum, b_sum, a_sum, 
	r_out_sum, g_out_sum, b_out_sum, a_out_sum,
	r_in_sum, g_in_sum, b_in_sum, a_in_sum, 
	pr, pg, pb, pa, rbs;
			
	var div = radius + radius + 1;
	var w4 = width << 2;
	var widthMinus1  = width - 1;
	var heightMinus1 = height - 1;
	var radiusPlus1  = radius + 1;
	var sumFactor = radiusPlus1 * ( radiusPlus1 + 1 ) / 2;
	
	var stackStart = new BlurStack();
	var stack = stackStart;
	for ( i = 1; i < div; i++ )
	{
		stack = stack.next = new BlurStack();
		if ( i == radiusPlus1 ) var stackEnd = stack;
	}
	stack.next = stackStart;
	var stackIn = null;
	var stackOut = null;
	
	yw = yi = 0;
	
	var mul_sum = mul_table[radius];
	var shg_sum = shg_table[radius];
	
	for ( y = 0; y < height; y++ )
	{
		r_in_sum = g_in_sum = b_in_sum = a_in_sum = r_sum = g_sum = b_sum = a_sum = 0;
		
		r_out_sum = radiusPlus1 * ( pr = pixels[yi] );
		g_out_sum = radiusPlus1 * ( pg = pixels[yi+1] );
		b_out_sum = radiusPlus1 * ( pb = pixels[yi+2] );
		a_out_sum = radiusPlus1 * ( pa = pixels[yi+3] );
		
		r_sum += sumFactor * pr;
		g_sum += sumFactor * pg;
		b_sum += sumFactor * pb;
		a_sum += sumFactor * pa;
		
		stack = stackStart;
		
		for( i = 0; i < radiusPlus1; i++ )
		{
			stack.r = pr;
			stack.g = pg;
			stack.b = pb;
			stack.a = pa;
			stack = stack.next;
		}
		
		for( i = 1; i < radiusPlus1; i++ )
		{
			p = yi + (( widthMinus1 < i ? widthMinus1 : i ) << 2 );
			r_sum += ( stack.r = ( pr = pixels[p])) * ( rbs = radiusPlus1 - i );
			g_sum += ( stack.g = ( pg = pixels[p+1])) * rbs;
			b_sum += ( stack.b = ( pb = pixels[p+2])) * rbs;
			a_sum += ( stack.a = ( pa = pixels[p+3])) * rbs;
			
			r_in_sum += pr;
			g_in_sum += pg;
			b_in_sum += pb;
			a_in_sum += pa;
			
			stack = stack.next;
		}
		
		
		stackIn = stackStart;
		stackOut = stackEnd;
		for ( x = 0; x < width; x++ )
		{
			pixels[yi+3] = pa = (a_sum * mul_sum) >> shg_sum;
			if ( pa != 0 )
			{
				pa = 255 / pa;
				pixels[yi]   = ((r_sum * mul_sum) >> shg_sum) * pa;
				pixels[yi+1] = ((g_sum * mul_sum) >> shg_sum) * pa;
				pixels[yi+2] = ((b_sum * mul_sum) >> shg_sum) * pa;
			} else {
				pixels[yi] = pixels[yi+1] = pixels[yi+2] = 0;
			}
			
			r_sum -= r_out_sum;
			g_sum -= g_out_sum;
			b_sum -= b_out_sum;
			a_sum -= a_out_sum;
			
			r_out_sum -= stackIn.r;
			g_out_sum -= stackIn.g;
			b_out_sum -= stackIn.b;
			a_out_sum -= stackIn.a;
			
			p =  ( yw + ( ( p = x + radius + 1 ) < widthMinus1 ? p : widthMinus1 ) ) << 2;
			
			r_in_sum += ( stackIn.r = pixels[p]);
			g_in_sum += ( stackIn.g = pixels[p+1]);
			b_in_sum += ( stackIn.b = pixels[p+2]);
			a_in_sum += ( stackIn.a = pixels[p+3]);
			
			r_sum += r_in_sum;
			g_sum += g_in_sum;
			b_sum += b_in_sum;
			a_sum += a_in_sum;
			
			stackIn = stackIn.next;
			
			r_out_sum += ( pr = stackOut.r );
			g_out_sum += ( pg = stackOut.g );
			b_out_sum += ( pb = stackOut.b );
			a_out_sum += ( pa = stackOut.a );
			
			r_in_sum -= pr;
			g_in_sum -= pg;
			b_in_sum -= pb;
			a_in_sum -= pa;
			
			stackOut = stackOut.next;

			yi += 4;
		}
		yw += width;
	}

	
	for ( x = 0; x < width; x++ )
	{
		g_in_sum = b_in_sum = a_in_sum = r_in_sum = g_sum = b_sum = a_sum = r_sum = 0;
		
		yi = x << 2;
		r_out_sum = radiusPlus1 * ( pr = pixels[yi]);
		g_out_sum = radiusPlus1 * ( pg = pixels[yi+1]);
		b_out_sum = radiusPlus1 * ( pb = pixels[yi+2]);
		a_out_sum = radiusPlus1 * ( pa = pixels[yi+3]);
		
		r_sum += sumFactor * pr;
		g_sum += sumFactor * pg;
		b_sum += sumFactor * pb;
		a_sum += sumFactor * pa;
		
		stack = stackStart;
		
		for( i = 0; i < radiusPlus1; i++ )
		{
			stack.r = pr;
			stack.g = pg;
			stack.b = pb;
			stack.a = pa;
			stack = stack.next;
		}
		
		yp = width;
		
		for( i = 1; i <= radius; i++ )
		{
			yi = ( yp + x ) << 2;
			
			r_sum += ( stack.r = ( pr = pixels[yi])) * ( rbs = radiusPlus1 - i );
			g_sum += ( stack.g = ( pg = pixels[yi+1])) * rbs;
			b_sum += ( stack.b = ( pb = pixels[yi+2])) * rbs;
			a_sum += ( stack.a = ( pa = pixels[yi+3])) * rbs;
		   
			r_in_sum += pr;
			g_in_sum += pg;
			b_in_sum += pb;
			a_in_sum += pa;
			
			stack = stack.next;
		
			if( i < heightMinus1 )
			{
				yp += width;
			}
		}
		
		yi = x;
		stackIn = stackStart;
		stackOut = stackEnd;
		for ( y = 0; y < height; y++ )
		{
			p = yi << 2;
			pixels[p+3] = pa = (a_sum * mul_sum) >> shg_sum;
			if ( pa > 0 )
			{
				pa = 255 / pa;
				pixels[p]   = ((r_sum * mul_sum) >> shg_sum ) * pa;
				pixels[p+1] = ((g_sum * mul_sum) >> shg_sum ) * pa;
				pixels[p+2] = ((b_sum * mul_sum) >> shg_sum ) * pa;
			} else {
				pixels[p] = pixels[p+1] = pixels[p+2] = 0;
			}
			
			r_sum -= r_out_sum;
			g_sum -= g_out_sum;
			b_sum -= b_out_sum;
			a_sum -= a_out_sum;
		   
			r_out_sum -= stackIn.r;
			g_out_sum -= stackIn.g;
			b_out_sum -= stackIn.b;
			a_out_sum -= stackIn.a;
			
			p = ( x + (( ( p = y + radiusPlus1) < heightMinus1 ? p : heightMinus1 ) * width )) << 2;
			
			r_sum += ( r_in_sum += ( stackIn.r = pixels[p]));
			g_sum += ( g_in_sum += ( stackIn.g = pixels[p+1]));
			b_sum += ( b_in_sum += ( stackIn.b = pixels[p+2]));
			a_sum += ( a_in_sum += ( stackIn.a = pixels[p+3]));
		   
			stackIn = stackIn.next;
			
			r_out_sum += ( pr = stackOut.r );
			g_out_sum += ( pg = stackOut.g );
			b_out_sum += ( pb = stackOut.b );
			a_out_sum += ( pa = stackOut.a );
			
			r_in_sum -= pr;
			g_in_sum -= pg;
			b_in_sum -= pb;
			a_in_sum -= pa;
			
			stackOut = stackOut.next;
			
			yi += width;
		}
	}
	
	context.putImageData( imageData, top_x, top_y );
	
}

function BlurStack()
{
	this.r = 0;
	this.g = 0;
	this.b = 0;
	this.a = 0;
	this.next = null;
}

$( document ).ready(function() {
  var BLUR_RADIUS = 40;
  var sourceImages = [];

  $('.src-image').each(function(){
    sourceImages.push($(this).attr('src'));
  });

  $('.avatar img').each(function(index){
    $(this).attr('src', sourceImages[index] );
  });

  var drawBlur = function(canvas, image) {
    var w = canvas.width;
    var h = canvas.height;
    var canvasContext = canvas.getContext('2d');
    canvasContext.drawImage(image, 0, 0, w, h);
    stackBlurCanvasRGBA(canvas, 0, 0, w, h, BLUR_RADIUS);
  }; 
    
  
  $('.card canvas').each(function(index){
    var canvas = $(this)[0];
    
    var image = new Image();
    image.src = sourceImages[index];
    
    image.onload = function() {
      drawBlur(canvas, image);
    }
  });
});
</script>