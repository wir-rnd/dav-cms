<style type="text/css">
	content table .row{
		margin-right: 0px;
	}
	.contact-form{ margin-top:15px;}
	.contact-form .textarea{ min-height:220px; resize:none;}
	.form-control{ box-shadow:none; border-color:#eee; height:49px;}
	.form-control:focus{ box-shadow:none; border-color:#00b09c;}
	.form-control-feedback{ line-height:50px;}
	.main-btn{ background:#00b09c; border-color:#00b09c; color:#fff;}
	.main-btn:hover{ background:#00a491;color:#fff;}
	.form-control-feedback {
		line-height: 50px !important;
		top: 0px !important;
	}
	.help-block{
		display: none;
	}
	.form-control-feedback{
		display: none;
	}
	.bg-success{
		display: none;
	}
</style>

<div class="row">
	<div class="col-md-6">
		<form role="form" id="contact-form" class="contact-form">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<div class="bg-success" style="padding:10px 10px;border-radius:4px">asdfasdf</div>
					</div>
				</div>
			</div>
			<div class="row">

				<div class="col-md-12">
					<div class="form-group">
						<input type="text" class="form-control" name="name" autocomplete="off" id="Name" placeholder="Name">
						<i class="form-control-feedback bv-no-label glyphicon glyphicon-remove" data-bv-icon-for="email"></i>
						<small class="help-block"></small>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<input type="text" class="form-control" name="email" autocomplete="off" id="email" placeholder="Email">
						<i class="form-control-feedback bv-no-label glyphicon glyphicon-remove" data-bv-icon-for="email"></i>
						<small class="help-block"></small>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<textarea class="form-control textarea" rows="3" name="message" id="Message" placeholder="Message"></textarea>
						<i class="form-control-feedback bv-no-label glyphicon glyphicon-remove"  data-bv-icon-for="email"></i>
						<small class="help-block"></small>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<button type="submit" class="btn main-btn pull-right">Send a message</button>
					</div>

				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
$("#contact-form").submit(function(){

	$.ajax({
	  url: "<?php echo site_url('contact_us/post_contact_us') ?>",
	  dataType: "JSON",
	  method:"POST",
	  data: $(this).serialize(),
	  beforeSend:function(){
	  		$(".form-group").removeClass('has-error has-feedback');
	  		$("small.help-block").hide();
	  		$("i.form-control-feedback").hide();
	  },
	  success :function(data){
	  	if(data.message[0]){
	  		$(".form-group").eq(1).addClass('has-error has-feedback');
	  		$(".help-block").eq(0).text(data.message[0]).show();
	  		$(".form-control-feedback").eq(0).show();
	  	}
	  	if(data.message[1]){
	  		$(".form-group").eq(2).addClass('has-error has-feedback');
	  		$(".help-block").eq(1).text(data.message[1]).show();
	  		$(".form-control-feedback").eq(1).show();
	  	}
	  	if(data.message[2]){
	  		$(".form-group").eq(3).addClass('has-error has-feedback');
	  		$(".help-block").eq(2).text(data.message[2]).show();
	  		$(".form-control-feedback").eq(2).show();
	  	}
	  	if(data.message[3]){
	  		$(".bg-success").text(data.message[3]);
	  		$(".bg-success").fadeIn( "normal" );
	  		setTimeout(function(){ 
	  			$(".bg-success").fadeOut( "normal" );
	  		}, 5000);
	  	}
	  	if(data.t == -1){
	  		window.location = "<?php echo site_url('login/logout'); ?>";
	  	}
	  }
	});
	return false;
});
</script>

