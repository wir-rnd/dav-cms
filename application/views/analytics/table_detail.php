<style>
@font-face {
    font-family: dav_font;
    src: url(<?php echo base_url()."assets/fonts/ufonts.com_digital-medium.ttf";?>);
}
.box {
	background-color: #3fc8f4;
	
	height: 129px;
	text-align: center;
	font-family: dav_font;
	padding-top: 10px;
	font-size: 20px;
	color: white;
	-webkit-box-shadow: inset -5px 65px 29px -49px rgba(0, 0, 0, 0.21);
	-moz-box-shadow: inset -5px 65px 29px -49px rgba(0, 0, 0, 0.21);
	box-shadow: inset -5px 65px 29px -49px rgba(0, 0, 0, 0.21);
}

.number {
	font-family: dav_font;
	font-size: 45px;
	color: white;
}

.box1 {
	position: relative;
	color: white;
	margin-top: -20px;
	background-color: #3fc8f4;
	height: 350px;
	font-family: dav_font;
	background-image: url("<?php echo base_url()."assets/img/box1-bg.png";?>");
	background-repeat: no-repeat;
}
.box1-footer{
	position: absolute;
	bottom: 0;
	text-align: center;
	background-color: #1f85a8;
	margin-left: -15px;
	width: 100%;
	padding-top: 5px;
	height: 50px;
}
.box2 {
	margin-top: -20px;
	font-family: dav_font;
	color: white;
	font-size: 24px;
	text-align: center;
	padding-top: 75px;
	background-color: #324e5c;
	height: 350px;
}
.box3 {
	margin-top: -20px;
	background-color: #1a86aa;
	height: 350px;
	font-family: dav_font;
	color: white;
	font-size: 24px;
	padding-top: 75px;
	text-align: center;
}
.img-circle {
	display: block;
	margin-top: 76px;
    margin-left: auto;
    margin-right: auto;
}

span.text1 {
	font-family: dav_font;
	color: white;
	font-size: 18px;
	text-align: center;
}


.img-center-div {
	display: block;
    margin-left: auto;
    margin-right: auto;
}
/*makes an anchor inactive(not clickable)*/
.inactive-link {
	pointer-events: none;
	cursor: default;
}

.resume-heading .social-btns {
	margin-top: 15px;
}

.resume-heading .social-btns i.fa {
	margin-left: -5px;
}

@media ( max-width : 992px) {
	.resume-heading .social-btn-holder {
		padding: 5px;
	}
}
/* resume stuff */
.bs-callout {
	-moz-border-bottom-colors: none;
	-moz-border-left-colors: none;
	-moz-border-right-colors: none;
	-moz-border-top-colors: none;
	border-color: #eee;
	border-image: none;
	border-radius: 3px;
	border-style: solid;
	border-width: 0px 0px 0px 0px;
	margin-bottom: 5px;
	padding: 20px;
}


.bs-callout:last-child {
	margin-bottom: 0px;
}

.bs-callout h4 {
	margin-bottom: 10px;
	margin-top: 0;
}

.bs-callout-danger {
	border-left-color: #d9534f;
}

.bs-callout-danger h4 {
	color: #d9534f;
}

.resume .list-group-item:first-child,.resume .list-group-item:last-child
	{
	border-radius: 0;
	background: transparent;
}
/* skill meter in resume. copy pasted from http://bootsnipp.com/snippets/featured/progress-bar-meter */
.progress-bar {
	text-align: left;
	white-space: nowrap;
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
	cursor: pointer;
}

.progress-bar>.progress-type {
	padding-left: 10px;
}

.progress-meter {
	min-height: 15px;
	border-bottom: 2px solid rgb(160, 160, 160);
	margin-bottom: 15px;
}

.progress-meter>.meter {
	position: relative;
	float: left;
	min-height: 15px;
	border-width: 0px;
	border-style: solid;
	border-color: rgb(160, 160, 160);
}

.progress-meter>.meter-left {
	border-left-width: 2px;
}

.progress-meter>.meter-right {
	float: right;
	border-right-width: 2px;
}

.progress-meter>.meter-right:last-child {
	/* border-left-width: 2px; */
}

.progress-meter>.meter>.meter-text {
	position: absolute;
	display: inline-block;
	bottom: -20px;
	width: 100%;
	font-weight: 700;
	font-size: 0.85em;
	color: rgb(160, 160, 160);
	text-align: left;
}

.progress-meter>.meter.meter-right>.meter-text {
	text-align: right;
}

/* CALENDAR */
#byStore, #byRegion, .input-group-addon {
	height: 30px;
	background: #324e5d;
	border: 1px solid #324e5d;
	font-family: dav_font;
	width: 200px;
	-webkit-border-radius:0px;
    -moz-border-radius:0px;
    border-radius:0px;
    color: white;
}
h3 {
	font-family: dav_font;
	color: white;
}
.progress{
	-webkit-border-radius:0px;
    -moz-border-radius:0px;
    border-radius:0px;
    background-color: #00adee;
}
.progress-bar-dav {
	background-color : #324e5d;
}

.text2{
	color: white;
	font-family: dav_font;
	float: right;
	padding-right: 8px;
}

.text3{
	color: white;
	font-family: dav_font;
	float: left;
	padding-left: 8px;
}

.box_number {
	width: 20px;
	height: 20px;
	background-color: #00adee;
	position: absolute;
	
	color: white;
	text-align: center;
	line-height: 22px;
	-webkit-border-radius:3px;
    -moz-border-radius:3px;
    border-radius:3px;
    font-family: dav_font;
    font-size: 12px;
}

.left {
	left: 0;
}

.right {
	right: 0;
}
/* The CSS */
select {
    padding:3px;
    margin: 0;
   	font-family: dav_font;
   	font-size: 12px;
    background: #324e5d;
    color:#FFF;
    border:none;
    outline:none;
    display: inline-block;
    width: 200px;
    height: 30px;
    -webkit-appearance:none;
    -moz-appearance:none;
    appearance:none;
    cursor:pointer;
}

/* Targetting Webkit browsers only. FF will show the dropdown arrow with so much padding. */
@media screen and (-webkit-min-device-pixel-ratio:0) {
    select {padding-right:18px}
}

label.dropdown1 {position:relative}
label.dropdown1:after {
    content:'>';
    
    color:#fff;
    -webkit-transform:rotate(90deg);
    -moz-transform:rotate(90deg);
    -ms-transform:rotate(90deg);
    transform:rotate(90deg);
    right:9px; top:5px;
    padding:0 0 2px;
   
    position:absolute;
    pointer-events:none;
}
label.dropdown1:before {
    content:'';
    right:0px; top:0px;
    width:25px; height:30px;
    background:#00adee;
    position:absolute;
    pointer-events:none;
    display:block;
}
/* CALENDAR */
#reservation, .input-group-addon {
	height: 30px;
	background: #324e5d;
	border: 1px solid #324e5d;
	font-family: dav_font;
	width: 200px;
	-webkit-border-radius:0px;
    -moz-border-radius:0px;
    border-radius:0px;
    color: white;
}

.interaction_detail{
	height: 100%;
	background-color: #1f85a8;
	padding-bottom: 15px;
}
.table-interaction {
	font-family: dav_font;
	color: white;
	font-size: 14px;
	margin-left: 80px;
	border: 10px solid #011627;
	width: 80%;
	background-color: #0a2a3b;
}
.most-brand-table tr {
	 border: 1px solid #011627;
	 padding-top: 10px;
}

.bs-callout {
	font-family: dav_font;
	color: white;
	font-size: 14px;
}

.no_of_interaction {
	padding-left: 3px;
	padding-top: 5px;
	color: white;
}

.font_small {
	font-size: 14px;
}
</style>

<script type="text/javascript" src="<?php echo base_url().'assets/analytics/js/da.common.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/analytics/js/da.report-topten.js'; ?>"></script>

<div class="col-md-11 byStore">
	<div class="bs-callout bs-callout-danger">
	<h3>TOP TEN INTERACTIONS BY STORE</h3>
		<ul class="list-group">
			<a class="list-group-item inactive-link" href="#" style="background-color: transparent; border: 0px;">
				<div id="top10byStore"></div>
				<div class="progress-meter">
					<div style="width:25%;" class="meter meter-left">
						<span style="margin-left: 70%;" class="meter-text">0 - 10</span>
					</div>
					<div style="width:25%;" class="meter meter-left">
						<span style="margin-left: 80%;" class="meter-text">100</span>
					</div>
					<div style="width:25%;" class="meter meter-left">
						<span style="margin-left: 70%;" class="meter-text">1000</span>
					</div>
					<div style="width:25%;" class="meter meter-left">
						<span style="margin-left: 60%;" class="meter-text">10000</span>
					</div>
				</div>
				<div class="no_of_interaction">No. of Interactions</div>
			</a>
		</ul>
	</div>
</div>

<script type="text/javascript">
            $(function() {
            	$('#reservation').daterangepicker();
            });

            $('#byStore').daterangepicker(
				{ 
							
				},
				function(start, end, label) {
					var arr = {}
					arr.cityId = <?php echo $city_id;?>;
					arr.startDate = start.format('YYYY-MM-DD');
					arr.endDate = end.format('YYYY-MM-DD');
					arr.variant_id = <?php echo $variant_id;?>;
					var form = $(".byStore");
					$.ajax({
						url : "<?php echo base_url()."index.php/report/top_ten_by_date";?>",
						type : 'POST',
						data : arr,
						error:function(){ 
							$(".byStore").css('opacity', '0.5');
							$(".spinner").show();
					},
					success : function(data) {
						$(".spinner").show();
						$(form).fadeOut(800, function(){
							form.html(data).fadeIn().delay(2000);
							$(".spinner").hide();
						});
					}	
				});
					//alert('A date range was chosen: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
				}
			);
            
            function print() {
                window.print();
            }
</script>