<style>
@font-face {
    font-family: dav_font;
    src: url(<?php echo base_url()."assets/fonts/ufonts.com_digital-medium.ttf";?>);
}
/* LIST #4 */
#list-menu { width:200px; font-size:18px; padding-left: 0px; font-family: dav_font; padding-top: 20px;}
#list-menu ul { list-style: none; }
#list-menu ul li { }
#list-menu ul li a { display:block; text-decoration:none; color:white; background-color:#0a293b; line-height:30px;
   padding-left:10px; cursor:pointer; }
#list-menu ul li a:hover { color:#1a86aa; }
#list-menu ul li a strong { margin-right:10px; }

ul.level2 {
	display:block; text-decoration:none; color:white; background-color:#0a293b; line-height:30px;
   padding-left:50px; cursor:pointer;
}

ul.level2 li:hover { color:#1a86aa; }

</style>
<table style="margin-top:-20px;">
	<tr>
		<td colspan="2">
			<div id="list-menu">
				<ul>
					<li><a href="<?php echo base_url();?>">Dashboard</a></li>
					<li><a href="<?php echo base_url('brand_report');?>">Brand Report</a></li>
					<li><a href="#">Admin Setting</a></li>
					<li><a href="<?php echo base_url('contact_us');?>">Contact Us</a></li>
				</ul>
			</div>
		</td>
	</tr>
</table>
