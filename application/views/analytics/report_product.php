<link rel="stylesheet" href="http://css-spinners.com/css/spinner/spinner.css" type="text/css">
<style>
@font-face {
    font-family: dav_font;
    src: url(<?php echo base_url()."assets/fonts/ufonts.com_digital-medium.ttf";?>);
}
.box {
	background-color: #3fc8f4;
	
	height: 129px;
	text-align: center;
	font-family: dav_font;
	padding-top: 10px;
	font-size: 20px;
	color: white;
	-webkit-box-shadow: inset -5px 65px 29px -49px rgba(0, 0, 0, 0.21);
	-moz-box-shadow: inset -5px 65px 29px -49px rgba(0, 0, 0, 0.21);
	box-shadow: inset -5px 65px 29px -49px rgba(0, 0, 0, 0.21);
}

.number {
	font-family: dav_font;
	font-size: 45px;
	color: white;
}


.box1 {
	position: relative;
	color: white;
	margin-top: -20px;
	background-color: #3fc8f4;
	height: 350px;
	font-family: dav_font;
	background-image: url("<?php echo base_url()."assets/img/box1-bg.png";?>");
	background-repeat: no-repeat;
}
.box1-footer{
	position: absolute;
	bottom: 0;
	text-align: center;
	background-color: #1f85a8;
	margin-left: -15px;
	width: 100%;
	padding-top: 5px;
	height: 50px;
}
.box2 {
	margin-top: -20px;
	font-family: dav_font;
	color: white;
	font-size: 24px;
	text-align: center;
	padding-top: 75px;
	background-color: #324e5c;
	height: 350px;
}
.box3 {
	margin-top: -20px;
	background-color: #1a86aa;
	height: 350px;
	font-family: dav_font;
	color: white;
	font-size: 24px;
	padding-top: 75px;
	text-align: center;
}
.img-circle {
	display: block;
	margin-top: 76px;
    margin-left: 33px;
    margin-right: auto;
}

.text1 {
	font-family: dav_font;
	color: white;
	font-size: 18px;
	
}

/* The CSS */
select {
    padding:3px;
    margin: 0;
   	font-family: dav_font;
   	font-size: 12px;
    background: #324e5d;
    color:#FFF;
    border:none;
    outline:none;
    display: inline-block;
    width: 200px;
    height: 30px;
    -webkit-appearance:none;
    -moz-appearance:none;
    appearance:none;
    cursor:pointer;
}

/* Targetting Webkit browsers only. FF will show the dropdown arrow with so much padding. */
@media screen and (-webkit-min-device-pixel-ratio:0) {
    select {padding-right:18px}
}

label.dropdown1 {position:relative}
label.dropdown1:after {
    content:'>';
    
    color:#fff;
    -webkit-transform:rotate(90deg);
    -moz-transform:rotate(90deg);
    -ms-transform:rotate(90deg);
    transform:rotate(90deg);
    right:9px; top:5px;
    padding:0 0 2px;
    position:absolute;
    pointer-events:none;
}
label.dropdown1:before {
    content:'';
    right:0px; top:0px;
    width:25px; height:30px;
    background:#00adee;
    position:absolute;
    pointer-events:none;
    display:block;
}

/* CALENDAR */
#periode, .input-group-addon {
	height: 30px;
	background: #324e5d;
	border: 1px solid #324e5d;
	font-family: dav_font;
	width: 200px;
	-webkit-border-radius:0px;
    -moz-border-radius:0px;
    border-radius:0px;
    color: white;
}

.table-interaction {
	position: relative;
	font-family: dav_font;
	color: white;
	font-size: 14px;
	margin-left: 150px;
	border: 5px solid #011627;
	width: 80%;
}
.most-brand-table tr {
	 border: 1px solid #011627;
	 padding-top: 10px;
	 font-family: dav_font;
	 }
}
.spinner{
	position: absolute;
	left:47%;
	top: 10%;
    margin-left:-20px;
}
.most_brand_interaction{
	height: 100%;
	background-color: #1f85a8;
	padding-bottom: 15px;
	margin-top: 20px;
}

/* unvisited link */
a:link {
    color: #FF0000;
}

/* visited link */
a:visited {
    color: white;
}

/* mouse over link */
a:hover {
    color: #00adee;
    text-decoration: none;
}

/* selected link */
a:active {
    color: #0000FF;
}

.font_small {
	font-size: 14px;
}

</style>
<script type="text/javascript" src="<?php echo base_url().'assets/analytics/js/da.common.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/analytics/js/da.report-product.js'; ?>"></script>
<?php $this->load->view('analytics/header'); ?>
<div class="col-md-12" style="padding-top:20px;">
	<div class="col-md-4">
		<div class="col-xs-6" style="padding-top: 10px;">
			<img width="115px" height="129px" src="<?php echo base_url()."assets/analytics/images/dav.png";?>" />
		</div>
		<div class="col-xs-6" style="padding-top: 10px; ">
			<?php 
				$attributes = array('method' => 'post', 'id' => 'form_ti');
				echo form_open('brand_report/topten', $attributes); ?>
			
			<table>
				<tr>
					<td colspan="3"> <span class="text1">TOTAL INTERACTIONS</span></td>
				</tr>
				<tr> 
					<td>
						<label class="dropdown1">
						    <select name="province" id="province">
						        <option value="0" selected>ALL PROVINCE</option>
						        <?php foreach($province as $v) { ?>
						        	<option value="<?php echo $v->id;?>"><?php echo $v->name;?></option>
						        <?php } ?>
						    </select>
						</label>
					</td>
					<td style="padding-left: 10px;">
						<label class="dropdown1">
						    <select name="city" id="city">
						        <option value="0" selected>ALL CITY</option>
						    </select>
						</label>
					</td>
					<td style="padding-left: 10px; padding-top: 10px;">
						<div class="form-group">
							<div class="input-group">
								<input type="hidden" name="start_date" id="start_date" value="0" />
								<input type="hidden" name="end_date" id="end_date" value="0" />
								<input style="width: 250px;" type="text" class="form-control pull-right" placeholder="Filter by period" id="periode"/>
								<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
							</div>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="3"> 
						<table width="100%" border="0" class="most-brand-table">
							<tbody>
								<tr>
									<td style="vertical-align: middle; padding-left: 10px; height: 50px; color: white;"><?php echo $product->name.", ".$variant->name; ?></td>
									<td style="vertical-align: middle; text-align: center;" width="10%"><font color="white" style="font-size: 24px;">&nbsp;<span id="total_interaction"></span></font></td>
									<td style="vertical-align: middle; text-align: left; color: white;">&nbsp; TOTAL INTERACTIONS </td>
									<td style="vertical-align: middle; text-align: center;"> <a class="view_detail" href="#" onclick="$.fn.topTen()">View Detail</a></td>
								</tr>
							</tbody>
						</table>   
					</td>
				</tr>
			</table>
			<?php echo form_close(); ?>
		</div>
	</div>
	
	<div class="col-md-12 dav-graph" >
		<!-- Today Interactions  -->
		<div class="col-md-12" style="margin-top: 50px;">
			<div id="todayChart" style="height: 300px; width: 100%;"></div>
		</div>
		<!-- Weekly Interactions  -->
		<div class="col-md-12" style="margin-top: 50px;">
			<div id="weekChart" style="height: 300px; width: 100%;"></div>
		</div>
		<!-- Monthly Interactions  -->
		<div class="col-md-12" style="margin-top: 50px;margin-bottom: 50px;">
			<div id="monthChart" style="height: 300px; width: 100%;"></div>
		</div>
	</div>
</div>