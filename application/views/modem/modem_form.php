<div class="modal-body">
	<?php if($page=="modem/edit") { ?>
	<input type="hidden" name="id" value="<?php echo $id; ?>" />
	<?php } ?>
	<div class="form-group">
		<label for="targetName">Brand Device</label>
		<div class="row">    	
			<div class="col-md-11">
				<select name="brand_devices_id" class="form-control">          
					<?php if(isset($brand_devices)) { ?>
						<?php foreach($brand_devices as $val) { ?>
							<option <?php if($page=="modem/edit") { if($modem->brand_devices_id==$val->id) { ?> selected="selected" <?php } } ?> value="<?php echo $val->id;?>"><?php echo $val->brand_name;?></option>
						<?php } ?>
					<?php }?>
				</select> 
			</div>        
		</div>
	</div>
	<div class="form-group">
		<label for="provider">Provider</label>
		<div class="row">    	
			<div class="col-md-11">
				<input type="text" class="form-control" name="provider" placeholder="Provider" <?php if($page=="modem/edit") {  ?> value="<?php echo $modem->provider; ?>" <?php  } ?> /> 
			</div>    
		</div>
	</div>
	<div class="form-group">
		<label for="model">Model</label>
		<div class="row">    	
			<div class="col-md-11">
				<input type="text" class="form-control" name="model" placeholder="Model" <?php if($page=="modem/edit") {  ?> value="<?php echo $modem->model; ?>" <?php  } ?> /> 
			</div>    
		</div>
	</div>
	<div class="form-group">
		<div class="row">    	
			<div class="col-md-11">
				<label for="IMEI">IMEI</label>
				<input type="text" class="form-control" name="imei" placeholder="IMEI" <?php if($page=="modem/edit") {  ?> value="<?php echo $modem->IMEI; ?>" <?php  } ?> /> 
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="row">    	
			<div class="col-md-11">
				<label for="SIM_number">SIM Number</label>
				<input type="text" class="form-control" name="SIM_number" placeholder="SIM Number" <?php if($page=="modem/edit") {  ?> value="<?php echo $modem->SIM_number; ?>" <?php  } ?> /> 
			</div>
		</div>
	</div>
	<br />
	<input class="btn btn-primary" id="upload" type="submit" value="<?php if($page=="modem/edit") { ?>Update<?php } else { ?>Submit<?php } ?>" />
	<a href="<?php echo base_url().'modem'; ?>"><input class="btn btn-primary" data-dismiss="modal" id="cancel" type="button" value="Cancel" /></a>
</div>