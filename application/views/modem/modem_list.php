<div class="col-md-12">				
	<div style="margin-left: -15px;">
		<a title="Add Widget" data-toggle="modal" href="#addWidgetModal" class="btn btn-primary">Register New Modem</a>
	</div>
	<br />
	<table id="tbl" class="table table-striped table-bordered" border="0">
		<thead>
			<tr>
				<th style="text-align: center;">No</th>
				<th style="text-align: center;">Store</th>
				<th style="text-align: center;">Provider</th>
				<th style="text-align: center;">Brand/Model</th>
				<th style="text-align: center;">IMEI</th>
				<th style="text-align: center;">SIM Number</th>
				<th style="text-align: center;">Action</th>
			</tr>
		</thead>
		<tbody>
			<?php $no=1;?>
			<?php foreach($modem as $val) {?>
			<tr>
				<td style="text-align: center;"> <?php echo $no++;?></td>
				<td style="text-align: center;"> <?php echo $val->store_id; ?></td>
				<td style="text-align: center;"> <?php echo $val->provider;?></td>
				<td style="text-align: center;"> <?php echo $val->brand_name;?>/<?php echo $val->model;?></td>
				<td style="text-align: center;"> <?php echo $val->IMEI;?></td>
				<td style="text-align: center;"> <?php echo $val->SIM_number;?></td>
				
				<td style="vertical-align: middle; text-align: center;">
					<div id = "test">
					<p>
						<a class="edit" href="<?php echo $this->config->item('base_url')."/modem/edit/".$val->id;?>">
							<button class="btn btn-primary btn-xs edit" data-title="Edit" rel="tooltip">
								<span class="glyphicon glyphicon-pencil"></span>
							</button>
						</a>
						<a class="trash" href="#" onclick="$.fn.delete('<?php echo base_url().'modem/delete/'. $val->id; ?>');">
						<button class="btn btn-danger btn-xs" data-title="Delete" rel="tooltip">
							<span class="glyphicon glyphicon-trash"></span>
						</button>
						</a>
					</p>
					</div>
				</td>
			</tr>
			<?php }?>
		</tbody>
	</table>
	<?php echo $this->pagination->create_links(); ?>
	<?php $this->load->view('ping_footer');	?>
</div>
<!--/col-span-6-->