<?php $this->load->view('generaljs'); ?>
<div class="row">
	<?php include 'modem_list.php'?>
</div>
<div class="modal fade" id="addWidgetModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<a href="#" class="pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle pull-right"></span></a>
				<h4 class="modal-title">Add Modem</h4>
			</div>
			<?php echo form_open('modem/add');?>
			<?php $this->load->view('modem/modem_form'); ?>
			<?php echo form_close();?>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dalog -->
</div>
<!-- /.modal -->


	