<?php $this->load->view('generaljs'); ?>
<div class="row">
	<?php include 'user_list.php'?>
</div>
	
<div class="modal fade" id="addWidgetModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<a href="#" class="pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle pull-right"></span></a>
				<h4 class="modal-title">Add User</h4>
			</div>
			<?php echo form_open('user/add');?>
			<?php $this->load->view('user/user_form'); ?>
			<?php echo form_close();?>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dalog -->
</div>
<!-- /.modal -->


