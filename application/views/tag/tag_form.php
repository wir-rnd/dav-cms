<div class="modal-body">
	<?php $path = 'tag/add'; if($page=="tag/edit") $path = 'tag/update'; else $path = 'tag/add'; echo form_open($path);?>
		<?php if($page=="tag/edit") { ?><input type="hidden" name="id" value="<?php echo $tag->id; ?><?php } ?>
		<div class="form-group">
			<label for="targetName">Tag Name</label>
			<input type="text" class="form-control" id="tag_name" name="name" placeholder="" <?php if($page=="tag/edit") {  ?> value="<?php echo $tag->name; ?>" <?php  } ?> />
		</div>
		<br /><br />
		<input class="btn btn-primary" id="upload" type="submit" value="<?php if($page=="tag/edit") echo 'Update'; else echo 'Add'; ?>" />
		<input class="btn btn-primary" data-dismiss="modal" id="cancel" type="reset" value="Cancel" />
	<?php echo form_close();?>
</div>