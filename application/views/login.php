<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('head'); ?>
<style>
body {
	background: url("<?php echo base_url()."/assets/img/background1.png";?>") no-repeat center center fixed;
	-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
	background-size: cover;
}

.vertical-offset-100{
    padding-top:100px;
}
</style>
<body>
	<div id="top-nav" class="navbar navbar-inverse navbar-static-top">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target=".navbar-collapse">
				<span class="icon-toggle"></span>
			</button>
			
		</div>
		<div class="navbar-collapse collapse">
			
				<img style="margin-left: 20px; margin-top: 7px;" src="<?php echo $this->config->item('assets')."/img/dcontrol_center.png"?>" />
			
			
		</div>
	</div>
	<!-- /Header -->
	<div class="container">
		<div class="row vertical-offset-100">
			<div class="col-md-4 col-md-offset-4">
				<?php $this->load->view('message'); ?>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Please sign in</h3>
					</div>
					<div class="panel-body">
						<?php $attr = array('id'=>'MyForm');?>
						<?php echo form_open('auth/login', $attr)?>
						<fieldset>
							<div class="form-group">
								<input class="form-control" placeholder="Username" name="username" type="text">
							</div>
							<div class="form-group">
								<input class="form-control" placeholder="Password" name="password" type="password" value="">
							</div>
							<!-- div class="checkbox">
								<label>
									<input name="remember" type="checkbox" value="Remember Me"> Remember Me
								</label>
							</div -->
							<input class="btn btn-lg btn-success btn-block" type="submit" value="Login">
						</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>