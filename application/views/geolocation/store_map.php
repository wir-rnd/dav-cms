<style>
	#map-canvas {
		height:400px;
		width: 100%;
		margin-left: 24px;
		padding: 0px;
	}
</style>
<script>
	$(document).ready(function() {
	  	$("#map-canvas").show();
	  	$("#full_map").click(function() {
	  		$("#map-canvas").slideUp(700);
		});
	});
</script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
<script>
	var map;

	function initialize() {
		  var myLatlng = new google.maps.LatLng(<?php if(isset($geo[0]->longitude)) {echo $geo[0]->longitude;}?>, <?php if(isset($geo[0]->latitude)) {echo $geo[0]->latitude;}?>);
		  var mapOptions = {
		    zoom: 15,
		    center: myLatlng
		  }
		  var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

		  var marker = new google.maps.Marker({
		      position: myLatlng,
		      map: map
		  });
	}

	google.maps.event.addDomListener(window, 'load', initialize);
</script>
<div class="row">
	<div class="col-md-12">
		<div id="map-canvas"></div>
		<br />
		<form action="http://maps.google.com/maps" method="get" target="_blank">
		  
		   <input type="hidden" name="daddr" value="-6.198038,106.7849122" style="visibility: hidden;" />
		   <input type="submit" value="Get directions" />
		</form>
		<?php echo form_open('store/geolocation_update');?>
		<input type="text" name="store_id" class="form-control" placeholder="Longitude" style="visibility: hidden;" value="<?php echo $this->uri->segment("3"); ?>" >
		<label>Longitude</label>
		<input type="text" name="longitude" class="form-control" placeholder="Longitude" value="<?php if(isset($geo[0]->longitude)) {echo $geo[0]->longitude;}?>">
		<br />
		<label>Latitude</label>
		<input type="text" name="latitude" class="form-control" placeholder="Latitude" value="<?php if(isset($geo[0]->latitude)) {echo $geo[0]->latitude;}?>">
		<br />
		<button class="btn btn-info">SAVE</button>&nbsp;<a href="<?php echo base_url()."store"?>" class="btn btn-info">Back</a>
		<?php echo form_close();?>
	</div>
</div>