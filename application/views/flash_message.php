<?php $flash_message = $this->session->flashdata('flash_message'); ?>
<?php if($flash_message) { ?>
	<?php foreach($flash_message as $m) { ?>
		noty({
			text: '<?php echo $m; ?>',
			layout: 'center',
			type: 'information',
			timeout: 2000
		});
	<?php } ?>
<?php } ?>
<?php // $this->session->set_userdata('message', null); ?>
