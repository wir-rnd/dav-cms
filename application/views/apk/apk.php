<div class="col-md-12">				
    <div style="margin-left: -15px;">
        
		<a title="Add Widget" data-toggle="modal" href="#addWidgetModal" class="btn btn-primary ">Add New APK</a>
		
    </div>
	<br />
	<div>
		<?php 
			$error = $this->session->flashdata('upload_error');
			
			if($error!='')
			{
				echo 'Error<br /><ul>';
				foreach($error as $er)
				{
					echo '<li>'.$er. '</li>';
				}
				echo '</ul>';
			}
		?>
	</div>
    <br />
	<?php echo $this->session->flashdata('message'); ?>
    <br />
    <br />
    <div class="panel panel-default">
        <table class="table table-striped paginated" border="0">
            <thead>
				<tr>
					<th style="text-align: center;">No</th>
					<th style="text-align: center;">Version</th>
					<th style="text-align: center;">Code</th>
					<th style="text-align: center;">Size</th>
					<th style="text-align: center;">MD5</th>
				</tr>
            </thead>
            <?php $no=1; ?>
            <?php foreach($apk as $val) { ?>
            <tr>
				<td style="text-align: center;"><?php echo $no;?></td>
                <td style="text-align: center;"><?php echo $val->version;?></td>
                <td style="text-align: center;"><?php echo $val->number_ver;?></td>
                <td style="text-align: center;"><?php echo $val->size;?> MB</td>
                <td style="text-align: center;"><?php echo $val->md5;?></td>
                <td style="vertical-align: middle; text-align: center;">
                    <div id = "test">
                    <p>
                        <a class="edit" href="<?php echo $this->config->item('base_url')."/shelf/delete/".$val->id;?>" onclick="javascript: return confirm('Are you SURE you want to delete these item?')">
                            <button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" data-placement="top" rel="tooltip">
                                <span class="glyphicon glyphicon-trash"></span>
                            </button>
                        </a>
                    </p>
                    </div>
                </td>
            </tr>
            <?php $no++; ?>
            <?php }?>
			
			<tbody>
				
			</tbody>
		</table>
	</div>
	<?php echo $this->pagination->create_links(); ?>
	<?php $this->load->view('ping_footer'); ?>
</div>

<div class="modal fade" id="addWidgetModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <a href="#" class="pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle pull-right"></span></a>
                <h4 class="modal-title">Add APK</h4>
            </div>
            <div class="modal-body">
                <?php echo form_open_multipart('apk/add', 'id="apk"'); ?>
                    <div class="form-group">
						<div class="row">    	
							<div class="col-md-11">
								<label for="userfile">File</label>
                                    <input style="margin-bottom: 5px;" type="file" name="userfile" id="userfile" size="20" />
							</div>
                        </div>
                    </div>
                    <br />
                    <input class="btn btn-primary" id="upload" type="submit" value="Submit" />
                    <input class="btn btn-primary" data-dismiss="modal" id="cancel" type="reset" value="Cancel" />
                <?php echo form_close();?>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
<!-- /.modal-dalog -->
</div>
<!-- /.modal -->


	