
<div class="row">
	<?php include 'nav.php'?>
</div>
<div class="row">
	<?php include 'device_list.php'?>
</div>
	
<div class="modal fade" id="addWidgetModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<a href="#" class="pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle pull-right"></span></a>
				<h4 class="modal-title">Add Device</h4>
			</div>
			<?php echo form_open('device/add');?>
			<?php $this->load->view('device/device_form'); ?>
			<?php echo form_close();?>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dalog -->
</div>
<!-- /.modal -->


