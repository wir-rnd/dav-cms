<div class="modal-body">
	<?php if($page=="device/edit") { ?>
	<input type="hidden" name="id" value="<?php echo $id; ?>" />
	<?php } ?>
	<div class="form-group">
		<label for="targetName">Brand Device</label>
		<div class="row">    	
			<div class="col-md-11">
				<select name="brand_devices_id" class="form-control">          
					<?php if(isset($brand_devices)) { ?>
						<?php foreach($brand_devices as $val) { ?>
							<option <?php if($page=="device/edit") { if($devices->brand_devices_id==$val->id) { ?> selected="selected" <?php } } ?> value="<?php echo $val->id;?>"><?php echo $val->brand_name;?></option>
						<?php } ?>
					<?php }?>
				</select> 
			</div>        
		</div>
	</div>
	<div class="form-group">
		<label for="os_platform">Operating System</label>
		<div class="row">    	
			<div class="col-md-11">
				<select name="os_devices_id" class="form-control" > 
				  <?php if(isset($os_devices)) {?>
						<?php foreach($os_devices as $val) {?>
							<option <?php if($page=="device/edit") { if($devices->os_devices_id==$val->id) { ?> selected="selected" <?php } } ?> value="<?php echo $val->id;?>"><?php echo $val->os_name;?></option>
						<?php }?>
					<?php }?>
				</select> 
			</div>        
		</div>
	</div>
	<div class="form-group">
		<div class="row">    	
			<div class="col-md-11">
				<label for="serial_number">Serial Number</label>
				<input type="text" class="form-control" name="serial_number" placeholder="Serial number" <?php if($page=="device/edit") {  ?> value="<?php echo $devices->serial_number; ?>" <?php  } ?> /> 
			</div>
		</div>
	</div>
	<div class="form-group">
		<div class="row">    	
			<div class="col-md-11">
				<label for="imei">IMEI</label>
				<input type="text" class="form-control" name="imei" placeholder="IMEI" <?php if($page=="device/edit") {  ?> value="<?php echo $devices->imei; ?>" <?php  } ?> /> 
			</div>
		</div>
	</div>
	<br />
	<input class="btn btn-primary" id="upload" type="submit" value="<?php if($page=="device/edit") { ?>Update<?php } else { ?>Submit<?php } ?>" />
	<a href="<?php echo base_url().'device'; ?>"><input class="btn btn-primary" data-dismiss="modal" id="cancel" type="button" value="Cancel" /></a>
</div>