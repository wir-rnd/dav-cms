<div class="col-md-12">				
	<table id="tbl" class="table table-striped table-bordered" border="0">
		<thead>
			<tr>
				<th style="text-align: center;">No</th>
				<th style="text-align: center;">Store</th>
				<th style="text-align: center;">Shelf</th>
				<th style="text-align: center;">Serial Number</th>
				<th style="text-align: center;">IMEI</th>
				<th style="text-align: center;">Action</th>
			</tr>
		</thead>
		<tbody>
			<?php $no=1;?>
			<?php foreach($devices as $val) {?>
			<tr>
				<td style="text-align: center;"> <?php echo $no;?></td>
				<td style="text-align: center;"> <?php echo $val->store_id;?></td>
				<td style="text-align: center;"> <?php echo $val->shelf;?></td>
				<td style="text-align: center;"> <?php echo $val->serial_number;?></td>
				<td style="text-align: center;"> <?php echo $val->imei;?></td>
				<td style="vertical-align: middle; text-align: center;">
					<p>
						<a class="edit" href="<?php echo $this->config->item('base_url')."/device/edit/".$val->id;?>">
							<button class="btn btn-primary btn-xs edit" data-title="Edit" rel="tooltip">
								<span class="glyphicon glyphicon-pencil"></span>
							</button>
						</a>
						<a class="edit" href="<?php echo $this->config->item('base_url')."/device/reset/".$val->id;?>">
							<button class="btn btn-primary btn-xs edit" data-title="Edit" rel="tooltip">
								<span class="glyphicon glyphicon-refresh"></span>
							</button>
						</a>
						<a class="edit" href="<?php echo $this->config->item('base_url')."/device/dummy_update/".$val->id;?>">
							<button class="btn btn-primary btn-xs edit" data-title="Edit" rel="tooltip">
								<span class="glyphicon glyphicon-download-alt"></span>
							</button>
						</a>
						<a class="trash" href="#" onclick="$.fn.delete('<?php echo base_url().'device/delete/'. $val->id; ?>');">
						<button class="btn btn-danger btn-xs" data-title="Delete" rel="tooltip">
							<span class="glyphicon glyphicon-trash"></span>
						</button>
						</a>
					</p>
				</td>
			</tr>
				<?php $no++;?>
			<?php }?>
		</tbody>
	</table>
	<?php echo $this->pagination->create_links(); ?>
	<?php $this->load->view('ping_footer');	?>
</div>
<!--/col-span-6-->