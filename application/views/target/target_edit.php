<?php
    if(isset($status))
    {
        echo $status;
    }
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
			<div>
            <?php $msg = $this->session->flashdata('message'); ?>
			<?php if($msg) { ?>
				<ul>
				<?php foreach($msg as $m) { ?>
					<li><?php echo $m; ?></li>
				<?php } ?>
				</ul>
			<?php } ?>
			</div>
			<br />
            <?php echo form_open('target/update'); ?>
            <table class="table table-striped" border="0">
                <tr>
                    <td>ID</td>
                    <td><input type="hidden" value="<?php echo $target->target_id; ?>" name="target_id" /><?php echo $target->target_id; ?></td>
                </tr>
                <tr>
                    <td>Target Name</td>
                    <td><?php echo $target->name; ?></td>
                </tr>
                <tr>
                    <td>Size</td>
                    <td>
                        <?php echo $target->width; ?> x <?php echo $target->height; ?> px
                    </td>
                </tr>
                <tr>
                    <td>Tracking Rating</td>
                    <td>
                        <?php 
                        for($i=0; $i<=5;$i++)
                        {
                            if ($target->tracking_rating == $i)
                            {
                        ?>
                        <img src="<?php echo $this->config->item('assets')."/img/stars-".$i.".png"?>" />
						
                        <?php
                            }
                        }
                        ?>
						<select name="tracking_rating">
							<?php for($i=0;$i<6;$i++) { ?>
								<option <?php if($i==$target->tracking_rating) { ?>selected="selected"<?php } ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
							<?php } ?>
						</select>
                    </td>
                </tr>
                <tr>
                    <td style="width:30%;">Image</td>
                    <td>
                        <img width="<?php echo $target->width/2; ?>"  height="<?php echo $target->height/2; ?>" src="<?php echo base_url().'/targets/'. $target->name . '/' . $target->name .'.tex.jpg'; ?>" />
                    </td>
                </tr>
				<tr>
                    <td style="width:30%;"></td>
                    <td>
                        <input type="submit" value="Update" />
                    </td>
                </tr>
                <?php
                if($this->uri->segment(2)=="edit") { 
                    echo form_close();
                }
                ?>    
            </table>
        </div>
        <?php echo $this->pagination->create_links(); ?>
    </div>
</div>

<div class="modal fade" id="addWidgetModal">
    <div class="modal-dialog">
            <div class="modal-content">
                    <div class="modal-header">
                            <a href="#" class="pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle pull-right"></span></a>
                            <h4 class="modal-title">Add Target</h4>
                    </div>
                    <div class="modal-body">
                            <?php echo form_open_multipart('marker/upload');?>
                                    <div class="form-group">
                                        <label for="targetName">Brand Name</label>
                                        <input type="text" class="form-control" id="targetName" name="targetName" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label for="dimension">Target Dimension Width</label>
                                        <input type="text" class="form-control" id="dimension" name="dimension" placeholder="">
                                    </div>
                                    <div style="margin-bottom: 10px;">
                                            <label for="userfile">Target Image File</label>
                                            <input style="margin-bottom: 5px;" type="file" name="userfile" id="userfile" size="20" />
                                    </div>
                                    <br /><br />
                                    <input class="btn btn-primary" id="upload" type="submit" value="Upload" />
                                    <input class="btn btn-primary" data-dismiss="modal" id="cancel" type="reset" value="Cancel" />
                            <?php echo form_close();?>
                    </div>
            </div>
            <!-- /.modal-content -->
    </div>
    <!-- /.modal-dalog -->
</div>
<!-- /.modal -->
