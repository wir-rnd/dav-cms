<div class="col-md-12">
	<div>
		<a title="Add Widget" data-toggle="modal" href="#addWidgetModal" class="btn btn-primary ">Add Target</a>&nbsp;
		<!-- a title="Sync" data-toggle="modal" href="<?php echo base_url().'target/refresh'; ?>" class="btn btn-primary ">Sync</a -->
	</div>
	<br />
	<table id="tbl" class="table table-striped table-bordered" border="0">
		<thead>
			<tr>
				<!-- <th>Target ID</th>  -->
				<th>No</th>
				<th>Target Name</th>
				<th>Width</th>
				<th>Height</th>
				<th>Augmentable</th>
				<th style="text-align: center;">Action</th>
			</tr>
		</thead>
		<tbody>
		<?php $no = 1;?>
		<?php foreach ($target as $val) {?>
			<tr>
				<td style="vertical-align: middle;">
					<?php echo $no++; ?>
				</td>
				<td><a href="<?php echo base_url().'target/detail/'. $val->target_id; ?>"><?php echo $val->name; ?></a></td>
				<td style="vertical-align: middle;">
					<?php echo $val->width; ?>
				</td>
				<td style="vertical-align: middle;">
					<?php echo $val->height; ?>
				</td>
				<td>
					<?php 
					for ($i=0; $i<=5;$i++) {
						if ($val->tracking_rating == $i) {
					?>
							<img src="<?php echo $this->config->item('assets')."/img/stars-".$i.".png"?>" />
					<?php
						}
					}
					?>
				</td>

				<td style="vertical-align: middle; text-align: center;"><p >
					<!-- a style="text-decoration: none;" href="<?php echo $this->config->item('base_url')."/target/tag/".$val->target_id;?>" title="assign tag">
							<button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" data-placement="top" rel="tooltip" >
							<span class="glyphicon glyphicon-tags"></span>
							</button>
					</a -->
					<a style="text-decoration: none;" href="<?php echo $this->config->item('base_url')."/target/content/".$val->target_id;?>" title="assign content">
						<button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" data-placement="top" rel="tooltip">
						<span class="glyphicon glyphicon-picture"></span>
						</button>
					</a>
					<a href="<?php echo base_url().'target/edit/'. $val->target_id;?>">
						<button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" data-placement="top" rel="tooltip">
								<span class="glyphicon glyphicon-pencil"></span>
						</button>
					</a>
					<a class="trash" href="#" onclick="$.fn.delete('<?php echo base_url().'target/delete/'. $val->target_id; ?>');">
					<button class="btn btn-danger btn-xs" data-title="Delete" rel="tooltip">
						<span class="glyphicon glyphicon-trash"></span>
					</button>
					</a>
				</p>

				</td>
				</tr>
				<?php $i++; ?>
				<?php }?>
		</tbody>
	</table>
	<?php echo $this->pagination->create_links(); ?>
	<?php $this->load->view('ping_footer'); ?>
</div>