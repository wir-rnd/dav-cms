<?php $this->load->view('generaljs'); ?>
<div class="row">
    <?php include 'target_list.php' ?>
</div>

<div class="modal fade" id="addWidgetModal">
    <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
					<a href="#" class="pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle pull-right"></span></a>
					<h4 class="modal-title">Add Target</h4>
			</div>
			<div class="modal-body">
					<?php echo form_open_multipart('target/upload');?>
							<div style="margin-bottom: 10px;">
									<label for="userfile">Target File (Zip Compressed)</label>
									<input style="margin-bottom: 5px;" type="file" name="userfile" id="userfile" size="20" />
							</div>
							<div class="form-group">
								<label for="targetRating">Rating</label>
								<select class="form-control" name="tracking_rating">
									<option value="0">0</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
								</select>
							</div>
							<br /><br />
							<input class="btn btn-primary" id="upload" type="submit" value="Upload" />
							<input class="btn btn-primary" data-dismiss="modal" id="cancel" type="reset" value="Cancel" />
					<?php echo form_close();?>
			</div>
		</div>
    </div>
</div>
