<div class="col-md-12">				
	<div>
		<a title="Add Widget" data-toggle="modal" href="#addWidgetModal" class="btn btn-primary ">Add Content</a>
	</div>
	<br />
        <div>
            <?php echo $this->session->flashdata('message'); ?>
        </div>
	<div class="panel panel-default">
            <table class="table table-striped paginated" border="0">
                <thead>
                    <tr>
                        <th style="text-align: center;">Target ID</th>
                        <th style="text-align: center;">Content Name</th>
                    </tr>
                </thead>

                <?php if (isset($target_content)) { ?>
                <?php foreach($target_content as $val) { ?>
                <tr>
                    <td style="text-align: center;"> <?php echo $this->uri->segment(3); ?></td>
                    <td style="text-align: center;"> <?php echo $val->Name;?></td>

                    <td style="vertical-align: middle; text-align: center;">
                        <div id = "test">
                        <p>
                            <a class="edit" href="<?php echo $this->config->item('base_url')."/target/content_delete/".$val->ContentID;?>" onclick="javascript: return confirm('Are you SURE you want to delete these item?')">
                                <button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" data-placement="top" rel="tooltip">
                                        <span class="glyphicon glyphicon-trash"></span>
                                </button>
                            </a>
                        </p>
                        </div>
                    </td>
                </tr>

                <?php } } else { ?>
                <tr>
                        <td colspan="7" style="text-align: center;"> <?php echo "NO CONTENT ADDED FOR THIS MARKER";?></td>
                </tr>
                <?php }?>
            </table>
	</div>
	
</div>
<!--/col-span-6-->

<div class="modal fade" id="addWidgetModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                    <a href="#" class="pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle pull-right"></span></a>
                    <h4 class="modal-title">Add Content</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped" border="0">
                    <thead>
                            <tr>
                                <th style="text-align: center;">Content ID</th>
                                <th style="text-align: center;">Name</th>
                            </tr>
                            <?php $uri = $this->uri->segment(3);?>
                            <?php if (isset($content)) { ?>
                            <?php foreach($content as $val) {?>
                                <tr>
                                    <td style="text-align: center;"><a href="<?php echo $this->config->item('base_url')."/target/content_add/".$this->uri->segment(3)."/".$val->id; ?>"><?php echo $val->id;?></a></td>
                                    <td style="text-align: center;"><a href="<?php echo $this->config->item('base_url')."/target/content_add/".$this->uri->segment(3)."/".$val->id; ?>"><?php echo $val->name; ?></a></td>
                                </tr>
                            <?php } } else { ?>
                            <tr>
                                <td colspan="5" style="text-align: center;"> <?php echo "NO CONTENT ADDED";?></td>
                            </tr>
                            <?php }?>
                    </thead>
                </table>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dalog -->
</div>