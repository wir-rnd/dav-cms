<div class="row">
    <div class="col-md-12">
		<a title="Back" data-toggle="modal" href="<?php echo base_url().'target'; ?>" class="btn btn-primary ">Back</a><br /><br />
        <div class="panel panel-default">
            <table class="table table-striped" border="0">
                <tr>
                    <td>ID</td>
                    <td><?php echo $target->target_id; ?></td>
                </tr>
                <tr>
                    <td>Target Name</td>
                    <td>
                        <?php if($this->uri->segment(2)=="edit") { ?>
                            <input style="width:100%;" type="text" name="name" value="<?php echo $target->name; ?>" />
                        <?php } else { ?>
                            <?php echo $target->name; ?>
                        <?php } ?>
                    </td>
                </tr>
                <tr>
                    <td>Size</td>
                    <td>
                        <?php echo $target->width; ?> x <?php echo $target->height; ?> px
                    </td>
                </tr>
                <tr>
                    <td>Tracking Rating</td>
                    <td>
                        <?php 
                        for($i=0; $i<=5;$i++)
                        {
                            if ($target->tracking_rating == $i)
                            {
                        ?>
                        <img src="<?php echo $this->config->item('assets')."/img/stars-".$i.".png"?>" />
                        <?php
                            }
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td style="width:30%;">Image</td>
                    <td>
                        <img width="<?php echo $target->width/2; ?>"  height="<?php echo $target->height/2; ?>" src="<?php echo base_url().'/targets/'. $target->name . '/' . $target->name .'.tex.jpg'; ?>" />
                        <?php if($this->uri->segment(2)=="edit") { ?>
                            <input style="margin-bottom: 5px;" type="file" name="userfile" id="userfile" size="20" />
                        <?php } ?>
                    </td>
                </tr>
            </table>
        </div>
        <?php echo $this->pagination->create_links(); ?>
		<?php $this->load->view('ping_footer'); ?>
    </div>
</div>

<div class="modal fade" id="addWidgetModal">
    <div class="modal-dialog">
            <div class="modal-content">
                    <div class="modal-header">
                            <a href="#" class="pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle pull-right"></span></a>
                            <h4 class="modal-title">Add Target</h4>
                    </div>
                    <div class="modal-body">
                            <?php echo form_open_multipart('marker/upload');?>
                                    <div class="form-group">
                                        <label for="targetName">Brand Name</label>
                                        <input type="text" class="form-control" id="targetName" name="targetName" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label for="dimension">Target Dimension Width</label>
                                        <input type="text" class="form-control" id="dimension" name="dimension" placeholder="">
                                    </div>
                                    <div style="margin-bottom: 10px;">
                                            <label for="userfile">Target Image File</label>
                                            <input style="margin-bottom: 5px;" type="file" name="userfile" id="userfile" size="20" />
                                    </div>
                                    <br /><br />
                                    <input class="btn btn-primary" id="upload" type="submit" value="Upload" />
                                    <input class="btn btn-primary" data-dismiss="modal" id="cancel" type="reset" value="Cancel" />
                            <?php echo form_close();?>
                    </div>
            </div>
            <!-- /.modal-content -->
    </div>
    <!-- /.modal-dalog -->
</div>
<!-- /.modal -->
