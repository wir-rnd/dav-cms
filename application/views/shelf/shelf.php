<?php $this->load->view('generaljs'); ?>
<div class="row">
	<?php include 'shelf_list.php'?>
</div>

<div class="modal fade" id="addWidgetModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <a href="#" class="pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove-circle pull-right"></span></a>
                <h4 class="modal-title">Add Shelf</h4>
            </div>
            <?php include 'shelf_form.php' ?>
        </div>
        <!-- /.modal-content -->
    </div>
<!-- /.modal-dalog -->
</div>
<!-- /.modal -->


	