<script type="text/javascript">
$(document).ready(function() {
	  // Handler for .ready() called.
	 
	
	$('#id').hide();
});

</script>
<div class="modal-header">
	<h4 class="modal-title">EDIT SHELF</h4>
</div>
<?php 
	//print_r($location_list_detail);
	if (isset($shelf)) {
?>
    <div class="modal-body">
        <?php echo form_open('shelf/update');?>
        <input type="hidden" name="id" value="<?php echo $shelf->id; ?>" />
            <div class="form-group">
                <div class="row">    	
                    <div class="col-md-11">
                        <label for="serial_number">Shelf Name</label>
                                <input type="text" class="form-control" name="name" value="<?php echo $shelf->name; ?>" placeholder="Shelf Name"> 
                    </div>
                </div>
            </div>

            <input class="btn btn-primary" id="upload" type="submit" value="Submit" />
            <a href="<?php echo $this->config->item('base_url')."/shelf";?>"><input class="btn btn-primary" data-dismiss="modal" id="cancel" type="button" value="Cancel" /></a>
        <?php echo form_close();?>
    </div>
				
<?php } else {
        echo "DATA NOT FOUND";
}?>