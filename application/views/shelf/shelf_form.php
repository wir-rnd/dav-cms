<div class="modal-body">
	<?php $path = 'shelf/add'; if($page=="shelf/edit") $path = 'shelf/update'; else $path = 'shelf/add'; echo form_open($path);?>
		<?php if($page=="shelf/edit") { ?><input type="hidden" name="id" value="<?php echo $shelf->id; ?><?php } ?>
		<div class="form-group">
			<label for="name">Shelf Name</label>
			<input type="text" class="form-control" name="name" placeholder="Shelf Name" <?php if($page=="shelf/edit") {  ?> value="<?php echo $shelf->name; ?>" <?php  } ?> /> 
		</div>
		<br />
		<input class="btn btn-primary" id="upload" type="submit" value="<?php if($page=='shelf/edit') echo 'Update'; else echo 'Add'; ?>" />
		<input class="btn btn-primary" data-dismiss="modal" id="cancel" type="reset" value="Cancel" />
	<?php echo form_close();?>
</div>