<?php
class MY_Controller extends CI_Controller
{
	var $db;
	var $session_data;
	function __construct()
	{
		parent::__construct();
		$this->load->model('common');
		$this->load->model('update');
		$this->load->model('delete');
		$this->load->library('pagination');
		$this->session->set_userdata('message', array());
		$this->session->set_userdata('flash_message', array());
		$this->db = $this->common->db();
		$this->session_data = $this->session->userdata('is_logged_in');
		//$this->message('BROADCAST MESSAGE TEST', 'info');
		//$this->message('test constructor 2', 'info');
		//$this->message('test constructor 3');
		//$this->flash_message('flash message');
	}
	
	function authCheck()
	{
		if($this->session->userdata('is_logged_in'))
		{
			$this->authData = $this->session->userdata('is_logged_in');
			return;
		}
		else
			redirect('auth', 'refresh');
	}
	
	function _get_post_data()
	{
		if (isset($_POST))
		{
			$val_submit = array();
			foreach($_POST as $idx => $val){
				if(is_array($val)){
					foreach($val as $vidx=>$vval)
						$val_submit[$idx][$vidx] = (string)$val[$vidx];
				}
				else
					$val_submit[$idx] = $this->input->post($idx);
	
			}
			return $val_submit;
		}
	
		return false;
	}
	
	function saveAuth($data)
	{
		$this->session->set_userdata('is_logged_in', $data);
	}
	
	function statusCheck($type)
	{
		$auth = $this->session->userdata('is_logged_in');
		if($type<=$auth->status)
			return;
		else
		{
			$this->message('You don\'t have access for previous page that you have been accessed');
			redirect('/');
		}
	}
	
	function _flashMsg($error)
	{
		$this->session->set_flashdata($error);
	}

	function _print_data($data)
	{
		print '<div style="background:#262725; color:#11ff65; padding:20px; border-top:3px solid #11ff65; border-bottom:3px solid #11ff65;">';
		print '<pre>';
		print_r($data);
		print '<pre>';
		print '</div>';
		die();
	}
	
	function message($msg, $type = 'success')
	{
		$current_msg = $this->session->userdata('message');
		$cMsg = new stdClass();
		$cMsg->message = $msg;
		$cMsg->type = $type;
		array_push($current_msg, $cMsg);
		$this->session->set_userdata('message', null);
		$this->session->set_userdata('message', $current_msg);
		$this->session->set_flashdata('message', $current_msg);
		//$this->session->set_flashdata('message', $this->session->userdata('message'));
	}
	
	function total_message()
	{
		return count($this->session->userdata('message'));
	}
	
	function flash_message($msg)
	{
		$current_msg = $this->session->userdata('flash_message');
		array_push($current_msg, $msg);
		$this->session->set_userdata('flash_message', null);
		$this->session->set_userdata('flash_message', $current_msg);
		$this->session->set_flashdata('flash_message', $current_msg);
		//$this->session->set_flashdata('message', $this->session->userdata('message'));
	}
}
?>