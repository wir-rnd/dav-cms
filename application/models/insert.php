<?php 
class Insert extends CI_Model {
	
	function insertVideo($data, $desc) {
		$data = array(
				'video_filename' => $data['upload_data']['file_name'] ,
				'desc' => $desc
		);
		
		$this->db->insert('video', $data);
	}
	
	function addIntoTable($table, $data){
		$this->db->insert($table, $data);
	}
	
	function addKiosk($data){
		$this->db->insert('kiosk', $data);
	}
	
	function addMerchant($data){
		$this->db->insert('merchant', $data);
	}
	
	function addUser($data){
		$this->db->insert('users', $data);
	}
	
	function addTag($marker_id, $tag_id) {
		$SQL = "INSERT INTO `marker_tag` VALUES ('".$marker_id."', '".$tag_id."')";
		$this->db->query($SQL);
	}
	
}

?>