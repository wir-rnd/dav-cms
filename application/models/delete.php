<?php

class Delete extends CI_Model {
	
	
	function deleteRecord($table, $data)
	{
		if(!is_array($data))
			$this->db->delete($table, array('id' => $data));
		else
		{
			$this->db->where($data);
			$this->db->delete($table);
		}
	}
	
	function deleteTarget($data)
	{
		if(!is_array($data))
			$this->db->delete('target', array('target_id' => $data));
		else
		{
			$this->db->where($data);
			$this->db->delete('target');
		}
	}
	
	
	function truncate($table)
	{
		$this->db->empty_table($table);
	}
}

?>