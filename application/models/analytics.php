<?php 
class analytics extends CI_Model
{
	function product($brand_id)
	{
		$raw_prod = $this->db->get_where('product', array('brand_id' => $brand_id))->result();
		//product.id AS product_id, variant.id AS variant_id, brand.name AS brand_name, product.name AS product_name, variant.name AS variant_name, variant.target_id
		$product = array();
		$j=0;
		for($i=0;$i<count($raw_prod);$i++)
		{
			//print_r($raw_prod[$i]->id);
			$var = $this->db->get_where('variant', array('product_id' => $raw_prod[$i]->id))->result();
			if(count($var)>0)
			{
				for($k=0;$k<count($var);$k++)
				{
					$product[$j] = new stdClass;
					$product[$j] = $var[$k];
					$product[$j]->variant_name = $var[$k]->name;
					unset($var[$k]->name);
					$product[$j]->variant_id = $var[$k]->id;
					unset($var[$k]->id);
					$product[$j]->product_id = $raw_prod[$i]->id;
					$product[$j]->product_name = $raw_prod[$i]->name;
					$j++;
				}
			}
		}
		return $product;
	}
}
?>