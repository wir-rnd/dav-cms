<?php 
class Read extends CI_Model
{
	function devicestatus($id = null)
	{
		return $this->GetTableProcedure('device_activity', $id);
	}
	
	function store($id = null)
	{
		return $this->GetTableProcedure('store', $id);
	}
	
	function device_history($id = null)
	{
		return $this->GetTableProcedure('device_history', $id);
	}
	
	function village_area($id = null)
	{
		return $this->GetTableProcedure('area_desa', $id);
	}
	
	function subdistrict_area($id = null)
	{
		return $this->GetTableProcedure('area_kecamatan', $id);
	}
	
	function district_area($id = null)
	{
		return $this->GetTableProcedure('area_kabupaten', $id);
	}
	
	function province_area($id = null)
	{
		return $this->GetTableProcedure('area_provinsi', $id);
	}

	function brand_devices($id = null)
	{
		return $this->GetTableProcedure('brand_devices', $id);
	}

	function os_devices($id = null)
	{
		return $this->GetTableProcedure('os_devices', $id);
	}

	function shelf($id = null)
	{
		return $this->GetTableProcedure('shelf', $id);
	}
	
	function brand($id = null)
	{
		return $this->GetTableProcedure('brand', $id);
	}
	
	function product($id = null)
	{
		return $this->GetTableProcedure('product', $id);
	}
	
	function agent($id = null)
	{
		return $this->GetTableProcedure('agent', $id);
	}
	
	function variant($id = null)
	{
		return $this->GetTableProcedure('variant', $id);
	}
	
	function geolocation($id)
	{
		$this->db->select('longitude, latitude');
		$this->db->from('store');
		$this->db->where('store_id', $id);
		return $this->db->get()->result();
	}
	function devices($id = null)
	{
		$this->db->select('count(*)');
		$this->db->from('devices a');
		$this->db->join('store b', 'a.store_id = b.id');
		$query = $this->db->get();
		return $query->result();
		//return $this->GetTableProcedure('devices', $id);
	}
	
	function get($table_name, $id = null)
	{
		return $this->GetTableProcedure($table_name, $id);
	}

	function devices_detail($id)
	{
		$this->db->select('a.id, c.brand_name, b.os_name, a.serial_number, a.status');
		$this->db->from('devices a');
		$this->db->join('os_devices b', 'b.id = a.os_devices_id');
		$this->db->join('brand_devices c', 'c.id = a.brand_devices_id');
		/*
		$SQL = "SELECT a.id, a.serial_number, a.os_version, a.phone_number, a.status, b.os_name, c.brand_name FROM `devices` a
				INNER JOIN `os_devices` b ON b.id = a.os_platform
				INNER JOIN `brand_devices` c ON c.id = a.phone_brand
				WHERE a.id =".$id;
		$query = $this->db->query($SQL);
		if ($query->num_rows > 0) {
			return $query->result();
		}*/
	}

	function target($id = null)
	{
		if(isset($id))
		{
			if(!is_array($id))
			{
				$res = $this->GetTableProcedure('target', array('target_id' => $id));
				if(count($res)==1)
					return $res[0];
				else
					return $res;
			}
			else
			{
				$res = $this->GetTableProcedure('target', $id);
				return $res;
			}
		}
		else
		{
			return $this->GetTableProcedure('target');
		}
	}
	
	function target_content($target_id)
	{
		$this->db->select('tc.target_id, c.id as ContentID, c.name as Name');
		$this->db->from('target_content tc');
		$this->db->join('content c', 'tc.content_id = c.id');
		$this->db->where('tc.target_id', $target_id);
		return $this->db->get()->result();
	}
	
	function marker_content_json($target_id)
	{
		$SQL = "SELECT b.id as ContentID, b.name as Name FROM `marker_content` a JOIN `content` b ON a.content_id = b.id WHERE a.target_id = '" . $target_id . "'";
		$query = $this->db->query($SQL);
		if ($query->num_rows > 0) {
			return $query->result();
		}
	}
	
	function check_target_content($data)
	{
		//$SQL = "SELECT * FROM `target_content` WHERE target_id = '" . $target_id . "' AND content_id = '" . $content_id . "'";
		$this->db->select('*');
		$this->db->from('target_content');
		$this->db->where($data);
		$query = $this->db->get();
		if ($query->num_rows > 0) return false;
		else return true;
	}
	
	function content($id = null)
	{
		return $this->GetTableProcedure('content', $id);
	}
	
	function content_detail($id = null)
	{
		return $this->GetTableProcedure('content_detail', $id);
	}
	
	function getContentNameById($id)
	{
		$SQL = "SELECT name FROM `content` WHERE id = '". $id ."'";
		$query = $this->db->query($SQL);
		if ($query->num_rows > 0) {
			$res = $query->first_row();
			return $res->name;
		}
	}
	
	function checkContentByName($name)
	{
		$SQL = "SELECT * FROM `content` WHERE name = '". $name ."'";
		$query = $this->db->query($SQL);
		if ($query->num_rows > 0) {
			return false;
		}
		else
		{
			return true;
		}
	}
	
	
	function pv_position($id = null, $type = null, $order_by = "id")
	{
		$this->db->select('*');
		$this->db->from('pv_position');
		$this->db->order_by($order_by, 'ASC');
		if(isset($id))
		{
			if(!isset($type))
			{
				if(!is_array($id))
				{
					$this->db->where('id', $id);
					return $this->db->get()->first_row();
				}
				else
				{
					$this->db->where($id);
					return $this->db->get()->result();
				}
			}
			else
			{
				if(!is_array($type))
				{
					if($type=="store")
					{
						$this->db->where('store_id', $id);
					}
					else if($type=="variant")
					{
						$this->db->where('variant_id', $id);
					}
					else if($type=="shelf")
					{
						$this->db->where('shelf_id', $id);
					}
				}
				else
				{
					$this->db->where($id);
				}
				return $this->db->get()->result();
			}
		}
		else
		{
			return $this->db->get()->result();
		}
	}
	
	function alamat() {
		$SQL = "SELECT id, alfamart_type, alamat FROM `store_locations`";
		$query = $this->db->query($SQL);
		if ($query->num_rows > 0) {
			return $query->result();
		}
	}
	
	function getTotalDevicePerLocation($location_id)
	{
		$this->db->select("*");
		$this->db->from("store_locations a");
		$this->db->join("location_phone b", "a.id = b.location_id");
		$this->db->where("a.id", $location_id);
		$q = $this->db->get();
		return $q->num_rows();
	}
	
	function getTotalContentPerLocation($location_id)
	{
		//SELECT * FROM location_phone a join devices b on a.phone_id = b.id join activity c on c.serial_number = b.serial_number join marker_content d on d.target_id = c.target_id where a.id = 1
		$this->db->distinct();
		$this->db->select("d.content_id");
		$this->db->from("location_phone a");
		$this->db->join("devices b", "a.phone_id = b.id");
		$this->db->join("activity c", "c.serial_number = b.serial_number");
		$this->db->join("marker_content d", "d.target_id = c.target_id");
		$this->db->where("a.location_id", $location_id);
		$q = $this->db->get();
		return $q->num_rows();
	}
	
	function getTotalInteractionPerLocation($location_id)
	{
		$this->db->select("c.serial_number");
		$this->db->from("location_phone a");
		$this->db->join("devices b", "a.phone_id = b.id");
		$this->db->join("activity c", "c.serial_number = b.serial_number");
		$this->db->where("a.location_id", $location_id);
		$q = $this->db->get();
		return $q->num_rows();
	}
	
	
	function checkAvailableSerial($sn)
	{
		$SQL = "SELECT * FROM devices WHERE serial_number = '" . $sn . "'";
		$query = $this->db->query($SQL);
		if ($query->num_rows > 0)
			return 0;
		else
			return 1;
	}
	
	function getLastVersion($modul)
	{
		
		$this->db->select('id, version, number_ver');
		$this->db->from('mobile_version');
		$this->db->where('modul', $modul);
		$this->db->order_by('number_ver', 'desc');
		$query = $this->db->get();
		return $query->first_row();
	}
	
	function updateMobileVersion($sn)
	{
		$modul = 'DAV';
		// get last version at modul
		$res = $this->getLastVersion($modul);
		$version = $res->version;
		$mobile_version_id = $res->id;
		
		// get mobile device id by serial number
		$this->db->select('id');
		$this->db->from('devices');
		$this->db->where('serial_number', $sn);
		$query = $this->db->get();
		$device_id = $query->first_row()->id;
		
		// get date and time now
		date_default_timezone_set('Asia/Jakarta');
		$dt = new DateTime();
		$datetime = $dt->format('Y-m-d H:i:s');
		
		$this->load->model('insert');
		$data = array('mobile_version_id' => $mobile_version_id, 'device_id' => $device_id, 'datetime' => $datetime);
		$this->insert->addIntoTable('mobile_update_log', $data);
	}
			
	function checkMobileVersion($sn)
	{
		$modul = 'DAV';
		
		// get last version at modul
		$this->db->select('id, version');
		$this->db->from('mobile_version');
		$this->db->where('modul', $modul);
		$this->db->order_by('number_ver', 'desc');
		$query = $this->db->get();
		//$version = $query->first_row()->version;
		$version = $query->result();
		$mobile_version_id = $query->first_row()->id;
		
		// get mobile device id by serial number
		$this->db->select('id');
		$this->db->from('devices');
		$this->db->where('serial_number', $sn);
		$query = $this->db->get();
		$device_id = $query->first_row()->id;
		
		// get date and time now
		date_default_timezone_set('Asia/Jakarta');
		$dt = new DateTime();
		$datetime = $dt->format('Y-m-d H:i:s');
		
		// check last update of current device
		$this->db->select('*');
		$this->db->from('mobile_update_log');
		$this->db->where(array('device_id' => $device_id, 'mobile_version_id' => $mobile_version_id));
		$query = $this->db->get();
		if($query->num_rows()==0)
		{
			// if nothing insert first record
			return $version[0];
		}
		else
		{
			return 0;
			/*$this->db->select('*');
			$this->db->from('mobile_update_log');
			$this->db->where(array('device_id' => $device_id, 'mobile_version_id' => $mobile_version_id));
			$query = $this->db->get();
			if($query->num_rows()==0)
			{
				
			}*/
		}
	}
	
	function mobile_version($id = null)
	{
		return $this->GetTableProcedure('mobile_version', $id);
	}
	
	function GetTableProcedure($table, $id = null)
	{
		$this->db->select('*');
		$this->db->from($table);
		if(isset($id))
		{
			if(!is_array($id))
			{
				$this->db->where('id', $id);
				return $this->db->get()->first_row();
			}
			else
			{
				$this->db->where($id);
				return $this->db->get()->result();
			}
		}
		else
		{
			return $this->db->get()->result();
		}
	}
	
	
	function config($config_name)
	{
		$this->db->select('*');
		$this->db->from('config');
		$this->db->where('name', $config_name);
		return $this->db->get()->first_row();
	}

	function brand_activity(){
		$SQL = "SELECT a.target_id, a.serial_number, a.datetime, b.name as variant_name, c.name as product_name, d.name as brand_name 
				FROM `activity` a 
				JOIN `variant` b ON a.target_id = b.target_id 
				JOIN `product` c ON c.id = b.product_id
				INNER JOIN `brand` d ON d.id = c.brand_id
				ORDER BY a.serial_number";
		
		$SQL2 = "SELECT b.id as variant_id, b.name as variant_name, c.id as product_id, c.name as product_name, d.name as brand_name 
				FROM `activity` a 
				JOIN `variant` b ON a.target_id = b.target_id 
				JOIN `product` c ON c.id = b.product_id
				INNER JOIN `brand` d ON d.id = c.brand_id
				GROUP BY b.name, c.name, d.name ";
		
		$SQuery = $this->db->query($SQL2);
		$query = $this->db->query($SQL);
		if ($query->num_rows > 0) {
			foreach ($query->result_array() as $val) {
				$totalReco[] = $val['product_name'];
			}
			$counts = array_count_values($totalReco);
			
			if ($SQuery->num_rows > 0) {
				foreach ($SQuery->result_array() as $v) {
					$data['variant_id'] = $v['variant_id'];
					$data['product_id'] = $v['product_id'];
					$data['brand_name'] = $v['brand_name'];
					$data['product_name'] = $v['product_name'];
					$data['variant_name'] = $v['variant_name'];
					$data['total_reco'] = $counts[$v['product_name']];
					$post[] = $data;
				}
			}
			return $post;
		}
	}
	
	function brand_activity_detail($product_id, $variant_id) {
		$SQL = "SELECT a.serial_number, c.name as product_name, d.name as brand_name
				FROM `activity` a 
				JOIN `variant` b ON a.target_id = b.target_id 
				JOIN `product` c ON c.id = b.product_id
				INNER JOIN `brand` d ON d.id = c.brand_id
				INNER JOIN `devices` e ON a.serial_number = e.serial_number
				WHERE c.id = $product_id AND b.id = $variant_id";
		
		$SQL2 = "SELECT a.serial_number, a.target_id, b.id as variant_id, c.id as product_id, c.name as product_name, d.name as brand_name, f.id as store_id_int, f.store_id, f.store_name, g.name as shelf_name,
				h.nama as province
				FROM `activity` a 
				JOIN `variant` b ON a.target_id = b.target_id 
				JOIN `product` c ON c.id = b.product_id
				INNER JOIN `brand` d ON d.id = c.brand_id
				INNER JOIN `devices` e ON a.serial_number = e.serial_number
				INNER JOIN `store` f ON e.store_id = f.id
				INNER JOIN `shelf` g ON e.shelf_id = g.id
				INNER JOIN `area_provinsi` h ON f.store_province_id = h.id
				WHERE c.id = $product_id AND b.id = $variant_id 
				GROUP BY f.store_id";
		
		$SQuery = $this->db->query($SQL2);
		$query = $this->db->query($SQL);
		
		if ($query->num_rows > 0) {
			//return $query->result();
			foreach ($query->result_array() as $val) {
				$totalReco[] = $val['product_name'];
			}
			//print_r (count($totalReco));
			$counts = array_count_values($totalReco);
			if ($SQuery->num_rows > 0) {
				foreach ($SQuery->result_array() as $v) {
					$sq = "SELECT COUNT( * ) as totalReco FROM  `activity` a
								INNER JOIN `devices` e ON a.serial_number = e.serial_number
								WHERE target_id =  '".$v['target_id']."' AND e.store_id ='".$v['store_id_int']."'";
					$q = $this->db->query($sq);
					
					
					$data['serial_number'] = $v['serial_number'];
					$data['product_name'] = $v['product_name'];
					$data['brand_name'] = $v['brand_name'];
					$data['store_id'] = $v['store_id'];
					$data['store_name'] = $v['store_name'];
					$data['variant_id'] = $v['variant_id'];
					$data['product_id'] = $v['product_id'];
					$data['province'] = $v['province'];
					$data['total_reco'] = $total;
					$post[] = $data;
				}
			}
			return $post;
		}
	}
	
	function brand_activity_more_detail($product_id, $variant_id, $store_id) {
		$SQL = "SELECT a.serial_number, c.name as product_name, d.name as brand_name
				FROM `activity` a 
				JOIN `variant` b ON a.target_id = b.target_id 
				JOIN `product` c ON c.id = b.product_id
				INNER JOIN `brand` d ON d.id = c.brand_id
				INNER JOIN `devices` e ON a.serial_number = e.serial_number
				WHERE c.id = $product_id AND b.id = $variant_id";
	
	
		$SQL2 = "SELECT a.serial_number, a.datetime, c.name as product_name, d.name as brand_name, f.store_id, f.store_name, g.name as shelf_name
		FROM `activity` a
		JOIN `variant` b ON a.target_id = b.target_id
		JOIN `product` c ON c.id = b.product_id
		INNER JOIN `brand` d ON d.id = c.brand_id
		INNER JOIN `devices` e ON a.serial_number = e.serial_number
		INNER JOIN `store` f ON e.store_id = f.id
		INNER JOIN `shelf` g ON e.shelf_id = g.id
		WHERE c.id = $product_id AND b.id = $variant_id AND f.store_id = '".$store_id."'";
	
		$SQuery = $this->db->query($SQL2);
		$query = $this->db->query($SQL);
	
		if ($query->num_rows > 0) {
			//return $query->result();
			foreach ($query->result_array() as $val) {
				$totalReco[] = $val['product_name'];
			}
			$counts = array_count_values($totalReco);
				if ($SQuery->num_rows > 0) {
				foreach ($SQuery->result_array() as $v) {
					$data['serial_number'] = $v['serial_number'];
					$data['product_name'] = $v['product_name'];
					$data['brand_name'] = $v['brand_name'];
					$data['store_id'] = $v['store_id'];
					$data['store_name'] = $v['store_name'];
					$data['datetime'] = $v['datetime'];
					$data['shelf_name'] = $v['shelf_name'];
					$post[] = $data;
				}
			}
			return $post;
		}
	}
	
}
?>