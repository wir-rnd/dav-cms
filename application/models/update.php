<?php 
class Update extends CI_Model {
	
	function tableUpdate($table, $data, $id)
	{
		$this->db->where('id', $id);
		$this->db->update($table, $data);
	}
	
	function _where($table, $data, $id, $where_key) {
		$this->db->where($where_key, $id);
		$this->db->update($table, $data);
	}
	
	function tableUpdates($table, $data, $where)
	{
		$this->db->where($where);
		$this->db->update($table, $data);
	}
	
	function targetUpdate($table, $data ,$id)
	{
		$this->db->where('target_id', $id);
		$this->db->update($table, $data);
	}
	
	function geolocationUpdate($table, $data ,$id)
	{
		$this->db->where('store_id', $id);
		$this->db->update($table, $data);
	}
		
}

?>