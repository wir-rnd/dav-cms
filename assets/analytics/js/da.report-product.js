$(document).ready(function()
{
	var data = {};
	var total_data = 0;
	$.fn.Parameter = function()
	{
		data.variant_id = $('#variant_id').val();
		data.province_id = $('#province').find('option:selected').val();
		data.city_id = $('#city').find('option:selected').val();
		data.start_date = $('#start_date').val();
		data.end_date = $('#end_date').val();
	}
	$.fn.Parameter();

	$.fn.callAjaxInteraction('POST', data, 'ajax/total_interaction/', $('#today_interaction'));
	$.fn.callAjaxInteraction('POST', data, 'ajax/daily_average/', $('#average_interaction'));
	

	$('#periode').daterangepicker(
	{
		format: 'YYYY-MM-DD'
	},
	function(start, end, label) {
		$('#start_date').val(start.format('YYYY-MM-DD'));
		$('#end_date').val(end.format('YYYY-MM-DD'));
		
		$.fn.totalRecognition();
		$.fn.Parameter();
		$.fn.callAjaxChart('POST', data, 'ajax/graph_daily_interaction/', 'daily');
		//alert($('#start_date').val() + " " + $('#end_date').val() + ' ' + $('#periode').val());
	});
	
	$(".spinner").hide();
	$("#province").change(function()
	{
		var $el = $("#city");
		$el.empty();
		//var arr = {}
		//arr.province_id = $(this).find("option:selected").val();
		$.ajax({
			url : $.fn.domain() + 'ajax/city/' + $(this).find("option:selected").val(),
			type : 'POST',
			success : function(data) {
				$('#city').append($('<option>', {value: 0, text : 'ALL CITY'}));
				$.each(data, function (i, item) {
					$('#city').append($('<option>', { 
						value: item.id,
						text : item.name
					}));
				});
			}	
		});
		$.fn.totalRecognition();
		$.fn.Parameter();
		$.fn.callAjaxChart('POST', data, 'ajax/graph_daily_interaction/', 'daily');
		
	});
	
	$("#city").change(function() {
		$.fn.totalRecognition();
		$.fn.Parameter();
		$.fn.callAjaxChart('POST', data, 'ajax/graph_daily_interaction/', 'daily');
	});

	$.fn.totalRecognition = function()
	{
		var param = {};
		param.province_id = $('#province').find('option:selected').val();
		param.city_id = $('#city').find('option:selected').val();
		param.start_date = $('#start_date').val();
		param.end_date = $('#end_date').val();
		param.variant_id = $('#variant_id').val();
		var elm = $(".most-brand-table");
		if($('#periode').val()!='' && $('#start_date').val()==0)
		{
			var date = $('#periode').val().split('-');
			date = date[0]+'-'+date[1]+'-'+date[2];
			$('#start_date').val(date);
			$('#end_date').val(date);
		}
		$(elm).fadeOut(800, function()
		{
			$('.spinner').show();
			$.ajax({
				url : $.fn.domain() + 'ajax/total_interaction',
				type : 'POST',
				data : param,
				error:function(data){ 
					$(elm).css('opacity', '0.5');
					$('.spinner').show();
				},
				success : function(data) {
					$('#total_interaction').html(data.total);
					$(elm).fadeIn(800);
					$('.spinner').hide();
				}
			});
		});
		
	};
	
	$.fn.topTen = function()
	{
		var param = {};
		param.start_date = $('#start_date').val();
		param.end_date = $('#end_date').val();
		
		param.province_id = $('#province').find('option:selected').val();
		param.city_id = $('#city').find('option:selected').val();
		
		
		$('#form_ti').submit();
	};
	

	$.fn.totalRecognition();
	
	$.fn.callAjaxChart = function(type, sData, url, sType)
	{
		//alert(JSON.stringify(sData));
		$.ajax({
			type: type,
			data: sData,
			url: $.fn.domain() + url,
			success: function(data) {
				switch(sType)
				{
					case 'daily':
						//alert(data);
						$.fn.dailyChart(data);
						break;
					case 'week':
						$.fn.weeklyChart(data);
						break;
					case 'month':
						$.fn.monthlyChart(data);
						break;
				}
			},
			error: function(e)
			{
				alert(JSON.stringify(e));
			}
		});
	};
	
	$.fn.dailyChart = function(sData)
	{
		
		sData = $.fn.convertJSON(sData);
		//alert(sData);
		var dChart = new CanvasJS.Chart("todayChart",
		{
			backgroundColor: "#F0F0F0",
			title:
			{
				text: "Daily Interactions",
				padding: "20",
				fontColor: "#0A293B",
				fontSize: "20",
				fontFamily: "dav_font"
			},
			axisX :
			{
				labelAngle : 0,
				labelFontColor: "#0A293B",
				labelFontFamily: "dav_font",
			},
			axisY :
			{
				labelAngle : 0,
				labelFontColor: "#0A293B",
				labelFontFamily: "dav_font",
			},
			data:
			[
				{
					type: "line",
					color: "#00ADEF",
					markerColor: "#00ADEF",
					lineColor: "#369EAD" ,
					lineThickness: 2,
					//click: $.fn.dailyOnClick,
					dataPoints: sData
				}
			]
		});
		dChart.render();
	};
	
	$.fn.callAjaxChart('POST', data, 'ajax/graph_daily_interaction/', 'daily');
	
	$.fn.weeklyChart = function(sData)
	{
		sData = $.fn.convertJSON(sData);
		if(sData.length==0)
			sData = [{y: 0, label: "Week 1"}, {y: 0, label: "Week 2"}, {y: 0, label: "Week 3"}, {y: 0, label: "Week 4"}];
			
		
		CanvasJS.addColorSet("greenShades",
            [//colorSet Array
	            "#1f85a8",
	            "#1f85a8",
	            "#1f85a8",
	            "#1f85a8"              
            ]);
			
		var wChart = new CanvasJS.Chart("weekChart",
		{
			colorSet: "greenShades",
			backgroundColor: "#F0F0F0",
			title:
			{
				text: "Weekly Interactions",
				fontColor: "#0A293B",
				fontFamily: "dav_font"
			},
			axisX :
			{
				labelAngle : 0,
				labelFontColor: "#0A293B",
				labelFontFamily: "dav_font",
			},
			axisY :
			{
				labelAngle : 0,
				labelFontColor: "#0A293B",
				labelFontFamily: "dav_font",
			},
			data: 
			[
				{
					//click: $.fn.weeklyOnClick,
					dataPoints: sData
				}
			]
		});
		wChart.render();
	};
	
	
	
	$.fn.monthlyChart = function(sData)
	{
		sData = $.fn.convertJSON(sData);
		if(sData.length==0)
			sData = [{y: 0, label: "January"}, {y: 0, label: "February"}, {y: 0, label: "March"}, {y: 0, label: "April"}, {y: 0, label: "May"}, {y: 0, label: "June"}, {y: 0, label: "July"}, {y: 0, label: "August"}, {y: 0, label: "September"}, {y: 0, label: "October"}, {y: 0, label: "November"}, {y: 0, label: "December"}];
			
		var mChart = new CanvasJS.Chart("monthChart",
		{
			backgroundColor: "#F0F0F0",
			title:
			{
				text: "Monthly Interactions",
				fontColor: "#0A293B",
				fontFamily: "dav_font"
			},
			axisX :
			{
				labelAngle : 0,
				labelFontColor: "#0A293B",
				labelFontFamily: "dav_font",
			},
			axisY :
			{
				labelAngle : 0,
				labelFontColor: "#0A293B",
				labelFontFamily: "dav_font",
			},
			data:
			[
				{
					type: "bar",
					//showInLegend: true,
					color: "#0D5D73",
					//click: $.fn.monthlyOnClick,
					//markerColor: "#0A293B",
					dataPoints: sData
				},
				{
					type: "line",
					color: "#0D5D73",
					//click: $.fn.monthlyOnClick,
					dataPoints: sData
				}
			]
		});
		mChart.render();
	};
	
	
});