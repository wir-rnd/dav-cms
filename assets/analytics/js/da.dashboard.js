$(document).ready(function()
{
	$.fn.callAjaxChart = function(type, sData, url, sType)
	{
		$.ajax({
			type: type,
			data: sData,
			url: $.fn.domain() + url,
			success: function(data) {
				switch(sType)
				{
					case 'today':
						//alert(sType);
						$.fn.todayChart(data);
						break;
					case 'week':
						$.fn.weeklyChart(data);
						break;
					case 'month':
						$.fn.monthlyChart(data);
						break;
				}
			}
		});
	};
	
	
	$.fn.todayChart = function(sData)
	{
		sData = $.fn.convertJSON(sData);
		
		var tChart = new CanvasJS.Chart("todayChart",
		{
			backgroundColor: "#F0F0F0",
			title:
			{
				text: "Today's Interactions",
				padding: "20",
				fontColor: "#0A293B",
				fontSize: "20",
				fontFamily: "dav_font"
			},
			axisX :
			{
				labelAngle : 0,
				labelFontColor: "#0A293B",
				labelFontFamily: "dav_font",
			},
			axisY :
			{
				labelAngle : 0,
				labelFontColor: "#0A293B",
				labelFontFamily: "dav_font",
			},
			data:
			[
				{
					type: "line",
					color: "#00ADEF",
					markerColor: "#00ADEF",
					lineColor: "#369EAD" ,
					lineThickness: 2,
					click: $.fn.todayOnClick,
					dataPoints: sData
				}
			]
		});
		
		tChart.render();
	};
 
    $.fn.todayOnClick = function(e)
	{
		//alert(  e.dataSeries.type + ", dataPoint { x:" + e.dataPoint.x + ", y: "+ e.dataPoint.y + " }" );
    	$( ".dav-graph" ).fadeOut( "slow", function() {
			
    	    //AJAX call
    	    var arr = 0;
    		$.ajax({
    			url : '',
    			type : 'POST',
    			data : arr,
    			error:function(){ 
    				// 
    			},
    			success : function(data) {
					$(".detail").show();
    			}	
    		});
    	});
	};
	
	$.fn.weeklyChart = function(sData)
	{
		sData = $.fn.convertJSON(sData);
		if(sData.length==0)
			sData = [{y: 0, label: "Week 1"}, {y: 0, label: "Week 2"}, {y: 0, label: "Week 3"}, {y: 0, label: "Week 4"}];
			
		
		CanvasJS.addColorSet("greenShades",
            [//colorSet Array
	            "#1f85a8",
	            "#1f85a8",
	            "#1f85a8",
	            "#1f85a8"              
            ]);
			
		var wChart = new CanvasJS.Chart("weekChart",
		{
			colorSet: "greenShades",
			backgroundColor: "#F0F0F0",
			title:
			{
				text: "Weekly Interactions",
				fontColor: "#0A293B",
				fontFamily: "dav_font"
			},
			axisX :
			{
				labelAngle : 0,
				labelFontColor: "#0A293B",
				labelFontFamily: "dav_font",
			},
			axisY :
			{
				labelAngle : 0,
				labelFontColor: "#0A293B",
				labelFontFamily: "dav_font",
			},
			data: 
			[
				{
					click: $.fn.weeklyOnClick,
					dataPoints: sData
				}
			]
		});
		wChart.render();
	};

    $.fn.weeklyOnClick = function(e)
	{
		//alert(  e.dataSeries.type + ", dataPoint { x:" + e.dataPoint.x + ", y: "+ e.dataPoint.y + " }" );
    	$( ".dav-graph" ).fadeOut( "slow", function() {
			$("#detail").show();
    	    //AJAX call
    	    var arr = 0;
    		$.ajax({
    			url : '',
    			type : 'POST',
    			data : arr,
    			error:function(){ 
    				// 
    			},
    			success : function(data) {
					$(".detail").show();
    			}
    		});
    	});
	};
	
	$.fn.monthlyChart = function(sData)
	{
		sData = $.fn.convertJSON(sData);
		if(sData.length==0)
			sData = [{y: 0, label: "January"}, {y: 0, label: "February"}, {y: 0, label: "March"}, {y: 0, label: "April"}, {y: 0, label: "May"}, {y: 0, label: "June"}, {y: 0, label: "July"}, {y: 0, label: "August"}, {y: 0, label: "September"}, {y: 0, label: "October"}, {y: 0, label: "November"}, {y: 0, label: "December"}];
			
		var mChart = new CanvasJS.Chart("monthChart",
		{
			backgroundColor: "#F0F0F0",
			title:
			{
				text: "Monthly Interactions",
				fontColor: "#0A293B",
				fontFamily: "dav_font"
			},
			axisX :
			{
				labelAngle : 0,
				labelFontColor: "#0A293B",
				labelFontFamily: "dav_font",
			},
			axisY :
			{
				labelAngle : 0,
				labelFontColor: "#0A293B",
				labelFontFamily: "dav_font",
			},
			data:
			[
				{
					type: "bar",
					//showInLegend: true,
					color: "#0D5D73",
					click: $.fn.monthlyOnClick,
					//markerColor: "#0A293B",
					dataPoints: sData
				},
				{
					type: "line",
					color: "#0D5D73",
					click: $.fn.monthlyOnClick,
					dataPoints: sData
				}
			]
		});
		mChart.render();
	};

	$.fn.monthlyOnClick = function(e)
	{
		
	};
	
	// 
	
	$(".detail").hide();
	$(".spinner").hide();
	var title = $(".dav_selector option:selected").text();
	$(".dav_title").text(title);
	
	var product = $('.dav_selector');
	
	$(product).change(function() {
		//alert($(product).val());
		$.fn.initProduct();
		$.fn.collectData();
		$('.dav_title').text($('.dav_selector option:selected').text());
	});

	$.fn.initProduct = function() {
		$.get('./assets/analytics/images/variant/' + $(product).val() + '.jpg')
			.done(function() { 
				$('#product_img').html('<img id="dav_img" width="60px" height="60px" style="border: 1px solid #0A293B;" src="./assets/analytics/images/variant/' + $(product).val() + '.jpg" / >');
			}).fail(function() { 
				$('#product_img').html('<img id="dav_img" width="60px" height="60px" style="border: 1px solid #0A293B;" src="./assets/analytics/images/variant/default.png" / >');
			});
	};
	
	
	$.fn.collectData = function() {
		var data = {};
		data.variant_id = $(product).val();
		$.fn.callAjaxInteraction('POST', data, 'ajax/total_interaction/', $('#today_interaction'));
		$.fn.callAjaxInteraction('POST', data, 'ajax/daily_average/', $('#average_interaction'));
		$.fn.callAjaxChart('POST', data, 'ajax/graph_today_interaction/', 'today');
		$.fn.callAjaxChart('POST', data, 'ajax/graph_weekly_interaction/', 'week');
		$.fn.callAjaxChart('POST', data, 'ajax/graph_monthly_interaction/', 'month');
		//$.fn.callChart('month', '');
	};
	
	if($(product).size()>0)
	{
		$(product).val($('.dav_selector option:first').val());
		$.fn.initProduct();
		$.fn.collectData();
		$('.dav_title').text($('.dav_selector option:selected').text());
	}
});