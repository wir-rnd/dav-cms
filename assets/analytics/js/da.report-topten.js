$(document).ready(function()
{
	var data = {};
	data.variant_id = $('#variant_id').val();
	$.fn.callAjaxInteraction('POST', data, 'ajax/total_interaction/', $('#today_interaction'));
	$.fn.callAjaxInteraction('POST', data, 'ajax/daily_average/', $('#average_interaction'));
	
	$('#byStore').daterangepicker(
	{
		format: 'YYYY-MM-DD'
	},
	function(start, end, label) {
		$('#start_date').val(start.format('YYYY-MM-DD'));
		$('#end_date').val(end.format('YYYY-MM-DD'));
		$.fn.topten('byStore');
		//alert($('#start_date').val() + " " + $('#end_date').val() + ' ' + $('#periode').val());
	});
	
	$('#byRegion').daterangepicker(
	{
		format: 'YYYY-MM-DD'
	},
	function(start, end, label) {
		$('#start_date').val(start.format('YYYY-MM-DD'));
		$('#end_date').val(end.format('YYYY-MM-DD'));
		$.fn.topten('byRegion');
		//alert($('#start_date').val() + " " + $('#end_date').val() + ' ' + $('#periode').val());
	});
	
	$.fn.conv_progressbar = function(total)
	{
		var percen = 0;
		if(total<11)
			percen = (total*10)/4;
		else if(total<101)
			percen = 25+(total/4);
		else if(total<1001)
			percen = 50+((total/10)/4);
		else if(total<10001)
			percen = 50+((total/100)/4);
		
		return percen;
	};
	
	$.fn.topten = function(type)
	{
		var param = {};
		param.start_date = $('#start_date').val();
		param.end_date = $('#end_date').val();
		var elm = $('.' + type);
		var link;
		if(type=='byStore') link = 'topten_by_store';
		else if(type=='byRegion') link = 'topten_by_region';
		//alert(JSON.stringify(param));
		$('#top10' + type).html('');
		$(elm).fadeOut(800, function()
		{
			$('.spinner').show();
			$.ajax({
				url : $.fn.domain() + 'ajax/' + link,
				type : 'POST',
				data : param,
				success : function(data) {
					
					$.each(data, function (i, item) {
						var id = '';
						if(type=='byStore') id = 'ttbs-' + i;
						else if(type=='byRegion') id = 'ttbr-' + i;
						$('#top10' + type).append('<div class="' + id + '"></div>');
						$('#top10' + type + ' .' + id).append('<div class="box_number left">' + (i+1) + '</div>');
						$('#top10' + type + ' .' + id).append('<div class="progress"></div>');
						$('#top10' + type + ' .' + id + ' .progress').append('<div data-placement="top" style="width: 0%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="80" role="progressbar" class="progress-bar progress-bar-dav"></div>');
						$('#top10' + type + ' .' + id + ' .progress .progress-bar').animate(
						{
							width: $.fn.conv_progressbar(item.total) + '%'
						}, 1000);
						$('#top10' + type + ' .' + id + ' .progress .progress-bar').append('<span class="progress-type"></span>');
						var desc;
						if(type=='byStore') desc = '<span class="progress-type">' + item.store_id + ' (' + item.store_name + ')</span>';
						else if(type=='byRegion') desc = '<span class="progress-type">' + item.subdistrict_name + '</span>';
						$('#top10' + type + ' .' + id + ' .progress .progress-bar .progress-type').append(desc);
						$('#top10' + type + ' .' + id + ' .progress').append('<span class="text2" style="width: 30px; left-margin: -100px;">' + item.total + '</div>');
					});
					$(elm).fadeIn('slow');
					//alert($('#top10bystore').html());
				},
				error: function(e)
				{
					alert(e);
				}
			});
		});
	};
	$.fn.topten('byStore');
	$.fn.topten('byRegion');
});