$(document).ready(function()
{
	// init
	//$('#average_interaction').hide();
	//$('#today_interaction').hide();
	
	$.fn.domain = function()
	{
		return $('#base_url').val();
	};
	
	$.fn.callAjax = function(sType, sData, sUrl, comp)
	{
		$.ajax({
			type: sType,
			data: sData,
			url: sUrl,
			success: function(data) {
				$(comp).html(data);
			}
		});
	};
	
	$.fn.callAjaxInteraction = function(sType, sData, sUrl, comp)
	{
		$(comp).fadeOut(250, function() {
			$.ajax({
				type: sType,
				data: sData,
				url: $.fn.domain() + sUrl,
				success: function(data) {
					//alert(JSON.stringify(sUrl));
					$(comp).html(data.total);
					$(comp).fadeIn('slow');
				}
			});
		});
	};
	
	
	$.fn.convertJSON = function(sData)
	{
		var theData = [];
		if(sData.length>0)
			for(var i=0;i<sData.length;i++)
				theData.push({ y: parseInt(sData[i].y), label: sData[i].label});
		
		return theData;
	}
});